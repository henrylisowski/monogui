float4x4 World;
float4x4 View;
float4x4 Projection;

float fill_percentage;
bool mask_present;
float4 destination_rectangle;
float4 source_rectangle;
float4 gradient_source_rectangle;
float4 mask_source_rectangle;

sampler TextureSampler : register(s0);
texture fill_gradient;
sampler fill_gradient_sampler = sampler_state{Texture = fill_gradient; magfilter = POINT; minfilter = POINT; mipfilter=POINT; AddressU = clamp; AddressV = clamp;};
texture fill_mask;
sampler fill_mask_sampler = sampler_state{Texture = fill_mask;magfilter = POINT; minfilter = POINT; mipfilter=POINT; AddressU = clamp; AddressV = clamp;};
float4 PixelShaderFunction(float4 pos : SV_POSITION, float4 color1 : COLOR0, float2 texCoord : TEXCOORD0) : SV_TARGET0
{
	float4 texColor = tex2D(TextureSampler, texCoord);

	float2 updated_coords_gradient_source = float2(
		(texCoord.x - source_rectangle.x) / (source_rectangle.z),
		(texCoord.y - source_rectangle.y) / (source_rectangle.w));

	float2 updated_coords_gradient_destination = float2(
		(updated_coords_gradient_source.x * (destination_rectangle.z) + destination_rectangle.x),
		(updated_coords_gradient_source.y  * (destination_rectangle.w)+ destination_rectangle.y));

	float2 updated_coords_gradient_full_source = float2(
		(updated_coords_gradient_destination.x * gradient_source_rectangle.z) + (gradient_source_rectangle.x),
		(updated_coords_gradient_destination.y * gradient_source_rectangle.w) + (gradient_source_rectangle.y));

	float2 updated_coords_mask_source = float2(
		(updated_coords_gradient_destination.x * mask_source_rectangle.z) + (mask_source_rectangle.x),
		(updated_coords_gradient_destination.y * mask_source_rectangle.w) + (mask_source_rectangle.y));


	float gradient_amount = tex2D(fill_gradient_sampler, updated_coords_gradient_full_source).r;
	if(fill_percentage > gradient_amount || fill_percentage == 1){
		if(mask_present){
			float mask = tex2D(fill_mask_sampler, updated_coords_mask_source).a;
			if(mask > 0){
				return texColor;
			}
			else{
				return float4(0, 0, 0, 0);
			}
		}
		else{
			return texColor;
		}
	}
	return float4(0, 0, 0, 0);
}

technique Technique1
{
	pass Pass1
	{
		PixelShader = compile ps_4_0_level_9_1 PixelShaderFunction();
	}
}
