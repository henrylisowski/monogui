﻿using MonoGUI.Manager;
using MonoGUI.ScreenItems.Containers;
using MonoGUI.ScreenItems.Containers.StackContainer;
using MonoGUI.Skinning;
using MonoGUI.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI_Test_App.TestScreens
{
    public class LayoutWindow : GUIWindow
    {
        public LayoutWindow(ScreenManager Manager)
            : base(Manager, "Several Automatic Layout Widgets", false)
        {
            this.Width = MonoGUI.ScreenItems.ItemSizeOptions.FillParent;
            this.Height = MonoGUI.ScreenItems.ItemSizeOptions.FillParent;


            StackPanel leftStack = new StackPanel(Manager);
            leftStack.SkinVariant = SkinReferencingConstants.SkinVariant1;
            leftStack.SkinTargetWidget = SkinReferencingConstants.WidgetTargetCanvas;
            leftStack.Width = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            leftStack.Height = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            leftStack.Direction = StackDirection.Vertical;
            leftStack.PreferredDimensions = new Microsoft.Xna.Framework.Vector2(0.3f, 1f);
            AddChild(leftStack);
            Label leftStackLabel = new Label(Manager, "Stack Panel");
            leftStackLabel.Width = MonoGUI.ScreenItems.ItemSizeOptions.FillParent;
            leftStackLabel.Height = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            leftStackLabel.PreferredDimensions = new Microsoft.Xna.Framework.Vector2(1f, 0.1f);
            leftStack.AddChild(leftStackLabel);
            for (int i = 0; i < 6; i++)
            {
                DisplayItem item = new DisplayItem(Manager);
                item.Width = MonoGUI.ScreenItems.ItemSizeOptions.FillParent;
                item.Height = MonoGUI.ScreenItems.ItemSizeOptions.FillParent;
                item.PreferredDimensions = new Microsoft.Xna.Framework.Vector2(1f, 1 / 6f);
                leftStack.AddChild(item);
            }

            Label gridLabel = new Label(Manager, "Grid Panel");
            gridLabel.Width = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            gridLabel.Height = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            gridLabel.VerticalPositionType = MonoGUI.ScreenItems.ItemPositionOptions.Percentage;
            gridLabel.HorizontalPositionType = MonoGUI.ScreenItems.ItemPositionOptions.Percentage;
            gridLabel.PreferredDimensions = new Microsoft.Xna.Framework.Vector2(0.45f, 0.1f);
            gridLabel.PreferredPosition = new Microsoft.Xna.Framework.Vector2(0.5f, 0f);
            gridLabel.TextHorizontalJustification = MonoGUI.ScreenItems.AlignH.Center;
            AddChild(gridLabel);

            GridPanel grid = new GridPanel(Manager, 6, 0.1f);
            grid.SkinTargetWidget = SkinReferencingConstants.WidgetTargetCanvas;
            grid.SkinVariant = SkinReferencingConstants.SkinVariant1;
            grid.Width = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            grid.Height = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            grid.VerticalPositionType = MonoGUI.ScreenItems.ItemPositionOptions.Percentage;
            grid.PreferredDimensions = new Microsoft.Xna.Framework.Vector2(0.5f, 0.9f);
            grid.PreferredPosition = new Microsoft.Xna.Framework.Vector2(0f, 0.1f);
            grid.HorizontalAlignment = MonoGUI.ScreenItems.AlignH.Right;
            AddChild(grid);
            for (int i = 0; i < 20; i++)
            {
                Icon ic = new Icon(Manager, "brobot_blueJump");
                grid.AddChild(ic);
            }
        }
    }
}
