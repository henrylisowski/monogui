﻿using MonoGUI.Manager;
using MonoGUI.Performance;
using MonoGUI.ScreenItems.Containers.CanvasContainer;
using MonoGUI.Skinning;
using MonoGUI.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI_Test_App.TestScreens
{
    public class TopBar : CanvasFrame
    {
        public TopBar(ScreenManager Manager)
            : base(Manager)
        {
            this.SkinVariant = SkinReferencingConstants.SkinVariant2;
            Button leftButton = new Button(Manager, "Back");
            leftButton.Width = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            leftButton.Height = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            leftButton.PreferredDimensions = new Microsoft.Xna.Framework.Vector2(0.1f, 1f);
            leftButton.SkinVariant = SkinReferencingConstants.SkinVariant1;
            leftButton.ButtonClicked += leftButton_ButtonClicked;
            AddChild(leftButton);

            Button rightButton = new Button(Manager, "Forward");
            rightButton.Width = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            rightButton.Height = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            rightButton.HorizontalAlignment = MonoGUI.ScreenItems.AlignH.Right;
            rightButton.PreferredDimensions = new Microsoft.Xna.Framework.Vector2(0.1f, 1f);
            rightButton.SkinVariant = SkinReferencingConstants.SkinVariant1;
            rightButton.ButtonClicked += rightButton_ButtonClicked;
            AddChild(rightButton);

            Checkbox performanceCheck = new Checkbox(Manager, "Performance");
            performanceCheck.Width = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            performanceCheck.Height = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            performanceCheck.HorizontalPositionType = MonoGUI.ScreenItems.ItemPositionOptions.Percentage;
            performanceCheck.VerticalPositionType = MonoGUI.ScreenItems.ItemPositionOptions.Percentage;
            performanceCheck.PreferredDimensions = new Microsoft.Xna.Framework.Vector2(0.2f, 1f);
            performanceCheck.PreferredPosition = new Microsoft.Xna.Framework.Vector2(0.15f, 0f);
            performanceCheck.ButtonClicked += performanceCheck_ButtonClicked;
            AddChild(performanceCheck);

            RadioButton.RadioButtonController skinController = new RadioButton.RadioButtonController();
            RadioButton spaceSkin = new RadioButton(Manager, skinController, "Space Skin");
            spaceSkin.Width = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            spaceSkin.Height = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            spaceSkin.HorizontalPositionType = MonoGUI.ScreenItems.ItemPositionOptions.Percentage;
            spaceSkin.VerticalPositionType = MonoGUI.ScreenItems.ItemPositionOptions.Percentage;
            spaceSkin.PreferredDimensions = new Microsoft.Xna.Framework.Vector2(0.25f, 1f);
            spaceSkin.PreferredPosition = new Microsoft.Xna.Framework.Vector2(0.4f, 0f);
            spaceSkin.ButtonClicked += spaceSkin_ButtonClicked;
            AddChild(spaceSkin);

            RadioButton rpgSkin = new RadioButton(Manager, skinController, "RPG Skin");
            rpgSkin.Width = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            rpgSkin.Height = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            rpgSkin.HorizontalPositionType = MonoGUI.ScreenItems.ItemPositionOptions.Percentage;
            rpgSkin.VerticalPositionType = MonoGUI.ScreenItems.ItemPositionOptions.Percentage;
            rpgSkin.PreferredDimensions = new Microsoft.Xna.Framework.Vector2(0.25f, 1f);
            rpgSkin.PreferredPosition = new Microsoft.Xna.Framework.Vector2(0.65f, 0f);
            rpgSkin.ButtonClicked += rpgSkin_ButtonClicked;
            AddChild(rpgSkin);
            spaceSkin.ClickButton();
        }

        void rpgSkin_ButtonClicked(object sender, EventArgs e)
        {
            if (OnChangeToRPGSkin != null)
            {
                OnChangeToRPGSkin();
            }
        }

        void spaceSkin_ButtonClicked(object sender, EventArgs e)
        {
            if (OnChangeToSpaceSkin != null)
            {
                OnChangeToSpaceSkin();
            }
        }

        public event Action OnChangeToSpaceSkin;
        public event Action OnChangeToRPGSkin;

        void performanceCheck_ButtonClicked(object sender, EventArgs e)
        {
            PF.TogglePerformanceMode();
        }

        void rightButton_ButtonClicked(object sender, EventArgs e)
        {
            if (OnGoRight != null)
            {
                OnGoRight();
            }
        }

        void leftButton_ButtonClicked(object sender, EventArgs e)
        {
            if (OnGoLeft != null)
            {
                OnGoLeft();
            }
        }

        public event Action OnGoLeft;
        public event Action OnGoRight;
    }
}
