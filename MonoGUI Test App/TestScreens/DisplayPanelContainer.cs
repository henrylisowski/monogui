﻿using Microsoft.Xna.Framework;
using MonoGUI.Animations;
using MonoGUI.Animations.Tweens;
using MonoGUI.Manager;
using MonoGUI.ScreenItems;
using MonoGUI.ScreenItems.Containers.CanvasContainer;
using MonoGUI.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI_Test_App.TestScreens
{
    public class DisplayPanelContainer : CanvasFrame
    {
        private List<ScreenItem> _panels;
        private int curIndex;
        private TweenModifier _onLeftAnimation;
        private TweenModifier _onRightAnimation;
        private TweenModifier _offLeftAnimation;
        private TweenModifier _offRightAnimation;
        private TweenModifier _initialPositionAnimation;
        private string _animationKey = "_animation_key";
        public DisplayPanelContainer(ScreenManager Manager)
            : base(Manager)
        {
            _panels = new List<ScreenItem>();
            _panels.Add(new LayoutWindow(Manager));
            _panels.Add(new AnimationWindow(Manager));
            _panels.Add(new TextWindow(Manager));
            _panels.Add(new ThreeDimensionalWindow(Manager));

            foreach (ScreenItem item in _panels)
            {
                AddChild(item);
            }

            _onLeftAnimation = new TweenModifier(
                new VectorTween(new Vector2(-1, 0), new Vector2(0, 0), 2f, EasingFunction.Equations.BackEaseIn,
                    EasingFunction.Equations.BackEaseIn, true),
                    null, null, MonoGUI.Utilities.VectorAnchor.Center, PositionType.Percentage, 0f);
            _onRightAnimation = new TweenModifier(
                new VectorTween(new Vector2(1, 0), new Vector2(0, 0), 2f, EasingFunction.Equations.BackEaseIn,
                    EasingFunction.Equations.BackEaseIn, true),
                    null, null, MonoGUI.Utilities.VectorAnchor.Center, PositionType.Percentage, 0f);
            _offLeftAnimation = new TweenModifier(
                new VectorTween(new Vector2(0, 0), new Vector2(-1, 0), 2f, EasingFunction.Equations.BackEaseIn,
                    EasingFunction.Equations.BackEaseIn, true),
                    null, null, MonoGUI.Utilities.VectorAnchor.Center, PositionType.Percentage, 0f);
            _offRightAnimation = new TweenModifier(
                new VectorTween(new Vector2(0, 0), new Vector2(1, 0), 2f, EasingFunction.Equations.BackEaseIn,
                    EasingFunction.Equations.BackEaseIn, true),
                    null, null, MonoGUI.Utilities.VectorAnchor.Center, PositionType.Percentage, 0f);
            _initialPositionAnimation = new TweenModifier(
                new VectorTween(new Vector2(0, 0), new Vector2(1, 0), 0f, EasingFunction.Equations.BackEaseIn,
                    EasingFunction.Equations.BackEaseIn, true),
                    null, null, MonoGUI.Utilities.VectorAnchor.Center, PositionType.Percentage, 0f);

            foreach (ScreenItem item in _panels)
            {
                item.AnimationManager.AddModifier(_animationKey, _initialPositionAnimation.Copy());
            }

            _panels[0].AnimationManager.RemoveModifiers(_animationKey);
            _panels[0].AnimationManager.AddModifier(_animationKey, _onRightAnimation.Copy());
        }

        public void MoveLeft()
        {
            if (CanMoveLeft())
            {
                _panels[curIndex].AnimationManager.RemoveModifiers(_animationKey);
                _panels[curIndex].AnimationManager.AddModifier(_animationKey, _offRightAnimation.Copy());
                curIndex--;
                _panels[curIndex].AnimationManager.RemoveModifiers(_animationKey);
                _panels[curIndex].AnimationManager.AddModifier(_animationKey, _onLeftAnimation.Copy());
            }
        }

        public void MoveRight()
        {
            if (CanMoveRight())
            {
                _panels[curIndex].AnimationManager.RemoveModifiers(_animationKey);
                _panels[curIndex].AnimationManager.AddModifier(_animationKey, _offLeftAnimation.Copy());
                curIndex++;
                _panels[curIndex].AnimationManager.RemoveModifiers(_animationKey);
                _panels[curIndex].AnimationManager.AddModifier(_animationKey, _onRightAnimation.Copy());
            }
        }

        public bool CanMoveLeft()
        {
            return curIndex > 0;
        }

        public bool CanMoveRight()
        {
            return curIndex < _panels.Count - 1;
        }
    }
}
