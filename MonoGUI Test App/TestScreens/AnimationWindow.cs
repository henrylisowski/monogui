﻿using Microsoft.Xna.Framework;
using MonoGUI.Animations;
using MonoGUI.Animations.Tweens;
using MonoGUI.Manager;
using MonoGUI.ScreenItems.Containers.StackContainer;
using MonoGUI.Skinning;
using MonoGUI.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI_Test_App.TestScreens
{
    public class AnimationWindow : GUIWindow
    {
        private StackPanel leftStack;
        private bool leftTransitionedOn;
        public AnimationWindow(ScreenManager Manager)
            : base(Manager, "Widget State and Event Animations", false)
        {
            Width = MonoGUI.ScreenItems.ItemSizeOptions.FillParent;
            Height = MonoGUI.ScreenItems.ItemSizeOptions.FillParent;
            Button b = new Button(Manager, "Click Me!");
            b.Width = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            b.Height = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            b.HorizontalPositionType = MonoGUI.ScreenItems.ItemPositionOptions.Percentage;
            b.VerticalPositionType = MonoGUI.ScreenItems.ItemPositionOptions.Percentage;
            b.PreferredDimensions = new Microsoft.Xna.Framework.Vector2(0.2f, 0.2f);
            b.PreferredPosition = new Microsoft.Xna.Framework.Vector2(0.1f, 0.3f);
            AddChild(b);

            TweenModifier jiggleLeft = new TweenModifier(
                new VectorTween(new Vector2(0, 0), new Vector2(0f, -0.4f), 1f, EasingFunction.Equations.Linear,
                    EasingFunction.Equations.BounceEaseIn, false, 0f, true, true),
                    null, null, MonoGUI.Utilities.VectorAnchor.Center, PositionType.Percentage, 0f);
            
            List<IModifier> hoverJiggle = new List<IModifier>(){jiggleLeft};

            TweenModifier colorFadeDown = new TweenModifier(
                null, null,
                new ColorTween(Color.White, new Color(255, 128, 128), 0.2f,
                    EasingFunction.Equations.Linear, false, 0f, true, true), 
                    MonoGUI.Utilities.VectorAnchor.Center
                );
            List<IModifier> colorFade = new List<IModifier>() { colorFadeDown };

            b.StateTransitionAnimations = new SkinnedPropertyStateValue<List<IModifier>>(
                new int[] { 1, 2 }, new List<IModifier>[] { hoverJiggle, colorFade }
                );

            Button transitionButton = new Button(Manager, "Magic!");
            transitionButton.Width = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            transitionButton.Height = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            transitionButton.HorizontalPositionType = MonoGUI.ScreenItems.ItemPositionOptions.Percentage;
            transitionButton.VerticalPositionType = MonoGUI.ScreenItems.ItemPositionOptions.Percentage;
            transitionButton.PreferredDimensions = new Vector2(0.2f, 0.2f);
            transitionButton.PreferredPosition = new Vector2(0.65f, 0f);
            transitionButton.ButtonClicked += transitionButton_ButtonClicked;
            AddChild(transitionButton);

            leftStack = new StackPanel(Manager);
            leftStack.SkinVariant = SkinReferencingConstants.SkinVariant1;
            leftStack.SkinTargetWidget = SkinReferencingConstants.WidgetTargetCanvas;
            leftStack.Width = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            leftStack.Height = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            leftStack.HorizontalPositionType = MonoGUI.ScreenItems.ItemPositionOptions.Percentage;
            leftStack.VerticalPositionType = MonoGUI.ScreenItems.ItemPositionOptions.Percentage;
            leftStack.PreferredPosition = new Vector2(0.6f, 0.2f);
            leftStack.Direction = StackDirection.Vertical;
            leftStack.PreferredDimensions = new Microsoft.Xna.Framework.Vector2(0.3f, 0.6f);
            AddChild(leftStack);
            for (int i = 0; i < 6; i++)
            {
                DisplayItem item = new DisplayItem(Manager);
                item.Width = MonoGUI.ScreenItems.ItemSizeOptions.FillParent;
                item.Height = MonoGUI.ScreenItems.ItemSizeOptions.FillParent;
                item.PreferredDimensions = new Microsoft.Xna.Framework.Vector2(1f, 1 / 6f);
                leftStack.AddChild(item);
            }

            IModifier leftStackOn = new TweenModifier(
                new VectorTween(new Vector2(0, 2f), new Vector2(0, 0f), 1f, EasingFunction.Equations.Linear,
                    EasingFunction.Equations.QuadEaseOut, true), null, null, MonoGUI.Utilities.VectorAnchor.Center,
                    PositionType.Percentage, 0f
                );
            leftStack.TransitionOnAnimations = new List<IModifier>();
            leftStack.TransitionOnAnimations.Add(leftStackOn);

            IModifier leftStackoff = new TweenModifier(
                new VectorTween(new Vector2(0, 0f), new Vector2(0, 2f), 1f, EasingFunction.Equations.Linear,
                    EasingFunction.Equations.QuadEaseIn, true, 1.2f, false, false), null, null, MonoGUI.Utilities.VectorAnchor.Center,
                    PositionType.Percentage, 0f
                );
            leftStack.TransitionOffAnimations = new List<IModifier>();
            leftStack.TransitionOffAnimations.Add(leftStackoff);

            

            IModifier contentsOn = new TweenModifier(
                new VectorTween(new Vector2(2f, 0f), new Vector2(0, 0), 0.4f, EasingFunction.Equations.Linear,
                    EasingFunction.Equations.SineEaseInOut, false), null, null, MonoGUI.Utilities.VectorAnchor.Center,
                    PositionType.Percentage, 0f
                );
            leftStack.ChildTransitionOnAnimations = new List<IModifier>();
            leftStack.ChildTransitionOnAnimations.Add(contentsOn);
            leftStack.ChildTransitionOnInitialDelay = 0.8f;
            leftStack.ChildTransitionOnStepDelay = 0.1f;

            IModifier contentsOff = new TweenModifier(
                new VectorTween(new Vector2(0f, 0f), new Vector2(2f, 0), 0.4f, EasingFunction.Equations.Linear,
                    EasingFunction.Equations.SineEaseInOut, true), null, null, MonoGUI.Utilities.VectorAnchor.Center,
                    PositionType.Percentage, 0f
                );
            leftStack.ChildTransitionOffAnimations = new List<IModifier>();
            leftStack.ChildTransitionOffAnimations.Add(contentsOff);
            leftStack.ChildTransitionOffInitialDelay = 0f;
            leftStack.ChildTransitionOffStepDelay = 0.1f;


            leftStack.RequestTransitionOff();
        }

        void transitionButton_ButtonClicked(object sender, EventArgs e)
        {
            if (leftTransitionedOn)
            {
                leftStack.RequestTransitionOff();
                leftTransitionedOn = false;
            }
            else
            {
                leftStack.RequestTransitionOn();
                leftTransitionedOn = true;
            }
        }
    }
}
