﻿using MonoGUI.Manager;
using MonoGUI.ScreenItems.Containers.CanvasContainer;
using MonoGUI.ScreenItems.Containers.ZoomContainer;
using MonoGUI.Skinning;
using MonoGUI.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI_Test_App.TestScreens
{
    public class DisplayItem : CanvasFrame
    {
        
        public DisplayItem(ScreenManager Manager)
            : base(Manager)
        {
            this.SkinVariant = SkinReferencingConstants.SkinVariant1;
            Icon ic = new Icon(Manager, "brobot_blueJump");
            ic.Width = MonoGUI.ScreenItems.ItemSizeOptions.WrapContent;
            ic.Height = MonoGUI.ScreenItems.ItemSizeOptions.WrapContent;

            ZoomPanel z = new ZoomPanel(Manager);
            z.Width = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            z.Height = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            z.PreferredDimensions = new Microsoft.Xna.Framework.Vector2(0.2f, 1f);
            z.SetChild(ic);

            Label l = new Label(Manager, "I'm an item!");
            l.Width = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            l.Height = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            l.PreferredDimensions = new Microsoft.Xna.Framework.Vector2(0.8f, 1f);
            l.HorizontalAlignment = MonoGUI.ScreenItems.AlignH.Right;
            l.TextVerticalJustification = MonoGUI.ScreenItems.AlignV.Center;
            AddChild(z);
            AddChild(l);
        }

        
    }
}
