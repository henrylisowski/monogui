﻿using Microsoft.Xna.Framework;
using MonoGUI.Manager;
using MonoGUI.ScreenItems.Containers.StackContainer;
using MonoGUI.Skinning;
using MonoGUI.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI_Test_App.TestScreens
{
    public class TextWindow : GUIWindow
    {
        private Slider horizSlider, vertSlider;
        private RadioButton hLeft, hMid, hRight, vTop, vMid, vBottom, 
            labelFill, labelClampHeight, labelClampWidth;
        private TextBlock _block;
        private Label _label;

        private float minHoriz = 0.1f;
        private float maxHoriz = 0.39f;
        private float minVert = 0.05f;
        private float maxVert = 0.4f;



        public TextWindow(ScreenManager Manager)
            : base(Manager, "Dynamic TextBox and Label that adjusts to Screen Sizes", false)
        {
            Width = MonoGUI.ScreenItems.ItemSizeOptions.FillParent;
            Height = MonoGUI.ScreenItems.ItemSizeOptions.FillParent;

            horizSlider = new Slider(Manager);
            horizSlider.Width = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            horizSlider.Height = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            horizSlider.PreferredDimensions = new Vector2(0.8f, 0.1f);
            horizSlider.HorizontalPositionType = MonoGUI.ScreenItems.ItemPositionOptions.Percentage;
            horizSlider.VerticalPositionType = MonoGUI.ScreenItems.ItemPositionOptions.Percentage;
            horizSlider.PreferredPosition = new Vector2(0.1f, 0.9f);
            horizSlider.ProgressChanged += horizSlider_ProgressChanged;

            vertSlider = new Slider(Manager);
            vertSlider.FillDirection = ProgressBar.StandardFillDirection.FillDown;
            vertSlider.Width = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            vertSlider.Height = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            vertSlider.PreferredDimensions = new Vector2(0.05f, 0.8f);
            vertSlider.HorizontalPositionType = MonoGUI.ScreenItems.ItemPositionOptions.Percentage;
            vertSlider.VerticalPositionType = MonoGUI.ScreenItems.ItemPositionOptions.Percentage;
            vertSlider.PreferredPosition = new Vector2(0.95f, 0.1f);
            vertSlider.ProgressChanged += vertSlider_ProgressChanged;

            _block = new TextBlock(Manager, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.");
            _block.HorizontalAlignment = MonoGUI.ScreenItems.AlignH.Center;
            _block.VerticalAlignment = MonoGUI.ScreenItems.AlignV.Center;
            _block.Width = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            _block.Height = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            _block.HorizontalPositionType = MonoGUI.ScreenItems.ItemPositionOptions.Percentage;
            _block.VerticalPositionType = MonoGUI.ScreenItems.ItemPositionOptions.Percentage;
            _block.PreferredPosition = new Vector2(-0.30f, -0.1f);
            _block.PreferredDimensions = new Vector2(minHoriz, minVert);
            _block.DoSelfClipping = true;
            AddChild(_block);

            _label = new Label(Manager, "I'm a really super long label of great length!");
            _label.SkinVariant = SkinReferencingConstants.SkinVariant1;
            _label.HorizontalAlignment = MonoGUI.ScreenItems.AlignH.Center;
            _label.VerticalAlignment = MonoGUI.ScreenItems.AlignV.Center;
            _label.Width = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            _label.Height = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            _label.HorizontalPositionType = MonoGUI.ScreenItems.ItemPositionOptions.Percentage;
            _label.VerticalPositionType = MonoGUI.ScreenItems.ItemPositionOptions.Percentage;
            _label.PreferredPosition = new Vector2(0.1f, -0.1f);
            _label.PreferredDimensions = new Vector2(minHoriz, minVert);
            _label.DoSelfClipping = true;
            AddChild(_label);

            AddChild(horizSlider);
            AddChild(vertSlider);

            StackPanel horizAlignmentStack = new StackPanel(Manager);
            horizAlignmentStack.SkinTargetWidget = SkinReferencingConstants.WidgetTargetCanvas;
            horizAlignmentStack.SkinVariant = SkinReferencingConstants.SkinVariant1;
            horizAlignmentStack.Direction = StackDirection.Horizontal;
            horizAlignmentStack.Width = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            horizAlignmentStack.Height = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            horizAlignmentStack.HorizontalPositionType = MonoGUI.ScreenItems.ItemPositionOptions.Percentage;
            horizAlignmentStack.VerticalPositionType = MonoGUI.ScreenItems.ItemPositionOptions.Percentage;
            horizAlignmentStack.PreferredDimensions = new Vector2(0.6f, 0.2f);
            horizAlignmentStack.PreferredPosition = new Vector2(0.1f, 0.7f);
            AddChild(horizAlignmentStack);

            RadioButton.RadioButtonController hController = new RadioButton.RadioButtonController();
            hLeft = new RadioButton(Manager, hController, "Left");
            hLeft.Width = MonoGUI.ScreenItems.ItemSizeOptions.FillParent;
            hLeft.Height = MonoGUI.ScreenItems.ItemSizeOptions.FillParent;
            hLeft.ButtonClicked += hLeft_ButtonClicked;
            horizAlignmentStack.AddChild(hLeft);

            hMid = new RadioButton(Manager, hController, "Center");
            hMid.Width = MonoGUI.ScreenItems.ItemSizeOptions.FillParent;
            hMid.Height = MonoGUI.ScreenItems.ItemSizeOptions.FillParent;
            hMid.ButtonClicked += hMid_ButtonClicked;
            horizAlignmentStack.AddChild(hMid);

            hRight = new RadioButton(Manager, hController, "Right");
            hRight.Width = MonoGUI.ScreenItems.ItemSizeOptions.FillParent;
            hRight.Height = MonoGUI.ScreenItems.ItemSizeOptions.FillParent;
            hRight.ButtonClicked += hRight_ButtonClicked;
            horizAlignmentStack.AddChild(hRight);
            hLeft.ClickButton();

            StackPanel vertAlighnmentStack = new StackPanel(Manager);
            vertAlighnmentStack.SkinTargetWidget = SkinReferencingConstants.WidgetTargetCanvas;
            vertAlighnmentStack.SkinVariant = SkinReferencingConstants.SkinVariant1;
            vertAlighnmentStack.Direction = StackDirection.Vertical;
            vertAlighnmentStack.Width = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            vertAlighnmentStack.Height = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            vertAlighnmentStack.HorizontalPositionType = MonoGUI.ScreenItems.ItemPositionOptions.Percentage;
            vertAlighnmentStack.VerticalPositionType = MonoGUI.ScreenItems.ItemPositionOptions.Percentage;
            vertAlighnmentStack.PreferredDimensions = new Vector2(0.15f, 0.6f);
            vertAlighnmentStack.PreferredPosition = new Vector2(0.8f, 0.1f);
            AddChild(vertAlighnmentStack);

            RadioButton.RadioButtonController vController = new RadioButton.RadioButtonController();
            vTop = new RadioButton(Manager, vController, "Top");
            vTop.Width = MonoGUI.ScreenItems.ItemSizeOptions.FillParent;
            vTop.Height = MonoGUI.ScreenItems.ItemSizeOptions.FillParent;
            vTop.ButtonClicked += vTop_ButtonClicked;
            vertAlighnmentStack.AddChild(vTop);

            vMid = new RadioButton(Manager, vController, "Center");
            vMid.Width = MonoGUI.ScreenItems.ItemSizeOptions.FillParent;
            vMid.Height = MonoGUI.ScreenItems.ItemSizeOptions.FillParent;
            vMid.ButtonClicked += vMid_ButtonClicked;
            vertAlighnmentStack.AddChild(vMid);

            vBottom = new RadioButton(Manager, vController, "Bottom");
            vBottom.Width = MonoGUI.ScreenItems.ItemSizeOptions.FillParent;
            vBottom.Height = MonoGUI.ScreenItems.ItemSizeOptions.FillParent;
            vBottom.ButtonClicked += vBottom_ButtonClicked;
            vertAlighnmentStack.AddChild(vBottom);
            vTop.ClickButton();

            StackPanel labelClampStack = new StackPanel(Manager);
            labelClampStack.SkinTargetWidget = SkinReferencingConstants.WidgetTargetCanvas;
            labelClampStack.SkinVariant = SkinReferencingConstants.SkinVariant1;
            labelClampStack.Direction = StackDirection.Horizontal;
            labelClampStack.Width = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            labelClampStack.Height = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            labelClampStack.HorizontalPositionType = MonoGUI.ScreenItems.ItemPositionOptions.Percentage;
            labelClampStack.VerticalPositionType = MonoGUI.ScreenItems.ItemPositionOptions.Percentage;
            labelClampStack.PreferredDimensions = new Vector2(0.7f, 0.15f);
            labelClampStack.PreferredPosition = new Vector2(0.05f, 0f);
            AddChild(labelClampStack);

            RadioButton.RadioButtonController labelFillController = new RadioButton.RadioButtonController();
            labelFill = new RadioButton(Manager, labelFillController, "Fill");
            labelFill.Width = MonoGUI.ScreenItems.ItemSizeOptions.FillParent;
            labelFill.Height = MonoGUI.ScreenItems.ItemSizeOptions.FillParent;
            labelFill.ButtonClicked += labelFill_ButtonClicked;
            labelClampStack.AddChild(labelFill);

            labelClampHeight = new RadioButton(Manager, labelFillController, "Clamp Height");
            labelClampHeight.Width = MonoGUI.ScreenItems.ItemSizeOptions.FillParent;
            labelClampHeight.Height = MonoGUI.ScreenItems.ItemSizeOptions.FillParent;
            labelClampHeight.ButtonClicked += labelClampHeight_ButtonClicked;
            labelClampStack.AddChild(labelClampHeight);

            labelClampWidth = new RadioButton(Manager, labelFillController, "Clamp Width");
            labelClampWidth.Width = MonoGUI.ScreenItems.ItemSizeOptions.FillParent;
            labelClampWidth.Height = MonoGUI.ScreenItems.ItemSizeOptions.FillParent;
            labelClampWidth.ButtonClicked += labelClampWidth_ButtonClicked;
            labelClampStack.AddChild(labelClampWidth);
            labelFill.ClickButton();
        }

        void labelClampWidth_ButtonClicked(object sender, EventArgs e)
        {
            _label.FontMethod = MonoGUI.ScreenItems.FontFillMethod.ClampWidth;
        }

        void labelClampHeight_ButtonClicked(object sender, EventArgs e)
        {
            _label.FontMethod = MonoGUI.ScreenItems.FontFillMethod.ClampHeight;
        }

        void labelFill_ButtonClicked(object sender, EventArgs e)
        {
            _label.FontMethod = MonoGUI.ScreenItems.FontFillMethod.Fill;
        }

        void vBottom_ButtonClicked(object sender, EventArgs e)
        {
            _block.TextVerticalJustification = MonoGUI.ScreenItems.AlignV.Bottom;
            _label.TextVerticalJustification = MonoGUI.ScreenItems.AlignV.Bottom;
        }

        void vMid_ButtonClicked(object sender, EventArgs e)
        {
            _block.TextVerticalJustification = MonoGUI.ScreenItems.AlignV.Center;
            _label.TextVerticalJustification = MonoGUI.ScreenItems.AlignV.Center;
        }

        void vTop_ButtonClicked(object sender, EventArgs e)
        {
            _block.TextVerticalJustification = MonoGUI.ScreenItems.AlignV.Top;
            _label.TextVerticalJustification = MonoGUI.ScreenItems.AlignV.Top;
        }

        void hRight_ButtonClicked(object sender, EventArgs e)
        {
            _block.TextHorizontalJustification = MonoGUI.ScreenItems.AlignH.Right;
            _label.TextHorizontalJustification = MonoGUI.ScreenItems.AlignH.Right;
        }

        void hMid_ButtonClicked(object sender, EventArgs e)
        {
            _block.TextHorizontalJustification = MonoGUI.ScreenItems.AlignH.Center;
            _label.TextHorizontalJustification = MonoGUI.ScreenItems.AlignH.Center;
        }

        void hLeft_ButtonClicked(object sender, EventArgs e)
        {
            _block.TextHorizontalJustification = MonoGUI.ScreenItems.AlignH.Left;
            _label.TextHorizontalJustification = MonoGUI.ScreenItems.AlignH.Left;
        }

        void vertSlider_ProgressChanged(object sender, EventArgs e)
        {
            _block.PreferredDimensions = new Vector2(
                minHoriz + (maxHoriz - minHoriz) * horizSlider.Progress,
                minVert + (maxVert - minVert) * vertSlider.Progress
                );
            _block.RequestResize();
            _label.PreferredDimensions = new Vector2(
                minHoriz + (maxHoriz - minHoriz) * horizSlider.Progress,
                minVert + (maxVert - minVert) * vertSlider.Progress
                );
            _label.RequestResize();
        }

        void horizSlider_ProgressChanged(object sender, EventArgs e)
        {
            _block.PreferredDimensions = new Vector2(
                minHoriz + (maxHoriz - minHoriz) * horizSlider.Progress,
                minVert + (maxVert - minVert) * vertSlider.Progress
                );
            _block.RequestResize();
            _label.PreferredDimensions = new Vector2(
                minHoriz + (maxHoriz - minHoriz) * horizSlider.Progress,
                minVert + (maxVert - minVert) * vertSlider.Progress
                );
            _label.RequestResize();
        }
    }
}
