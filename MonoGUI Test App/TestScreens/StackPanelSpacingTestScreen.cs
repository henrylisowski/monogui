﻿using Microsoft.Xna.Framework;
using MonoGUI.Manager;
using MonoGUI.ScreenItems;
using MonoGUI.ScreenItems.Containers.CanvasContainer;
using MonoGUI.ScreenItems.Containers.StackContainer;
using MonoGUI.Skinning;
using MonoGUI.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI_Test_App.TestScreens
{
    public class StackPanelSpacingTestScreen : CanvasFrame
    {
        public StackPanelSpacingTestScreen(ScreenManager ScreenManager)
            : base(ScreenManager)
        {
            this.Width = ItemSizeOptions.FillParent;
            this.Height = ItemSizeOptions.FillParent;
            StackPanel leftCanvas = new StackPanel(ScreenManager);
            StackPanel rightCanvas = new StackPanel(ScreenManager);
            leftCanvas.Direction = StackDirection.Vertical;
            leftCanvas.Width = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            leftCanvas.Height = MonoGUI.ScreenItems.ItemSizeOptions.FillParent;
            leftCanvas.PreferredDimensions = new Microsoft.Xna.Framework.Vector2(0.5f, 0f);
            leftCanvas.HorizontalAlignment = MonoGUI.ScreenItems.AlignH.Left;
            leftCanvas.SkinVariant = SkinReferencingConstants.SkinVariant1;

            rightCanvas.Direction = StackDirection.Horizontal;
            rightCanvas.Width = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            rightCanvas.Height = MonoGUI.ScreenItems.ItemSizeOptions.FillParent;
            rightCanvas.PreferredDimensions = new Microsoft.Xna.Framework.Vector2(0.5f, 0f);
            rightCanvas.HorizontalAlignment = MonoGUI.ScreenItems.AlignH.Right;
            rightCanvas.SkinVariant = SkinReferencingConstants.SkinVariant1;

            StackPanel leftOne = new StackPanel(ScreenManager);
            leftOne.Direction = StackDirection.Horizontal;
            leftOne.HorizontalAlignment = MonoGUI.ScreenItems.AlignH.Left;
            leftOne.VerticalAlignment = AlignV.Top;
            leftOne.SkinVariant = SkinReferencingConstants.SkinVariant1;
            StackPanel leftTwo = new StackPanel(ScreenManager);
            leftTwo.Direction = StackDirection.Horizontal;
            leftTwo.HorizontalAlignment = MonoGUI.ScreenItems.AlignH.Center;
            leftTwo.VerticalAlignment = AlignV.Center;
            leftTwo.SkinVariant = SkinReferencingConstants.SkinVariant1;
            leftTwo.Width = ItemSizeOptions.FillParent;
            leftTwo.Height = ItemSizeOptions.FillParent;
            StackPanel leftThree = new StackPanel(ScreenManager);
            leftThree.Direction = StackDirection.Horizontal;
            leftThree.HorizontalAlignment = MonoGUI.ScreenItems.AlignH.Right;
            leftThree.VerticalAlignment = AlignV.Bottom;
            leftThree.Width = ItemSizeOptions.FillParent;
            leftThree.Height = ItemSizeOptions.FillParent;
            leftThree.SkinVariant = SkinReferencingConstants.SkinVariant1;

            leftOne.AddChild(getIcon(new Vector2(50, 50), AlignH.Center, AlignV.Top));
            leftOne.AddChild(getIcon(new Vector2(75, 75), AlignH.Center, AlignV.Center));
            leftOne.AddChild(getIcon(new Vector2(50, 50), AlignH.Center, AlignV.Bottom));

            leftTwo.AddChild(getIcon(new Vector2(0.33f, 1f), AlignH.Center, AlignV.Top, ItemSizeOptions.Percentage, ItemSizeOptions.Percentage));
            leftTwo.AddChild(getIcon(new Vector2(0.33f, 1f), AlignH.Center, AlignV.Top, ItemSizeOptions.Percentage, ItemSizeOptions.Percentage));
            leftTwo.AddChild(getIcon(new Vector2(0.33f, 1f), AlignH.Center, AlignV.Top, ItemSizeOptions.Percentage, ItemSizeOptions.Percentage));

            leftThree.AddChild(getIcon(new Vector2(), AlignH.Center, AlignV.Center, ItemSizeOptions.FillParent, ItemSizeOptions.FillParent));

            StackPanel rightOne = new StackPanel(ScreenManager);
            rightOne.Direction = StackDirection.Vertical;
            rightOne.HorizontalAlignment = MonoGUI.ScreenItems.AlignH.Left;
            rightOne.VerticalAlignment = AlignV.Top;
            rightOne.SkinVariant = SkinReferencingConstants.SkinVariant1;
            StackPanel rightTwo = new StackPanel(ScreenManager);
            rightTwo.Direction = StackDirection.Vertical;
            rightTwo.HorizontalAlignment = MonoGUI.ScreenItems.AlignH.Center;
            rightTwo.VerticalAlignment = AlignV.Center;
            rightTwo.SkinVariant = SkinReferencingConstants.SkinVariant1;
            rightTwo.Width = ItemSizeOptions.FillParent;
            rightTwo.Height = ItemSizeOptions.FillParent;
            StackPanel rightThree = new StackPanel(ScreenManager);
            rightThree.Direction = StackDirection.Vertical;
            rightThree.HorizontalAlignment = MonoGUI.ScreenItems.AlignH.Right;
            rightThree.VerticalAlignment = AlignV.Bottom;
            rightThree.Width = ItemSizeOptions.FillParent;
            rightThree.Height = ItemSizeOptions.FillParent;
            rightThree.SkinVariant = SkinReferencingConstants.SkinVariant1;

            rightOne.AddChild(getIcon(new Vector2(50, 50), AlignH.Center, AlignV.Top));
            rightOne.AddChild(getIcon(new Vector2(75, 75), AlignH.Center, AlignV.Center));
            rightOne.AddChild(getIcon(new Vector2(50, 50), AlignH.Center, AlignV.Bottom));

            rightTwo.AddChild(getIcon(new Vector2(1f, 0.33f), AlignH.Center, AlignV.Top, ItemSizeOptions.Percentage, ItemSizeOptions.Percentage));
            rightTwo.AddChild(getIcon(new Vector2(1f, 0.33f), AlignH.Center, AlignV.Top, ItemSizeOptions.Percentage, ItemSizeOptions.Percentage));
            rightTwo.AddChild(getIcon(new Vector2(1f, 0.33f), AlignH.Center, AlignV.Top, ItemSizeOptions.Percentage, ItemSizeOptions.Percentage));

            rightThree.AddChild(getIcon(new Vector2(), AlignH.Center, AlignV.Center, ItemSizeOptions.FillParent, ItemSizeOptions.FillParent));

            leftCanvas.AddChild(leftOne);
            leftCanvas.AddChild(leftTwo);
            leftCanvas.AddChild(leftThree);

            rightCanvas.AddChild(rightOne);
            rightCanvas.AddChild(rightTwo);
            rightCanvas.AddChild(rightThree);

            this.AddChild(leftCanvas);
            this.AddChild(rightCanvas);
        }

        private Icon getIcon(Vector2 PreferredDimensions, AlignH HorizontalAlignment, AlignV VerticalAlignment)
        {
            return getIcon(PreferredDimensions, HorizontalAlignment, VerticalAlignment, ItemSizeOptions.Absolute, ItemSizeOptions.Absolute);
        }

        private Icon getIcon(Vector2 PreferredDimensions, AlignH HorizontalAlignment, AlignV VerticalAlignment, 
            ItemSizeOptions WidthFill, ItemSizeOptions HeightFill)
        {
            Icon retVal = new Icon(Manager);

            retVal.PreferredDimensions = PreferredDimensions;
            retVal.HorizontalAlignment = HorizontalAlignment;
            retVal.VerticalAlignment = VerticalAlignment;
            retVal.Width = WidthFill;
            retVal.Height = HeightFill;
            return retVal;
        }

        public override void UpdateChildRelativeSize()
        {
            base.UpdateChildRelativeSize();
        }

        public override void UpdateRequestedSizeDueToChildResize()
        {
            base.UpdateRequestedSizeDueToChildResize();
        }
    }
}
