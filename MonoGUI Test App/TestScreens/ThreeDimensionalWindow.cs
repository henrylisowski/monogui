﻿using Microsoft.Xna.Framework;
using MonoGUI.Manager;
using MonoGUI.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI_Test_App.TestScreens
{
    public class ThreeDimensionalWindow : GUIWindow
    {
        private GUIWindow _displayWindow;
        private Slider horizSlider, horizSliderAnchor, vertSlider, vertSliderAnchor;
        public ThreeDimensionalWindow(ScreenManager Manager)
            : base(Manager, "3D Panel Support (In Development)", false)
        {
            Width = MonoGUI.ScreenItems.ItemSizeOptions.FillParent;
            Height = MonoGUI.ScreenItems.ItemSizeOptions.FillParent;

            _displayWindow = new GUIWindow(Manager, "I'm a 3D Window!", false);
            _displayWindow.Width = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            _displayWindow.Height = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            _displayWindow.HorizontalPositionType = MonoGUI.ScreenItems.ItemPositionOptions.Percentage;
            _displayWindow.VerticalPositionType = MonoGUI.ScreenItems.ItemPositionOptions.Percentage;
            _displayWindow.PreferredDimensions = new Vector2(0.8f, 0.8f);

            Icon displayIcon = new Icon(Manager, "brobot_blueJump");
            displayIcon.Width = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            displayIcon.Height = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            displayIcon.HorizontalAlignment = MonoGUI.ScreenItems.AlignH.Center;
            displayIcon.PreferredDimensions = new Vector2(0.5f, 0.5f);
            _displayWindow.AddChild(displayIcon);

            Label displayLabel = new Label(Manager, "I'm stuff on a 3D Plane!");
            displayLabel.Width = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            displayLabel.Height = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            displayLabel.HorizontalAlignment = MonoGUI.ScreenItems.AlignH.Center;
            displayLabel.VerticalPositionType = MonoGUI.ScreenItems.ItemPositionOptions.Percentage;
            displayLabel.PreferredDimensions = new Vector2(1f, 0.4f);
            displayLabel.PreferredPosition = new Vector2(0f, 0.6f);
            displayLabel.TextHorizontalJustification = MonoGUI.ScreenItems.AlignH.Center;
            _displayWindow.AddChild(displayLabel);

            horizSlider = new Slider(Manager);
            horizSlider.Width = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            horizSlider.Height = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            horizSlider.PreferredDimensions = new Vector2(0.8f, 0.1f);
            horizSlider.HorizontalPositionType = MonoGUI.ScreenItems.ItemPositionOptions.Percentage;
            horizSlider.VerticalPositionType = MonoGUI.ScreenItems.ItemPositionOptions.Percentage;
            horizSlider.PreferredPosition = new Vector2(0.1f, 0.9f);
            horizSlider.ProgressChanged += horizSlider_ProgressChanged;

            horizSliderAnchor = new Slider(Manager);
            horizSliderAnchor.Width = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            horizSliderAnchor.Height = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            horizSliderAnchor.PreferredDimensions = new Vector2(0.8f, 0.1f);
            horizSliderAnchor.HorizontalPositionType = MonoGUI.ScreenItems.ItemPositionOptions.Percentage;
            horizSliderAnchor.VerticalPositionType = MonoGUI.ScreenItems.ItemPositionOptions.Percentage;
            horizSliderAnchor.PreferredPosition = new Vector2(0.1f, 0.8f);
            horizSliderAnchor.ProgressChanged += horizSlider_ProgressChanged;

            vertSlider = new Slider(Manager);
            vertSlider.FillDirection = ProgressBar.StandardFillDirection.FillDown;
            vertSlider.Width = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            vertSlider.Height = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            vertSlider.PreferredDimensions = new Vector2(0.05f, 0.8f);
            vertSlider.HorizontalPositionType = MonoGUI.ScreenItems.ItemPositionOptions.Percentage;
            vertSlider.VerticalPositionType = MonoGUI.ScreenItems.ItemPositionOptions.Percentage;
            vertSlider.PreferredPosition = new Vector2(0.95f, 0.1f);
            vertSlider.ProgressChanged += horizSlider_ProgressChanged;

            vertSliderAnchor = new Slider(Manager);
            vertSliderAnchor.FillDirection = ProgressBar.StandardFillDirection.FillDown;
            vertSliderAnchor.Width = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            vertSliderAnchor.Height = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            vertSliderAnchor.PreferredDimensions = new Vector2(0.05f, 0.8f);
            vertSliderAnchor.HorizontalPositionType = MonoGUI.ScreenItems.ItemPositionOptions.Percentage;
            vertSliderAnchor.VerticalPositionType = MonoGUI.ScreenItems.ItemPositionOptions.Percentage;
            vertSliderAnchor.PreferredPosition = new Vector2(0.9f, 0.1f);
            vertSliderAnchor.ProgressChanged += horizSlider_ProgressChanged;

            horizSlider.Progress = 0.5f;
            horizSliderAnchor.Progress = 0.5f;
            vertSlider.Progress = 0.5f;
            vertSliderAnchor.Progress = 0.5f;

            AddChild(horizSlider);
            AddChild(horizSliderAnchor);
            AddChild(vertSlider);
            AddChild(vertSliderAnchor);
            AddChild(_displayWindow);
        }

        void horizSlider_ProgressChanged(object sender, EventArgs e)
        {
            float xAnchor = horizSliderAnchor.Progress;
            float xTurn = (horizSlider.Progress - 0.5f) * 180;
            float yAnchor = vertSliderAnchor.Progress;
            float yTurn = (vertSlider.Progress - 0.5f) * 180;

            _displayWindow.SetXYRotation(yTurn, xTurn, new Vector2(yAnchor, xAnchor));
        }
    }
}
