﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGUI.Animations;
using MonoGUI.Animations.Tweens;
using MonoGUI.ScreenItems;
using MonoGUI.Skinning;
using MonoGUI.Skinning.Fonts;
using MonoGUI.Skinning.SkinElements;
using MonoGUI.Skinning.WidgetSkins;
using MonoGUI.Utilities;
using MonoGUI.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI_Test_App
{
    public static class PixelRPGSkinConstructor
    {
        public static MonoGUISkin GetPixelThemeSkin()
        {

            MonoGUISkin retVal = new MonoGUISkin(GetWidgetSkins());
            retVal.GlobalSkinElements = GetSkinElements();
            retVal.FontManager = new FontManager();
            getFonts(retVal.FontManager);


            return retVal;
        }

        private static List<ISkinElement> GetSkinElements()
        {
            List<ISkinElement> retVal = new List<ISkinElement>();
            getPanelElements(retVal);
            getStretchBarElements(retVal);
            //getButtonElements(retVal);
            getIconElements(retVal);
            return retVal;
        }

        private static void getPanelElements(List<ISkinElement> retVal)
        {
            TextureElement TopLeftBrown1Flat = new TextureElement("uipack_rpg_sheet",
                new Rectangle(0, 237, 12, 12), new Rectangle(0, 0, 12, 12), SpriteEffects.None);
            TextureElement TopMiddleBrown1Flat = new TextureElement("uipack_rpg_sheet",
                new Rectangle(12, 237, 166, 12), new Rectangle(12, 0, 1, 12), SpriteEffects.None,
                SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.NoFillCenter);
            TextureElement TopRightBrown1Flat = new TextureElement("uipack_rpg_sheet",
                new Rectangle(178, 237, 12, 12), new Rectangle(13, 0, 12, 12), SpriteEffects.None);
            TextureElement MiddleLeftBrown1Flat = new TextureElement("uipack_rpg_sheet",
                new Rectangle(0, 249, 12, 21), new Rectangle(0, 12, 12, 1), SpriteEffects.None,
                SingleFillElement.FillTypes.NoFillCenter, SingleFillElement.FillTypes.Stretch);
            TextureElement MiddleMiddleBrown1Flat = new TextureElement("uipack_rpg_sheet",
                new Rectangle(12, 249, 166, 21), new Rectangle(12, 12, 1, 1), SpriteEffects.None,
                SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.Stretch);
            TextureElement MiddleRightBrown1Flat = new TextureElement("uipack_rpg_sheet",
                new Rectangle(178, 249, 12, 21), new Rectangle(13, 12, 12, 1), SpriteEffects.None,
                SingleFillElement.FillTypes.NoFillCenter, SingleFillElement.FillTypes.Stretch);
            TextureElement BottomLeftBrown1Flat = new TextureElement("uipack_rpg_sheet",
                new Rectangle(0, 270, 12, 12), new Rectangle(0, 13, 12, 12), SpriteEffects.None);
            TextureElement BottomMiddleBrown1Flat = new TextureElement("uipack_rpg_sheet",
                new Rectangle(12, 270, 166, 12), new Rectangle(12, 13, 1, 12), SpriteEffects.None,
                SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.NoFillCenter);
            TextureElement BottomRightBrown1Flat = new TextureElement("uipack_rpg_sheet",
                new Rectangle(178, 270, 12, 12), new Rectangle(13, 13, 12, 12), SpriteEffects.None);

            TextureElement BottomLeftBrown1Raised = new TextureElement("uipack_rpg_sheet",
                new Rectangle(0, 319, 12, 12), new Rectangle(0, 13, 12, 12), SpriteEffects.None);
            TextureElement BottomMiddleBrown1Raised = new TextureElement("uipack_rpg_sheet",
                new Rectangle(12, 319, 166, 12), new Rectangle(12, 13, 1, 12), SpriteEffects.None,
                SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.NoFillCenter);
            TextureElement BottomRightBrown1Raised = new TextureElement("uipack_rpg_sheet",
                new Rectangle(178, 319, 12, 12), new Rectangle(13, 13, 12, 12), SpriteEffects.None);

            NineSpliceElement brown1Flat = new NineSpliceElement(TopLeftBrown1Flat.Clone(), TopRightBrown1Flat.Clone(),
                BottomLeftBrown1Flat.Clone(), BottomRightBrown1Flat.Clone(), TopMiddleBrown1Flat.Clone(),
                MiddleRightBrown1Flat.Clone(), BottomMiddleBrown1Flat.Clone(), MiddleLeftBrown1Flat.Clone(),
                MiddleMiddleBrown1Flat.Clone(), new Rectangle(0, 0, 25, 25));
            brown1Flat.ElementName = "Brown1Flat";
            retVal.Add(brown1Flat);

            NineSpliceElement brown1FlatOffset = new NineSpliceElement(TopLeftBrown1Flat.Clone(), TopRightBrown1Flat.Clone(),
                BottomLeftBrown1Flat.Clone(), BottomRightBrown1Flat.Clone(), TopMiddleBrown1Flat.Clone(),
                MiddleRightBrown1Flat.Clone(), BottomMiddleBrown1Flat.Clone(), MiddleLeftBrown1Flat.Clone(),
                MiddleMiddleBrown1Flat.Clone(), new Rectangle(0, 4, 25, 29));
            brown1FlatOffset.ElementName = "Brown1FlatOffset";
            retVal.Add(brown1FlatOffset);

            NineSpliceElement brown1Raised = new NineSpliceElement(TopLeftBrown1Flat.Clone(), TopRightBrown1Flat.Clone(),
                BottomLeftBrown1Raised.Clone(), BottomRightBrown1Raised.Clone(), TopMiddleBrown1Flat.Clone(),
                MiddleRightBrown1Flat.Clone(), BottomMiddleBrown1Raised.Clone(), MiddleLeftBrown1Flat.Clone(),
                MiddleMiddleBrown1Flat.Clone(), new Rectangle(0, 0, 25, 25));
            brown1Raised.ElementName = "Brown1Raised";
            retVal.Add(brown1Raised);



            TextureElement TopLeftGrey1Flat = new TextureElement("uipack_rpg_sheet",
                new Rectangle(0, 143, 12, 12), new Rectangle(0, 0, 12, 12), SpriteEffects.None);
            TextureElement TopMiddleGrey1Flat = new TextureElement("uipack_rpg_sheet",
                new Rectangle(12, 143, 166, 12), new Rectangle(12, 0, 1, 12), SpriteEffects.None,
                SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.NoFillCenter);
            TextureElement TopRightGrey1Flat = new TextureElement("uipack_rpg_sheet",
                new Rectangle(178, 143, 12, 12), new Rectangle(13, 0, 12, 12), SpriteEffects.None);
            TextureElement MiddleLeftGrey1Flat = new TextureElement("uipack_rpg_sheet",
                new Rectangle(0, 155, 12, 21), new Rectangle(0, 12, 12, 1), SpriteEffects.None,
                SingleFillElement.FillTypes.NoFillCenter, SingleFillElement.FillTypes.Stretch);
            TextureElement MiddleMiddleGrey1Flat = new TextureElement("uipack_rpg_sheet",
                new Rectangle(12, 155, 166, 21), new Rectangle(12, 12, 1, 1), SpriteEffects.None,
                SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.Stretch);
            TextureElement MiddleRightGrey1Flat = new TextureElement("uipack_rpg_sheet",
                new Rectangle(178, 155, 12, 21), new Rectangle(13, 12, 12, 1), SpriteEffects.None,
                SingleFillElement.FillTypes.NoFillCenter, SingleFillElement.FillTypes.Stretch);
            TextureElement BottomLeftGrey1Flat = new TextureElement("uipack_rpg_sheet",
                new Rectangle(0, 176, 12, 12), new Rectangle(0, 13, 12, 12), SpriteEffects.None);
            TextureElement BottomMiddleGrey1Flat = new TextureElement("uipack_rpg_sheet",
                new Rectangle(12, 176, 166, 12), new Rectangle(12, 13, 1, 12), SpriteEffects.None,
                SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.NoFillCenter);
            TextureElement BottomRightGrey1Flat = new TextureElement("uipack_rpg_sheet",
                new Rectangle(178, 176, 12, 12), new Rectangle(13, 13, 12, 12), SpriteEffects.None);

            TextureElement BottomLeftGrey1Raised = new TextureElement("uipack_rpg_sheet",
                new Rectangle(0, 225, 12, 12), new Rectangle(0, 13, 12, 12), SpriteEffects.None);
            TextureElement BottomMiddleGrey1Raised = new TextureElement("uipack_rpg_sheet",
                new Rectangle(12, 225, 166, 12), new Rectangle(12, 13, 1, 12), SpriteEffects.None,
                SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.NoFillCenter);
            TextureElement BottomRightGrey1Raised = new TextureElement("uipack_rpg_sheet",
                new Rectangle(178, 225, 12, 12), new Rectangle(13, 13, 12, 12), SpriteEffects.None);

            NineSpliceElement grey1Flat = new NineSpliceElement(TopLeftGrey1Flat.Clone(), TopRightGrey1Flat.Clone(),
                BottomLeftGrey1Flat.Clone(), BottomRightGrey1Flat.Clone(), TopMiddleGrey1Flat.Clone(),
                MiddleRightGrey1Flat.Clone(), BottomMiddleGrey1Flat.Clone(), MiddleLeftGrey1Flat.Clone(),
                MiddleMiddleGrey1Flat.Clone(), new Rectangle(0, 0, 25, 25));
            grey1Flat.ElementName = "Grey1Flat";
            retVal.Add(grey1Flat);

            NineSpliceElement grey1FlatOffset = new NineSpliceElement(TopLeftGrey1Flat.Clone(), TopRightGrey1Flat.Clone(),
                BottomLeftGrey1Flat.Clone(), BottomRightGrey1Flat.Clone(), TopMiddleGrey1Flat.Clone(),
                MiddleRightGrey1Flat.Clone(), BottomMiddleGrey1Flat.Clone(), MiddleLeftGrey1Flat.Clone(),
                MiddleMiddleGrey1Flat.Clone(), new Rectangle(0, 4, 25, 29));
            grey1FlatOffset.ElementName = "Grey1FlatOffset";
            retVal.Add(grey1FlatOffset);

            NineSpliceElement grey1Raised = new NineSpliceElement(TopLeftGrey1Flat.Clone(), TopRightGrey1Flat.Clone(),
                BottomLeftGrey1Raised.Clone(), BottomRightGrey1Raised.Clone(), TopMiddleGrey1Flat.Clone(),
                MiddleRightGrey1Flat.Clone(), BottomMiddleGrey1Raised.Clone(), MiddleLeftGrey1Flat.Clone(),
                MiddleMiddleGrey1Flat.Clone(), new Rectangle(0, 0, 25, 25));
            grey1Raised.ElementName = "Grey1Raised";
            retVal.Add(grey1Raised);



            TextureElement TopLeftDarkBrown1Flat = new TextureElement("uipack_rpg_sheet",
                new Rectangle(0, 98, 12, 12), new Rectangle(0, 0, 12, 12), SpriteEffects.None);
            TextureElement TopMiddleDarkBrown1Flat = new TextureElement("uipack_rpg_sheet",
                new Rectangle(12, 98, 166, 12), new Rectangle(12, 0, 1, 12), SpriteEffects.None,
                SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.NoFillCenter);
            TextureElement TopRightDarkBrown1Flat = new TextureElement("uipack_rpg_sheet",
                new Rectangle(178, 98, 12, 12), new Rectangle(13, 0, 12, 12), SpriteEffects.None);
            TextureElement MiddleLeftDarkBrown1Flat = new TextureElement("uipack_rpg_sheet",
                new Rectangle(0, 110, 12, 21), new Rectangle(0, 12, 12, 1), SpriteEffects.None,
                SingleFillElement.FillTypes.NoFillCenter, SingleFillElement.FillTypes.Stretch);
            TextureElement MiddleMiddleDarkBrown1Flat = new TextureElement("uipack_rpg_sheet",
                new Rectangle(12, 110, 166, 21), new Rectangle(12, 12, 1, 1), SpriteEffects.None,
                SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.Stretch);
            TextureElement MiddleRightDarkBrown1Flat = new TextureElement("uipack_rpg_sheet",
                new Rectangle(178, 110, 12, 21), new Rectangle(13, 12, 12, 1), SpriteEffects.None,
                SingleFillElement.FillTypes.NoFillCenter, SingleFillElement.FillTypes.Stretch);
            TextureElement BottomLeftDarkBrown1Flat = new TextureElement("uipack_rpg_sheet",
                new Rectangle(0, 131, 12, 12), new Rectangle(0, 13, 12, 12), SpriteEffects.None);
            TextureElement BottomMiddleDarkBrown1Flat = new TextureElement("uipack_rpg_sheet",
                new Rectangle(12, 131, 166, 12), new Rectangle(12, 13, 1, 12), SpriteEffects.None,
                SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.NoFillCenter);
            TextureElement BottomRightDarkBrown1Flat = new TextureElement("uipack_rpg_sheet",
                new Rectangle(178, 131, 12, 12), new Rectangle(13, 13, 12, 12), SpriteEffects.None);

            TextureElement BottomLeftDarkBrown1Raised = new TextureElement("uipack_rpg_sheet",
                new Rectangle(0, 86, 12, 12), new Rectangle(0, 13, 12, 12), SpriteEffects.None);
            TextureElement BottomMiddleDarkBrown1Raised = new TextureElement("uipack_rpg_sheet",
                new Rectangle(12, 86, 166, 12), new Rectangle(12, 13, 1, 12), SpriteEffects.None,
                SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.NoFillCenter);
            TextureElement BottomRightDarkBrown1Raised = new TextureElement("uipack_rpg_sheet",
                new Rectangle(178, 86, 12, 12), new Rectangle(13, 13, 12, 12), SpriteEffects.None);

            NineSpliceElement darkBrownFlat = new NineSpliceElement(TopLeftDarkBrown1Flat.Clone(), TopRightDarkBrown1Flat.Clone(),
                BottomLeftDarkBrown1Flat.Clone(), BottomRightDarkBrown1Flat.Clone(), TopMiddleDarkBrown1Flat.Clone(),
                MiddleRightDarkBrown1Flat.Clone(), BottomMiddleDarkBrown1Flat.Clone(), MiddleLeftDarkBrown1Flat.Clone(),
                MiddleMiddleDarkBrown1Flat.Clone(), new Rectangle(0, 0, 25, 25));
            darkBrownFlat.ElementName = "DarkBrown1Flat";
            retVal.Add(darkBrownFlat);

            NineSpliceElement drakBrownFlatOffset = new NineSpliceElement(TopLeftDarkBrown1Flat.Clone(), TopRightDarkBrown1Flat.Clone(),
                BottomLeftDarkBrown1Flat.Clone(), BottomRightDarkBrown1Flat.Clone(), TopMiddleDarkBrown1Flat.Clone(),
                MiddleRightDarkBrown1Flat.Clone(), BottomMiddleDarkBrown1Flat.Clone(), MiddleLeftDarkBrown1Flat.Clone(),
                MiddleMiddleDarkBrown1Flat.Clone(), new Rectangle(0, 4, 25, 29));
            drakBrownFlatOffset.ElementName = "DarkBrown1FlatOffset";
            retVal.Add(drakBrownFlatOffset);

            NineSpliceElement darkBrownRaised = new NineSpliceElement(TopLeftDarkBrown1Flat.Clone(), TopRightDarkBrown1Flat.Clone(),
                BottomLeftDarkBrown1Raised.Clone(), BottomRightDarkBrown1Raised.Clone(), TopMiddleDarkBrown1Flat.Clone(),
                MiddleRightDarkBrown1Flat.Clone(), BottomMiddleDarkBrown1Raised.Clone(), MiddleLeftDarkBrown1Flat.Clone(),
                MiddleMiddleDarkBrown1Flat.Clone(), new Rectangle(0, 0, 25, 25));
            darkBrownRaised.ElementName = "DarkBrown1Raised";
            retVal.Add(darkBrownRaised);
        }

        private static void getStretchBarElements(List<ISkinElement> retVal)
        {
            TextureElement LeftBlueStretchBar = new TextureElement("uipack_rpg_sheet", 
                new Rectangle(372, 294, 9, 18), new Rectangle(0, 0, 9, 18),
                SpriteEffects.None, SingleFillElement.FillTypes.NoFillCenter, SingleFillElement.FillTypes.Stretch);
            TextureElement RightBlueStretchBar = new TextureElement("uipack_rpg_sheet", 
                new Rectangle(372, 312, 9, 18), new Rectangle(10, 0, 9, 18),
                SpriteEffects.None, SingleFillElement.FillTypes.NoFillCenter, SingleFillElement.FillTypes.Stretch);
            TextureElement MiddleBlueStretchBar = new TextureElement("uipack_rpg_sheet", 
                new Rectangle(356, 431, 18, 18), new Rectangle(9, 0, 1, 18),
                SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.Stretch);

            ContainerFillElement horizontalBlueStretchBar = new ContainerFillElement();
            horizontalBlueStretchBar.AddSkinElement(LeftBlueStretchBar, new ScalingValues(0, 0, 0, 1f));
            horizontalBlueStretchBar.AddSkinElement(MiddleBlueStretchBar, new ScalingValues(0, 0, 1f, 1f));
            horizontalBlueStretchBar.AddSkinElement(RightBlueStretchBar, new ScalingValues(1, 0, 0, 1f));
            horizontalBlueStretchBar.ElementName = "HorizontalBlueStretchBar";
            horizontalBlueStretchBar.DestinationBox = new Rectangle(0, 0, 19, 18);
            retVal.Add(horizontalBlueStretchBar);

            TextureElement LeftWhiteStretchBar = new TextureElement("uipack_rpg_sheet",
                new Rectangle(374, 404, 9, 18), new Rectangle(0, 0, 9, 18),
                SpriteEffects.None, SingleFillElement.FillTypes.NoFillCenter, SingleFillElement.FillTypes.Stretch);
            TextureElement RightWhiteStretchBar = new TextureElement("uipack_rpg_sheet",
                new Rectangle(403, 404, 9, 18), new Rectangle(10, 0, 9, 18),
                SpriteEffects.None, SingleFillElement.FillTypes.NoFillCenter, SingleFillElement.FillTypes.Stretch);
            TextureElement MiddleWhiteStretchBar = new TextureElement("uipack_rpg_sheet",
                new Rectangle(383, 404, 18, 18), new Rectangle(9, 0, 1, 18),
                SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.Stretch);

            ContainerFillElement horizontalWhiteStretchBar = new ContainerFillElement();
            horizontalWhiteStretchBar.AddSkinElement(LeftWhiteStretchBar, new ScalingValues(0, 0, 0, 1f));
            horizontalWhiteStretchBar.AddSkinElement(MiddleWhiteStretchBar, new ScalingValues(0, 0, 1f, 1f));
            horizontalWhiteStretchBar.AddSkinElement(RightWhiteStretchBar, new ScalingValues(1, 0, 0, 1f));
            horizontalWhiteStretchBar.ElementName = "HorizontalWhiteStretchBar";
            horizontalWhiteStretchBar.DestinationBox = new Rectangle(0, 0, 19, 18);
            retVal.Add(horizontalWhiteStretchBar);

            TextureElement TopVWhiteStretchBar = new TextureElement("uipack_rpg_sheet",
                new Rectangle(374, 368, 18, 9), new Rectangle(0, 0, 18, 9),
                SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.NoFillCenter);
            TextureElement BottomVWhiteStretchBar = new TextureElement("uipack_rpg_sheet",
                new Rectangle(374, 395, 18, 9), new Rectangle(0, 10, 18, 9),
                SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.NoFillCenter);
            TextureElement MiddleVWhiteStretchBar = new TextureElement("uipack_rpg_sheet",
                new Rectangle(374, 377, 18, 18), new Rectangle(0, 9, 18, 1),
                SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.Stretch);

            ContainerFillElement verticalWhiteStretchBar = new ContainerFillElement();
            verticalWhiteStretchBar.AddSkinElement(TopVWhiteStretchBar, new ScalingValues(0, 0, 1f, 0f));
            verticalWhiteStretchBar.AddSkinElement(MiddleVWhiteStretchBar, new ScalingValues(0, 0, 1f, 1f));
            verticalWhiteStretchBar.AddSkinElement(BottomVWhiteStretchBar, new ScalingValues(0, 1, 1f, 0f));
            verticalWhiteStretchBar.ElementName = "VerticalWhiteStretchBar";
            verticalWhiteStretchBar.DestinationBox = new Rectangle(0, 0, 18, 19);
            retVal.Add(verticalWhiteStretchBar);

            TextureElement TopVBlueStretchBar = new TextureElement("uipack_rpg_sheet",
                new Rectangle(356, 404, 18, 9), new Rectangle(0, 0, 18, 9),
                SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.NoFillCenter);
            TextureElement BottomVBlueStretchBar = new TextureElement("uipack_rpg_sheet",
                new Rectangle(344, 189, 18, 9), new Rectangle(0, 10, 18, 9),
                SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.NoFillCenter);
            TextureElement MiddleVBlueStretchBar = new TextureElement("uipack_rpg_sheet",
                new Rectangle(356, 386, 18, 18), new Rectangle(0, 9, 18, 1),
                SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.Stretch);

            ContainerFillElement verticalBlueStretchBar = new ContainerFillElement();
            verticalBlueStretchBar.AddSkinElement(TopVBlueStretchBar, new ScalingValues(0, 0, 1f, 0f));
            verticalBlueStretchBar.AddSkinElement(MiddleVBlueStretchBar, new ScalingValues(0, 0, 1f, 1f));
            verticalBlueStretchBar.AddSkinElement(BottomVBlueStretchBar, new ScalingValues(0, 1, 1f, 0f));
            verticalBlueStretchBar.ElementName = "VerticalBlueStretchBar";
            verticalBlueStretchBar.DestinationBox = new Rectangle(0, 0, 18, 19);
            retVal.Add(verticalBlueStretchBar);
        }

        private static void getIconElements(List<ISkinElement> retVal)
        {
            TextureElement checkIcon = new TextureElement("uipack_rpg_sheet", new Rectangle(370, 0, 15, 15), new Rectangle(0, 0, 15, 15),
                SpriteEffects.None, SingleFillElement.FillTypes.Stretch);
            checkIcon.ElementName = "BrownCheckIcon";
            retVal.Add(checkIcon);

            TextureElement radioOutline = new TextureElement("uipack_rpg_sheet", new Rectangle(335, 114, 35, 35),
                new Rectangle(0, 0, 35, 35), SpriteEffects.None, SingleFillElement.FillTypes.Stretch);
            radioOutline.ElementName = "BrownRadioOutline";
            retVal.Add(radioOutline);

            TextureElement radioFill = new TextureElement("uipack_rpg_sheet", new Rectangle(369, 152, 17, 17),
                new Rectangle(0, 0, 17, 17), SpriteEffects.None, SingleFillElement.FillTypes.Stretch);
            radioFill.ElementName = "BrownRadioFill";
            retVal.Add(radioFill);

            TextureElement verticalSliderIcon = new TextureElement("uipack_rpg_sheet",
                new Rectangle(325, 486, 22, 21), new Rectangle(0, 0, 22, 21), SpriteEffects.FlipHorizontally,
                SingleFillElement.FillTypes.Stretch);
            verticalSliderIcon.ElementName = "DarkBrownVerticalSlider";
            retVal.Add(verticalSliderIcon);

            TextureElement horizontalSliderIcon = new TextureElement("uipack_rpg_sheet",
                new Rectangle(374, 423, 20, 21), new Rectangle(0, 0, 20, 21), SpriteEffects.None,
                SingleFillElement.FillTypes.Stretch);
            horizontalSliderIcon.ElementName = "DarkBrownHorizontalSlider";
            retVal.Add(horizontalSliderIcon);
        }

        private static List<GenericWidgetSkin> GetWidgetSkins()
        {
            List<GenericWidgetSkin> retVal = new List<GenericWidgetSkin>();
            getCanvasSkins(retVal);
            getScrollbarSkins(retVal);
            getSliderSkins(retVal);
            getButtonSkins(retVal);
            getIconSkins(retVal);
            getLabelSkins(retVal);
            getToggleButtonSkins(retVal);
            getCheckRadioSkins(retVal);
            getWindowSkins(retVal);
            getTabbedPanelSkins(retVal);
            return retVal;
        }

        private static void getCanvasSkins(List<GenericWidgetSkin> retVal)
        {
            GenericContainerSkin simpleContainer = new GenericContainerSkin(SkinReferencingConstants.WidgetTargetCanvas,
                SkinReferencingConstants.SkinVariant1);
            simpleContainer.BackgroundSkin = new SkinnedPropertyStateValue<ISkinElement>(new GlobalReferenceElement("Brown1Flat"));
            simpleContainer.SkinnedChildArea = new SkinnedPropertyStateValue<AreaDefinition>(
                new AreaDefinition(new RectangleFloat(6, 6, 13, 13), new ScalingValues(0, 0, 1, 1)));
            simpleContainer.MinimumDimensions = new AreaDefinition(25, 25);
            retVal.Add(simpleContainer);

            GenericContainerSkin simpleContainerVariant = new GenericContainerSkin(SkinReferencingConstants.WidgetTargetCanvas,
                SkinReferencingConstants.SkinVariant2);
            simpleContainerVariant.BackgroundSkin = new SkinnedPropertyStateValue<ISkinElement>(new GlobalReferenceElement("Grey1Flat"));
            simpleContainerVariant.SkinnedChildArea = new SkinnedPropertyStateValue<AreaDefinition>(
                new AreaDefinition(new RectangleFloat(6, 6, 13, 13), new ScalingValues(0, 0, 1, 1)));
            simpleContainerVariant.MinimumDimensions = new AreaDefinition(25, 25);
            retVal.Add(simpleContainerVariant);

            GenericContainerSkin darkBrownRaised = new GenericContainerSkin(SkinReferencingConstants.WidgetTargetCanvas,
                SkinReferencingConstants.SkinVariant10);
            darkBrownRaised.BackgroundSkin = new SkinnedPropertyStateValue<ISkinElement>(
                new GlobalReferenceElement("DarkBrown1Flat"));
            darkBrownRaised.SkinnedChildArea = new SkinnedPropertyStateValue<AreaDefinition>(
                new AreaDefinition(new RectangleFloat(6, 6, 13, 13), new ScalingValues(0, 0, 1, 1)));
            darkBrownRaised.MinimumDimensions = new AreaDefinition(25, 25);
            retVal.Add(darkBrownRaised);

        }

        private static void getScrollbarSkins(List<GenericWidgetSkin> retVal)
        {
            ScrollBarSkin defaultScroll = new ScrollBarSkin(SkinReferencingConstants.WidgetTargetScrollBar,
                SkinReferencingConstants.SkinVariantDefault);

            //defaultScroll.
        }

        private static void getSliderSkins(List<GenericWidgetSkin> retVal)
        {
            SliderSkin slider = new SliderSkin(SkinReferencingConstants.WidgetTargetSlider,
                SkinReferencingConstants.SkinVariantDefault);
            slider.ProgressFillType = ProgressBar.FillType.StandardFill;
            slider.HorizontalOutlineVariant = "HorizontalWhiteStretchBar";
            slider.HorizontalOutlineArea = new SkinnedPropertyStateValue<AreaDefinition>(
                new AreaDefinition(19, 18));
            slider.HorizontalFillVariant = "HorizontalBlueStretchBar";
            slider.HorizontalFillArea = new SkinnedPropertyStateValue<AreaDefinition>(
                new AreaDefinition(19, 18));
            slider.MinimumDimensions = new AreaDefinition(19, 18);

            slider.VerticalOutlineVariant = "VerticalWhiteStretchBar";
            slider.VerticalOutlineArea = new SkinnedPropertyStateValue<AreaDefinition>(
                new AreaDefinition(18, 19));
            slider.VerticalFillVariant = "VerticalBlueStretchBar";
            slider.VerticalFillArea = new SkinnedPropertyStateValue<AreaDefinition>(
                new AreaDefinition(18, 19));

            slider.HorizontalSliderIconSkin = new SkinnedPropertyStateValue<ISkinElement>(
                new GlobalReferenceElement("DarkBrownHorizontalSlider"));
            slider.HorizontalSliderIconMinimumDimensions = new AreaDefinition(20, 21);
            slider.HorizontalSliderIconArea = new SkinnedPropertyStateValue<AreaDefinition>(
                new AreaDefinition(new RectangleFloat(-10, 10, 20, 21), new ScalingValues(0, 0.5f, 0, 0)));

            slider.VerticalSliderIconSkin = new SkinnedPropertyStateValue<ISkinElement>(
                new GlobalReferenceElement("DarkBrownVerticalSlider"));
            slider.VerticalSliderIconMinimumDimensions = new AreaDefinition(22, 21);
            slider.VerticalSliderIconArea = new SkinnedPropertyStateValue<AreaDefinition>(
                new AreaDefinition(new RectangleFloat(11, -11, 22, 21), new ScalingValues(0.5f, 0, 0, 0)));

            retVal.Add(slider);
        }

        private static void getButtonSkins(List<GenericWidgetSkin> retVal)
        {
            ButtonSkin basicBrownButton = new ButtonSkin(SkinReferencingConstants.WidgetTargetButton, 
                SkinReferencingConstants.SkinVariant1);
            basicBrownButton.ButtonForeground = new SkinnedPropertyStateValue<ISkinElement>(new int[] { 0, 2 },
                new ISkinElement[]{
                    new GlobalReferenceElement("Brown1Raised"), new GlobalReferenceElement("Brown1FlatOffset")
                });
            basicBrownButton.MinimumDimensions = new AreaDefinition(25, 25);
            basicBrownButton.SkinnedChildArea = new SkinnedPropertyStateValue<AreaDefinition>(
                new int[] { 0, 2 },
                new AreaDefinition[]{
                new AreaDefinition(new RectangleFloat(6, 6, 13, 13), new ScalingValues(0, 0, 1, 1)),
                new AreaDefinition(new RectangleFloat(6, 10, 13, 13), new ScalingValues(0, 0, 1, 1))
                });
            basicBrownButton.LabelSkinVariant = SkinReferencingConstants.SkinVariantDefault;
            retVal.Add(basicBrownButton);

            ButtonSkin basicBrownButton2 = new ButtonSkin(SkinReferencingConstants.WidgetTargetButton,
                SkinReferencingConstants.SkinVariantDefault);
            basicBrownButton2.ButtonForeground = new SkinnedPropertyStateValue<ISkinElement>(new int[] { 0, 2 },
                new ISkinElement[]{
                    new GlobalReferenceElement("Brown1Raised"), new GlobalReferenceElement("Brown1FlatOffset")
                });
            basicBrownButton2.MinimumDimensions = new AreaDefinition(25, 25);
            basicBrownButton2.SkinnedChildArea = new SkinnedPropertyStateValue<AreaDefinition>(
                new int[]{0, 2},
                new AreaDefinition[]{
                new AreaDefinition(new RectangleFloat(6, 6, 13, 13), new ScalingValues(0, 0, 1, 1)),
                new AreaDefinition(new RectangleFloat(6, 10, 13, 13), new ScalingValues(0, 0, 1, 1))
                });
            basicBrownButton2.LabelSkinVariant = SkinReferencingConstants.SkinVariantDefault;
            retVal.Add(basicBrownButton2);

        }

        private static void getIconSkins(List<GenericWidgetSkin> retVal)
        {
            IconSkin checkIcon = new IconSkin(SkinReferencingConstants.WidgetTargetIcon, "BrownCheckIcon");
            checkIcon.IconImage = new SkinnedPropertyStateValue<ISkinElement>(
                new GlobalReferenceElement("BrownCheckIcon"));
            checkIcon.MinimumDimensions = new AreaDefinition(15, 15);

            IconSkin radioOutline = new IconSkin(SkinReferencingConstants.WidgetTargetIcon, "BrownRadioOutline");
            radioOutline.IconImage = new SkinnedPropertyStateValue<ISkinElement>(
                new GlobalReferenceElement("BrownRadioOutline"));
            radioOutline.MinimumDimensions = new AreaDefinition(35, 35);
            retVal.Add(radioOutline);

            IconSkin radioFill = new IconSkin(SkinReferencingConstants.WidgetTargetIcon, "BrownRadioFill");
            radioFill.IconImage = new SkinnedPropertyStateValue<ISkinElement>(
                new GlobalReferenceElement("BrownRadioFill"));
            radioFill.MinimumDimensions = new AreaDefinition(17, 17);
            retVal.Add(radioFill);

            IconSkin horizontalBlue = new IconSkin(SkinReferencingConstants.WidgetTargetIcon,
                "HorizontalBlueStretchBar");
            horizontalBlue.IconImage = new SkinnedPropertyStateValue<ISkinElement>(
                new GlobalReferenceElement("HorizontalBlueStretchBar"));
            horizontalBlue.MinimumDimensions = new AreaDefinition(19, 18);

            IconSkin horizontalWhite = new IconSkin(SkinReferencingConstants.WidgetTargetIcon,
                "HorizontalWhiteStretchBar");
            horizontalWhite.IconImage = new SkinnedPropertyStateValue<ISkinElement>(
                new GlobalReferenceElement("HorizontalWhiteStretchBar"));
            horizontalWhite.MinimumDimensions = new AreaDefinition(19, 18);

            IconSkin verticalBlue = new IconSkin(SkinReferencingConstants.WidgetTargetIcon,
                "VerticalBlueStretchBar");
            verticalBlue.IconImage = new SkinnedPropertyStateValue<ISkinElement>(
                new GlobalReferenceElement("VerticalBlueStretchBar"));
            verticalBlue.MinimumDimensions = new AreaDefinition(18, 19);

            IconSkin verticalWhite = new IconSkin(SkinReferencingConstants.WidgetTargetIcon,
                "VerticalWhiteStretchBar");
            verticalWhite.IconImage = new SkinnedPropertyStateValue<ISkinElement>(
                new GlobalReferenceElement("VerticalWhiteStretchBar"));
            verticalWhite.MinimumDimensions = new AreaDefinition(18, 19);

            retVal.Add(checkIcon);
            retVal.Add(horizontalBlue);
            retVal.Add(horizontalWhite);
            retVal.Add(verticalBlue);
            retVal.Add(verticalWhite);
        }

        private static void getLabelSkins(List<GenericWidgetSkin> retVal)
        {
            LabelSkin defaultLabelSkin = new LabelSkin(SkinReferencingConstants.WidgetTargetLabel, SkinReferencingConstants.SkinVariantDefault);
            defaultLabelSkin.FontName = "EndorDefault";
            defaultLabelSkin.FontRoundingMethod = FontRoundMethod.Closest;
            defaultLabelSkin.FontSize = 14;
            defaultLabelSkin.FontMethod = FontFillMethod.Fill;
            defaultLabelSkin.TextVerticalJustification = AlignV.Center;

            LabelSkin variantLabelSkin = new LabelSkin(SkinReferencingConstants.WidgetTargetLabel,
                SkinReferencingConstants.SkinVariant1);
            variantLabelSkin.FontName = "EndorDefault";
            variantLabelSkin.FontRoundingMethod = FontRoundMethod.Closest;
            variantLabelSkin.FontSize = 14;
            variantLabelSkin.FontMethod = FontFillMethod.Fill;
            variantLabelSkin.TextVerticalJustification = AlignV.Center;
            variantLabelSkin.BackgroundSkin = new SkinnedPropertyStateValue<ISkinElement>(
                new GlobalReferenceElement("Brown1Flat")
                );
            //defaultLabelSkin.BackgroundSkin = new SkinnedPropertyStateValue<ISkinElement>(new GlobalReferenceElement("GreyCircleCenter"));

            TextBlockSkin defaultTextBlockSkin = new TextBlockSkin(SkinReferencingConstants.WidgetTargetTextBlock, SkinReferencingConstants.SkinVariantDefault);
            defaultTextBlockSkin.FontName = "EndorDefault";
            defaultTextBlockSkin.FontRoundingMethod = FontRoundMethod.Closest;
            defaultTextBlockSkin.FontSize = 14;
            defaultTextBlockSkin.FontMethod = FontFillMethod.Fill;
            defaultTextBlockSkin.BackgroundSkin = new SkinnedPropertyStateValue<ISkinElement>(
                new GlobalReferenceElement("Brown1Flat")
                );

            retVal.Add(defaultLabelSkin);
            retVal.Add(variantLabelSkin);
            retVal.Add(defaultTextBlockSkin);
        }

        private static void getToggleButtonSkins(List<GenericWidgetSkin> retVal)
        {
            ToggleButtonSkin checkToggleSkin = new ToggleButtonSkin(SkinReferencingConstants.WidgetTargetToggleButton,
                "CheckboxToggle");
            checkToggleSkin.MinimumDimensions = new AreaDefinition(25, 25);
            checkToggleSkin.ButtonForegroundClicked = new SkinnedPropertyStateValue<ISkinElement>(new GlobalReferenceElement("Brown1Flat"));
            checkToggleSkin.ButtonForegroundNotClicked = checkToggleSkin.ButtonForegroundClicked;
            checkToggleSkin.ButtonToggledOnIconVariant = "BrownCheckIcon";
            checkToggleSkin.ButtonToggledOnIconArea = new SkinnedPropertyStateValue<AreaDefinition>(new AreaDefinition(
                new RectangleFloat(6, 6, 13, 13), new ScalingValues(0, 0, 1, 1), true
                ));
            checkToggleSkin.LabelSkinVariant = SkinReferencingConstants.SkinVariantDefault;
            retVal.Add(checkToggleSkin);

            ToggleButtonSkin radioToggleSkin = new ToggleButtonSkin(SkinReferencingConstants.WidgetTargetToggleButton,
                "RadioToggle");
            radioToggleSkin.MinimumDimensions = new AreaDefinition(35, 35);
            radioToggleSkin.ButtonForegroundClicked = new SkinnedPropertyStateValue<ISkinElement>(
                new GlobalReferenceElement("BrownRadioOutline"));
            radioToggleSkin.ButtonForegroundNotClicked = radioToggleSkin.ButtonForegroundClicked;
            radioToggleSkin.ButtonToggledOnIconVariant = "BrownRadioFill";
            radioToggleSkin.ButtonToggledOnIconArea = new SkinnedPropertyStateValue<AreaDefinition>(new AreaDefinition(
                new RectangleFloat(6, 6, 23, 23), new ScalingValues(0, 0, 1, 1), true
                ));
            radioToggleSkin.LabelSkinVariant = SkinReferencingConstants.SkinVariantDefault;
            retVal.Add(radioToggleSkin);
        }

        private static void getCheckRadioSkins(List<GenericWidgetSkin> retVal)
        {
            CheckboxSkin checkSkin = new CheckboxSkin(SkinReferencingConstants.WidgetTargetCheckbox, SkinReferencingConstants.SkinVariantDefault);
            checkSkin.ToggleButtonArea = new SkinnedPropertyStateValue<AreaDefinition>(new AreaDefinition(
                new RectangleFloat(3, 3, 36, 36), new ScalingValues(0, 0.5f, 0, 0), true));
            checkSkin.ToggleButtonSkinVariant = "CheckboxToggle";
            checkSkin.LabelSkinVariant = SkinReferencingConstants.SkinVariantDefault;
            checkSkin.TextArea = new SkinnedPropertyStateValue<AreaDefinition>(new AreaDefinition(
                new RectangleFloat(42, 3, 36, 36), new ScalingValues(0, 0.5f, 0.5f, 1), true));
            checkSkin.MinimumDimensions = new AreaDefinition(81, 42);
            checkSkin.BackgroundSkin = new SkinnedPropertyStateValue<ISkinElement>(new GlobalReferenceElement("Brown1Flat"));
            retVal.Add(checkSkin);

            CheckboxSkin radioSkin = new CheckboxSkin(SkinReferencingConstants.WidgetTargetRadioButton, SkinReferencingConstants.SkinVariantDefault);
            radioSkin.ToggleButtonArea = new SkinnedPropertyStateValue<AreaDefinition>(new AreaDefinition(
                new RectangleFloat(9, 9, 24, 24), new ScalingValues(0, 0.5f, 0, 0), true));
            radioSkin.ToggleButtonSkinVariant = "RadioToggle";
            radioSkin.LabelSkinVariant = SkinReferencingConstants.SkinVariantDefault;
            radioSkin.TextArea = new SkinnedPropertyStateValue<AreaDefinition>(new AreaDefinition(
                new RectangleFloat(42, 3, 36, 36), new ScalingValues(0, 0.5f, 0.5f, 1), true));
            radioSkin.MinimumDimensions = new AreaDefinition(81, 42);
            radioSkin.BackgroundSkin = new SkinnedPropertyStateValue<ISkinElement>(new GlobalReferenceElement("Brown1Flat"));
            retVal.Add(radioSkin);
        }

        private static void getWindowSkins(List<GenericWidgetSkin> retVal)
        {
            GUIWindowSkin windowSkin = new GUIWindowSkin(SkinReferencingConstants.WidgetTargetGUIWindow, SkinReferencingConstants.SkinVariantDefault);
            windowSkin.CloseButtonSkinVariant = "UNUSED";
            windowSkin.TitleLabelSkinVariant = SkinReferencingConstants.SkinVariantDefault;
            windowSkin.TitleBarCanvasSkinVariant = SkinReferencingConstants.SkinVariant10;
            windowSkin.TitleBarCanvasArea = new SkinnedPropertyStateValue<AreaDefinition>(new AreaDefinition(
                new RectangleFloat(0, 0, 25, 50), new ScalingValues(0, 0, 1, 0)));
            windowSkin.TitleLabelArea = new SkinnedPropertyStateValue<AreaDefinition>(new AreaDefinition(
                new RectangleFloat(3, 3, 44, 44), new ScalingValues(0, 0, 1, 0)));
            windowSkin.CloseButtonArea = new SkinnedPropertyStateValue<AreaDefinition>(new AreaDefinition(
                new RectangleFloat(0, 0, 1, 1), new ScalingValues(1f, 0, 0, 0)));
            windowSkin.MinimumDimensions = new AreaDefinition(25, 75);
            windowSkin.ChildrenArea = new SkinnedPropertyStateValue<AreaDefinition>(
                new AreaDefinition(new RectangleFloat(6, 56, 13, 13), new ScalingValues(Vector2.Zero, new Vector2(1, 1))));
            windowSkin.BackgroundSkin = new SkinnedPropertyStateValue<ISkinElement>(new GlobalReferenceElement("Brown1Flat"));
            retVal.Add(windowSkin);
        }

        private static void getTabbedPanelSkins(List<GenericWidgetSkin> retVal)
        {

        }

        private static void getFonts(FontManager fonts)
        {
            fonts.ThemeFontDescriptors.Add(new MonoGUI.Skinning.Fonts.FontDescriptor("EndorDefault", "Fonts\\Endor_40", 40));
            fonts.ThemeFontDescriptors.Add(new MonoGUI.Skinning.Fonts.FontDescriptor("EndorDefault", "Fonts\\Endor_36", 36));
            fonts.ThemeFontDescriptors.Add(new MonoGUI.Skinning.Fonts.FontDescriptor("EndorDefault", "Fonts\\Endor_32", 32));
            fonts.ThemeFontDescriptors.Add(new MonoGUI.Skinning.Fonts.FontDescriptor("EndorDefault", "Fonts\\Endor_28", 28));
            fonts.ThemeFontDescriptors.Add(new MonoGUI.Skinning.Fonts.FontDescriptor("EndorDefault", "Fonts\\Endor_24", 24));
            fonts.ThemeFontDescriptors.Add(new MonoGUI.Skinning.Fonts.FontDescriptor("EndorDefault", "Fonts\\Endor_20", 20));
            fonts.ThemeFontDescriptors.Add(new MonoGUI.Skinning.Fonts.FontDescriptor("EndorDefault", "Fonts\\Endor_18", 18));
            fonts.ThemeFontDescriptors.Add(new MonoGUI.Skinning.Fonts.FontDescriptor("EndorDefault", "Fonts\\Endor_16", 16));
            fonts.ThemeFontDescriptors.Add(new MonoGUI.Skinning.Fonts.FontDescriptor("EndorDefault", "Fonts\\Endor_14", 14));
            fonts.ThemeFontDescriptors.Add(new MonoGUI.Skinning.Fonts.FontDescriptor("EndorDefault", "Fonts\\Endor_12", 12));
            fonts.ThemeFontDescriptors.Add(new MonoGUI.Skinning.Fonts.FontDescriptor("EndorDefault", "Fonts\\Endor_10", 10));
            fonts.ThemeFontDescriptors.Add(new MonoGUI.Skinning.Fonts.FontDescriptor("EndorDefault", "Fonts\\Endor_8", 8));
            fonts.DefaultFont = new MonoGUI.Skinning.Fonts.FontDescriptor("EndorDefault", "Fonts\\Endor_14", 14);
        }
    }
}
