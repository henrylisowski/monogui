﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGUI.Animations;
using MonoGUI.Animations.Tweens;
using MonoGUI.ScreenItems;
using MonoGUI.Skinning;
using MonoGUI.Skinning.Fonts;
using MonoGUI.Skinning.SkinElements;
using MonoGUI.Skinning.WidgetSkins;
using MonoGUI.Utilities;
using MonoGUI.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI_Test_App
{
    public static class SpaceSkinConstructor
    {
        public static MonoGUISkin GetPixelThemeSkin()
        {

            MonoGUISkin retVal = new MonoGUISkin(GetWidgetSkins());
            retVal.GlobalSkinElements = GetSkinElements();
            retVal.FontManager = new FontManager();
            getFonts(retVal.FontManager);


            return retVal;
        }

        private static List<ISkinElement> GetSkinElements()
        {
            List<ISkinElement> retVal = new List<ISkinElement>();
            getPanelElements(retVal);
            getStretchBarElements(retVal);
            //getButtonElements(retVal);
            getIconElements(retVal);
            return retVal;
        }

        private static void getPanelElements(List<ISkinElement> retVal)
        {
            TextureElement TopLeftSimpleMetal = new TextureElement("uipackSpace_sheet", new Rectangle(200, 200, 10, 10), new Rectangle(0, 0, 10, 10),
                SpriteEffects.None);
            TextureElement TopRightSimpleMetal = new TextureElement("uipackSpace_sheet", new Rectangle(290, 200, 10, 10), new Rectangle(11, 0, 10, 10),
                SpriteEffects.None);
            TextureElement BottomLeftSimpleMetal = new TextureElement("uipackSpace_sheet", new Rectangle(200, 290, 10, 10), new Rectangle(0, 11, 10, 10),
                SpriteEffects.None);
            TextureElement BottomRightSimpleMetal = new TextureElement("uipackSpace_sheet", new Rectangle(290, 290, 10, 10), new Rectangle(11, 11, 10, 10),
                SpriteEffects.None);
            TextureElement LeftSimpleMetal = new TextureElement("uipackSpace_sheet", new Rectangle(200, 210, 10, 80), new Rectangle(0, 10, 10, 1),
                SpriteEffects.None, SingleFillElement.FillTypes.NoFillCenter, SingleFillElement.FillTypes.Stretch);
            TextureElement RightSimpleMetal = new TextureElement("uipackSpace_sheet", new Rectangle(290, 210, 10, 80), new Rectangle(11, 10, 10, 1),
                SpriteEffects.None, SingleFillElement.FillTypes.NoFillCenter, SingleFillElement.FillTypes.Stretch);
            TextureElement TopSimpleMetal = new TextureElement("uipackSpace_sheet", new Rectangle(210, 200, 80, 10), new Rectangle(10, 0, 1, 10),
                SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.NoFillCenter);
            TextureElement BottomSimpleMetal = new TextureElement("uipackSpace_sheet", new Rectangle(210, 290, 80, 10), new Rectangle(10, 11, 1, 10),
                SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.NoFillCenter);
            TextureElement CenterSimpleMetal = new TextureElement("uipackSpace_sheet", new Rectangle(210, 210, 80, 80), new Rectangle(10, 10, 1, 1),
                SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.Stretch);

            NineSpliceElement simpleMetalSplice = new NineSpliceElement(TopLeftSimpleMetal.Clone(), TopRightSimpleMetal.Clone(), BottomLeftSimpleMetal.Clone(),
                BottomRightSimpleMetal.Clone(), TopSimpleMetal.Clone(), RightSimpleMetal.Clone(), BottomSimpleMetal.Clone(), LeftSimpleMetal.Clone(),
                CenterSimpleMetal.Clone(), new Rectangle(0, 0, 21, 21));
            simpleMetalSplice.ElementName = "SimpleMetal";
            retVal.Add(simpleMetalSplice);

            TextureElement TopLeftIndentedMetal = new TextureElement("uipackSpace_sheet", new Rectangle(300, 212, 10, 10), new Rectangle(0, 0, 10, 10),
                SpriteEffects.None);
            TextureElement TopRightIndentedMetal = new TextureElement("uipackSpace_sheet", new Rectangle(370, 212, 10, 10), new Rectangle(11, 0, 10, 10),
                SpriteEffects.None);
            TextureElement BottomLeftIndentedMetal = new TextureElement("uipackSpace_sheet", new Rectangle(300, 282, 10, 10), new Rectangle(0, 11, 10, 10),
                SpriteEffects.None);
            TextureElement BottomRightIndentedMetal = new TextureElement("uipackSpace_sheet", new Rectangle(370, 282, 10, 10), new Rectangle(11, 11, 10, 10),
                SpriteEffects.None);
            TextureElement LeftIndentedMetal = new TextureElement("uipackSpace_sheet", new Rectangle(300, 222, 10, 60), new Rectangle(0, 10, 10, 1),
                SpriteEffects.None, SingleFillElement.FillTypes.NoFillCenter, SingleFillElement.FillTypes.Stretch);
            TextureElement RightIndentedMetal = new TextureElement("uipackSpace_sheet", new Rectangle(370, 222, 10, 60), new Rectangle(11, 10, 10, 1),
                SpriteEffects.None, SingleFillElement.FillTypes.NoFillCenter, SingleFillElement.FillTypes.Stretch);
            TextureElement TopIndentedMetal = new TextureElement("uipackSpace_sheet", new Rectangle(310, 212, 60, 10), new Rectangle(10, 0, 1, 10),
                SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.NoFillCenter);
            TextureElement BottomIndentedMetal = new TextureElement("uipackSpace_sheet", new Rectangle(310, 282, 60, 10), new Rectangle(10, 11, 1, 10),
                SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.NoFillCenter);
            TextureElement CenterIndentedMetal = new TextureElement("uipackSpace_sheet", new Rectangle(310, 222, 60, 60), new Rectangle(10, 10, 1, 1),
                SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.Stretch);

            NineSpliceElement indentedMetalSplice = new NineSpliceElement(TopLeftIndentedMetal.Clone(), TopRightIndentedMetal.Clone(),
                BottomLeftIndentedMetal.Clone(), BottomRightIndentedMetal.Clone(), TopIndentedMetal.Clone(), RightIndentedMetal.Clone(),
                BottomIndentedMetal.Clone(), LeftIndentedMetal.Clone(), CenterIndentedMetal.Clone(), new Rectangle(0, 0, 21, 21));
            indentedMetalSplice.ElementName = "IndentedMetal";
            retVal.Add(indentedMetalSplice);

            TextureElement TopLeftGlassButton = new TextureElement("uipackSpace_sheet", new Rectangle(200, 300, 10, 10), new Rectangle(0, 0, 10, 10),
                SpriteEffects.None);
            TextureElement TopRightGlassButton = new TextureElement("uipackSpace_sheet", new Rectangle(290, 300, 10, 10), new Rectangle(11, 0, 10, 10),
                SpriteEffects.None);
            TextureElement BottomLeftGlassButton = new TextureElement("uipackSpace_sheet", new Rectangle(200, 390, 10, 10), new Rectangle(0, 11, 10, 10),
                SpriteEffects.None);
            TextureElement BottomRightGlassButton = new TextureElement("uipackSpace_sheet", new Rectangle(290, 390, 10, 10), new Rectangle(11, 11, 10, 10),
                SpriteEffects.None);
            TextureElement LeftGlassButton = new TextureElement("uipackSpace_sheet", new Rectangle(200, 310, 10, 80), new Rectangle(0, 10, 10, 1),
                SpriteEffects.None, SingleFillElement.FillTypes.NoFillCenter, SingleFillElement.FillTypes.Stretch);
            TextureElement RightGlassButton = new TextureElement("uipackSpace_sheet", new Rectangle(290, 310, 10, 80), new Rectangle(11, 10, 10, 1),
                SpriteEffects.None, SingleFillElement.FillTypes.NoFillCenter, SingleFillElement.FillTypes.Stretch);
            TextureElement TopGlassButton = new TextureElement("uipackSpace_sheet", new Rectangle(210, 300, 80, 10), new Rectangle(10, 0, 1, 10),
                SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.NoFillCenter);
            TextureElement BottomGlassButton = new TextureElement("uipackSpace_sheet", new Rectangle(210, 390, 80, 10), new Rectangle(10, 11, 1, 10),
                SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.NoFillCenter);
            TextureElement CenterGlassButton = new TextureElement("uipackSpace_sheet", new Rectangle(210, 310, 80, 80), new Rectangle(10, 10, 1, 1),
                SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.Stretch);

            NineSpliceElement glassButtonSplice = new NineSpliceElement(TopLeftGlassButton.Clone(), TopRightGlassButton.Clone(), BottomLeftGlassButton.Clone(),
                BottomRightGlassButton.Clone(), TopGlassButton.Clone(), RightGlassButton.Clone(), BottomGlassButton.Clone(), LeftGlassButton.Clone(),
                CenterGlassButton.Clone(), new Rectangle(0, 0, 21, 21));
            glassButtonSplice.ElementName = "GlassButton";
            retVal.Add(glassButtonSplice);

            TextureElement TopLeftGlassButtonPressed = new TextureElement("uipackSpace_sheet", new Rectangle(407, 101, 10, 10), new Rectangle(0, 0, 10, 10),
                SpriteEffects.None);
            TextureElement TopRightGlassButtonPressed = new TextureElement("uipackSpace_sheet", new Rectangle(497, 101, 10, 10), new Rectangle(11, 0, 10, 10),
                SpriteEffects.None);
            TextureElement BottomLeftGlassButtonPressed = new TextureElement("uipackSpace_sheet", new Rectangle(407, 191, 10, 10), new Rectangle(0, 11, 10, 10),
                SpriteEffects.None);
            TextureElement BottomRightGlassButtonPressed = new TextureElement("uipackSpace_sheet", new Rectangle(497, 191, 10, 10), new Rectangle(11, 11, 10, 10),
                SpriteEffects.None);
            TextureElement LeftGlassButtonPressed = new TextureElement("uipackSpace_sheet", new Rectangle(407, 111, 10, 80), new Rectangle(0, 10, 10, 1),
                SpriteEffects.None, SingleFillElement.FillTypes.NoFillCenter, SingleFillElement.FillTypes.Stretch);
            TextureElement RightGlassButtonPressed = new TextureElement("uipackSpace_sheet", new Rectangle(497, 111, 10, 80), new Rectangle(11, 10, 10, 1),
                SpriteEffects.None, SingleFillElement.FillTypes.NoFillCenter, SingleFillElement.FillTypes.Stretch);
            TextureElement TopGlassButtonPressed = new TextureElement("uipackSpace_sheet", new Rectangle(417, 101, 80, 10), new Rectangle(10, 0, 1, 10),
                SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.NoFillCenter);
            TextureElement BottomGlassButtonPressed = new TextureElement("uipackSpace_sheet", new Rectangle(417, 191, 80, 10), new Rectangle(10, 11, 1, 10),
                SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.NoFillCenter);
            TextureElement CenterGlassButtonPressed = new TextureElement("uipackSpace_sheet", new Rectangle(417, 111, 80, 80), new Rectangle(10, 10, 1, 1),
                SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.Stretch);

            NineSpliceElement GlassButtonPressedSplice = new NineSpliceElement(TopLeftGlassButtonPressed.Clone(), TopRightGlassButtonPressed.Clone(), BottomLeftGlassButtonPressed.Clone(),
                BottomRightGlassButtonPressed.Clone(), TopGlassButtonPressed.Clone(), RightGlassButtonPressed.Clone(), BottomGlassButtonPressed.Clone(), LeftGlassButtonPressed.Clone(),
                CenterGlassButtonPressed.Clone(), new Rectangle(0, 0, 21, 21));
            GlassButtonPressedSplice.ElementName = "GlassButtonPressed";
            retVal.Add(GlassButtonPressedSplice);

            TextureElement TopLeftGlassSimple = new TextureElement("uipackSpace_sheet", new Rectangle(0, 0, 15, 15), new Rectangle(0, 0, 15, 15),
                SpriteEffects.None);
            TextureElement TopRightGlassSimple = new TextureElement("uipackSpace_sheet", new Rectangle(85, 0, 15, 15), new Rectangle(16, 0, 15, 15),
                SpriteEffects.None);
            TextureElement BottomLeftGlassSimple = new TextureElement("uipackSpace_sheet", new Rectangle(0, 85, 15, 15), new Rectangle(0, 16, 15, 15),
                SpriteEffects.None);
            TextureElement BottomRightGlassSimple = new TextureElement("uipackSpace_sheet", new Rectangle(85, 85, 15, 15), new Rectangle(16, 16, 15, 15),
                SpriteEffects.None);
            TextureElement LeftGlassSimple = new TextureElement("uipackSpace_sheet", new Rectangle(0, 15, 15, 70), new Rectangle(0, 15, 15, 1),
                SpriteEffects.None, SingleFillElement.FillTypes.NoFillCenter, SingleFillElement.FillTypes.Stretch);
            TextureElement RightGlassSimple = new TextureElement("uipackSpace_sheet", new Rectangle(85, 15, 15, 70), new Rectangle(16, 15, 15, 1),
                SpriteEffects.None, SingleFillElement.FillTypes.NoFillCenter, SingleFillElement.FillTypes.Stretch);
            TextureElement TopGlassSimple = new TextureElement("uipackSpace_sheet", new Rectangle(15, 0, 70, 15), new Rectangle(15, 0, 1, 15),
                SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.NoFillCenter);
            TextureElement BottomGlassSimple = new TextureElement("uipackSpace_sheet", new Rectangle(15, 85, 70, 15), new Rectangle(15, 16, 1, 15),
                SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.NoFillCenter);
            TextureElement CenterGlassSimple = new TextureElement("uipackSpace_sheet", new Rectangle(15, 15, 70, 70), new Rectangle(15, 15, 1, 1),
                SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.Stretch);

            NineSpliceElement glassSimpleSplice = new NineSpliceElement(TopLeftGlassSimple.Clone(), TopRightGlassSimple.Clone(),
                BottomLeftGlassSimple.Clone(), BottomRightGlassSimple.Clone(), TopGlassSimple.Clone(), RightGlassSimple.Clone(),
                BottomGlassSimple.Clone(), LeftGlassSimple.Clone(), CenterGlassSimple.Clone(), new Rectangle(0, 0, 31, 31));
            glassSimpleSplice.ElementName = "GlassSimple";
            retVal.Add(glassSimpleSplice);

            TextureElement TopLeftGlassClipped = new TextureElement("uipackSpace_sheet", new Rectangle(100, 200, 15, 15), new Rectangle(0, 0, 15, 15),
                SpriteEffects.None);
            TextureElement TopRightGlassClipped = new TextureElement("uipackSpace_sheet", new Rectangle(185, 200, 15, 15), new Rectangle(16, 0, 15, 15),
                SpriteEffects.None);
            TextureElement BottomLeftGlassClipped = new TextureElement("uipackSpace_sheet", new Rectangle(100, 285, 15, 15), new Rectangle(0, 16, 15, 15),
                SpriteEffects.None);
            TextureElement BottomRightGlassClipped = new TextureElement("uipackSpace_sheet", new Rectangle(185, 285, 15, 15), new Rectangle(16, 16, 15, 15),
                SpriteEffects.None);

            NineSpliceElement glassClippedBR = new NineSpliceElement(TopLeftGlassSimple.Clone(), TopRightGlassSimple.Clone(),
                BottomLeftGlassSimple.Clone(), BottomRightGlassClipped.Clone(), TopGlassSimple.Clone(), RightGlassSimple.Clone(),
                BottomGlassSimple.Clone(), LeftGlassSimple.Clone(), CenterGlassSimple.Clone(), new Rectangle(0, 0, 31, 31));
            glassClippedBR.ElementName = "GlassClippedBR";
            retVal.Add(glassClippedBR);


            TextureElement TopLeftBlueFullBar = new TextureElement("uipackSpace_sheet", new Rectangle(200, 100, 10, 30), new Rectangle(0, 0, 10, 30),
                SpriteEffects.None);
            TextureElement TopRightBlueFullBar = new TextureElement("uipackSpace_sheet", new Rectangle(290, 100, 10, 30), new Rectangle(11, 0, 10, 30),
                SpriteEffects.None);
            TextureElement TopBlueFullBar = new TextureElement("uipackSpace_sheet", new Rectangle(230, 100, 40, 30), new Rectangle(10, 0, 1, 30),
                SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.NoFillCenter);
            ContainerFillElement blueBarTop = new ContainerFillElement();
            blueBarTop.AddSkinElement(TopLeftBlueFullBar, new ScalingValues(0, 0, 0, 0));
            blueBarTop.AddSkinElement(TopBlueFullBar, new ScalingValues(0, 0, 1, 0));
            blueBarTop.AddSkinElement(TopRightBlueFullBar, new ScalingValues(1, 0, 0, 0));
            blueBarTop.ElementName = "BlueBarTopFull";
            blueBarTop.DestinationBox = new Rectangle(0, 0, 21, 30);
            retVal.Add(blueBarTop);

            TextureElement TopLeftBlueSplitBar = new TextureElement("uipackSpace_sheet", new Rectangle(200, 0, 10, 31), new Rectangle(0, 0, 10, 31),
                SpriteEffects.None);
            TextureElement TopRightBlueSplitBar = new TextureElement("uipackSpace_sheet", new Rectangle(249, 0, 11, 31), new Rectangle(11, 0, 11, 31),
                SpriteEffects.None);
            TextureElement TopBlueSplitBar = new TextureElement("uipackSpace_sheet", new Rectangle(210, 0, 39, 31), new Rectangle(10, 0, 1, 31),
                SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.NoFillCenter);
            ContainerFillElement blueBarSplitTop = new ContainerFillElement();
            blueBarSplitTop.AddSkinElement(TopLeftBlueSplitBar, new ScalingValues(0, 0, 0, 0));
            blueBarSplitTop.AddSkinElement(TopBlueSplitBar, new ScalingValues(0, 0, 1f, 0));
            blueBarSplitTop.AddSkinElement(TopRightBlueSplitBar, new ScalingValues(1f, 0, 0, 0));
            blueBarSplitTop.ElementName = "BlueBarTopSplit";
            blueBarSplitTop.DestinationBox = new Rectangle(0, 0, 22, 31);
            retVal.Add(blueBarSplitTop);


        }

        private static void getStretchBarElements(List<ISkinElement> retVal)
        {
            TextureElement LeftBlueStretchBar = new TextureElement("uipackSpace_sheet", new Rectangle(400, 78, 6, 26), new Rectangle(0, 0, 6, 26),
                SpriteEffects.None, SingleFillElement.FillTypes.NoFillCenter, SingleFillElement.FillTypes.Stretch);
            TextureElement RightBlueStretchBar = new TextureElement("uipackSpace_sheet", new Rectangle(400, 0, 6, 26), new Rectangle(7, 0, 6, 26),
                SpriteEffects.None, SingleFillElement.FillTypes.NoFillCenter, SingleFillElement.FillTypes.Stretch);
            TextureElement MiddleBlueStretchBar = new TextureElement("uipackSpace_sheet", new Rectangle(388, 420, 16, 26), new Rectangle(6, 0, 1, 26),
                SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.Stretch);

            ContainerFillElement horizontalBlueStretchBar = new ContainerFillElement();
            horizontalBlueStretchBar.AddSkinElement(LeftBlueStretchBar, new ScalingValues(0, 0, 0, 1f));
            horizontalBlueStretchBar.AddSkinElement(MiddleBlueStretchBar, new ScalingValues(0, 0, 1f, 1f));
            horizontalBlueStretchBar.AddSkinElement(RightBlueStretchBar, new ScalingValues(1, 0, 0, 1f));
            horizontalBlueStretchBar.ElementName = "HorizontalBlueStretchBar";
            horizontalBlueStretchBar.DestinationBox = new Rectangle(0, 0, 13, 26);
            retVal.Add(horizontalBlueStretchBar);

            TextureElement TopBlueVStretchBar = new TextureElement("uipackSpace_sheet", new Rectangle(104, 506, 26, 6), new Rectangle(0, 0, 26, 6),
                SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.NoFillCenter);
            TextureElement BottomBlueVStretchBar = new TextureElement("uipackSpace_sheet", new Rectangle(26, 500, 26, 6), new Rectangle(0, 7, 26, 6),
                SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.NoFillCenter);
            TextureElement MiddleBlueVStretchBar = new TextureElement("uipackSpace_sheet", new Rectangle(336, 322, 26, 16), new Rectangle(0, 6, 26, 1),
                SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.Stretch);

            ContainerFillElement verticalBlueStretchBar = new ContainerFillElement();
            verticalBlueStretchBar.AddSkinElement(TopBlueVStretchBar, new ScalingValues(0, 0, 1f, 0f));
            verticalBlueStretchBar.AddSkinElement(MiddleBlueVStretchBar, new ScalingValues(0, 0, 1f, 1f));
            verticalBlueStretchBar.AddSkinElement(BottomBlueVStretchBar, new ScalingValues(0, 1, 1f, 0f));
            verticalBlueStretchBar.ElementName = "VerticalBlueStretchBar";
            verticalBlueStretchBar.DestinationBox = new Rectangle(0, 0, 26, 13);
            retVal.Add(verticalBlueStretchBar);

            TextureElement LeftWhiteStretchBar = new TextureElement("uipackSpace_sheet", new Rectangle(402, 314, 6, 26), new Rectangle(0, 0, 6, 26),
                SpriteEffects.None, SingleFillElement.FillTypes.NoFillCenter, SingleFillElement.FillTypes.Stretch);
            TextureElement RightWhiteStretchBar = new TextureElement("uipackSpace_sheet", new Rectangle(402, 366, 6, 26), new Rectangle(7, 0, 6, 26),
                SpriteEffects.None, SingleFillElement.FillTypes.NoFillCenter, SingleFillElement.FillTypes.Stretch);
            TextureElement MiddleWhiteStretchBar = new TextureElement("uipackSpace_sheet", new Rectangle(399, 236, 16, 26), new Rectangle(6, 0, 1, 26),
                SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.Stretch);

            ContainerFillElement horizontalWhiteStretchBar = new ContainerFillElement();
            horizontalWhiteStretchBar.AddSkinElement(LeftWhiteStretchBar, new ScalingValues(0, 0, 0, 1f));
            horizontalWhiteStretchBar.AddSkinElement(MiddleWhiteStretchBar, new ScalingValues(0, 0, 1f, 1f));
            horizontalWhiteStretchBar.AddSkinElement(RightWhiteStretchBar, new ScalingValues(1, 0, 0, 1f));
            horizontalWhiteStretchBar.ElementName = "HorizontalWhiteStretchBar";
            horizontalWhiteStretchBar.DestinationBox = new Rectangle(0, 0, 13, 26);
            retVal.Add(horizontalWhiteStretchBar);

            TextureElement TopWhiteVStretchBar = new TextureElement("uipackSpace_sheet", new Rectangle(78, 500, 26, 6), new Rectangle(0, 0, 26, 6),
                SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.NoFillCenter);
            TextureElement BottomWhiteVStretchBar = new TextureElement("uipackSpace_sheet", new Rectangle(26, 506, 26, 6), new Rectangle(0, 7, 26, 6),
                SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.NoFillCenter);
            TextureElement MiddleWhiteVStretchBar = new TextureElement("uipackSpace_sheet", new Rectangle(336, 354, 26, 16), new Rectangle(0, 6, 26, 1),
                SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.Stretch);

            ContainerFillElement verticalWhiteStretchBar = new ContainerFillElement();
            verticalWhiteStretchBar.AddSkinElement(TopWhiteVStretchBar, new ScalingValues(0, 0, 1f, 0f));
            verticalWhiteStretchBar.AddSkinElement(MiddleWhiteVStretchBar, new ScalingValues(0, 0, 1f, 1f));
            verticalWhiteStretchBar.AddSkinElement(BottomWhiteVStretchBar, new ScalingValues(0, 1, 1f, 0f));
            verticalWhiteStretchBar.ElementName = "VerticalWhiteStretchBar";
            verticalWhiteStretchBar.DestinationBox = new Rectangle(0, 0, 26, 13);
            retVal.Add(verticalWhiteStretchBar);
        }

        private static void getIconElements(List<ISkinElement> retVal)
        {
            TextureElement checkIcon = new TextureElement("uipackSpace_sheet", new Rectangle(300, 400, 36, 36), new Rectangle(0, 0, 36, 36),
                SpriteEffects.None, SingleFillElement.FillTypes.Stretch);
            checkIcon.ElementName = "CheckIcon";
            retVal.Add(checkIcon);
        }

        private static List<GenericWidgetSkin> GetWidgetSkins()
        {
            List<GenericWidgetSkin> retVal = new List<GenericWidgetSkin>();
            getCanvasSkins(retVal);
            getScrollbarSkins(retVal);
            getSliderSkins(retVal);
            getButtonSkins(retVal);
            getIconSkins(retVal);
            getLabelSkins(retVal);
            getToggleButtonSkins(retVal);
            getCheckRadioSkins(retVal);
            getWindowSkins(retVal);
            getTabbedPanelSkins(retVal);
            return retVal;
        }

        private static void getCanvasSkins(List<GenericWidgetSkin> retVal)
        {
            GenericContainerSkin simpleContainer = new GenericContainerSkin(SkinReferencingConstants.WidgetTargetCanvas,
                SkinReferencingConstants.SkinVariant1);
            simpleContainer.BackgroundSkin = new SkinnedPropertyStateValue<ISkinElement>(new GlobalReferenceElement("SimpleMetal"));
            simpleContainer.SkinnedChildArea = new SkinnedPropertyStateValue<AreaDefinition>(
                new AreaDefinition(new RectangleFloat(10, 10, 1, 1), new ScalingValues(0, 0, 1, 1)));
            simpleContainer.MinimumDimensions = new AreaDefinition(21, 21);
            retVal.Add(simpleContainer);

            GenericContainerSkin simpleIndentedContainer = new GenericContainerSkin(SkinReferencingConstants.WidgetTargetCanvas,
                SkinReferencingConstants.SkinVariant2);
            simpleIndentedContainer.BackgroundSkin = new SkinnedPropertyStateValue<ISkinElement>(new GlobalReferenceElement("IndentedMetal"));
            simpleIndentedContainer.SkinnedChildArea = new SkinnedPropertyStateValue<AreaDefinition>(
                new AreaDefinition(new RectangleFloat(10, 10, 1, 1), new ScalingValues(0, 0, 1, 1)));
            simpleIndentedContainer.MinimumDimensions = new AreaDefinition(21, 21);
            retVal.Add(simpleIndentedContainer);

            GenericContainerSkin glassButtonContainer = new GenericContainerSkin(SkinReferencingConstants.WidgetTargetCanvas,
                SkinReferencingConstants.SkinVariant3);
            glassButtonContainer.BackgroundSkin = new SkinnedPropertyStateValue<ISkinElement>(new GlobalReferenceElement("GlassButton"));
            glassButtonContainer.SkinnedChildArea = new SkinnedPropertyStateValue<AreaDefinition>(
                new AreaDefinition(new RectangleFloat(10, 10, 1, 1), new ScalingValues(0, 0, 1, 1)));
            glassButtonContainer.MinimumDimensions = new AreaDefinition(21, 21);
            retVal.Add(glassButtonContainer);

            GenericContainerSkin glassSimpleContainer = new GenericContainerSkin(SkinReferencingConstants.WidgetTargetCanvas,
                SkinReferencingConstants.SkinVariant4);
            glassSimpleContainer.BackgroundSkin = new SkinnedPropertyStateValue<ISkinElement>(new GlobalReferenceElement("GlassSimple"));
            glassSimpleContainer.SkinnedChildArea = new SkinnedPropertyStateValue<AreaDefinition>(
                new AreaDefinition(new RectangleFloat(10, 10, 11, 11), new ScalingValues(0, 0, 1, 1)));
            glassSimpleContainer.MinimumDimensions = new AreaDefinition(31, 31);
            retVal.Add(glassSimpleContainer);

            GenericContainerSkin glassClippedContainerBR = new GenericContainerSkin(SkinReferencingConstants.WidgetTargetCanvas,
                SkinReferencingConstants.SkinVariant5);
            glassClippedContainerBR.BackgroundSkin = new SkinnedPropertyStateValue<ISkinElement>(new GlobalReferenceElement("GlassClippedBR"));
            glassClippedContainerBR.SkinnedChildArea = new SkinnedPropertyStateValue<AreaDefinition>(
                new AreaDefinition(new RectangleFloat(10, 10, 11, 11), new ScalingValues(0, 0, 1, 1)));
            glassClippedContainerBR.MinimumDimensions = new AreaDefinition(31, 31);
            retVal.Add(glassClippedContainerBR);

            GenericContainerSkin blueBarTopFull = new GenericContainerSkin(SkinReferencingConstants.WidgetTargetCanvas,
                SkinReferencingConstants.SkinVariant10);
            blueBarTopFull.BackgroundSkin = new SkinnedPropertyStateValue<ISkinElement>(new GlobalReferenceElement("BlueBarTopFull"));
            blueBarTopFull.SkinnedChildArea = new SkinnedPropertyStateValue<AreaDefinition>(
                new AreaDefinition(new RectangleFloat(3, 3, 15, 24), new ScalingValues(0, 0, 1, 1)));
            blueBarTopFull.MinimumDimensions = new AreaDefinition(21, 30);
            retVal.Add(blueBarTopFull);

            GenericContainerSkin blueBarTopSplit = new GenericContainerSkin(SkinReferencingConstants.WidgetTargetCanvas,
                SkinReferencingConstants.SkinVariant11);
            blueBarTopSplit.BackgroundSkin = new SkinnedPropertyStateValue<ISkinElement>(new GlobalReferenceElement("BlueBarTopSplit"));
            blueBarTopSplit.SkinnedChildArea = new SkinnedPropertyStateValue<AreaDefinition>(
                new AreaDefinition(new RectangleFloat(3, 3, 15, 25), new ScalingValues(0, 0, 1, 1)));
            blueBarTopSplit.MinimumDimensions = new AreaDefinition(22, 31);
            retVal.Add(blueBarTopSplit);

        }

        private static void getScrollbarSkins(List<GenericWidgetSkin> retVal)
        {
            ScrollBarSkin defaultScroll = new ScrollBarSkin(SkinReferencingConstants.WidgetTargetScrollBar,
                SkinReferencingConstants.SkinVariantDefault);

            //defaultScroll.
        }

        private static void getSliderSkins(List<GenericWidgetSkin> retVal)
        {
            SliderSkin slider = new SliderSkin(SkinReferencingConstants.WidgetTargetSlider,
                SkinReferencingConstants.SkinVariantDefault);
            slider.ProgressFillType = ProgressBar.FillType.StandardFill;
            slider.HorizontalOutlineVariant = "HorizontalWhiteStretchBar";
            slider.HorizontalOutlineArea = new SkinnedPropertyStateValue<AreaDefinition>(
                new AreaDefinition(13, 26));
            slider.HorizontalFillVariant = "HorizontalBlueStretchBar";
            slider.HorizontalFillArea = new SkinnedPropertyStateValue<AreaDefinition>(
                new AreaDefinition(13, 26));
            slider.MinimumDimensions = new AreaDefinition(26, 26);

            slider.VerticalOutlineVariant = "VerticalWhiteStretchBar";
            slider.VerticalOutlineArea = new SkinnedPropertyStateValue<AreaDefinition>(
                new AreaDefinition(26, 13));
            slider.VerticalFillVariant = "VerticalBlueStretchBar";
            slider.VerticalFillArea = new SkinnedPropertyStateValue<AreaDefinition>(
                new AreaDefinition(26, 13));

            slider.HorizontalSliderIconSkin = new SkinnedPropertyStateValue<ISkinElement>(
                new GlobalReferenceElement("CheckIcon"));
            slider.HorizontalSliderIconMinimumDimensions = new AreaDefinition(36, 36);
            slider.HorizontalSliderIconArea = new SkinnedPropertyStateValue<AreaDefinition>(
                new AreaDefinition(new RectangleFloat(-18, -5, 36, 36), new ScalingValues(0, 0.5f, 0, 0)));

            slider.VerticalSliderIconSkin = new SkinnedPropertyStateValue<ISkinElement>(
                new GlobalReferenceElement("CheckIcon"));
            slider.VerticalSliderIconMinimumDimensions = new AreaDefinition(36, 36);
            slider.VerticalSliderIconArea = new SkinnedPropertyStateValue<AreaDefinition>(
                new AreaDefinition(new RectangleFloat(-5, -18, 36, 36), new ScalingValues(0.5f, 0, 0, 0)));

            retVal.Add(slider);
        }

        private static void getButtonSkins(List<GenericWidgetSkin> retVal)
        {
            ButtonSkin standardMetalButton = new ButtonSkin(SkinReferencingConstants.WidgetTargetButton, SkinReferencingConstants.SkinVariant1);
            standardMetalButton.ButtonForeground = new SkinnedPropertyStateValue<ISkinElement>(new int[] { 0, 2 },
                new ISkinElement[]{
                    new GlobalReferenceElement("SimpleMetal"), new GlobalReferenceElement("IndentedMetal")
                });
            standardMetalButton.MinimumDimensions = new AreaDefinition(21, 21);
            standardMetalButton.SkinnedChildArea = new SkinnedPropertyStateValue<AreaDefinition>(
                new AreaDefinition(new RectangleFloat(10, 10, 1, 1), new ScalingValues(0, 0, 1, 1)));

            List<IModifier> hoverAnimation = new List<IModifier>();
            hoverAnimation.Add(new TweenModifier(
                null, new VectorTween(new Vector2(1, 1), new Vector2(1.02f, 1.02f), 0.1f, EasingFunction.Equations.Linear, EasingFunction.Equations.Linear, true),
               null, VectorAnchor.Center));

            List<IModifier> pressAnimation = new List<IModifier>();
            pressAnimation.Add(new TweenModifier(
                null, new VectorTween(new Vector2(1, 1), new Vector2(0.98f, 0.98f), 0.1f, EasingFunction.Equations.Linear, EasingFunction.Equations.Linear, true),
                new ColorTween(Color.White, new Color(150, 150, 150), 0.1f, EasingFunction.Equations.Linear, true), VectorAnchor.Center));
            standardMetalButton.StateTransitionAnimations = new SkinnedPropertyStateValue<List<IModifier>>(new int[] { 1, 2 },
                new List<IModifier>[] { hoverAnimation, pressAnimation });
            retVal.Add(standardMetalButton);


            ButtonSkin standardMetalIndenterButton = new ButtonSkin(SkinReferencingConstants.WidgetTargetButton, SkinReferencingConstants.SkinVariant2);
            standardMetalIndenterButton.ButtonForeground = new SkinnedPropertyStateValue<ISkinElement>(new GlobalReferenceElement("IndentedMetal"));
            standardMetalIndenterButton.MinimumDimensions = new AreaDefinition(21, 21);
            standardMetalIndenterButton.SkinnedChildArea = new SkinnedPropertyStateValue<AreaDefinition>(
                new AreaDefinition(new RectangleFloat(5, 5, 11, 11), new ScalingValues(0, 0, 1, 1)));
            standardMetalIndenterButton.LabelSkinVariant = SkinReferencingConstants.SkinVariantDefault;

            List<IModifier> hoverIndentedAnimation = new List<IModifier>();
            hoverIndentedAnimation.Add(new TweenModifier(
                null, new VectorTween(new Vector2(1, 1), new Vector2(1.02f, 1.02f), 0.1f, EasingFunction.Equations.Linear, EasingFunction.Equations.Linear, true),
                null, VectorAnchor.Center));

            List<IModifier> pressIndentedAnimation = new List<IModifier>();
            pressIndentedAnimation.Add(new TweenModifier(
                null, new VectorTween(new Vector2(1, 1), new Vector2(0.98f, 0.98f), 0.1f, EasingFunction.Equations.Linear, EasingFunction.Equations.Linear, true),
                new ColorTween(Color.White, new Color(150, 150, 150), 0.1f, EasingFunction.Equations.Linear, true), VectorAnchor.Center));
            //standardMetalIndenterButton.StateTransitionAnimations = new SkinnedPropertyStateValue<List<IModifier>>(new int[] { 1, 2 },
            //    new List<IModifier>[] { hoverIndentedAnimation, pressIndentedAnimation });
            retVal.Add(standardMetalIndenterButton);




            ButtonSkin glassButton = new ButtonSkin(SkinReferencingConstants.WidgetTargetButton, SkinReferencingConstants.SkinVariantDefault);
            glassButton.ButtonForeground = new SkinnedPropertyStateValue<ISkinElement>(new int[] { 0, 2 }, new ISkinElement[]{
                new GlobalReferenceElement("GlassButton"), new GlobalReferenceElement("GlassButton")});
            glassButton.MinimumDimensions = new AreaDefinition(21, 21);
            glassButton.SkinnedChildArea = new SkinnedPropertyStateValue<AreaDefinition>(
                new AreaDefinition(new RectangleFloat(5, 5, 11, 11), new ScalingValues(0, 0, 1, 1)));
            glassButton.LabelSkinVariant = SkinReferencingConstants.SkinVariantDefault;

            List<IModifier> hoverAnimationGlass = new List<IModifier>();
            hoverAnimationGlass.Add(new TweenModifier(
                null, new VectorTween(new Vector2(1, 1), new Vector2(1.02f, 1.02f), 0.1f, EasingFunction.Equations.Linear, EasingFunction.Equations.Linear, true),
                null, VectorAnchor.Center));

            List<IModifier> pressAnimationGlass = new List<IModifier>();
            pressAnimationGlass.Add(new TweenModifier(
                null, new VectorTween(new Vector2(1, 1), new Vector2(0.98f, 0.98f), 0.1f, EasingFunction.Equations.Linear, EasingFunction.Equations.Linear, true),
                null, VectorAnchor.Center));
            glassButton.StateTransitionAnimations = new SkinnedPropertyStateValue<List<IModifier>>(new int[] { 1, 2 },
                new List<IModifier>[] { hoverAnimationGlass, pressAnimationGlass });

            retVal.Add(glassButton);

            ButtonSkin greyGlassButton = new ButtonSkin(SkinReferencingConstants.WidgetTargetButton, SkinReferencingConstants.SkinVariant3);
            greyGlassButton.ButtonForeground = new SkinnedPropertyStateValue<ISkinElement>(
                new GlobalReferenceElement("GlassButtonPressed"));
            greyGlassButton.MinimumDimensions = new AreaDefinition(21, 21);
            greyGlassButton.SkinnedChildArea = new SkinnedPropertyStateValue<AreaDefinition>(
                new AreaDefinition(new RectangleFloat(5, 5, 11, 11), new ScalingValues(0, 0, 1, 1)));
            greyGlassButton.LabelSkinVariant = SkinReferencingConstants.SkinVariantDefault;

            greyGlassButton.StateTransitionAnimations = new SkinnedPropertyStateValue<List<IModifier>>(new int[] { 1, 2 },
                new List<IModifier>[] { hoverAnimationGlass, pressAnimationGlass });

            retVal.Add(greyGlassButton);


            ButtonSkin blueHorizontalStretchButton = new ButtonSkin(SkinReferencingConstants.WidgetTargetButton, SkinReferencingConstants.SkinVariant10);
            blueHorizontalStretchButton.ButtonForeground = new SkinnedPropertyStateValue<ISkinElement>(
                new GlobalReferenceElement("HorizontalBlueStretchBar"));
            blueHorizontalStretchButton.MinimumDimensions = new AreaDefinition(13, 26);
            blueHorizontalStretchButton.SkinnedChildArea = new SkinnedPropertyStateValue<AreaDefinition>(
                new AreaDefinition(new RectangleFloat(2, 4, 9, 18), new ScalingValues(0, 0, 1, 1)));
            blueHorizontalStretchButton.LabelSkinVariant = SkinReferencingConstants.SkinVariantDefault;
            retVal.Add(blueHorizontalStretchButton);


            ButtonSkin whiteHorizontalStretchButton = new ButtonSkin(SkinReferencingConstants.WidgetTargetButton, SkinReferencingConstants.SkinVariant11);
            whiteHorizontalStretchButton.ButtonForeground = new SkinnedPropertyStateValue<ISkinElement>(
                new GlobalReferenceElement("HorizontalWhiteStretchBar"));
            whiteHorizontalStretchButton.MinimumDimensions = new AreaDefinition(13, 26);
            whiteHorizontalStretchButton.SkinnedChildArea = new SkinnedPropertyStateValue<AreaDefinition>(
                new AreaDefinition(new RectangleFloat(2, 4, 9, 18), new ScalingValues(0, 0, 1, 1)));
            whiteHorizontalStretchButton.LabelSkinVariant = SkinReferencingConstants.SkinVariantDefault;
            retVal.Add(whiteHorizontalStretchButton);
        }

        private static void getIconSkins(List<GenericWidgetSkin> retVal)
        {
            IconSkin checkIcon = new IconSkin(SkinReferencingConstants.WidgetTargetIcon, "CheckIcon");
            checkIcon.IconImage = new SkinnedPropertyStateValue<ISkinElement>(
                new GlobalReferenceElement("CheckIcon"));
            checkIcon.MinimumDimensions = new AreaDefinition(36, 36);

            IconSkin horizontalBlue = new IconSkin(SkinReferencingConstants.WidgetTargetIcon, 
                "HorizontalBlueStretchBar");
            horizontalBlue.IconImage = new SkinnedPropertyStateValue<ISkinElement>(
                new GlobalReferenceElement("HorizontalBlueStretchBar"));
            horizontalBlue.MinimumDimensions = new AreaDefinition(13, 26);

            IconSkin horizontalWhite = new IconSkin(SkinReferencingConstants.WidgetTargetIcon,
                "HorizontalWhiteStretchBar");
            horizontalWhite.IconImage = new SkinnedPropertyStateValue<ISkinElement>(
                new GlobalReferenceElement("HorizontalWhiteStretchBar"));
            horizontalWhite.MinimumDimensions = new AreaDefinition(13, 26);

            IconSkin verticalBlue = new IconSkin(SkinReferencingConstants.WidgetTargetIcon,
                "VerticalBlueStretchBar");
            verticalBlue.IconImage = new SkinnedPropertyStateValue<ISkinElement>(
                new GlobalReferenceElement("VerticalBlueStretchBar"));
            verticalBlue.MinimumDimensions = new AreaDefinition(26, 13);

            IconSkin verticalWhite = new IconSkin(SkinReferencingConstants.WidgetTargetIcon,
                "VerticalWhiteStretchBar");
            verticalWhite.IconImage = new SkinnedPropertyStateValue<ISkinElement>(
                new GlobalReferenceElement("VerticalWhiteStretchBar"));
            verticalWhite.MinimumDimensions = new AreaDefinition(13, 26);

            retVal.Add(checkIcon);
            retVal.Add(horizontalBlue);
            retVal.Add(horizontalWhite);
            retVal.Add(verticalBlue);
            retVal.Add(verticalWhite);
        }

        private static void getLabelSkins(List<GenericWidgetSkin> retVal)
        {
            LabelSkin defaultLabelSkin = new LabelSkin(SkinReferencingConstants.WidgetTargetLabel, SkinReferencingConstants.SkinVariantDefault);
            defaultLabelSkin.FontName = "KenneyRegular";
            defaultLabelSkin.FontRoundingMethod = FontRoundMethod.Closest;
            defaultLabelSkin.FontSize = 14;
            defaultLabelSkin.FontMethod = FontFillMethod.Fill;
            defaultLabelSkin.TextVerticalJustification = AlignV.Center;

            LabelSkin variantLabelSkin = new LabelSkin(SkinReferencingConstants.WidgetTargetLabel, 
                SkinReferencingConstants.SkinVariant1);
            variantLabelSkin.FontName = "KenneyRegular";
            variantLabelSkin.FontRoundingMethod = FontRoundMethod.Closest;
            variantLabelSkin.FontSize = 14;
            variantLabelSkin.FontMethod = FontFillMethod.Fill;
            variantLabelSkin.TextVerticalJustification = AlignV.Center;
            variantLabelSkin.BackgroundSkin = new SkinnedPropertyStateValue<ISkinElement>(
                new GlobalReferenceElement("IndentedMetal")
                );
            //defaultLabelSkin.BackgroundSkin = new SkinnedPropertyStateValue<ISkinElement>(new GlobalReferenceElement("GreyCircleCenter"));

            TextBlockSkin defaultTextBlockSkin = new TextBlockSkin(SkinReferencingConstants.WidgetTargetTextBlock, SkinReferencingConstants.SkinVariantDefault);
            defaultTextBlockSkin.FontName = "KenneyRegular";
            defaultTextBlockSkin.FontRoundingMethod = FontRoundMethod.Closest;
            defaultTextBlockSkin.FontSize = 14;
            defaultTextBlockSkin.FontMethod = FontFillMethod.Fill;
            defaultTextBlockSkin.BackgroundSkin = new SkinnedPropertyStateValue<ISkinElement>(
                new GlobalReferenceElement("IndentedMetal")
                );

            retVal.Add(defaultLabelSkin);
            retVal.Add(variantLabelSkin);
            retVal.Add(defaultTextBlockSkin);
        }

        private static void getToggleButtonSkins(List<GenericWidgetSkin> retVal)
        {
            ToggleButtonSkin checkToggleSkin = new ToggleButtonSkin(SkinReferencingConstants.WidgetTargetToggleButton,
                "CheckboxToggle");
            checkToggleSkin.MinimumDimensions = new AreaDefinition(42, 42);
            checkToggleSkin.ButtonForegroundClicked = new SkinnedPropertyStateValue<ISkinElement>(new GlobalReferenceElement("GlassButton"));
            checkToggleSkin.ButtonForegroundNotClicked = checkToggleSkin.ButtonForegroundClicked;
            checkToggleSkin.ButtonToggledOnIconVariant = "CheckIcon";
            checkToggleSkin.ButtonToggledOnIconArea = new SkinnedPropertyStateValue<AreaDefinition>(new AreaDefinition(
                new RectangleFloat(3, 3, 36, 36), new ScalingValues(0, 0, 1, 1), true
                ));
            checkToggleSkin.LabelSkinVariant = SkinReferencingConstants.SkinVariantDefault;
            retVal.Add(checkToggleSkin);
        }

        private static void getCheckRadioSkins(List<GenericWidgetSkin> retVal)
        {
            CheckboxSkin checkSkin = new CheckboxSkin(SkinReferencingConstants.WidgetTargetCheckbox, SkinReferencingConstants.SkinVariantDefault);
            checkSkin.ToggleButtonArea = new SkinnedPropertyStateValue<AreaDefinition>(new AreaDefinition(
                new RectangleFloat(3, 3, 36, 36), new ScalingValues(0, 0.5f, 0, 0), true));
            checkSkin.ToggleButtonSkinVariant = "CheckboxToggle";
            checkSkin.LabelSkinVariant = SkinReferencingConstants.SkinVariantDefault;
            checkSkin.TextArea = new SkinnedPropertyStateValue<AreaDefinition>(new AreaDefinition(
                new RectangleFloat(42, 3, 36, 36), new ScalingValues(0, 0.5f, 0.5f, 1), true));
            checkSkin.MinimumDimensions = new AreaDefinition(81, 42);
            checkSkin.BackgroundSkin = new SkinnedPropertyStateValue<ISkinElement>(new GlobalReferenceElement("GlassButton"));
            retVal.Add(checkSkin);

            CheckboxSkin radioSkin = new CheckboxSkin(SkinReferencingConstants.WidgetTargetRadioButton, SkinReferencingConstants.SkinVariantDefault);
            radioSkin.ToggleButtonArea = new SkinnedPropertyStateValue<AreaDefinition>(new AreaDefinition(
                new RectangleFloat(3, 3, 36, 36), new ScalingValues(0, 0.5f, 0, 0), true));
            radioSkin.ToggleButtonSkinVariant = "CheckboxToggle";
            radioSkin.LabelSkinVariant = SkinReferencingConstants.SkinVariantDefault;
            radioSkin.TextArea = new SkinnedPropertyStateValue<AreaDefinition>(new AreaDefinition(
                new RectangleFloat(42, 3, 36, 36), new ScalingValues(0, 0.5f, 0.5f, 1), true));
            radioSkin.MinimumDimensions = new AreaDefinition(81, 42);
            radioSkin.BackgroundSkin = new SkinnedPropertyStateValue<ISkinElement>(new GlobalReferenceElement("GlassButton"));
            retVal.Add(radioSkin);
        }

        private static void getWindowSkins(List<GenericWidgetSkin> retVal)
        {
            GUIWindowSkin windowSkin = new GUIWindowSkin(SkinReferencingConstants.WidgetTargetGUIWindow, SkinReferencingConstants.SkinVariantDefault);
            windowSkin.CloseButtonSkinVariant = "UNUSED";
            windowSkin.TitleLabelSkinVariant = SkinReferencingConstants.SkinVariantDefault;
            windowSkin.TitleBarCanvasSkinVariant = SkinReferencingConstants.SkinVariant10;
            windowSkin.TitleBarCanvasArea = new SkinnedPropertyStateValue<AreaDefinition>(new AreaDefinition(
                new RectangleFloat(0, 0, 21, 30), new ScalingValues(0, 0, 1, 0)));
            windowSkin.TitleLabelArea = new SkinnedPropertyStateValue<AreaDefinition>(new AreaDefinition(
                new RectangleFloat(5, 5, 11, 20), new ScalingValues(0, 0, 1, 0)));
            windowSkin.CloseButtonArea = new SkinnedPropertyStateValue<AreaDefinition>(new AreaDefinition(
                new RectangleFloat(0, 0, 1, 1), new ScalingValues(1f, 0, 0, 0)));
            windowSkin.MinimumDimensions = new AreaDefinition(21, 41);
            windowSkin.ChildrenArea = new SkinnedPropertyStateValue<AreaDefinition>(
                new AreaDefinition(new RectangleFloat(3, 33, 15, 5), new ScalingValues(Vector2.Zero, new Vector2(1, 1))));
            windowSkin.BackgroundSkin = new SkinnedPropertyStateValue<ISkinElement>(new GlobalReferenceElement("SimpleMetal"));
            retVal.Add(windowSkin);

            GUIWindowSkin splitBlueWindow = new GUIWindowSkin(SkinReferencingConstants.WidgetTargetGUIWindow, SkinReferencingConstants.SkinVariant1);
            splitBlueWindow.CloseButtonSkinVariant = "UNUSED";
            splitBlueWindow.TitleLabelSkinVariant = SkinReferencingConstants.SkinVariantDefault;
            splitBlueWindow.TitleBarCanvasSkinVariant = SkinReferencingConstants.SkinVariant11;
            splitBlueWindow.TitleBarCanvasArea = new SkinnedPropertyStateValue<AreaDefinition>(new AreaDefinition(
                new RectangleFloat(0, 0, 22, 31), new ScalingValues(0, 0, 0.5f, 0)));
            splitBlueWindow.TitleLabelArea = new SkinnedPropertyStateValue<AreaDefinition>(new AreaDefinition(
                new RectangleFloat(5, 5, 11, 20), new ScalingValues(0, 0, 1, 0)));
            splitBlueWindow.CloseButtonArea = new SkinnedPropertyStateValue<AreaDefinition>(new AreaDefinition(
                new RectangleFloat(0, 0, 1, 1), new ScalingValues(1f, 0, 0, 0)));
            splitBlueWindow.MinimumDimensions = new AreaDefinition(21, 41);
            splitBlueWindow.ChildrenArea = new SkinnedPropertyStateValue<AreaDefinition>(
                new AreaDefinition(new RectangleFloat(3, 33, 15, 5), new ScalingValues(Vector2.Zero, new Vector2(1, 1))));
            splitBlueWindow.BackgroundSkin = new SkinnedPropertyStateValue<ISkinElement>(new GlobalReferenceElement("SimpleMetal"));
            retVal.Add(splitBlueWindow);
        }

        private static void getTabbedPanelSkins(List<GenericWidgetSkin> retVal)
        {
            TabbedPanelSkin basicTabbedPanel = new TabbedPanelSkin(
                SkinReferencingConstants.WidgetTargetTabbedPanel, SkinReferencingConstants.SkinVariantDefault);
            basicTabbedPanel.MinimumDimensions = new AreaDefinition(17, 52);
            basicTabbedPanel.TabSpacingAmount = -15;
            basicTabbedPanel.TabSelectedButtonVariant = SkinReferencingConstants.SkinVariant10;
            basicTabbedPanel.TabUnselectedButtonVariant = SkinReferencingConstants.SkinVariant11;
            basicTabbedPanel.ChildCanvasVariant = SkinReferencingConstants.SkinVariant1;
            basicTabbedPanel.TabFrameArea = new SkinnedPropertyStateValue<AreaDefinition>(
                new AreaDefinition(new RectangleFloat(0, 0, 17, 26), new ScalingValues(0, 0, 1f, 0.1f)));
            basicTabbedPanel.ChildPanelFrameArea = new SkinnedPropertyStateValue<AreaDefinition>(
                new AreaDefinition(new RectangleFloat(0, 21, 17, 31), new ScalingValues(0, 0.1f, 1, 0.9f)));
            retVal.Add(basicTabbedPanel);
        }

        private static void getFonts(FontManager fonts)
        {
            fonts.ThemeFontDescriptors.Add(new MonoGUI.Skinning.Fonts.FontDescriptor("KenneyRegular", "Fonts\\opensans_40", 40));
            fonts.ThemeFontDescriptors.Add(new MonoGUI.Skinning.Fonts.FontDescriptor("KenneyRegular", "Fonts\\opensans_36", 36));
            fonts.ThemeFontDescriptors.Add(new MonoGUI.Skinning.Fonts.FontDescriptor("KenneyRegular", "Fonts\\opensans_32", 32));
            fonts.ThemeFontDescriptors.Add(new MonoGUI.Skinning.Fonts.FontDescriptor("KenneyRegular", "Fonts\\opensans_28", 28));
            fonts.ThemeFontDescriptors.Add(new MonoGUI.Skinning.Fonts.FontDescriptor("KenneyRegular", "Fonts\\opensans_24", 24));
            fonts.ThemeFontDescriptors.Add(new MonoGUI.Skinning.Fonts.FontDescriptor("KenneyRegular", "Fonts\\opensans_20", 20));
            fonts.ThemeFontDescriptors.Add(new MonoGUI.Skinning.Fonts.FontDescriptor("KenneyRegular", "Fonts\\opensans_18", 18));
            fonts.ThemeFontDescriptors.Add(new MonoGUI.Skinning.Fonts.FontDescriptor("KenneyRegular", "Fonts\\opensans_16", 16));
            fonts.ThemeFontDescriptors.Add(new MonoGUI.Skinning.Fonts.FontDescriptor("KenneyRegular", "Fonts\\opensans_14", 14));
            fonts.ThemeFontDescriptors.Add(new MonoGUI.Skinning.Fonts.FontDescriptor("KenneyRegular", "Fonts\\opensans_13", 13));
            fonts.ThemeFontDescriptors.Add(new MonoGUI.Skinning.Fonts.FontDescriptor("KenneyRegular", "Fonts\\opensans_12", 12));
            fonts.ThemeFontDescriptors.Add(new MonoGUI.Skinning.Fonts.FontDescriptor("KenneyRegular", "Fonts\\opensans_11", 11));
            fonts.ThemeFontDescriptors.Add(new MonoGUI.Skinning.Fonts.FontDescriptor("KenneyRegular", "Fonts\\opensans_10", 10));
            fonts.ThemeFontDescriptors.Add(new MonoGUI.Skinning.Fonts.FontDescriptor("KenneyRegular", "Fonts\\opensans_8", 8));
            fonts.DefaultFont = new MonoGUI.Skinning.Fonts.FontDescriptor("KenneyRegular", "Fonts\\opensans_14", 14);
        }
    }
}
