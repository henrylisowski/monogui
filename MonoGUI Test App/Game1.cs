﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGUI.Manager;
using MonoGUI.Skinning.SkinElements;
using MonoGUI.Skinning;
using MonoGUI.ScreenItems.Containers.CanvasContainer;
using MonoGUI.Widgets;
using MonoGUI.ScreenItems.Containers.StackContainer;
using MonoGUI.ScreenItems.Containers;
using MonoGUI_Test_App.TestScreens;
using MonoGUI.MSpriteBatch;
#endregion

namespace MonoGUI_Test_App
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        ScreenManager manager;

        private DisplayPanelContainer testScreen;
        public Game1()
            : base()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            this.IsMouseVisible = true;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            manager = new ScreenManager(this, graphics);
            manager.Initialize();
            manager.LoadContent(Content);
            DrawBeginInfo beg = manager.ModifiableInitialDrawCall();
            beg.SamplerState = SamplerState.PointClamp;
            manager.SetInitialDrawCall(beg);

            MonoGUISkin rpgSkin = PixelRPGSkinConstructor.GetPixelThemeSkin();
            //rpgSkin.Scale(2);
            MonoGUISkin spaceSkin = SpaceSkinConstructor.GetPixelThemeSkin();
            SkinManager.Instance.InitializeSkin(rpgSkin, Content, "RPGSkin");
            SkinManager.Instance.InitializeSkin(spaceSkin, Content, "SpaceSkin");
            SkinManager.Instance.SetActiveSkin("SpaceSkin");
            

            MonoGUIGameWindow wind = manager.ActivateNewGameWindow();//new StackPanelSpacingTestScreen(manager));
            TopBar bar = new TopBar(manager);
            bar.Width = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            bar.Height = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            bar.PreferredDimensions = new Vector2(1f, 0.15f);
            bar.OnGoLeft += bar_OnGoLeft;
            bar.OnGoRight += bar_OnGoRight;
            bar.OnChangeToRPGSkin += bar_OnChangeToRPGSkin;
            bar.OnChangeToSpaceSkin += bar_OnChangeToSpaceSkin;
            wind.AddChild(bar);

            testScreen = new DisplayPanelContainer(manager);
            testScreen.Width = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            testScreen.Height = MonoGUI.ScreenItems.ItemSizeOptions.Percentage;
            testScreen.PreferredDimensions = new Vector2(1f, 0.85f);
            testScreen.VerticalPositionType = MonoGUI.ScreenItems.ItemPositionOptions.Percentage;
            testScreen.PreferredPosition = new Vector2(0f, 0.15f);
            wind.AddChild(testScreen);
            manager.ApplySkin();
            manager.RequestResize();
            // TODO: use this.Content to load your game content here
        }

        void bar_OnChangeToSpaceSkin()
        {
            SkinManager.Instance.SetActiveSkin("SpaceSkin");
            manager.ApplySkin();
        }

        void bar_OnChangeToRPGSkin()
        {
            SkinManager.Instance.SetActiveSkin("RPGSkin");
            manager.ApplySkin();
        }

        void bar_OnGoRight()
        {
            testScreen.MoveLeft();
        }

        void bar_OnGoLeft()
        {
            testScreen.MoveRight();
        }

        

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here
            manager.Update(gameTime);
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            manager.Draw(gameTime);
            base.Draw(gameTime);
        }
    }
}
