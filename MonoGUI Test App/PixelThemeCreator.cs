﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Intermediate;
using Microsoft.Xna.Framework.Graphics;
using MonoGUI.Animations;
using MonoGUI.Animations.Tweens;
using MonoGUI.Skinning;
using MonoGUI.Skinning.Fonts;
using MonoGUI.Skinning.SkinElements;
using MonoGUI.Skinning.WidgetSkins;
using MonoGUI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace MonoGUI_Test_App
{
    public static class PixelThemeCreator
    {
        public static bool AddAnimations { get; set; }
        public static MonoGUISkin GetPixelThemeSkin()
        {

            MonoGUISkin retVal = new MonoGUISkin(GetWidgetSkins());
            retVal.GlobalSkinElements = GetSkinElements();
            retVal.FontManager = new FontManager();
            getFonts(retVal.FontManager);

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            using (XmlWriter writer = XmlWriter.Create("defaultTheme.xml", settings))
            {
                IntermediateSerializer.Serialize(writer, retVal, null);
            }

            return retVal;
        }

        public static MonoGUISkin GetPixelThemeSkin(Point offset)
        {
            MonoGUISkin retVal = GetPixelThemeSkin();
            foreach (ISkinElement e in retVal.GlobalSkinElements)
            {
                updateSourceRect(offset, e);
            }
            return retVal;
        }

        private static void updateSourceRect(Point offset, ISkinElement element)
        {
            if (element.GetType() == typeof(ContainerFillElement) || element.GetType().IsSubclassOf(typeof(ContainerFillElement)))
            {
                ContainerFillElement cont = (ContainerFillElement)element;
                if (cont.ElementName != null && cont.ElementName.StartsWith("YellowStretch"))
                {
                    offset = new Point(offset.X / 2, offset.Y);
                }
                foreach (ISkinElement e in cont.SkinElements)
                {
                    updateSourceRect(offset, e);
                }
            }
            if (element.GetType() == typeof(TextureElement))
            {
                TextureElement e = (TextureElement)element;
                List<Rectangle> rects = e.SourceBoxes;
                List<Rectangle> newList = new List<Rectangle>();
                for (int i = 0; i < rects.Count; i++)
                {
                    Rectangle cur = rects[i];
                    if (e.SourceTextureName == "UIpackSheet_transparent")
                    {
                        if (e.ElementName != null && e.ElementName.Contains("ButtonYellow"))
                        {
                            int index = (offset.X + 108) / 108;
                            Point arrowOffset = new Point((index == 2 || index == 4) ? 108 : 0, (index > 2) ? 36 : 0);
                            newList.Add(new Rectangle(cur.X + arrowOffset.X, cur.Y + arrowOffset.Y, cur.Width, cur.Height));
                        }
                        else
                        {
                            newList.Add(new Rectangle(cur.X + offset.X, cur.Y + offset.Y, cur.Width, cur.Height));
                        }

                    }
                    else
                    {
                        if (e.ElementName != null && e.ElementName.Contains("Grey"))
                        {
                            newList.Add(new Rectangle(cur.X, cur.Y, cur.Width, cur.Height));
                        }
                        else
                        {
                            newList.Add(new Rectangle(cur.X + ((offset.X / 108) * 8), cur.Y, cur.Width, cur.Height));
                        }

                    }
                    
                }
                e.SourceBoxes = newList;
            }
        }

        private static void getFonts(FontManager fonts)
        {
            fonts.ThemeFontDescriptors.Add(new MonoGUI.Skinning.Fonts.FontDescriptor("KenneyRegular", "kenpixel_regular_14", 14));
            fonts.DefaultFont = new MonoGUI.Skinning.Fonts.FontDescriptor("KenneyRegular", "kenpixel_regular_14", 14);
        }

        private static List<ISkinElement> GetSkinElements()
        {
            List<ISkinElement> retVal = new List<ISkinElement>();
            getPanelElements(retVal);
            getButtonElements(retVal);
            getIconElements(retVal);
            return retVal;
        }

        private static void getPanelElements(List<ISkinElement> retVal)
        {
            TextureElement topLeftAYellow = new TextureElement("UIpackSheet_transparent", new Rectangle(108, 36, 16, 16), 
                new Rectangle(0, 0, 16, 16), SpriteEffects.None);
            TextureElement topRightAYellow = new TextureElement("UIpackSheet_transparent", new Rectangle(144, 36, 16, 16),
                new Rectangle(17, 0, 16, 16), SpriteEffects.None);
            TextureElement bottomLeftAYellow = new TextureElement("UIpackSheet_transparent", new Rectangle(162, 72, 16, 16),
                new Rectangle(0, 17, 16, 16), SpriteEffects.None);
            TextureElement bottomRightAYellow = new TextureElement("UIpackSheet_transparent", new Rectangle(198, 72, 16, 16),
                new Rectangle(17, 17, 16, 16), SpriteEffects.None);
            TextureElement topCenterAYellow = new TextureElement("UIpackSheet_transparent", new Rectangle(126, 36, 16, 16), 
                new Rectangle(16, 0, 1, 16), SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.NoFillCenter);
            TextureElement bottomCenterAYellow = new TextureElement("UIpackSheet_transparent", new Rectangle(180, 72, 16, 16),
                new Rectangle(16, 17, 1, 16), SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.NoFillCenter);
            TextureElement leftCenterAYellow = new TextureElement("UIpackSheet_transparent", new Rectangle(108, 54, 16, 16),
                new Rectangle(0, 16, 16, 1), SpriteEffects.None, SingleFillElement.FillTypes.NoFillCenter, SingleFillElement.FillTypes.Stretch);
            TextureElement rightCenterAYellow = new TextureElement("UIpackSheet_transparent", new Rectangle(144, 54, 16, 16),
                new Rectangle(17, 16, 16, 1), SpriteEffects.None, SingleFillElement.FillTypes.NoFillCenter, SingleFillElement.FillTypes.Stretch);
            TextureElement centerAYellow = new TextureElement("UIpackSheet_transparent", new Rectangle(126, 54, 16, 16),
                new Rectangle(16, 16, 1, 1), SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.Stretch);
            NineSpliceElement aYellowElementPressed = new NineSpliceElement(topLeftAYellow.Clone(), topRightAYellow.Clone(),
                bottomLeftAYellow.Clone(), bottomRightAYellow.Clone(),
                topCenterAYellow.Clone(), rightCenterAYellow.Clone(), bottomCenterAYellow.Clone(), leftCenterAYellow.Clone(), centerAYellow.Clone(), 
                new Rectangle(0, 0, 33, 33));
            aYellowElementPressed.ElementName = "PanelYellowAPressed";

            TextureElement bottomLeftAYellowReleased = new TextureElement("UIpackSheet_transparent", new Rectangle(108, 72, 16, 16),
                new Rectangle(0, 17, 16, 16), SpriteEffects.None);
            TextureElement bottomRightAYellowReleased = new TextureElement("UIpackSheet_transparent", new Rectangle(144, 72, 16, 16),
                new Rectangle(17, 17, 16, 16), SpriteEffects.None);
            TextureElement bottomCenterAYellowReleased = new TextureElement("UIpackSheet_transparent", new Rectangle(126, 72, 16, 16),
                new Rectangle(16, 17, 1, 16), SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.NoFillCenter);

            TextureElement topLeftAYellowOffset = new TextureElement("UIpackSheet_transparent", new Rectangle(108, 36, 16, 16),
                new Rectangle(0, 2, 16, 14), SpriteEffects.None);
            TextureElement topRightAYellowOffset = new TextureElement("UIpackSheet_transparent", new Rectangle(144, 36, 16, 16),
                new Rectangle(17, 2, 16, 14), SpriteEffects.None);
            TextureElement topCenterAYellowOffset = new TextureElement("UIpackSheet_transparent", new Rectangle(126, 36, 16, 16),
                new Rectangle(16, 2, 1, 14), SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.NoFillCenter);

            NineSpliceElement aYellowElementPressedOffset = new NineSpliceElement(topLeftAYellowOffset.Clone(), topRightAYellowOffset.Clone(),
                bottomLeftAYellow.Clone(), bottomRightAYellow.Clone(),
                topCenterAYellowOffset.Clone(), rightCenterAYellow.Clone(), bottomCenterAYellow.Clone(), leftCenterAYellow.Clone(),
                centerAYellow.Clone(), new Rectangle(0, 0, 33, 33));
            aYellowElementPressedOffset.ElementName = "PanelYellowAPressedOffset";

            NineSpliceElement aYellowReleasedElement = new NineSpliceElement(
                topLeftAYellow.Clone(), topRightAYellow.Clone(), bottomLeftAYellowReleased.Clone(), bottomRightAYellowReleased.Clone(),
                topCenterAYellow.Clone(), rightCenterAYellow.Clone(), bottomCenterAYellowReleased.Clone(), leftCenterAYellow.Clone(),
                centerAYellow.Clone(), new Rectangle(0, 0, 33, 33)
                );
            aYellowReleasedElement.ElementName = "PanelYellowAReleased";

            retVal.Add(aYellowElementPressed);
            retVal.Add(aYellowReleasedElement);
            retVal.Add(aYellowElementPressedOffset);

            TextureElement topLeftBYellow = new TextureElement("UIpackSheet_transparent", new Rectangle(108, 144, 16, 16),
                new Rectangle(0, 0, 16, 16), SpriteEffects.None);
            TextureElement topRightBYellow = new TextureElement("UIpackSheet_transparent", new Rectangle(144, 144, 16, 16),
                new Rectangle(17, 0, 16, 16), SpriteEffects.None);
            TextureElement bottomLeftBYellow = new TextureElement("UIpackSheet_transparent", new Rectangle(162, 180, 16, 16),
                new Rectangle(0, 17, 16, 16), SpriteEffects.None);
            TextureElement bottomRightBYellow = new TextureElement("UIpackSheet_transparent", new Rectangle(198, 180, 16, 16),
                new Rectangle(17, 17, 16, 16), SpriteEffects.None);
            TextureElement topCenterBYellow = new TextureElement("UIpackSheet_transparent", new Rectangle(126, 144, 16, 16),
                new Rectangle(16, 0, 1, 16), SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.NoFillCenter);
            TextureElement bottomCenterBYellow = new TextureElement("UIpackSheet_transparent", new Rectangle(180, 180, 16, 16),
                new Rectangle(16, 17, 1, 16), SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.NoFillCenter);
            TextureElement leftCenterBYellow = new TextureElement("UIpackSheet_transparent", new Rectangle(108, 162, 16, 16),
                new Rectangle(0, 16, 16, 1), SpriteEffects.None, SingleFillElement.FillTypes.NoFillCenter, SingleFillElement.FillTypes.Stretch);
            TextureElement rightCenterBYellow = new TextureElement("UIpackSheet_transparent", new Rectangle(144, 162, 16, 16),
                new Rectangle(17, 16, 16, 1), SpriteEffects.None, SingleFillElement.FillTypes.NoFillCenter, SingleFillElement.FillTypes.Stretch);
            TextureElement centerBYellow = new TextureElement("UIpackSheet_transparent", new Rectangle(126, 162, 16, 16),
                new Rectangle(16, 16, 1, 1), SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.Stretch);
            NineSpliceElement bYellowElementPressed = new NineSpliceElement(topLeftBYellow, topRightBYellow, bottomLeftBYellow, bottomRightBYellow,
                topCenterBYellow, rightCenterBYellow, bottomCenterBYellow, leftCenterBYellow, centerBYellow, new Rectangle(0, 0, 33, 33));
            bYellowElementPressed.ElementName = "PanelYellowBPressed";

            TextureElement bottomLeftBYellowReleased = new TextureElement("UIpackSheet_transparent", new Rectangle(108, 180, 16, 16),
                new Rectangle(0, 17, 16, 16), SpriteEffects.None);
            TextureElement bottomRightBYellowReleased = new TextureElement("UIpackSheet_transparent", new Rectangle(144, 180, 16, 16),
                new Rectangle(17, 17, 16, 16), SpriteEffects.None);
            TextureElement bottomCenterBYellowReleased = new TextureElement("UIpackSheet_transparent", new Rectangle(126, 180, 16, 16),
                new Rectangle(16, 17, 1, 16), SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.NoFillCenter);

            TextureElement topLeftBYellowOffset = new TextureElement("UIpackSheet_transparent", new Rectangle(108, 144, 16, 16),
                new Rectangle(0, 2, 16, 14), SpriteEffects.None);
            TextureElement topRightBYellowOffset = new TextureElement("UIpackSheet_transparent", new Rectangle(144, 144, 16, 16),
                new Rectangle(17, 2, 16, 14), SpriteEffects.None);
            TextureElement topCenterBYellowOffset = new TextureElement("UIpackSheet_transparent", new Rectangle(126, 144, 16, 16),
                new Rectangle(16, 2, 1, 14), SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.NoFillCenter);

            NineSpliceElement bYellowElementPressedOffset = new NineSpliceElement(
                topLeftBYellowOffset.Clone(), topRightBYellowOffset.Clone(), bottomLeftBYellow.Clone(), bottomRightBYellow.Clone(),
                topCenterBYellowOffset.Clone(), rightCenterBYellow.Clone(), bottomCenterBYellow.Clone(), leftCenterBYellow.Clone(),
                centerBYellow.Clone(), new Rectangle(0, 0, 33, 33));
            bYellowElementPressedOffset.ElementName = "PanelYellowBPressedOffset";

            NineSpliceElement bYellowReleasedElement = new NineSpliceElement(
                topLeftBYellow.Clone(), topRightBYellow.Clone(), bottomLeftBYellowReleased.Clone(), bottomRightBYellowReleased.Clone(),
                topCenterBYellow.Clone(), rightCenterBYellow.Clone(), bottomCenterBYellowReleased.Clone(), leftCenterBYellow.Clone(),
                centerBYellow.Clone(), new Rectangle(0, 0, 33, 33)
                );
            bYellowReleasedElement.ElementName = "PanelYellowBReleased";

            retVal.Add(bYellowElementPressed);
            retVal.Add(bYellowReleasedElement);
            retVal.Add(bYellowElementPressedOffset);
        }

        private static void getButtonElements(List<ISkinElement> retVal)
        {
            ISkinElement upButtonYellow = new TextureElement("UIpackSheet_transparent", new Rectangle(326, 415, 12, 14),
                new Rectangle(0, 0, 12, 14), SpriteEffects.None, SingleFillElement.FillTypes.Stretch);
            upButtonYellow.ElementName = "UpButtonYellow";
            ISkinElement downButtonYellow = new TextureElement("UIpackSheet_transparent", new Rectangle(344, 415, 12, 14),
                new Rectangle(0, 0, 12, 14), SpriteEffects.None, SingleFillElement.FillTypes.Stretch);
            downButtonYellow.ElementName = "DownButtonYellow";
            ISkinElement leftButtonYellow = new TextureElement("UIpackSheet_transparent", new Rectangle(325, 434, 14, 12),
                new Rectangle(0, 0, 14, 12), SpriteEffects.None, SingleFillElement.FillTypes.Stretch);
            leftButtonYellow.ElementName = "LeftButtonYellow";
            ISkinElement rightButtonYellow = new TextureElement("UIpackSheet_transparent", new Rectangle(343, 434, 14, 12),
                new Rectangle(0, 0, 14, 12), SpriteEffects.None, SingleFillElement.FillTypes.Stretch);
            rightButtonYellow.ElementName = "RightButtonYellow";

            ISkinElement greySquareButtonBase = new TextureElement("UIpackSheet_transparent", new Rectangle(163, 91, 14, 14),
                new Rectangle(0, 0, 14, 14), SpriteEffects.None, SingleFillElement.FillTypes.Stretch);
            greySquareButtonBase.ElementName = "GreySquareButtonBase";
            ISkinElement greyCircleButtonBase = new TextureElement("UIpackSheet_transparent", new Rectangle(181, 91, 14, 14),
                new Rectangle(0, 0, 14, 14), SpriteEffects.None, SingleFillElement.FillTypes.Stretch);
            greyCircleButtonBase.ElementName = "GreyCircleButtonBase";

            ISkinElement yellowXButton = new TextureElement("UIpackSheet_transparent", new Rectangle(163, 109, 14, 14),
                new Rectangle(0, 0, 14, 14), SpriteEffects.None, SingleFillElement.FillTypes.Stretch);
            yellowXButton.ElementName = "YellowXButtonElement";

            retVal.Add(upButtonYellow);
            retVal.Add(downButtonYellow);
            retVal.Add(leftButtonYellow);
            retVal.Add(rightButtonYellow);
            retVal.Add(greySquareButtonBase);
            retVal.Add(greyCircleButtonBase);
            retVal.Add(yellowXButton);
        }

        private static void getIconElements(List<ISkinElement> retVal)
        {
            ISkinElement yellowStretchBarHorizontalLeft = new TextureElement("UIpackSheet_transparent", new Rectangle(301, 290, 3, 12),
                new Rectangle(0, 0, 3, 12), SpriteEffects.None, SingleFillElement.FillTypes.NoFillCenter, SingleFillElement.FillTypes.Stretch);
            ISkinElement yellowStretchBarHorizontalRight = new TextureElement("UIpackSheet_transparent", new Rectangle(324, 290, 3, 12),
                new Rectangle(4, 0, 3, 12), SpriteEffects.None, SingleFillElement.FillTypes.NoFillCenter, SingleFillElement.FillTypes.Stretch);
            ISkinElement yellowStretchBarHorizontalCenter = new TextureElement("UIpackSheet_transparent", new Rectangle(306, 290, 16, 12),
                new Rectangle(3, 0, 1, 12), SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.Stretch);
            ContainerFillElement yellowStretchHorizontal = new ContainerFillElement();
            yellowStretchHorizontal.AddSkinElement(yellowStretchBarHorizontalLeft, new ScalingValues(0, 0, 0, 1));
            yellowStretchHorizontal.AddSkinElement(yellowStretchBarHorizontalCenter, new ScalingValues(0, 0, 1, 1));
            yellowStretchHorizontal.AddSkinElement(yellowStretchBarHorizontalRight, new ScalingValues(1, 0, 0, 1));
            yellowStretchHorizontal.ElementName = "YellowStretchBarHorizontal";
            yellowStretchHorizontal.DestinationBox = new Rectangle(0, 0, 7, 12);
            retVal.Add(yellowStretchHorizontal);

            ISkinElement yellowStretchBarHorizontalOutlineLeft = new TextureElement("UIpackSheet_transparent", new Rectangle(427, 362, 3, 12),
                new Rectangle(0, 0, 3, 12), SpriteEffects.None, SingleFillElement.FillTypes.NoFillCenter, SingleFillElement.FillTypes.Stretch);
            ISkinElement yellowStretchBarHorizontalOutlineRight = new TextureElement("UIpackSheet_transparent", new Rectangle(450, 362, 3, 12),
                new Rectangle(4, 0, 3, 12), SpriteEffects.None, SingleFillElement.FillTypes.NoFillCenter, SingleFillElement.FillTypes.Stretch);
            ISkinElement yellowStretchBarHorizontalOutlineCenter = new TextureElement("UIpackSheet_transparent", new Rectangle(432, 362, 16, 12),
                new Rectangle(3, 0, 1, 12), SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.Stretch);
            ContainerFillElement yellowStretchHorizontalOutline = new ContainerFillElement();
            yellowStretchHorizontalOutline.AddSkinElement(yellowStretchBarHorizontalOutlineLeft, new ScalingValues(0, 0, 0, 1));
            yellowStretchHorizontalOutline.AddSkinElement(yellowStretchBarHorizontalOutlineCenter, new ScalingValues(0, 0, 1, 1));
            yellowStretchHorizontalOutline.AddSkinElement(yellowStretchBarHorizontalOutlineRight, new ScalingValues(1, 0, 0, 1));
            yellowStretchHorizontalOutline.ElementName = "YellowStretchBarHorizontalOutline";
            yellowStretchHorizontalOutline.DestinationBox = new Rectangle(0, 0, 7, 12);
            retVal.Add(yellowStretchHorizontalOutline);

            ISkinElement yellowStretchBarVerticalTop = new TextureElement("UIpackSheet_transparent", new Rectangle(290, 319, 12, 3),
                new Rectangle(0, 0, 12, 3), SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.NoFillCenter);
            ISkinElement yellowStretchBarVerticalBottom = new TextureElement("UIpackSheet_transparent", new Rectangle(290, 342, 12, 3),
                new Rectangle(0, 4, 12, 3), SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.NoFillCenter);
            ISkinElement yellowStretchBarVerticalCenter = new TextureElement("UIpackSheet_transparent", new Rectangle(290, 324, 12, 16),
                new Rectangle(0, 3, 12, 1), SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.Stretch);
            ContainerFillElement yellowStretchVertical = new ContainerFillElement();
            yellowStretchVertical.AddSkinElement(yellowStretchBarVerticalTop, new ScalingValues(0, 0, 1, 0));
            yellowStretchVertical.AddSkinElement(yellowStretchBarVerticalCenter, new ScalingValues(0, 0, 1, 1));
            yellowStretchVertical.AddSkinElement(yellowStretchBarVerticalBottom, new ScalingValues(0, 1, 1, 0));
            yellowStretchVertical.ElementName = "YellowStretchBarVertical";
            yellowStretchVertical.DestinationBox = new Rectangle(0, 0, 12, 7);
            retVal.Add(yellowStretchVertical);

            ISkinElement yellowStretchBarVerticalOutlineTop = new TextureElement("UIpackSheet_transparent", new Rectangle(470, 373, 12, 3),
                new Rectangle(0, 0, 12, 3), SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.NoFillCenter);
            ISkinElement yellowStretchBarVerticalOutlineBottom = new TextureElement("UIpackSheet_transparent", new Rectangle(470, 396, 12, 3),
                new Rectangle(0, 4, 12, 3), SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.NoFillCenter);
            ISkinElement yellowStretchBarVerticalOutlineCenter = new TextureElement("UIpackSheet_transparent", new Rectangle(470, 378, 12, 16),
                new Rectangle(0, 3, 12, 1), SpriteEffects.None, SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.Stretch);
            ContainerFillElement yellowStretchVerticalOutline = new ContainerFillElement();
            yellowStretchVerticalOutline.AddSkinElement(yellowStretchBarVerticalOutlineTop, new ScalingValues(0, 0, 1, 0));
            yellowStretchVerticalOutline.AddSkinElement(yellowStretchBarVerticalOutlineCenter, new ScalingValues(0, 0, 1, 1));
            yellowStretchVerticalOutline.AddSkinElement(yellowStretchBarVerticalOutlineBottom, new ScalingValues(0, 1, 1, 0));
            yellowStretchVerticalOutline.ElementName = "YellowStretchBarVerticalOutline";
            yellowStretchVerticalOutline.DestinationBox = new Rectangle(0, 0, 12, 7);
            retVal.Add(yellowStretchVerticalOutline);

            ISkinElement greyCircleCenter = new TextureElement("UIpackSheet_extended", new Rectangle(0, 34, 6, 6),
                new Rectangle(0, 0, 6, 6), SpriteEffects.None, SingleFillElement.FillTypes.Stretch);
            greyCircleCenter.ElementName = "GreyCircleCenter";
            retVal.Add(greyCircleCenter);
            ISkinElement greySquareCenter = new TextureElement("UIpackSheet_extended", new Rectangle(0, 20, 6, 6),
                new Rectangle(0, 0, 6, 6), SpriteEffects.None, SingleFillElement.FillTypes.Stretch);
            greySquareCenter.ElementName = "GreySquareCenter";
            retVal.Add(greySquareCenter);
            ISkinElement greyCrossCenter = new TextureElement("UIpackSheet_extended", new Rectangle(0, 26, 8, 8),
                new Rectangle(0, 0, 8, 8), SpriteEffects.None, SingleFillElement.FillTypes.Stretch);
            greyCrossCenter.ElementName = "GreyCrossCenter";
            retVal.Add(greyCrossCenter);

            ISkinElement yellowCircleCenter = new TextureElement("UIpackSheet_extended", new Rectangle(0, 14, 6, 6),
                new Rectangle(0, 0, 6, 6), SpriteEffects.None, SingleFillElement.FillTypes.Stretch);
            yellowCircleCenter.ElementName = "YellowCircleCenter";
            retVal.Add(yellowCircleCenter);
            ISkinElement yellowSquareCenter = new TextureElement("UIpackSheet_extended", new Rectangle(0, 0, 6, 6),
                new Rectangle(0, 0, 6, 6), SpriteEffects.None, SingleFillElement.FillTypes.Stretch);
            yellowSquareCenter.ElementName = "YellowSquareCenter";
            retVal.Add(yellowSquareCenter);
            ISkinElement yellowCrossCenter = new TextureElement("UIpackSheet_extended", new Rectangle(0, 6, 8, 8),
                new Rectangle(0, 0, 8, 8), SpriteEffects.None, SingleFillElement.FillTypes.Stretch);
            yellowCrossCenter.ElementName = "YellowCrossCenter";
            retVal.Add(yellowCrossCenter);
        }

        private static List<GenericWidgetSkin> GetWidgetSkins()
        {
            List<GenericWidgetSkin> retVal = new List<GenericWidgetSkin>();
            getCanvasSkins(retVal);
            getScrollbarSkins(retVal);
            getButtonSkins(retVal);
            getIconSkins(retVal);
            getLabelSkins(retVal);
            getToggleButtonSkins(retVal);
            getCheckRadioSkins(retVal);
            getWindowSkins(retVal);
            return retVal;
        }

        private static void getCanvasSkins(List<GenericWidgetSkin> retVal)
        {
            GenericContainerSkin canvasHeaderSkin = new GenericContainerSkin(SkinReferencingConstants.WidgetTargetCanvas, "WindowHeaderCanvasYellow");
            canvasHeaderSkin.BackgroundSkin = new SkinnedPropertyStateValue<ISkinElement>(new GlobalReferenceElement("PanelYellowAPressed"));
            canvasHeaderSkin.MinimumDimensions = new AreaDefinition(83, 46);
            retVal.Add(canvasHeaderSkin);

            GenericContainerSkin canvasVariant1Skin = new GenericContainerSkin(
                SkinReferencingConstants.WidgetTargetCanvas, SkinReferencingConstants.SkinVariant1);
            canvasVariant1Skin.BackgroundSkin = new SkinnedPropertyStateValue<ISkinElement>(new GlobalReferenceElement("PanelYellowAPressed"));
            canvasVariant1Skin.MinimumDimensions = new AreaDefinition(33, 33);
            canvasVariant1Skin.SkinnedChildArea = new SkinnedPropertyStateValue<AreaDefinition>(new AreaDefinition(
                new RectangleFloat(3, 3, 27, 27), new ScalingValues(0, 0, 1, 1)));
            retVal.Add(canvasVariant1Skin);

            GenericContainerSkin canvasVariant2Skin = new GenericContainerSkin(
                SkinReferencingConstants.WidgetTargetCanvas, SkinReferencingConstants.SkinVariant2);
            canvasVariant2Skin.BackgroundSkin = new SkinnedPropertyStateValue<ISkinElement>(new GlobalReferenceElement("PanelYellowBPressed"));
            canvasVariant2Skin.MinimumDimensions = new AreaDefinition(33, 33);
            canvasVariant2Skin.SkinnedChildArea = new SkinnedPropertyStateValue<AreaDefinition>(new AreaDefinition(
                new RectangleFloat(3, 3, 27, 27), new ScalingValues(0, 0, 1, 1)));
            retVal.Add(canvasVariant2Skin);

            GenericContainerSkin stackPanelVariant2Skin = new GenericContainerSkin(
                SkinReferencingConstants.WidgetTargetStack, SkinReferencingConstants.SkinVariant2);
            stackPanelVariant2Skin.BackgroundSkin = new SkinnedPropertyStateValue<ISkinElement>(new GlobalReferenceElement("PanelYellowBPressed"));
            stackPanelVariant2Skin.MinimumDimensions = new AreaDefinition(33, 33);
            stackPanelVariant2Skin.SkinnedChildArea = new SkinnedPropertyStateValue<AreaDefinition>(new AreaDefinition(
                new RectangleFloat(3, 3, 27, 27), new ScalingValues(0, 0, 1, 1)));
            retVal.Add(stackPanelVariant2Skin);
        }

        private static void getScrollbarSkins(List<GenericWidgetSkin> retVal)
        {
            ScrollBarSkin yellowAScroll = new ScrollBarSkin(SkinReferencingConstants.WidgetTargetScrollBar, SkinReferencingConstants.SkinVariantDefault);
            yellowAScroll.BackgroundSkin = new SkinnedPropertyStateValue<ISkinElement>(new GlobalReferenceElement("PanelYellowBPressed"));
            yellowAScroll.MinimumDimensions = new AreaDefinition(70, 70);
            yellowAScroll.SkinnedChildArea = new SkinnedPropertyStateValue<AreaDefinition>(
                new AreaDefinition(new RectangleFloat(16, 16, 26, 26), new ScalingValues(0, 0, 1, 1)));
            yellowAScroll.UpButtonSkinVariant = "YellowUpArrow";
            yellowAScroll.UpButtonArea = new SkinnedPropertyStateValue<AreaDefinition>(new AreaDefinition(
                new RectangleFloat(42, 16, 12, 14), new ScalingValues(1f, 0f, 0f, 0f)));
            yellowAScroll.DownButtonSkinVariant = "YellowDownArrow";
            yellowAScroll.DownButtonArea = new SkinnedPropertyStateValue<AreaDefinition>(new AreaDefinition(
                new RectangleFloat(42, 30, 12, 14), new ScalingValues(1f, 1f, 0f, 0f)));
            yellowAScroll.LeftButtonSkinVariant = "YellowLeftArrow";
            yellowAScroll.LeftButtonArea = new SkinnedPropertyStateValue<AreaDefinition>(new AreaDefinition(
                new RectangleFloat(16, 42, 14, 12), new ScalingValues(0f, 1f, 0f, 0f)));
            yellowAScroll.RightButtonSkinVariant = "YellowRightArrow";
            yellowAScroll.RightButtonArea = new SkinnedPropertyStateValue<AreaDefinition>(new AreaDefinition(
                new RectangleFloat(30, 42, 14, 12), new ScalingValues(1f, 1f, 0f, 0f)));
            yellowAScroll.HorizontalScrollBackgroundVariant = "YellowHorizontalOutlineStretchBar";
            yellowAScroll.HorizontalScrollBackgroundArea = new SkinnedPropertyStateValue<AreaDefinition>(new AreaDefinition(
                new RectangleFloat(30, 42, 0, 12), new ScalingValues(0, 1, 1, 0)));
            yellowAScroll.VerticalScrollBackgroundVariant = "YellowVerticalOutlineStretchBar";
            yellowAScroll.VerticalScrollBackgroundArea = new SkinnedPropertyStateValue<AreaDefinition>(new AreaDefinition(
                new RectangleFloat(42, 30, 12, 0), new ScalingValues(1, 0, 0, 1)));
            yellowAScroll.HorizontalScrollKnobVariant = "YellowHorizontalStretchBar";
            yellowAScroll.HorizontalScrollKnobArea = new SkinnedPropertyStateValue<AreaDefinition>(new AreaDefinition(
                new RectangleFloat(30, 42, 0, 12), new ScalingValues(0, 1, 1, 0)));
            yellowAScroll.VerticalScrollKnobVariant = "YellowVerticalStretchBar";
            yellowAScroll.VerticalScrollKnobArea = new SkinnedPropertyStateValue<AreaDefinition>(new AreaDefinition(
                new RectangleFloat(42, 30, 12, 0), new ScalingValues(1, 0, 0, 1)));

            retVal.Add(yellowAScroll);
        }

        private static void getButtonSkins(List<GenericWidgetSkin> retVal)
        {
            ButtonSkin yellowUpArrow = new ButtonSkin(SkinReferencingConstants.WidgetTargetButton, "YellowUpArrow");
            yellowUpArrow.MinimumDimensions = new AreaDefinition(12, 14);
            yellowUpArrow.ButtonForeground = new SkinnedPropertyStateValue<ISkinElement>(new GlobalReferenceElement("UpButtonYellow"));
            yellowUpArrow.StateTransitionAnimations = new SkinnedPropertyStateValue<List<IModifier>>(
                    new int[] { 1, 2 }, new List<IModifier>[]{
                    getButtonGrowHoverAnimation(VectorAnchor.BottomMiddle), getButtonShrinkHoverAnimation(VectorAnchor.BottomMiddle)});
            retVal.Add(yellowUpArrow);

            ButtonSkin yellowDownArrow = new ButtonSkin(SkinReferencingConstants.WidgetTargetButton, "YellowDownArrow");
            yellowDownArrow.MinimumDimensions = new AreaDefinition(12, 14);
            yellowDownArrow.ButtonForeground = new SkinnedPropertyStateValue<ISkinElement>(new GlobalReferenceElement("DownButtonYellow"));
            yellowDownArrow.StateTransitionAnimations = new SkinnedPropertyStateValue<List<IModifier>>(
                    new int[] { 1, 2 }, new List<IModifier>[]{
                    getButtonGrowHoverAnimation(VectorAnchor.BottomMiddle), getButtonShrinkHoverAnimation(VectorAnchor.TopMiddle)});
            retVal.Add(yellowDownArrow);

            ButtonSkin yellowRightArrow = new ButtonSkin(SkinReferencingConstants.WidgetTargetButton, "YellowRightArrow");
            yellowRightArrow.MinimumDimensions = new AreaDefinition(14, 12);
            yellowRightArrow.ButtonForeground = new SkinnedPropertyStateValue<ISkinElement>(new GlobalReferenceElement("RightButtonYellow"));
            yellowRightArrow.StateTransitionAnimations = new SkinnedPropertyStateValue<List<IModifier>>(
                    new int[] { 1, 2 }, new List<IModifier>[]{
                    getButtonGrowHoverAnimation(VectorAnchor.BottomMiddle), getButtonShrinkHoverAnimation(VectorAnchor.MiddleLeft)});
            retVal.Add(yellowRightArrow);

            ButtonSkin yellowLeftArrow = new ButtonSkin(SkinReferencingConstants.WidgetTargetButton, "YellowLeftArrow");
            yellowLeftArrow.MinimumDimensions = new AreaDefinition(14, 12);
            yellowLeftArrow.ButtonForeground = new SkinnedPropertyStateValue<ISkinElement>(new GlobalReferenceElement("LeftButtonYellow"));
            yellowLeftArrow.StateTransitionAnimations = new SkinnedPropertyStateValue<List<IModifier>>(
                    new int[] { 1, 2 }, new List<IModifier>[]{
                    getButtonGrowHoverAnimation(VectorAnchor.BottomMiddle), getButtonShrinkHoverAnimation(VectorAnchor.MiddleRight)});
            retVal.Add(yellowLeftArrow);

            ButtonSkin standardButtonYellowA = new ButtonSkin(SkinReferencingConstants.WidgetTargetButton, SkinReferencingConstants.SkinVariantDefault);
            standardButtonYellowA.MinimumDimensions = new AreaDefinition(33, 33);
            standardButtonYellowA.ButtonForeground = new SkinnedPropertyStateValue<ISkinElement>(new int[] { 0, 1, 2 },
                new ISkinElement[]{new GlobalReferenceElement("PanelYellowAReleased"), new GlobalReferenceElement("PanelYellowAReleased"), 
                new GlobalReferenceElement("PanelYellowAPressedOffset")});
            standardButtonYellowA.SkinnedChildArea = new SkinnedPropertyStateValue<AreaDefinition>(new int[]{0, 2}, new AreaDefinition[]{new AreaDefinition(
                new RectangleFloat(3, 3, 27, 25), new ScalingValues(0, 0, 1, 1)),
            new AreaDefinition(new RectangleFloat(3, 5, 27, 25), new ScalingValues(0, 0, 1, 1))});
            standardButtonYellowA.StateTransitionAnimations = new SkinnedPropertyStateValue<List<IModifier>>(
                    new int[] { 1, 2 }, new List<IModifier>[]{
                    getButtonGrowHoverAnimation(VectorAnchor.BottomMiddle), getButtonShrinkHoverAnimation(VectorAnchor.Center)});
            standardButtonYellowA.LabelSkinVariant = SkinReferencingConstants.SkinVariantDefault;
            retVal.Add(standardButtonYellowA);

            ButtonSkin closeButtonYellow = new ButtonSkin(SkinReferencingConstants.WidgetTargetButton, "CloseButtonYellow");
            closeButtonYellow.MinimumDimensions = new AreaDefinition(14, 14);
            closeButtonYellow.ButtonForeground = new SkinnedPropertyStateValue<ISkinElement>(new GlobalReferenceElement("YellowXButtonElement"));
            closeButtonYellow.StateTransitionAnimations = new SkinnedPropertyStateValue<List<IModifier>>(
                    new int[] { 1, 2 }, new List<IModifier>[]{
                    getButtonGrowHoverAnimation(VectorAnchor.BottomMiddle), getButtonShrinkHoverAnimation(VectorAnchor.Center)});
            retVal.Add(closeButtonYellow);
        }

        private static void getIconSkins(List<GenericWidgetSkin> retVal)
        {
            IconSkin yellowHorizontalStretchBar = new IconSkin(SkinReferencingConstants.WidgetTargetIcon, "YellowHorizontalStretchBar");
            yellowHorizontalStretchBar.MinimumDimensions = new AreaDefinition(7, 12);
            yellowHorizontalStretchBar.IconImage = new SkinnedPropertyStateValue<ISkinElement>(new GlobalReferenceElement("YellowStretchBarHorizontal"));
            retVal.Add(yellowHorizontalStretchBar);

            IconSkin yellowVerticalStretchBar = new IconSkin(SkinReferencingConstants.WidgetTargetIcon, "YellowVerticalStretchBar");
            yellowVerticalStretchBar.MinimumDimensions = new AreaDefinition(7, 12);
            yellowVerticalStretchBar.IconImage = new SkinnedPropertyStateValue<ISkinElement>(new GlobalReferenceElement("YellowStretchBarVertical"));
            retVal.Add(yellowVerticalStretchBar);

            IconSkin yellowHorizontalOutlineStretchBar = new IconSkin(SkinReferencingConstants.WidgetTargetIcon, "YellowHorizontalOutlineStretchBar");
            yellowHorizontalOutlineStretchBar.MinimumDimensions = new AreaDefinition(7, 12);
            yellowHorizontalOutlineStretchBar.IconImage = new SkinnedPropertyStateValue<ISkinElement>(new GlobalReferenceElement("YellowStretchBarHorizontalOutline"));
            retVal.Add(yellowHorizontalOutlineStretchBar);

            IconSkin yellowVerticalOutlineStretchBar = new IconSkin(SkinReferencingConstants.WidgetTargetIcon, "YellowVerticalOutlineStretchBar");
            yellowVerticalOutlineStretchBar.MinimumDimensions = new AreaDefinition(7, 12);
            yellowVerticalOutlineStretchBar.IconImage = new SkinnedPropertyStateValue<ISkinElement>(new GlobalReferenceElement("YellowStretchBarVerticalOutline"));
            retVal.Add(yellowVerticalOutlineStretchBar);

            IconSkin greySquareCenter = new IconSkin(SkinReferencingConstants.WidgetTargetIcon, "GreySquareCenterIcon");
            greySquareCenter.MinimumDimensions = new AreaDefinition(6, 6);
            greySquareCenter.IconImage = new SkinnedPropertyStateValue<ISkinElement>(new GlobalReferenceElement("GreySquareCenter"));
            retVal.Add(greySquareCenter);

            IconSkin greyCircleCenter = new IconSkin(SkinReferencingConstants.WidgetTargetIcon, "GreyCircleCenterIcon");
            greyCircleCenter.MinimumDimensions = new AreaDefinition(6, 6);
            greyCircleCenter.IconImage = new SkinnedPropertyStateValue<ISkinElement>(new GlobalReferenceElement("GreyCircleCenter"));
            retVal.Add(greyCircleCenter);

            IconSkin greyCrossCenter = new IconSkin(SkinReferencingConstants.WidgetTargetIcon, "GreyCrossCenterIcon");
            greyCrossCenter.MinimumDimensions = new AreaDefinition(8, 8);
            greyCrossCenter.IconImage = new SkinnedPropertyStateValue<ISkinElement>(new GlobalReferenceElement("GreyCrossCenter"));
            retVal.Add(greyCrossCenter);

            IconSkin yellowSquareCenter = new IconSkin(SkinReferencingConstants.WidgetTargetIcon, "YellowSquareCenterIcon");
            yellowSquareCenter.MinimumDimensions = new AreaDefinition(6, 6);
            yellowSquareCenter.IconImage = new SkinnedPropertyStateValue<ISkinElement>(new GlobalReferenceElement("YellowSquareCenter"));
            retVal.Add(yellowSquareCenter);

            IconSkin yellowCircleCenter = new IconSkin(SkinReferencingConstants.WidgetTargetIcon, "YellowCircleCenterIcon");
            yellowCircleCenter.MinimumDimensions = new AreaDefinition(6, 6);
            yellowCircleCenter.IconImage = new SkinnedPropertyStateValue<ISkinElement>(new GlobalReferenceElement("YellowCircleCenter"));
            retVal.Add(yellowCircleCenter);

            IconSkin yellowCrossCenter = new IconSkin(SkinReferencingConstants.WidgetTargetIcon, "YellowCrossCenterIcon");
            yellowCrossCenter.MinimumDimensions = new AreaDefinition(8, 8);
            yellowCrossCenter.IconImage = new SkinnedPropertyStateValue<ISkinElement>(new GlobalReferenceElement("YellowCrossCenter"));
            retVal.Add(yellowCrossCenter);
        }

        private static void getLabelSkins(List<GenericWidgetSkin> retVal)
        {
            LabelSkin defaultLabelSkin = new LabelSkin(SkinReferencingConstants.WidgetTargetLabel, SkinReferencingConstants.SkinVariantDefault);
            defaultLabelSkin.FontName = "KenneyRegular";
            defaultLabelSkin.FontRoundingMethod = FontRoundMethod.Closest;
            defaultLabelSkin.FontSize = 14;
            //defaultLabelSkin.BackgroundSkin = new SkinnedPropertyStateValue<ISkinElement>(new GlobalReferenceElement("GreyCircleCenter"));

            retVal.Add(defaultLabelSkin);
        }

        private static void getToggleButtonSkins(List<GenericWidgetSkin> retVal)
        {
            ToggleButtonSkin defaultToggleSkin = new ToggleButtonSkin(SkinReferencingConstants.WidgetTargetToggleButton, 
                SkinReferencingConstants.SkinVariantDefault);
            defaultToggleSkin.MinimumDimensions = new AreaDefinition(57, 33);
            defaultToggleSkin.ButtonForegroundClicked = new SkinnedPropertyStateValue<ISkinElement>(new int[] { 0, 1, 2 },
                new ISkinElement[]{new GlobalReferenceElement("PanelYellowAReleased"), new GlobalReferenceElement("PanelYellowAReleased"), 
                new GlobalReferenceElement("PanelYellowAPressedOffset")});
            defaultToggleSkin.ButtonForegroundNotClicked = defaultToggleSkin.ButtonForegroundClicked;
            defaultToggleSkin.SkinnedChildArea = new SkinnedPropertyStateValue<AreaDefinition>(new int[] { 0, 2 }, new AreaDefinition[]{new AreaDefinition(
                new RectangleFloat(29, 3, 27, 25), new ScalingValues(0, 0, 1, 1), true),
            new AreaDefinition(
                new RectangleFloat(29, 5, 27, 25), new ScalingValues(0, 0, 1, 1), true)});
            defaultToggleSkin.ButtonToggledOnIconVariant = "GreySquareCenterIcon";
            defaultToggleSkin.ButtonToggledOnIconArea = new SkinnedPropertyStateValue<AreaDefinition>(
                new int[] { 0, 2 },
                new AreaDefinition[]{
                    new AreaDefinition(new RectangleFloat(3, 3, 26, 26), new ScalingValues(0, 0, 0, 0), true),
                    new AreaDefinition(new RectangleFloat(3, 5, 26, 26), new ScalingValues(0, 0, 0, 0), true)
                }
                );
            defaultToggleSkin.LabelSkinVariant = SkinReferencingConstants.SkinVariantDefault;
            retVal.Add(defaultToggleSkin);

            ToggleButtonSkin checkToggleSkinYellow = new ToggleButtonSkin(SkinReferencingConstants.WidgetTargetToggleButton,
                "CheckboxToggleYellow");
            checkToggleSkinYellow.MinimumDimensions = new AreaDefinition(14, 14);
            checkToggleSkinYellow.ButtonForegroundClicked = new SkinnedPropertyStateValue<ISkinElement>(new GlobalReferenceElement("GreySquareButtonBase"));
            checkToggleSkinYellow.ButtonForegroundNotClicked = checkToggleSkinYellow.ButtonForegroundClicked;
            checkToggleSkinYellow.ButtonToggledOnIconVariant = "YellowCrossCenterIcon";
            checkToggleSkinYellow.ButtonToggledOnIconArea = new SkinnedPropertyStateValue<AreaDefinition>(new AreaDefinition(
                new RectangleFloat(3, 3, 8, 8), new ScalingValues(0, 0, 1, 1), true
                ));
            checkToggleSkinYellow.LabelSkinVariant = SkinReferencingConstants.SkinVariantDefault;
            retVal.Add(checkToggleSkinYellow);

            ToggleButtonSkin circleToggleSkinYellow = new ToggleButtonSkin(SkinReferencingConstants.WidgetTargetToggleButton,
                "CircleToggleYellow");
            circleToggleSkinYellow.MinimumDimensions = new AreaDefinition(14, 14);
            circleToggleSkinYellow.ButtonForegroundClicked = new SkinnedPropertyStateValue<ISkinElement>(new GlobalReferenceElement("GreyCircleButtonBase"));
            circleToggleSkinYellow.ButtonForegroundNotClicked = circleToggleSkinYellow.ButtonForegroundClicked;
            circleToggleSkinYellow.ButtonToggledOnIconVariant = "YellowCircleCenterIcon";
            circleToggleSkinYellow.ButtonToggledOnIconArea = new SkinnedPropertyStateValue<AreaDefinition>(new AreaDefinition(
                new RectangleFloat(4, 4, 6, 6), new ScalingValues(0, 0, 0, 0), true
                ));
            circleToggleSkinYellow.LabelSkinVariant = SkinReferencingConstants.SkinVariantDefault;
            retVal.Add(circleToggleSkinYellow);

        }

        private static void getCheckRadioSkins(List<GenericWidgetSkin> retVal)
        {
            CheckboxSkin checkSkinYellow = new CheckboxSkin(SkinReferencingConstants.WidgetTargetCheckbox, SkinReferencingConstants.SkinVariantDefault);
            checkSkinYellow.ToggleButtonArea = new SkinnedPropertyStateValue<AreaDefinition>(new AreaDefinition(
                new RectangleFloat(3, 3, 14, 14), new ScalingValues(0, 0.5f, 0, 0), true));
            checkSkinYellow.ToggleButtonSkinVariant = "CheckboxToggleYellow";
            checkSkinYellow.LabelSkinVariant = SkinReferencingConstants.SkinVariantDefault;
            checkSkinYellow.TextArea = new SkinnedPropertyStateValue<AreaDefinition>(new AreaDefinition(
                new RectangleFloat(17, 0, 30, 14), new ScalingValues(0, 0.5f, 0, 1), true));
            checkSkinYellow.MinimumDimensions = new AreaDefinition(50, 20);
            checkSkinYellow.BackgroundSkin = new SkinnedPropertyStateValue<ISkinElement>(new GlobalReferenceElement("PanelYellowAReleased"));
            checkSkinYellow.ButtonEventFireAnimations = new List<Animation>();
            checkSkinYellow.ButtonEventFireAnimations.Add(getBounceUpAnimation());
            checkSkinYellow.TextEventFireAnimations = new List<Animation>();
            checkSkinYellow.TextEventFireAnimations.Add(getBounceRightAnimation());
            retVal.Add(checkSkinYellow);

            CheckboxSkin radioSkinYellow = new CheckboxSkin(SkinReferencingConstants.WidgetTargetRadioButton, SkinReferencingConstants.SkinVariantDefault);
            radioSkinYellow.ToggleButtonArea = new SkinnedPropertyStateValue<AreaDefinition>(new AreaDefinition(
                new RectangleFloat(3, 3, 14, 14), new ScalingValues(0, 0.5f, 0, 0), true));
            radioSkinYellow.ToggleButtonSkinVariant = "CircleToggleYellow";
            radioSkinYellow.LabelSkinVariant = SkinReferencingConstants.SkinVariantDefault;
            radioSkinYellow.TextArea = new SkinnedPropertyStateValue<AreaDefinition>(new AreaDefinition(
                new RectangleFloat(17, 0, 30, 14), new ScalingValues(0, 0.5f, 0, 1), true));
            radioSkinYellow.MinimumDimensions = new AreaDefinition(50, 20);
            radioSkinYellow.BackgroundSkin = new SkinnedPropertyStateValue<ISkinElement>(new GlobalReferenceElement("PanelYellowAReleased"));
            radioSkinYellow.ButtonEventFireAnimations = new List<Animation>();
            radioSkinYellow.ButtonEventFireAnimations.Add(getBounceUpAnimation());
            radioSkinYellow.TextEventFireAnimations = new List<Animation>();
            radioSkinYellow.TextEventFireAnimations.Add(getBounceRightAnimation());
            retVal.Add(radioSkinYellow);
        }

        private static void getWindowSkins(List<GenericWidgetSkin> retVal)
        {
            GUIWindowSkin windowSkin = new GUIWindowSkin(SkinReferencingConstants.WidgetTargetGUIWindow, SkinReferencingConstants.SkinVariantDefault);
            windowSkin.CloseButtonSkinVariant = "CloseButtonYellow";
            windowSkin.TitleLabelSkinVariant = SkinReferencingConstants.SkinVariantDefault;
            windowSkin.TitleBarCanvasSkinVariant = "WindowHeaderCanvasYellow";
            windowSkin.TitleBarCanvasArea = new SkinnedPropertyStateValue<AreaDefinition>(new AreaDefinition(
                new RectangleFloat(0, 0, 83, 46), new ScalingValues(0, 0, 1, 0)));
            windowSkin.TitleLabelArea = new SkinnedPropertyStateValue<AreaDefinition>(new AreaDefinition(
                new RectangleFloat(3, 3, 40, 40), new ScalingValues(0, 0, 1, 0)));
            windowSkin.CloseButtonArea = new SkinnedPropertyStateValue<AreaDefinition>(new AreaDefinition(
                new RectangleFloat(40, 3, 40, 40), new ScalingValues(1f, 0, 0, 0)));
            windowSkin.MinimumDimensions = new AreaDefinition(83, 63);
            windowSkin.ChildrenArea = new SkinnedPropertyStateValue<AreaDefinition>(
                new AreaDefinition(new RectangleFloat(3, 49, 77, 11), new ScalingValues(Vector2.Zero, new Vector2(1, 1))));
            windowSkin.BackgroundSkin = new SkinnedPropertyStateValue<ISkinElement>(new GlobalReferenceElement("PanelYellowBPressed"));

            List<IModifier> transitionOnList = new List<IModifier>();
            transitionOnList.Add(new TweenModifier(
                new VectorTween(new Vector2(0, 200), new Vector2(0, 0), 2.5f, EasingFunction.Equations.BackEaseIn, EasingFunction.Equations.ExpoEaseIn, false),
                new VectorTween(new Vector2(1.5f, 1.5f), new Vector2(1f, 1f), 2.5f, EasingFunction.Equations.ExpoEaseIn, EasingFunction.Equations.ExpoEaseIn, false),
                new ColorTween(new Color(1f, 1f, 1f, 0f), Color.White, 2.5f, EasingFunction.Equations.ExpoEaseIn, false), VectorAnchor.TopMiddle
                ));
            windowSkin.TransitionOnAnimations = transitionOnList;

            List<IModifier> transitionOffList = new List<IModifier>();
            transitionOffList.Add(new TweenModifier(
                new VectorTween(new Vector2(0, 0), new Vector2(0, 200), 1.5f, EasingFunction.Equations.BackEaseIn, EasingFunction.Equations.QuadEaseOut, false),
                new VectorTween(new Vector2(1f, 1f), new Vector2(1.5f, 1.5f), 1.5f, EasingFunction.Equations.QuadEaseOut, EasingFunction.Equations.QuadEaseOut, false),
                new ColorTween(new Color(1f, 1f, 1f, 1f), new Color(1f, 1f, 1f, 0), 1.5f, EasingFunction.Equations.QuadEaseOut, false), VectorAnchor.TopMiddle
                ));
            windowSkin.TransitionOffAnimations = transitionOffList;

            List<IModifier> childTransitionOnList = new List<IModifier>();
            childTransitionOnList.Add(new TweenModifier(
                new VectorTween(new Vector2(100, 0), new Vector2(0, 0), 1.5f, EasingFunction.Equations.ExpoEaseOut, EasingFunction.Equations.ExpoEaseOut, false),
                null,
                null,
                VectorAnchor.Center
                ));
            windowSkin.ChildTransitionOnAnimations = childTransitionOnList;
            windowSkin.ChildTransitionOnInitialDelay = 4f;
            windowSkin.ChildTransitionOnStepDelay = 0.5f;

            List<IModifier> childTransitionOffList = new List<IModifier>();
            childTransitionOffList.Add(new TweenModifier(
                new VectorTween(new Vector2(0, 0), new Vector2(100, 0), 1.5f, EasingFunction.Equations.ExpoEaseOut, EasingFunction.Equations.ExpoEaseOut, false),
                null,
                null,
                VectorAnchor.Center
                ));
            windowSkin.ChildTransitionOffAnimations = childTransitionOffList;
            windowSkin.ChildTransitionOffInitialDelay = 4f;
            windowSkin.ChildTransitionOffStepDelay = 0.5f;
            retVal.Add(windowSkin);
        }

        private static void getProgressSkins(List<GenericWidgetSkin> retVal)
        {
            ProgressBarSkin standardBar = new ProgressBarSkin(SkinReferencingConstants.WidgetTargetProgressBar, SkinReferencingConstants.SkinVariantDefault);
            
        }

        private static List<IModifier> getButtonGrowHoverAnimation(VectorAnchor scaleAnchor)
        {
            List<IModifier> retVal = new List<IModifier>();

            if (AddAnimations)
            {
                retVal.Add(new TweenModifier(
                    null,
                    new VectorTween(new Vector2(1, 1), new Vector2(1.05f, 1.05f), 0.2f, EasingFunction.Equations.Linear, EasingFunction.Equations.Linear, true),
                    new ColorTween(Color.White, new Color(200, 200, 200, 255), 0.2f, EasingFunction.Equations.Linear, true),
                    scaleAnchor));
            }
            return retVal;
        }

        private static List<IModifier> getButtonShrinkHoverAnimation(VectorAnchor scaleAnchor)
        {
            List<IModifier> retVal = new List<IModifier>();

            if (AddAnimations)
            {
                retVal.Add(new TweenModifier(
                    null,
                    new VectorTween(new Vector2(1, 1), new Vector2(0.95f, 0.95f), 0.2f, EasingFunction.Equations.Linear, EasingFunction.Equations.Linear, true),
                    new ColorTween(Color.White, new Color(150, 150, 150, 255), 0.2f, EasingFunction.Equations.Linear, true),
                    scaleAnchor));
            }
            return retVal;
        }

        private static Animation getBounceUpAnimation()
        {
            List<IModifier> mods = new List<IModifier>();
            if (AddAnimations)
            {
                mods.Add(new TweenModifier(
                    new VectorTween(Vector2.Zero, new Vector2(0, -10), 0.4f, EasingFunction.Equations.QuadEaseInOut,
                        EasingFunction.Equations.QuadEaseInOut, false, 0f, true, false),
                    null,
                    null,
                    VectorAnchor.Center
                    ));
                
            }

            Animation retVal = new Animation(mods);
            retVal.EventName = "ButtonClicked";
            return retVal;
        }

        private static Animation getBounceRightAnimation()
        {
            List<IModifier> mods = new List<IModifier>();
            if (AddAnimations)
            {
                mods.Add(new TweenModifier(
                    new VectorTween(Vector2.Zero, new Vector2(30, 0), 0.4f, EasingFunction.Equations.QuadEaseInOut,
                        EasingFunction.Equations.QuadEaseInOut, false, 0f, true, false),
                    null,
                    null,
                    VectorAnchor.Center
                    ));
            }

            Animation retVal = new Animation(mods);
            retVal.EventName = "ButtonClicked";
            return retVal;
        }
    }
}
