﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Threading
{
    public class ThreadCallbackMethod
    {
        public Delegate Method { get; private set; }
        public object[] Params { get; private set; }

        public ThreadCallbackMethod(Delegate Method, params object[] Params)
        {
            this.Method = Method;
            this.Params = Params;
        }
    }
}
