using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Microsoft.Xna.Framework.Graphics;
using System.IO;
using System.Xml.Serialization;
using Microsoft.Xna.Framework;
using Android.Preferences;
using MonoGUI.Manager;
using ProtoBuf;
using play.billing.v3;
using Android.Gms.Analytics;
using System.Threading;

namespace MonoGUI_AD.Manager
{
    public class ADSpecificLayer : PlatformSpecificLayer
    {
        public BillingService m_service { get; set; }
        private int m_requestId = 1;
        public Tracker defaultTracker;

        public ADSpecificLayer()
            : base()
        {
            Security.ExpectSignature = true;
        }

        public void Initialize(IPlayListener listener, string appKey, string analyticsAppKey, string appVersion)
        {
            m_service = new BillingService(Game.Activity, listener, appKey);
            m_service.Connect();

            defaultTracker = GoogleAnalytics.GetInstance(Game.Activity).NewTracker(analyticsAppKey);
            defaultTracker.EnableExceptionReporting(true);
            defaultTracker.SetAppVersion(appVersion);
            
        }

        public override Texture2D loadTexture2DFromFile(GraphicsDevice graphicsDevice, string fileName)
        {
            Texture2D image;
            using (FileStream stream = File.OpenRead(fileName))
            {
                image = Texture2D.FromStream(graphicsDevice, stream);
            }
            return image;
        }

        public override T XMLDeserialize<T>(string path, PlatformSpecificLayer.FolderTarget folderTarget)
        {
            return xmlDeserialize<T>(path, null, folderTarget);
        }

        public override T XMLDeserialize<T>(string path, string[] overrides, PlatformSpecificLayer.FolderTarget folderTarget)
        {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            for (int i = 0; i < overrides.Length; i++)
            {
                string attrib = overrides[i];
                XmlAttributes attribs = new XmlAttributes();
                attribs.XmlIgnore = true;
                attribs.XmlElements.Add(new XmlElementAttribute(attrib));
                ov.Add(typeof(T), attrib, attribs);
            }

            return xmlDeserialize<T>(path, ov, folderTarget);
        }

        private T xmlDeserialize<T>(string path, XmlAttributeOverrides overrides, PlatformSpecificLayer.FolderTarget folderTarget)
        {
            T retVal;
            //using (var wri = Game.Activity.Assets.OpenXmlResourceParser(path))
            using(var wri = Game.Activity.Assets.OpenXmlResourceParser(path))
            {
                XmlSerializer ser;
                if (overrides == null)
                {
                    ser = new XmlSerializer(typeof(T));
                }
                else
                {
                    ser = new XmlSerializer(typeof(T), overrides);
                }
                retVal = (T)ser.Deserialize(wri);
            }
            return retVal;
        }

        public override void XMLSerialize<T>(string path, T obj, PlatformSpecificLayer.FolderTarget folderTarget)
        {
            xmlSerialize<T>(path, obj, null, folderTarget);
        }

        public override void XMLSerialize<T>(string path, T obj, string[] overrides, PlatformSpecificLayer.FolderTarget folderTarget)
        {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            for (int i = 0; i < overrides.Length; i++)
            {
                string attrib = overrides[i];
                XmlAttributes attribs = new XmlAttributes();
                attribs.XmlIgnore = true;
                attribs.XmlElements.Add(new XmlElementAttribute(attrib));
                ov.Add(typeof(T), attrib, attribs);
            }

            xmlSerialize<T>(path, obj, ov, folderTarget);
        }

        private void xmlSerialize<T>(string path, T obj, XmlAttributeOverrides overrides, PlatformSpecificLayer.FolderTarget folderTarget)
        {
            using (Stream wri = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None))
            {
                XmlSerializer ser;
                if (overrides == null)
                {
                    ser = new XmlSerializer(typeof(T));
                }
                else
                {
                    ser = new XmlSerializer(typeof(T), overrides);
                }
                ser.Serialize(wri, obj);
            }
        }

        public override void StoreStringValue(string key, string value)
        {
            ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(Game.Activity.ApplicationContext);
            ISharedPreferencesEditor editor = prefs.Edit();
            editor.PutString(key, value);
            editor.Commit();
        }

        public override string RetrieveStringValue(string key, string defaultValue)
        {
            ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(Game.Activity.ApplicationContext);
            return prefs.GetString(key, defaultValue);
        }

        public override void StoreIntValue(string key, int value)
        {
            ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(Game.Activity.ApplicationContext);
            ISharedPreferencesEditor editor = prefs.Edit();
            editor.PutInt(key, value);
            editor.Commit();
        }

        public override int RetrieveIntValue(string key, int defaultValue)
        {
            ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(Game.Activity.ApplicationContext);
            return prefs.GetInt(key, defaultValue);
        }

        public override void StoreBoolValue(string key, bool value)
        {
            ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(Game.Activity.ApplicationContext);
            ISharedPreferencesEditor editor = prefs.Edit();
            editor.PutBoolean(key, value);
            editor.Commit();
        }

        public override bool RetrieveBoolValue(string key, bool defaultValue)
        {
            ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(Game.Activity.ApplicationContext);
            return prefs.GetBoolean(key, defaultValue);
        }

        public override void StoreFloatValue(string key, float value)
        {
            ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(Game.Activity.ApplicationContext);
            ISharedPreferencesEditor editor = prefs.Edit();
            editor.PutFloat(key, value);
            editor.Commit();
        }

        public override float RetrieveFloatValue(string key, float defaultValue)
        {
            ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(Game.Activity.ApplicationContext);
            return prefs.GetFloat(key, defaultValue);
        }

        public override IEnumerable<string> EnumerateValueKeys()
        {
            ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(Game.Activity.ApplicationContext);
            return prefs.All.Keys;
        }

        public override IEnumerable<string> EnumerateDirectories(string path, FolderTarget target)
        {
            switch (target)
            {
                case FolderTarget.Root:
                    return Directory.EnumerateDirectories(path);
                case FolderTarget.FileSave:
                    path = Path.Combine(SaveFileLocation(), path);
                    return Directory.EnumerateDirectories(path);
                case FolderTarget.Assets:
                    var a = Game.Activity.Assets;
                    string[] list = a.List(path);
                    List<string> retVal = new List<string>();
                    foreach (string s in list)
                    {
                        string newPath = Path.Combine(path, s);
                        try
                        {
                            Game.Activity.Assets.Open(newPath);
                        }
                        catch (Exception)
                        {
                            retVal.Add(newPath);
                        }
                    }
                    return retVal;
                case FolderTarget.Downloads:
                    string downPath = Path.Combine(Android.OS.Environment.GetExternalStoragePublicDirectory(
                       Android.OS.Environment.DirectoryDownloads).AbsolutePath, path);
                    return Directory.EnumerateDirectories(downPath);
                case FolderTarget.Documents:
                    string docPath = Path.Combine(Android.OS.Environment.GetExternalStoragePublicDirectory(
                       Android.OS.Environment.DirectoryDownloads).AbsolutePath, path);
                    return Directory.EnumerateDirectories(docPath);
            }
            return new List<string>();
        }

        public override IEnumerable<string> EnumerateFiles(string path, FolderTarget target)
        {
            switch (target)
            {
                case FolderTarget.Root:
                    return Directory.EnumerateFiles(path);
                case FolderTarget.FileSave:
                    path = Path.Combine(SaveFileLocation(), path);
                    return Directory.EnumerateFiles(path);
                case FolderTarget.Assets:
                    var a = Game.Activity.Assets;
                    string[] list = a.List(path);
                    List<string> retVal = new List<string>();
                    foreach (string s in list)
                    {
                        string newPath = Path.Combine(path, s);
                        try
                        {
                            Game.Activity.Assets.Open(newPath);
                            retVal.Add(newPath);
                        }
                        catch (Exception)
                        {
                    
                        }
                    }
                    return retVal;
                case FolderTarget.Downloads:
                    string downPath = Path.Combine(Android.OS.Environment.GetExternalStoragePublicDirectory(
                       Android.OS.Environment.DirectoryDownloads).AbsolutePath, path);
                    return Directory.EnumerateFiles(downPath);
                case FolderTarget.Documents:
                    string docPath = Path.Combine(Android.OS.Environment.GetExternalStoragePublicDirectory(
                       Android.OS.Environment.DirectoryDownloads).AbsolutePath, path);
                    return Directory.EnumerateFiles(docPath);
            }
            return new List<string>();
        }

        public override bool IAPSupported()
        {
            return true;
        }

        public override bool IAPIsDurableItemPurchased(string key)
        {
            return IAPEnumerateLicenses().Contains(key);
        }

        public override void IAPPurchaseDurableItem(string key, Action<string> callBack)
        {
            var req = new Buy(key, m_requestId++);

            m_service.SendRequest<Response>(req).ContinueWith(t =>
            {
                Game.Activity.RunOnUiThread(() =>
                {
                    if (t.Result.Success)
                        callBack(key);
                    //else
                    //    Toast.MakeText(Game.Activity, "Purchase failure. Error: " + t.Result.Message, ToastLength.Long).Show();
                });
            });	
        }

        public override void IAPConsumeConsumeable(string key)
        {
            var p = m_service.CurrentInventory.Purchases.Where(x => x.Sku == key).FirstOrDefault();
            if (p != null)
            {
                m_service.SendRequest<Response>(new ConsumePurchase(p, m_requestId++)).ContinueWith(t =>
                {
                    Game.Activity.RunOnUiThread(() =>
                    {
                        if (t.Result.Success)
                        {
                            //p.Token?
                            m_service.CurrentInventory.ErasePurchase(p.Sku);
                            Toast.MakeText(Game.Activity, "Consume complete. Item: " + p.Sku, ToastLength.Long).Show();
                        }
                        else
                            Toast.MakeText(Game.Activity, "Consume failure. Error: " + t.Result.Message, ToastLength.Long).Show();
                    });
                });
            }
            else
            {
                LogInfo("Attempted consume was null!");
            }

        }

        public override bool IAPVerifyReceipt(string receipt, string purchasedId)
        {
            return true;
        }

        public override IEnumerable<string> IAPEnumerateLicenses()
        {
            var r = m_service.SendRequest<GetPurchasesResponse>(
                new GetPurchases(Consts.ITEM_TYPE_INAPP, m_requestId++)).Result;

            List<string> retVal = new List<string>();
            foreach (Purchase p in r.PurchasedItems)
            {
                retVal.Add(p.Sku);
            }
            return retVal;
        }

        public override void LaunchWebsite(string url)
        {
            var uri = Android.Net.Uri.Parse(url);
            var intent = new Intent(Intent.ActionView, uri);
            Game.Activity.StartActivity(intent);
        }

        public override void LaunchRateAppScreen()
        {
            try
            {
                Android.Net.Uri uri = Android.Net.Uri.Parse("market://details?id=" + Game.Activity.PackageName);
                Game.Activity.StartActivity(new Intent(Intent.ActionView,
                    uri));
                LogInfo("Launching rating page: " + uri.ToString());
            }
            catch (Exception)
            {

            }

        }

        public override void SendEmail(string emailAdress)
        {
            //throw new NotImplementedException();
        }

        public override void GoogleAnalyticsSendView(string screenName)
        {
            defaultTracker.SetScreenName(screenName);
            defaultTracker.Send(new HitBuilders.ScreenViewBuilder().Build());
            
        }

        public override void GoogleAnalyticsSendEvent(string category, string action, string label, int value)
        {
            defaultTracker.Send(new HitBuilders.EventBuilder()
                .SetCategory(category).SetAction(action).SetLabel(label).SetValue(value).Build());
        }

        public override void LogInfo(string message)
        {
            Console.WriteLine(message);
        }

        public override void LogWarning(string message)
        {
            Console.WriteLine(message);
        }

        public override void LogError(string message)
        {
            Console.WriteLine(message);
        }

        public override string CurrentDirectory()
        {
            string cur = System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData);
            return cur;
        }

        private string SaveFileLocation()
        {
            string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData);

            if (path.LastIndexOf(".config") == path.Length - 7)
                path = path.Substring(0, path.Length - 7);
            return path;
        }

        public override T ProtoDeserialize<T>(string path, PlatformSpecificLayer.FolderTarget folderTarget)
        {
            switch (folderTarget)
            {
                case FolderTarget.Assets:
                    using (var f = (Game.Activity.Assets.Open(path)))
                    {
                        return Serializer.Deserialize<T>(f);
                    }
                case FolderTarget.FileSave:
                    path = Path.Combine(SaveFileLocation(), path);
                    using (var f = File.OpenRead(path))
                    {
                        return Serializer.Deserialize<T>(f);
                    }
                case FolderTarget.Root:
                    using (var f = File.OpenRead(path))
                    {
                        return Serializer.Deserialize<T>(f);
                    }//TODO FIX FRIGGIN DOCUMENTS
                case FolderTarget.Documents:
                    string actualPath = Path.Combine(Android.OS.Environment.GetExternalStoragePublicDirectory(
                       Android.OS.Environment.DirectoryDownloads).AbsolutePath, path);
                    {
                        using (var f = File.OpenRead(actualPath))
                        {
                            return Serializer.Deserialize<T>(f);
                        }
                    }
                case FolderTarget.Downloads:
                    string downloadPath = Path.Combine(Android.OS.Environment.GetExternalStoragePublicDirectory(
                       Android.OS.Environment.DirectoryDownloads).AbsolutePath, path);
                    {
                        using (var f = File.OpenRead(downloadPath))
                        {
                            return Serializer.Deserialize<T>(f);
                        }
                    }
            }
            return default(T);
        }

        public override string ProtoSerialize<T>(string path, T obj, PlatformSpecificLayer.FolderTarget folderTarget)
        {
            switch (folderTarget)
            {
                case FolderTarget.Assets:
                    LogError("You can't save a file to Assets!");
                    return String.Empty;
                case FolderTarget.FileSave:
                    path = Path.Combine(SaveFileLocation(), path);
                    using (var f = File.Create(path))
                    {
                        Serializer.Serialize<T>(f, obj);
                    }
                    return path;
                case FolderTarget.Root:
                    using (var f = File.Create(path))
                    {
                        Serializer.Serialize<T>(f, obj);
                    }
                    return path;
                case FolderTarget.Documents:
                    string actualPath = Path.Combine(Android.OS.Environment.GetExternalStoragePublicDirectory(
                       Android.OS.Environment.DirectoryDownloads).AbsolutePath, path);
                    using (var f = File.Create(actualPath))
                    {
                        Serializer.Serialize<T>(f, obj);
                    }
                    return "Downloads\\" + path;
                case FolderTarget.Downloads:
                    string downloadPath = Path.Combine(Android.OS.Environment.GetExternalStoragePublicDirectory(
                       Android.OS.Environment.DirectoryDownloads).AbsolutePath, path);
                    using (var f = File.Create(downloadPath))
                    {
                        Serializer.Serialize<T>(f, obj);
                    }
                    return "Downloads\\" + path;
                    
            }
            return String.Empty;
        }

        public override bool HasFilePicker()
        {
            return false;
        }

        public override void LaunchFilePicker(Action<string[]> callBackFile, bool doMultiselect)
        {
            //TODO get to this eventually?
        }

        public override string FileNameWithoutExtension(string fileName)
        {
            return Path.GetFileNameWithoutExtension(fileName);
        }

        public override void ThreadingQueueWorkerThread(Delegate method, params object[] args)
        {
            ThreadPool.QueueUserWorkItem(new WaitCallback(
                delegate(object state)
                {
                    method.DynamicInvoke(args);
                }
                ));
        }

        public override void ThreadingQueueUIThread(Delegate method, params object[] args)
        {
            Game.Activity.RunOnUiThread(() =>
                { ScreenManager.QueueUIThreadCallback(new MonoGUI.Threading.ThreadCallbackMethod(method, args)); }
                );
        }
    }
}