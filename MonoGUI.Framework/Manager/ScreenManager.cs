﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using MonoGUI.Manager.Input;
using MonoGUI.MSpriteBatch;
using MonoGUI.Performance;
using MonoGUI.ScreenItems;
using MonoGUI.ScreenItems.Containers;
using MonoGUI.Skinning;
using MonoGUI.Themes;
using MonoGUI.Threading;
using System.Collections.Generic;
using System.Linq;

namespace MonoGUI.Manager
{
    public class ScreenManager
    {
        public Game Game;
        public GraphicsDeviceManager Graphics;
        public InputManager InputManager;
        public ContentManager Content;
        public MatrixSpriteBatch SpriteBatch;

        private MonoGUIGameWindow MonoGUIBackground;
        private MonoGUIGameWindow ProfilerWindow;
        private List<MonoGUIGameWindow> WindowsStack;
        private List<WindowStackType> WindowsStackTypes;

        private List<MonoGUIGameWindow> CurrentWindowsStack;
        private List<WindowStackType> CurrentWindowsStackTypes;

        private static object threadLock = new object();
        private static List<ThreadCallbackMethod> _uiThreadCallbacks = new List<ThreadCallbackMethod>();

        private enum WindowStackType{PushedOff, Overlay, TransitioningOff}

        private ParentNode FocussedItem;
        private DrawBeginInfo _initialDrawBegin;

        public Vector2 ScreenDimensions
        {
            get { return new Vector2(Graphics.PreferredBackBufferWidth, 
                Graphics.PreferredBackBufferHeight); }
        }

        public ScreenManager(Game Game, GraphicsDeviceManager Graphics)
        {
            this.Game = Game;
            this.Graphics = Graphics;
            ProfilerWindow = new MonoGUIGameWindow(this);
            _initialDrawBegin = new DrawBeginInfo(SpriteSortMode.Deferred, null);
            
        }

        #region Standard Update Loop
        public void Initialize()
        {
            CurrentWindowsStack = new List<MonoGUIGameWindow>();
            CurrentWindowsStackTypes = new List<WindowStackType>();
            WindowsStack = new List<MonoGUIGameWindow>();
            WindowsStackTypes = new List<WindowStackType>();
            SpriteBatch = new MatrixSpriteBatch(Game.GraphicsDevice);
            InputManager = new InputManager();

            ProfilerWindow.Initialize();
            ProfilerWindow.ApplySkin();
        }

        public void LoadContent(ContentManager Content)
        {
            this.Content = Content;
            ProfilerWindow.LoadContent(Content);
            ProfilerWindow.AddChild(new PerformanceMonitorOverlay(this));
        }

        public void Update(GameTime gameTime)
        {
            InputManager.Update();
            PF.Instance.StartNew();
            PF.Instance.StartUpdate();
            lock (threadLock)
            {
                foreach (ThreadCallbackMethod m in _uiThreadCallbacks)
                {
                    m.Method.DynamicInvoke(m.Params);
                }
                _uiThreadCallbacks.Clear();
            }

            CurrentWindowsStack.Clear();
            CurrentWindowsStackTypes.Clear();

            CurrentWindowsStack.AddRange(WindowsStack);
            CurrentWindowsStackTypes.AddRange(WindowsStackTypes);

#if DEBUG
            if (InputManager.KeyPressed(Microsoft.Xna.Framework.Input.Keys.P))
            {
                PF.TogglePerformanceMode();
            }
#endif

            if (MonoGUIBackground != null)
            {
                MonoGUIBackground.CalculateChildren();
                MonoGUIBackground.HandleApplySkin(false);
                MonoGUIBackground.HandleResizeRequested();
                MonoGUIBackground.Update(gameTime);
            }

            foreach (MonoGUIGameWindow w in CurrentWindowsStack)
            {
                w.CalculateChildren();
                w.HandleApplySkin(false);
                w.HandleResizeRequested();
                w.HandleTransitions();
            }

            for (int i = CurrentWindowsStack.Count - 1; i >= 0; i--)
            {
                MonoGUIGameWindow curWindow = CurrentWindowsStack[i];
                WindowStackType curType = CurrentWindowsStackTypes[i];
                if (curType == WindowStackType.Overlay || curType == WindowStackType.PushedOff)
                {
                    curWindow.HandleInput(gameTime, InputManager);
                    break;
                }
            }

            List<MonoGUIGameWindow> toRemove = new List<MonoGUIGameWindow>();
            bool bottomReached = false;
            bool overlayReached = false;
            for (int i = CurrentWindowsStack.Count - 1; i >= 0; i--)
            {
                MonoGUIGameWindow curWindow = CurrentWindowsStack[i];
                WindowStackType curType = CurrentWindowsStackTypes[i];
                if (curType == WindowStackType.Overlay && !bottomReached)
                {
                    if (!overlayReached)
                    {
                        curWindow.UpdateVisualState(gameTime, InputManager);
                    }
                    curWindow.Update(gameTime);
                    overlayReached = true;
                }
                else if (curType == WindowStackType.PushedOff && !bottomReached)
                {
                    if (!overlayReached)
                    {
                        curWindow.UpdateVisualState(gameTime, InputManager);
                    }
                    curWindow.Update(gameTime);
                    bottomReached = true;
                    overlayReached = true;
                }
                else if (curType == WindowStackType.TransitioningOff)
                {
                    //curWindow.UpdateVisualState(gameTime, InputManager);
                    curWindow.Update(gameTime);
                    if (curWindow.CurrentTransitionState == ScreenItem.TransitionState.TransitionedOff)
                    {
                        toRemove.Add(curWindow);
                    }
                }
            }
            foreach (MonoGUIGameWindow wind in toRemove)
            {
                WindowsStackTypes.RemoveAt(WindowsStack.IndexOf(wind));
                WindowsStack.Remove(wind);
            }
            toRemove.Clear();
            PF.Instance.StopUpdate();
        }

        public void Draw(GameTime gameTime)
        {
            PF.Instance.StartDraw();
            SpriteBatch.FirstBegin(_initialDrawBegin);
            if (MonoGUIBackground != null)
            {
                MonoGUIBackground.Draw(gameTime, SpriteBatch);
            }

            int drawIndex = CurrentWindowsStack.Count - 1;
            for (int i = CurrentWindowsStack.Count - 1; i >= 0; i--)
            {
                MonoGUIGameWindow curWindow = CurrentWindowsStack[i];
                WindowStackType curType = CurrentWindowsStackTypes[i];
                drawIndex = i;
                if (curType == WindowStackType.PushedOff)
                {
                    break;
                }
            }

            for (int i = 0; i < CurrentWindowsStack.Count; i++)
            {
                MonoGUIGameWindow curWindow = CurrentWindowsStack[i];
                WindowStackType curType = CurrentWindowsStackTypes[i];
                if (i >= drawIndex || curType == WindowStackType.TransitioningOff)
                {
                    curWindow.Draw(gameTime, SpriteBatch);
                }
            }
            PF.Instance.StopDraw();

            

            SpriteBatch.FinalEnd();
            PF.Instance.StopSampling();
            if (PF.CurrentPerformanceMode == PF.PerformanceMode.On)
            {
                SpriteBatch.FirstBegin(SpriteSortMode.Deferred, null, null, null, null);
                ProfilerWindow.CalculateChildren();
                ProfilerWindow.HandleApplySkin(false);
                ProfilerWindow.HandleResizeRequested();
                ProfilerWindow.Update(gameTime);
                ProfilerWindow.Draw(gameTime, SpriteBatch);
                SpriteBatch.FinalEnd();
            }
        }
        #endregion

        public static void QueueUIThreadCallback(ThreadCallbackMethod method)
        {
            lock (threadLock)
            {
                _uiThreadCallbacks.Add(method);
            }
        }

        public MonoGUIGameWindow CurrentTopActiveWindow()
        {
            if (CurrentWindowsStack.Count > 0)
            {
                for (int i = CurrentWindowsStack.Count - 1; i >= 0; i--)
                {
                    MonoGUIGameWindow cur = CurrentWindowsStack[i];
                    WindowStackType curType = CurrentWindowsStackTypes[i];
                    if (curType == WindowStackType.Overlay || curType == WindowStackType.PushedOff)
                    {
                        return cur;
                    }
                }
            }

            return null;
        }

        public MonoGUIGameWindow ActivateNewGameWindow()
        {
            MonoGUIGameWindow window = new MonoGUIGameWindow(this);
            window.Initialize();
            window.LoadContent(Content);
            WindowsStack.Add(window);
            WindowsStackTypes.Add(WindowStackType.PushedOff);
            if (WindowsStack.Count > 0)
            {
                int index = WindowsStack.Count - 1;
                MonoGUIGameWindow curWindow = WindowsStack[index];
                WindowStackType curType = WindowsStackTypes[index];
                while (curType == WindowStackType.TransitioningOff && index >= 0)
                {
                    index--;
                    curWindow = WindowsStack[index];
                    curType = WindowsStackTypes[index];
                }

                if (curType == WindowStackType.Overlay)
                {
                    do
                    {
                        if (curType != WindowStackType.TransitioningOff)
                        {
                            curWindow.RequestTransitionOff();
                        }
                        index--;
                        curWindow = WindowsStack[index];
                        curType = WindowsStackTypes[index];
                    }
                    while (curType != WindowStackType.PushedOff && index >= 0);
                    if (curType == WindowStackType.PushedOff)
                    {
                        curWindow.RequestTransitionOff();
                    }
                }
                else if (curType == WindowStackType.PushedOff)
                {
                    curWindow.RequestTransitionOff();
                }
            }
            window.RequestResize();
            return window;
        }
        public MonoGUIGameWindow ActivateNewGameWindow(ScreenItem screen)
        {
            MonoGUIGameWindow retVal = ActivateNewGameWindow();
            retVal.AddChild(screen);
            return retVal;
        }

        public MonoGUIGameWindow OverlayNewGameWindow()
        {
            MonoGUIGameWindow window = new MonoGUIGameWindow(this);
            window.Initialize();
            window.LoadContent(Content);
            WindowsStack.Add(window);
            WindowsStackTypes.Add(WindowStackType.Overlay);
            window.RequestResize();
            return window;
        }
        public MonoGUIGameWindow OverlayNewGameWindow(ScreenItem screen)
        {
            MonoGUIGameWindow retVal = OverlayNewGameWindow();
            retVal.AddChild(screen);
            return retVal;
        }

        public MonoGUIGameWindow ReplaceCurrentGameWindow()
        {
            MonoGUIGameWindow window = new MonoGUIGameWindow(this);
            window.Initialize();
            window.LoadContent(Content);
            WindowsStack.Add(window);
            WindowsStackTypes.Add(WindowStackType.PushedOff);
            if (WindowsStack.Count > 0)
            {
                WindowsStack[WindowsStack.Count - 1].RequestTransitionOff();
            }
            window.RequestResize();
            return window;
        }
        public MonoGUIGameWindow ReplaceCurrentGameWindow(ScreenItem screen)
        {
            MonoGUIGameWindow retVal = ReplaceCurrentGameWindow();
            retVal.AddChild(screen);
            return retVal;
        }
        public MonoGUIGameWindow TransitionBack()
        {
            if (WindowsStack.Count > 0)
            {
                int index = WindowsStack.Count - 1;
                MonoGUIGameWindow curWindow = WindowsStack[index];
                WindowStackType curType = WindowsStackTypes[index];

                while (curType == WindowStackType.TransitioningOff && index >= 1)
                {
                    index--;
                    curWindow = WindowsStack[index];
                    curType = WindowsStackTypes[index];
                }

                if (curType != WindowStackType.TransitioningOff)
                {
                    MonoGUIGameWindow toRemove = curWindow;
                    int indexOfRemoval = index;
                    do
                    {
                        index--;
                        curWindow = WindowsStack[index];
                        curType = WindowsStackTypes[index];
                        if (curType != WindowStackType.TransitioningOff)
                        {
                            curWindow.RequestTransitionOn();
                        }
                    }
                    while (curType != WindowStackType.PushedOff && index >= 0);

                    WindowsStackTypes[indexOfRemoval] = WindowStackType.TransitioningOff;
                    toRemove.RequestTransitionOff();
                    return toRemove;
                }
            }
            return null;
        }

        public void ResetBackStack()
        {
            CurrentWindowsStack.Clear();
            CurrentWindowsStackTypes.Clear();
        }
        
        public MonoGUIGameWindow SetBackground()
        {
            MonoGUIGameWindow window = new MonoGUIGameWindow(this);
            window.Initialize();
            window.LoadContent(Content);
            MonoGUIBackground = window;

            return window;
        }
        public MonoGUIGameWindow SetBackground(ScreenItem screen)
        {
            MonoGUIGameWindow retVal = SetBackground();
            retVal.AddChild(screen);
            return retVal;
        }

        public void RequestResize()
        {
            foreach (MonoGUIGameWindow w in WindowsStack)
            {
                w.RequestResize();
            }

            if (MonoGUIBackground != null)
            {
                MonoGUIBackground.RequestResize();
            }
        }

        public void RequestFocus(ParentNode RequestingNode)
        {
            ParentNode focus = FocussedItem;
            while (focus != null)
            {
                focus.IsFocused = false;
                focus.IsFocusWithin = false;
                focus = focus.Parent;
            }

            focus = RequestingNode;
            if (focus != null)
            {
                focus.IsFocused = true;
                focus = focus.Parent;
                while (focus != null)
                {
                    focus.IsFocusWithin = true;
                    focus = focus.Parent;
                }
            }
            FocussedItem = RequestingNode;
        }

        public void ApplySkin()
        {
            foreach (MonoGUIGameWindow s in WindowsStack)
            {
                s.ApplySkin();
            }
        }

        public void SetInitialDrawCall(DrawBeginInfo info)
        {
            _initialDrawBegin = info;
        }

        public DrawBeginInfo ModifiableInitialDrawCall()
        {
            return _initialDrawBegin.Clone();
        }
    }
}
