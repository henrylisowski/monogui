﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace MonoGUI.Manager
{
    public abstract class PlatformSpecificLayer
    {
        private static PlatformSpecificLayer _instance;
        public static PlatformSpecificLayer Instance
        {
            get
            {
                if (_instance == null)
                {
#if MONOGUI_DX
                    _instance = new MonoGUI_DX.Manager.DXSpecificLayer();
#endif
#if MONOGUI_AD
                    _instance = new MonoGUI_AD.Manager.ADSpecificLayer();
#endif
#if MONOGUI_W8
                    _instance = new MonoGUI_W8.Manager.W8SpecificLayer();
#endif
                }
                return _instance;
            }
        }

        public enum LogLevel { Info, Warning, Error }
        public LogLevel DisplayLogLevel { get; set; }

        public enum FolderTarget { Root, Assets, FileSave, Documents, Downloads};

        public PlatformSpecificLayer()
        {
            DisplayLogLevel = LogLevel.Info;
        }

        public abstract Texture2D loadTexture2DFromFile(GraphicsDevice graphicsDevice, String fileName);

        public abstract T XMLDeserialize<T>(string path, FolderTarget target);
        public abstract T XMLDeserialize<T>(string path, string[] overrides, FolderTarget target);
        public abstract void XMLSerialize<T>(string path, T obj, FolderTarget target);
        public abstract void XMLSerialize<T>(string path, T obj, string[] overrides, FolderTarget target);

        public abstract T ProtoDeserialize<T>(string path, FolderTarget target);
        public abstract string ProtoSerialize<T>(string path, T obj, FolderTarget target);

        public abstract void LogInfo(string message);
        public abstract void LogWarning(string message);
        public abstract void LogError(string message);

        public abstract void StoreStringValue(string key, string value);
        public abstract string RetrieveStringValue(string key, string defaultValue);
        public abstract void StoreIntValue(string key, int value);
        public abstract int RetrieveIntValue(string key, int defaultValue);
        public abstract void StoreBoolValue(string key, bool value);
        public abstract bool RetrieveBoolValue(string key, bool defaultValue);
        public abstract void StoreFloatValue(string key, float value);
        public abstract float RetrieveFloatValue(string key, float defaultValue);


        public abstract IEnumerable<string> EnumerateValueKeys();

        public abstract string CurrentDirectory();
        public abstract IEnumerable<string> EnumerateDirectories(string path, FolderTarget target);
        public abstract IEnumerable<string> EnumerateFiles(string path, FolderTarget target);

        public abstract bool IAPSupported();
        public abstract bool IAPIsDurableItemPurchased(string key);
        public abstract void IAPPurchaseDurableItem(string key, Action<string> callBack);
        public abstract bool IAPVerifyReceipt(string receipt, string purchasedId);
        public abstract void IAPConsumeConsumeable(string key);
        public abstract IEnumerable<string> IAPEnumerateLicenses();

        public abstract void LaunchWebsite(String url);
        public abstract void LaunchRateAppScreen();
        public abstract void SendEmail(string emailAdress);

        public abstract void GoogleAnalyticsSendView(string screenName);
        public abstract void GoogleAnalyticsSendEvent(string category, string action, string label, int value);

        public abstract bool HasFilePicker();
        public abstract void LaunchFilePicker(Action<string[]> callBackFile, bool doMultiselect);
        public abstract string FileNameWithoutExtension(string fileName);

        public abstract void ThreadingQueueWorkerThread(Delegate method, params Object[] args);
        public abstract void ThreadingQueueUIThread(Delegate method, params Object[] args);
    }
}
