﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Manager.Input
{
    /// <summary>
    /// Used to determine which gamepad thumbstick input values apply to
    /// </summary>
    public enum GamepadThumbstick
    {
        Left, Right
    }
}
