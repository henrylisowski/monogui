﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using MonoGUI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Manager.Input
{
    public class InputManager
    {
        private KeyboardState PreviousKeyboardState;
        private KeyboardState CurrentKeyboardState;

        private MouseState PreviousMouseState;
        private MouseState CurrentMouseState;

        private GamePadState[] PreviousGamePadState;
        private GamePadState[] CurrentGamePadState;

        private Dictionary<int, TouchPointState> PreviousTouchDictionary;
        private Dictionary<int, TouchPointState> CurrentTouchDictionary;

        private List<TouchPointState> _touchStates;

        private List<GestureSample> GestureList;

        private readonly int MAX_GAMEPADS = 4;
        private readonly int MAX_TOUCH_POINTS;

        public bool MouseTouchHandled { get; set; }
        public bool KeyboardHandled { get; set; }
        public bool[] GamepadHandled { get; set; }

        /// <summary>
        /// Initializes the InputManager setting all inputs to 0, preparing it to calculate input and 
        /// </summary>
        public InputManager()
        {
            //Keyboard Initialization
            PreviousKeyboardState = Keyboard.GetState();
            CurrentKeyboardState = Keyboard.GetState();
            //Mouse Initialization
            PreviousMouseState = Mouse.GetState();
            CurrentMouseState = Mouse.GetState();
            //Gamepad Initialization
            PreviousGamePadState = new GamePadState[MAX_GAMEPADS];
            CurrentGamePadState = new GamePadState[MAX_GAMEPADS];

            //Handled Initialization
            GamepadHandled = new bool[MAX_GAMEPADS];

            for (int i = 0; i < MAX_GAMEPADS; i++)
            {
                PreviousGamePadState[i] = GamePad.GetState(GetZeroBasedPlayerIndex(i));
                CurrentGamePadState[i] = GamePad.GetState(GetZeroBasedPlayerIndex(i));
            }
            //Simple Touch Initialization
            MAX_TOUCH_POINTS = TouchPanel.GetCapabilities().MaximumTouchCount;
            
            PreviousTouchDictionary = new Dictionary<int, TouchPointState>();
            CurrentTouchDictionary = new Dictionary<int, TouchPointState>();
            _touchStates = new List<TouchPointState>();

            TouchCollection collection = TouchPanel.GetState();
            for (int i = 0; i < collection.Count; i++)
            {
                TouchLocation cur = collection[i];
#if XNA
                CurrentTouchDictionary.Add(cur.Id, new TouchPointState(cur.Id, cur.Position, 1f, cur.State));
                PreviousTouchDictionary.Add(cur.Id, new TouchPointState(cur.Id, cur.Position, 1f, cur.State));
#else
                CurrentTouchDictionary.Add(cur.Id, new TouchPointState(cur.Id, cur.Position, cur.Pressure, cur.State));
                PreviousTouchDictionary.Add(cur.Id, new TouchPointState(cur.Id, cur.Position, cur.Pressure, cur.State));
#endif
                
            }
            //Gesture Touch Initialization
            GestureList = new List<GestureSample>();
            TouchPanel.EnabledGestures = GestureType.None;
            while (TouchPanel.IsGestureAvailable)
            {
                GestureSample sample = TouchPanel.ReadGesture();
                GestureList.Add(sample);
            }
        }

        /// <summary>
        /// Polls all inputs and updates state to resemble what input happened this frame
        /// </summary>
        public void Update()
        {
            PreviousKeyboardState = CurrentKeyboardState;
            CurrentKeyboardState = Keyboard.GetState();

            PreviousMouseState = CurrentMouseState;
            CurrentMouseState = Mouse.GetState();

            for (int i = 0; i < MAX_GAMEPADS; i++)
            {
                PreviousGamePadState[i] = CurrentGamePadState[i];
                CurrentGamePadState[i] = GamePad.GetState(GetZeroBasedPlayerIndex(i));
            }

            PreviousTouchDictionary.Clear();
            CurrentTouchDictionary.Clear();
            TouchCollection collection = TouchPanel.GetState();
            for (int i = 0; i < collection.Count; i++)
            {
                TouchLocation cur = collection[i];
                TouchLocation prev;
                if (cur.TryGetPreviousLocation(out prev))
                {
#if XNA
                    PreviousTouchDictionary.Add(prev.Id, new TouchPointState(prev.Id, prev.Position, 1f, prev.State));
#else
                    PreviousTouchDictionary.Add(prev.Id, new TouchPointState(prev.Id, prev.Position, prev.Pressure, prev.State));
#endif
                }


#if XNA
                CurrentTouchDictionary.Add(cur.Id, new TouchPointState(cur.Id, cur.Position, 1f, cur.State));
#else
                CurrentTouchDictionary.Add(cur.Id, new TouchPointState(cur.Id, cur.Position, cur.Pressure, cur.State));
#endif
            }
            
            MouseTouchHandled = false;
            KeyboardHandled = false;
            for (int i = 0; i < MAX_GAMEPADS; i++)
            {
                GamepadHandled[i] = false;
            }

            GestureList.Clear();
            while (TouchPanel.IsGestureAvailable)
            {
                GestureSample sample = TouchPanel.ReadGesture();
                GestureList.Add(sample);
            }
        }

        /// <summary>
        /// Used to set which gestures the TouchPanel looks for
        /// </summary>
        /// <param name="type">The types of Gestures to look for</param>
        public void EnableGestures(GestureType type)
        {
            TouchPanel.EnabledGestures = type;
        }

        /// <summary>
        /// Sets whether the TouchPanel will treat mouse gestures as touch gestures
        /// </summary>
        /// <param name="set">bool to set</param>
        public void SetMouseGestures(bool set)
        {
#if XNA

#else
            TouchPanel.EnableMouseGestures = set;
#endif
            
        }

        /// <summary>
        /// Sets whether the TouchPanel will treat mouse clicks as touch taps
        /// </summary>
        /// <param name="set">bool to set</param>
        public void SetMouseTouchPoint(bool set)
        {
#if XNA

#else
            TouchPanel.EnableMouseTouchPoint = set;
#endif
        }

        /// <summary>
        /// Retrieve a list of GestureSample of all gestures that happened this past frame
        /// </summary>
        public List<GestureSample> CurrentGestureSamples { get { return GestureList; } }

        #region Helper methods for Mouse Control
        /// <summary>
        /// Whether the current state of the given MouseButton is down
        /// </summary>
        /// <param name="Button">The MouseButton to check</param>
        /// <returns>Whether Button is currently down</returns>
        public bool MouseButtonIsDown(MouseButton Button)
        {
            switch (Button)
            {
                case MouseButton.LeftButton:
                    return CurrentMouseState.LeftButton == ButtonState.Pressed;
                case MouseButton.MiddleButton:
                    return CurrentMouseState.MiddleButton == ButtonState.Pressed;
                case MouseButton.RightButton:
                    return CurrentMouseState.RightButton == ButtonState.Pressed;
                case MouseButton.XButton1:
                    return CurrentMouseState.XButton1 == ButtonState.Pressed;
                case MouseButton.XButton2:
                    return CurrentMouseState.XButton2 == ButtonState.Pressed;
                default:
                    return false;
            }
        }
        /// <summary>
        /// Whether the current state of the given MouseButton is up
        /// </summary>
        /// <param name="Button">The Mouse Button to check</param>
        /// <returns>Whether Button is currently up</returns>
        public bool MouseButtonIsUp(MouseButton Button)
        {
            switch (Button)
            {
                case MouseButton.LeftButton:
                    return CurrentMouseState.LeftButton == ButtonState.Released;
                case MouseButton.MiddleButton:
                    return CurrentMouseState.MiddleButton == ButtonState.Released;
                case MouseButton.RightButton:
                    return CurrentMouseState.RightButton == ButtonState.Released;
                case MouseButton.XButton1:
                    return CurrentMouseState.XButton1 == ButtonState.Released;
                case MouseButton.XButton2:
                    return CurrentMouseState.XButton2 == ButtonState.Released;
                default:
                    return false;
            }
        }
        /// <summary>
        /// Whether the given MouseButton was pressed down this frame
        /// </summary>
        /// <param name="Button">The Mouse Button to check</param>
        /// <returns>Whether Button was pressed this frame</returns>
        public bool MousePress(MouseButton Button)
        {
            switch (Button)
            {
                case MouseButton.LeftButton:
                    return CurrentMouseState.LeftButton == ButtonState.Pressed && PreviousMouseState.LeftButton == ButtonState.Released;
                case MouseButton.MiddleButton:
                    return CurrentMouseState.MiddleButton == ButtonState.Pressed && PreviousMouseState.MiddleButton == ButtonState.Released;
                case MouseButton.RightButton:
                    return CurrentMouseState.RightButton == ButtonState.Pressed && PreviousMouseState.RightButton == ButtonState.Released;
                case MouseButton.XButton1:
                    return CurrentMouseState.XButton1 == ButtonState.Pressed && PreviousMouseState.XButton1 == ButtonState.Released;
                case MouseButton.XButton2:
                    return CurrentMouseState.XButton2 == ButtonState.Pressed && PreviousMouseState.XButton2 == ButtonState.Released;
                default:
                    return false;
            }
        }
        /// <summary>
        /// Whether the given MouseButton was released this frame
        /// </summary>
        /// <param name="Button">The Mouse Button to check</param>
        /// <returns>Whether Button was released this frame</returns>
        public bool MouseRelease(MouseButton Button)
        {
            switch (Button)
            {
                case MouseButton.LeftButton:
                    return CurrentMouseState.LeftButton == ButtonState.Released && PreviousMouseState.LeftButton == ButtonState.Pressed;
                case MouseButton.MiddleButton:
                    return CurrentMouseState.MiddleButton == ButtonState.Released && PreviousMouseState.MiddleButton == ButtonState.Pressed;
                case MouseButton.RightButton:
                    return CurrentMouseState.RightButton == ButtonState.Released && PreviousMouseState.RightButton == ButtonState.Pressed;
                case MouseButton.XButton1:
                    return CurrentMouseState.XButton1 == ButtonState.Released && PreviousMouseState.XButton1 == ButtonState.Pressed;
                case MouseButton.XButton2:
                    return CurrentMouseState.XButton2 == ButtonState.Released && PreviousMouseState.XButton2 == ButtonState.Pressed;
                default:
                    return false;
            }
        }
        /// <summary>
        /// Whether the given MouseButton was held down over the past two frames
        /// </summary>
        /// <param name="Button">The Mouse Button to check</param>
        /// <returns>Whether Button was held down over the past two frames</returns>
        public bool MouseButtonHeldDown(MouseButton Button)
        {
            switch (Button)
            {
                case MouseButton.LeftButton:
                    return CurrentMouseState.LeftButton == ButtonState.Pressed && PreviousMouseState.LeftButton == ButtonState.Pressed;
                case MouseButton.MiddleButton:
                    return CurrentMouseState.MiddleButton == ButtonState.Pressed && PreviousMouseState.MiddleButton == ButtonState.Pressed;
                case MouseButton.RightButton:
                    return CurrentMouseState.RightButton == ButtonState.Pressed && PreviousMouseState.RightButton == ButtonState.Pressed;
                case MouseButton.XButton1:
                    return CurrentMouseState.XButton1 == ButtonState.Pressed && PreviousMouseState.XButton1 == ButtonState.Pressed;
                case MouseButton.XButton2:
                    return CurrentMouseState.XButton2 == ButtonState.Pressed && PreviousMouseState.XButton2 == ButtonState.Pressed;
                default:
                    return false;
            }
        }
        /// <summary>
        /// Whether the given MouseButton was held up over the past two frames
        /// </summary>
        /// <param name="Button">The Mouse Button to check</param>
        /// <returns>Whether Button was held up over the past two frames</returns>
        public bool MouseButtonHeldUp(MouseButton Button)
        {
            switch (Button)
            {
                case MouseButton.LeftButton:
                    return CurrentMouseState.LeftButton == ButtonState.Released && PreviousMouseState.LeftButton == ButtonState.Released;
                case MouseButton.MiddleButton:
                    return CurrentMouseState.MiddleButton == ButtonState.Released && PreviousMouseState.MiddleButton == ButtonState.Released;
                case MouseButton.RightButton:
                    return CurrentMouseState.RightButton == ButtonState.Released && PreviousMouseState.RightButton == ButtonState.Released;
                case MouseButton.XButton1:
                    return CurrentMouseState.XButton1 == ButtonState.Released && PreviousMouseState.XButton1 == ButtonState.Released;
                case MouseButton.XButton2:
                    return CurrentMouseState.XButton2 == ButtonState.Released && PreviousMouseState.XButton2 == ButtonState.Released;
                default:
                    return false;
            }
        }
        /// <summary>
        /// The current on screen position of the mouse
        /// </summary>
        /// <returns>The current mouse position</returns>
        public Vector2 MousePosition()
        {
            return new Vector2(CurrentMouseState.X, CurrentMouseState.Y);
        }
        /// <summary>
        /// The current position of the mouse relative to the TransformationMatrix
        /// </summary>
        /// <param name="TransformationMatrix">The Matrix used to transform the mosue position</param>
        /// <returns>Current mouse position transformed by TransformationMatrix</returns>
        public Vector2 MousePosition(Matrix TransformationMatrix)
        {
            Vector2 retVal = MousePosition();
            retVal = Vector2.Transform(retVal, Matrix.Invert(TransformationMatrix));
            return retVal;
        }
        /// <summary>
        /// Delta movement of the mouse over the past two frames
        /// </summary>
        /// <returns>A vector that contains the delta movement of the mouse over the past two frames</returns>
        public Vector2 MouseDelta()
        {
            return new Vector2(CurrentMouseState.X - PreviousMouseState.X, CurrentMouseState.Y - PreviousMouseState.Y);
        }
        public Vector2 MouseDelta(Matrix TransformationMatrix)
        {
            Matrix inverted = Matrix.Invert(TransformationMatrix);
            Vector2 CurrentPosition = Vector2.Transform(new Vector2(CurrentMouseState.X, CurrentMouseState.Y), inverted);
            Vector2 PreviousPosition = Vector2.Transform(new Vector2(PreviousMouseState.X, PreviousMouseState.Y), inverted);
            return CurrentPosition - PreviousPosition;
        }
        public bool MouseScrollUp()
        {
            return CurrentMouseState.ScrollWheelValue > PreviousMouseState.ScrollWheelValue;
        }
        public bool MouseScrollDown()
        {
            return CurrentMouseState.ScrollWheelValue < PreviousMouseState.ScrollWheelValue;
        }
      
        #endregion

        #region Helper methods for Keyboard Control
        public bool KeyIsDown(Keys key)
        {
            return CurrentKeyboardState.IsKeyDown(key);
        }
        public bool KeyIsUp(Keys key)
        {
            return CurrentKeyboardState.IsKeyUp(key);
        }
        public bool KeyPressed(Keys key)
        {
            return CurrentKeyboardState.IsKeyDown(key) && PreviousKeyboardState.IsKeyUp(key);
        }
        public bool KeyReleased(Keys key)
        {
            return CurrentKeyboardState.IsKeyUp(key) && PreviousKeyboardState.IsKeyDown(key);
        }
        public bool KeyHeldDown(Keys key)
        {
            return CurrentKeyboardState.IsKeyDown(key) && PreviousKeyboardState.IsKeyDown(key);
        }
        public bool HeyHeldUp(Keys key)
        {
            return CurrentKeyboardState.IsKeyUp(key) && PreviousKeyboardState.IsKeyUp(key);
        }
        public Keys[] KeysPressed()
        {
            List<Keys> retVal = new List<Keys>();
            foreach (Keys k in EnumUtils.Keys)
            {
                if (KeyPressed(k))
                {
                    retVal.Add(k);
                }
            }
            return retVal.ToArray();
        }

        public bool KeyboardString(out bool startsWithBackspace, out string typedString)
        {
            startsWithBackspace = false;
            typedString = String.Empty;
            StringBuilder build = new StringBuilder();
            Keys[] allPressed = KeysPressed();

            if (allPressed.Length > 0)
            {
                for (int i = 0; i < allPressed.Length; i++)
                {
                    Keys curKey = allPressed[i];
                    if (curKey == Keys.Back)
                    {
                        if (build.Length == 0)
                        {
                            startsWithBackspace = true;
                        }
                        else
                        {
                            build.Remove(build.Length - 1, 1);
                        }
                    }

                    bool shift = KeyIsDown(Keys.LeftShift) || KeyIsDown(Keys.RightShift);
                    switch (curKey)
                    {
                        //Alphabet keys
                        case Keys.A: if (shift) { build.Append('A'); } else { build.Append('a'); } break;
                        case Keys.B: if (shift) { build.Append('B'); } else { build.Append('b'); } break;
                        case Keys.C: if (shift) { build.Append('C'); } else { build.Append('c'); } break;
                        case Keys.D: if (shift) { build.Append('D'); } else { build.Append('d'); } break;
                        case Keys.E: if (shift) { build.Append('E'); } else { build.Append('e'); } break;
                        case Keys.F: if (shift) { build.Append('F'); } else { build.Append('f'); } break;
                        case Keys.G: if (shift) { build.Append('G'); } else { build.Append('g'); } break;
                        case Keys.H: if (shift) { build.Append('H'); } else { build.Append('h'); } break;
                        case Keys.I: if (shift) { build.Append('I'); } else { build.Append('i'); } break;
                        case Keys.J: if (shift) { build.Append('J'); } else { build.Append('j'); } break;
                        case Keys.K: if (shift) { build.Append('K'); } else { build.Append('k'); } break;
                        case Keys.L: if (shift) { build.Append('L'); } else { build.Append('l'); } break;
                        case Keys.M: if (shift) { build.Append('M'); } else { build.Append('m'); } break;
                        case Keys.N: if (shift) { build.Append('N'); } else { build.Append('n'); } break;
                        case Keys.O: if (shift) { build.Append('O'); } else { build.Append('o'); } break;
                        case Keys.P: if (shift) { build.Append('P'); } else { build.Append('p'); } break;
                        case Keys.Q: if (shift) { build.Append('Q'); } else { build.Append('q'); } break;
                        case Keys.R: if (shift) { build.Append('R'); } else { build.Append('r'); } break;
                        case Keys.S: if (shift) { build.Append('S'); } else { build.Append('s'); } break;
                        case Keys.T: if (shift) { build.Append('T'); } else { build.Append('t'); } break;
                        case Keys.U: if (shift) { build.Append('U'); } else { build.Append('u'); } break;
                        case Keys.V: if (shift) { build.Append('V'); } else { build.Append('v'); } break;
                        case Keys.W: if (shift) { build.Append('W'); } else { build.Append('w'); } break;
                        case Keys.X: if (shift) { build.Append('X'); } else { build.Append('x'); } break;
                        case Keys.Y: if (shift) { build.Append('Y'); } else { build.Append('y'); } break;
                        case Keys.Z: if (shift) { build.Append('Z'); } else { build.Append('z'); } break;

                        //Decimal keys
                        case Keys.D0: if (shift) { build.Append(')'); } else { build.Append('0'); } break;
                        case Keys.D1: if (shift) { build.Append('!'); } else { build.Append('1'); } break;
                        case Keys.D2: if (shift) { build.Append('@'); } else { build.Append('2'); } break;
                        case Keys.D3: if (shift) { build.Append('#'); } else { build.Append('3'); } break;
                        case Keys.D4: if (shift) { build.Append('$'); } else { build.Append('4'); } break;
                        case Keys.D5: if (shift) { build.Append('%'); } else { build.Append('5'); } break;
                        case Keys.D6: if (shift) { build.Append('^'); } else { build.Append('6'); } break;
                        case Keys.D7: if (shift) { build.Append('&'); } else { build.Append('7'); } break;
                        case Keys.D8: if (shift) { build.Append('*'); } else { build.Append('8'); } break;
                        case Keys.D9: if (shift) { build.Append('('); } else { build.Append('9'); } break;

                        //Decimal numpad keys
                        case Keys.NumPad0: build.Append('0'); break;
                        case Keys.NumPad1: build.Append('1'); break;
                        case Keys.NumPad2: build.Append('2'); break;
                        case Keys.NumPad3: build.Append('3'); break;
                        case Keys.NumPad4: build.Append('4'); break;
                        case Keys.NumPad5: build.Append('5'); break;
                        case Keys.NumPad6: build.Append('6'); break;
                        case Keys.NumPad7: build.Append('7'); break;
                        case Keys.NumPad8: build.Append('8'); break;
                        case Keys.NumPad9: build.Append('9'); break;

                        //Special keys
                        case Keys.OemTilde: if (shift) { build.Append('~'); } else { build.Append('`'); } break;
                        case Keys.OemSemicolon: if (shift) { build.Append(':'); } else { build.Append(';'); } break;
                        case Keys.OemQuotes: if (shift) { build.Append('"'); } else { build.Append('\''); } break;
                        case Keys.OemQuestion: if (shift) { build.Append('?'); } else { build.Append('/'); } break;
                        case Keys.OemPlus: if (shift) { build.Append('+'); } else { build.Append('='); } break;
                        case Keys.OemPipe: if (shift) { build.Append('|'); } else { build.Append('\\'); } break;
                        case Keys.OemPeriod: if (shift) { build.Append('>'); } else { build.Append('.'); } break;
                        case Keys.OemOpenBrackets: if (shift) { build.Append('{'); } else { build.Append('['); } break;
                        case Keys.OemCloseBrackets: if (shift) { build.Append('}'); } else { build.Append(']'); } break;
                        case Keys.OemMinus: if (shift) { build.Append('_'); } else { build.Append('-'); } break;
                        case Keys.OemComma: if (shift) { build.Append('<'); } else { build.Append(','); } break;
                        case Keys.Space: build.Append(' '); break;
                    }

                }
                typedString = build.ToString();
                return true;
            }

            return false;
        }
        #endregion

        #region Helper methods for Gamepad Control
        public bool GamepadConnected(int playerNumber = 0)
        {
            if (playerNumber >= MAX_GAMEPADS)
            {
                throw new InputManagerException("Gamepad Index was out of bounds");
            }
            return CurrentGamePadState[playerNumber].IsConnected;
        }
        public bool GamepadHasChanged(int playerNumber = 0)
        {
            if (playerNumber >= MAX_GAMEPADS)
            {
                throw new InputManagerException("Gamepad Index was out of bounds");
            }
            return CurrentGamePadState[playerNumber].PacketNumber != PreviousGamePadState[playerNumber].PacketNumber;
        }
        public bool GamepadButtonDown(Buttons Button, int playerNumber = 0)
        {
            if (playerNumber >= MAX_GAMEPADS)
            {
                throw new InputManagerException("Gamepad Index was out of bounds");
            }
            return CurrentGamePadState[playerNumber].IsButtonDown(Button);
        }
        public bool GamepadButtonUp(Buttons Button, int playerNumber = 0)
        {
            if (playerNumber >= MAX_GAMEPADS)
            {
                throw new InputManagerException("Gamepad Index was out of bounds");
            }
            return CurrentGamePadState[playerNumber].IsButtonUp(Button);
        }
        public bool GamepadButtonPressed(Buttons Button, int playerNumber = 0)
        {
            if (playerNumber >= MAX_GAMEPADS)
            {
                throw new InputManagerException("Gamepad Index was out of bounds");
            }
            return CurrentGamePadState[playerNumber].IsButtonDown(Button) && PreviousGamePadState[playerNumber].IsButtonUp(Button);
        }
        public bool GamepadButtonReleased(Buttons Button, int playerNumber = 0)
        {
            if (playerNumber >= MAX_GAMEPADS)
            {
                throw new InputManagerException("Gamepad Index was out of bounds");
            }
            return CurrentGamePadState[playerNumber].IsButtonUp(Button) && PreviousGamePadState[playerNumber].IsButtonDown(Button);
        }
        public bool GamepadButtonHeldDown(Buttons Button, int playerNumber = 0)
        {
            if (playerNumber >= MAX_GAMEPADS)
            {
                throw new InputManagerException("Gamepad Index was out of bounds");
            }
            return CurrentGamePadState[playerNumber].IsButtonDown(Button) && PreviousGamePadState[playerNumber].IsButtonDown(Button);
        }
        public bool GamepadButtonHeldUp(Buttons Button, int playerNumber = 0)
        {
            if (playerNumber >= MAX_GAMEPADS)
            {
                throw new InputManagerException("Gamepad Index was out of bounds");
            }
            return CurrentGamePadState[playerNumber].IsButtonUp(Button) && PreviousGamePadState[playerNumber].IsButtonUp(Button);
        }
        public float GamepadTriggerValue(GamepadTrigger Trigger, int playerNumber = 0)
        {
            if (playerNumber >= MAX_GAMEPADS)
            {
                throw new InputManagerException("Gamepad Index was out of bounds");
            }
            switch (Trigger)
            {
                case GamepadTrigger.Left:
                    return CurrentGamePadState[playerNumber].Triggers.Left;
                case GamepadTrigger.Right:
                    return CurrentGamePadState[playerNumber].Triggers.Right;
                default:
                    throw new InputManagerException("Invalid GamepadTrigger Value");
            }
        }
        public float GamepadTriggerDelta(GamepadTrigger Trigger, int playerNumber = 0)
        {
            if (playerNumber >= MAX_GAMEPADS)
            {
                throw new InputManagerException("Gamepad Index was out of bounds");
            }
            switch (Trigger)
            {
                case GamepadTrigger.Left:
                    return CurrentGamePadState[playerNumber].Triggers.Left - PreviousGamePadState[playerNumber].Triggers.Left;
                case GamepadTrigger.Right:
                    return CurrentGamePadState[playerNumber].Triggers.Right - PreviousGamePadState[playerNumber].Triggers.Right;
                default:
                    throw new InputManagerException("Invalid GamepadTrigger Value");
            }
        }
        public Vector2 GamepadThumbstickPosition(GamepadThumbstick Thumbstick, int playerNumber = 0)
        {
            if (playerNumber >= MAX_GAMEPADS)
            {
                throw new InputManagerException("Gamepad Index was out of bounds");
            }
            switch (Thumbstick)
            {
                case GamepadThumbstick.Left:
                    return CurrentGamePadState[playerNumber].ThumbSticks.Left;
                case GamepadThumbstick.Right:
                    return CurrentGamePadState[playerNumber].ThumbSticks.Right;
                default:
                    throw new InputManagerException("Invalid GamepadThumbstick Value");
            }
        }
        public Vector2 GamepadThumbstickDelta(GamepadThumbstick Thumbstick, int playerNumber = 0)
        {
            if (playerNumber >= MAX_GAMEPADS)
            {
                throw new InputManagerException("Gamepad Index was out of bounds");
            }
            switch (Thumbstick)
            {
                case GamepadThumbstick.Left:
                    return CurrentGamePadState[playerNumber].ThumbSticks.Left - PreviousGamePadState[playerNumber].ThumbSticks.Left;
                case GamepadThumbstick.Right:
                    return CurrentGamePadState[playerNumber].ThumbSticks.Right - PreviousGamePadState[playerNumber].ThumbSticks.Right;
                default:
                    throw new InputManagerException("Invalid GamepadThumbstick Value");
            }
        }
        #endregion

        #region Helper methods for Standard Touchpad Control
        public int[] AllTouchIds()
        {
            return CurrentTouchDictionary.Keys.ToArray();
        }

        public int[] PreviousTouchIds()
        {
            return PreviousTouchDictionary.Keys.ToArray();
        }
        public int[] NewPressedIds()
        {
            List<int> retVal = new List<int>();
            foreach (KeyValuePair<int, TouchPointState> p in CurrentTouchDictionary)
            {
                if (p.Value.State == TouchLocationState.Pressed)
                {
                    retVal.Add(p.Value.Id);
                }
            }
            return retVal.ToArray();
        }
        public int[] NewReleasedIds()
        {
            List<int> retVal = new List<int>();
            foreach (KeyValuePair<int, TouchPointState> p in CurrentTouchDictionary)
            {
                if (p.Value.State == TouchLocationState.Released)
                {
                    retVal.Add(p.Value.Id);
                }
            }
            return retVal.ToArray();
        }
        public bool IsPressed(int touchId)
        {
            if (!CurrentTouchDictionary.ContainsKey(touchId))
            {
                throw new InputManagerException("Invalid Touch Id");
            }
            return CurrentTouchDictionary[touchId].State == TouchLocationState.Pressed;
        }
        public bool IsReleased(int touchId)
        {
            if (!CurrentTouchDictionary.ContainsKey(touchId))
            {
                throw new InputManagerException("Invalid Touch Id");
            }
            return CurrentTouchDictionary[touchId].State == TouchLocationState.Released;
        }
        public Vector2 TouchPosition(int touchId)
        {
            if (!CurrentTouchDictionary.ContainsKey(touchId))
            {
                throw new InputManagerException("Invalid Touch Id");
            }
            return CurrentTouchDictionary[touchId].Position;
        }
        public float TouchPressure(int touchId)
        {
            if (!CurrentTouchDictionary.ContainsKey(touchId))
            {
                throw new InputManagerException("Invalid Touch Id");
            }
            return CurrentTouchDictionary[touchId].Pressure;
        }
        public Vector2 TouchPositionDelta(int touchId)
        {
            if (!CurrentTouchDictionary.ContainsKey(touchId) || !PreviousTouchDictionary.ContainsKey(touchId))
            {
                throw new InputManagerException("Invalid Touch Id");
            }
            return CurrentTouchDictionary[touchId].Position - PreviousTouchDictionary[touchId].Position;
        }
        public float TouchPressureDelta(int touchId)
        {
            if (!CurrentTouchDictionary.ContainsKey(touchId) || !PreviousTouchDictionary.ContainsKey(touchId))
            {
                throw new InputManagerException("Invalid Touch Id");
            }
            return CurrentTouchDictionary[touchId].Pressure - PreviousTouchDictionary[touchId].Pressure;
        }
        public List<TouchPointState> TouchStates()
        {
            _touchStates.Clear();
            foreach (TouchPointState s in CurrentTouchDictionary.Values)
            {
                _touchStates.Add(s);
            }
            return _touchStates;
        }
        #endregion

        #region Helper methods for Gesture Touchpad Control
        //TODO do this
        #endregion

        public static PlayerIndex GetZeroBasedPlayerIndex(int i)
        {
            switch (i)
            {
                case 0:
                    return PlayerIndex.One;
                case 1:
                    return PlayerIndex.Two;
                case 2:
                    return PlayerIndex.Three;
                case 3:
                    return PlayerIndex.Four;
                default:
                    return PlayerIndex.One;
            }
        }
    }
}
