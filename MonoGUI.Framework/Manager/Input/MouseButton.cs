﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Manager.Input
{
    public enum MouseButton
    {
        LeftButton, 
        RightButton, 
        MiddleButton, 
        XButton1, 
        XButton2
    }
}
