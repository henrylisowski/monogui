﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace MonoGUI.Manager.Input
{
    public class InputManagerException : Exception
    {
        public InputManagerException()
        {
            // Add implementation.
        }
        public InputManagerException(string message)
        {
            // Add implementation.
        }
        public InputManagerException(string message, Exception inner)
        {
            // Add implementation.
        }
    }
}
