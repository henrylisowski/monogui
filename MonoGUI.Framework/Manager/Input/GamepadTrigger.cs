﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Manager.Input
{
    /// <summary>
    /// Enum used to determine which gamepad trigger an input pertains to
    /// </summary>
    public enum GamepadTrigger
    {
        Left, Right
    }
}
