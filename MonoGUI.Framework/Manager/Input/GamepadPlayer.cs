﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Manager.Input
{
    /// <summary>
    /// Enum used to determine which gamepad player an input applies to,
    /// or which player a GUI item can accept input from
    /// </summary>
    public enum GamepadPlayer : byte
    {
        Player1 = 0x00,
        Player2 = 0x01,
        Player3 = 0x02,
        Player4 = 0x04,
        AnyPlayer = 0x08

    }
}
