﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input.Touch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Manager.Input
{
    public class TouchPointState
    {
        public int Id { get; private set; }
        public Vector2 Position { get; private set; }
        public float Pressure { get; private set; }
        public TouchLocationState State { get; private set; }

        public TouchPointState(int Id, Vector2 Position, float Pressure, TouchLocationState State)
        {
            this.Id = Id;
            this.Position = Position;
            this.Pressure = Pressure;
            this.State = State;
        }
    }
}
