﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.MSpriteBatch
{
    public class DrawBeginInfo
    {
        private SpriteSortMode sortMode;
        public SpriteSortMode SortMode 
        { 
            get { return sortMode; } 
            set 
            {
                sortMode = value;
                if (Type < BeginType.BlendState) 
                    Type = BeginType.BlendState; 
            } 
        }
        private BlendState blendState;
        public BlendState BlendState 
        {
            get { return blendState; }
            set
            {
                blendState = value;
                if (Type < BeginType.BlendState)
                    Type = BeginType.BlendState;
            }
        }
        private SamplerState samplerState;
        public SamplerState SamplerState
        {
            get { return samplerState; }
            set
            {
                samplerState = value;
                if (Type < BeginType.RasterizerState)
                    Type = BeginType.RasterizerState;
            }
        }
        private DepthStencilState depthStencilState;
        public DepthStencilState DepthStencilState
        {
            get { return depthStencilState; }
            set
            {
                depthStencilState = value;
                if (Type < BeginType.RasterizerState)
                    Type = BeginType.RasterizerState;
            }
        }
        private RasterizerState rasterizerState;
        public RasterizerState RasterizerState
        {
            get { return rasterizerState; }
            set
            {
                rasterizerState = value;
                if (Type < BeginType.RasterizerState)
                    Type = BeginType.RasterizerState;
            }
        }
        private Effect effect;
        public Effect Effect
        {
            get { return effect; }
            set
            {
                effect = value;
                if (Type < BeginType.Effect)
                    Type = BeginType.Effect;
            }
        }
        private Matrix transformMatrix = Matrix.Identity;
        public Matrix TransformMatrix
        {
            get { return transformMatrix; }
            set
            {
                transformMatrix = value;
                if (Type < BeginType.Matrix)
                    Type = BeginType.Matrix;
            }
        }
        
        public enum BeginType { Basic = 0, BlendState = 1, RasterizerState = 2, Effect = 3, Matrix = 4 }
        public BeginType Type { get; private set; }

        public DrawBeginInfo()
        {
            Type = BeginType.Basic;
        }

        public DrawBeginInfo(SpriteSortMode sortMode, BlendState blendState)
        {
            this.sortMode = sortMode;
            this.blendState = blendState;
            Type = BeginType.BlendState;
        }

        public DrawBeginInfo(SpriteSortMode sortMode, BlendState blendState, SamplerState samplerState,
            DepthStencilState depthStencilState, RasterizerState rasterizerState)
        {
            this.sortMode = sortMode;
            this.blendState = blendState;
            this.samplerState = samplerState;
            this.depthStencilState = depthStencilState;
            this.rasterizerState = rasterizerState;
            Type = BeginType.RasterizerState;
        }

        public DrawBeginInfo(SpriteSortMode sortMode, BlendState blendState, SamplerState samplerState,
            DepthStencilState depthStencilState, RasterizerState rasterizerState, Effect effect)
        {
            this.sortMode = sortMode;
            this.blendState = blendState;
            this.samplerState = samplerState;
            this.depthStencilState = depthStencilState;
            this.rasterizerState = rasterizerState;
            this.effect = effect;
            Type = BeginType.Effect;
        }

        public DrawBeginInfo(SpriteSortMode sortMode, BlendState blendState, SamplerState samplerState,
            DepthStencilState depthStencilState, RasterizerState rasterizerState, Effect effect, Matrix transformMatrix)
        {
            this.sortMode = sortMode;
            this.blendState = blendState;
            this.samplerState = samplerState;
            this.depthStencilState = depthStencilState;
            this.rasterizerState = rasterizerState;
            this.effect = effect;
            this.transformMatrix = transformMatrix;
            Type = BeginType.Matrix;
        }

        public void Restore(MatrixSpriteBatch spriteBatch)
        {
            switch (Type)
            {
                case BeginType.Basic:
                    spriteBatch.Begin();
                    break;
                case BeginType.BlendState:
                    spriteBatch.Begin(sortMode, blendState);
                    break;
                case BeginType.RasterizerState:
                    spriteBatch.Begin(sortMode, blendState, samplerState, depthStencilState, rasterizerState);
                    break;
                case BeginType.Effect:
                    spriteBatch.Begin(sortMode, blendState, samplerState, depthStencilState, rasterizerState, effect);
                    break;
                case BeginType.Matrix:
                    spriteBatch.Begin(sortMode, blendState, samplerState, depthStencilState, rasterizerState, effect, transformMatrix);
                    break;
            }
        }

        public DrawBeginInfo Clone()
        {
            switch (Type)
            {
                case BeginType.Basic:
                    return new DrawBeginInfo();
                case BeginType.BlendState:
                    return new DrawBeginInfo(sortMode, blendState);
                case BeginType.RasterizerState:
                    return new DrawBeginInfo(sortMode, blendState, samplerState, 
                        depthStencilState, cloneRasterizer(rasterizerState));
                case BeginType.Effect:
                    return new DrawBeginInfo(sortMode, blendState, samplerState, 
                        depthStencilState, cloneRasterizer(rasterizerState), effect);
                case BeginType.Matrix:
                    return new DrawBeginInfo(sortMode, blendState, samplerState, 
                        depthStencilState, cloneRasterizer(rasterizerState), effect, transformMatrix);
                default:
                    return new DrawBeginInfo();
            }

        }

        private RasterizerState cloneRasterizer(RasterizerState rast)
        {
            RasterizerState retVal = new RasterizerState();
            retVal.CullMode = rast.CullMode;
            retVal.DepthBias = rast.DepthBias;
            retVal.FillMode = rast.FillMode;
            retVal.MultiSampleAntiAlias = rast.MultiSampleAntiAlias;
            retVal.ScissorTestEnable = rast.ScissorTestEnable;
            retVal.SlopeScaleDepthBias = rast.SlopeScaleDepthBias;
            return retVal;
        }
    }
}
