﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.MSpriteBatch
{
    public static class SpriteFontHelper
    {
        private static Dictionary<SpriteFont, Dictionary<char, SpriteFont.Glyph>> _glyphDictionaryMapping = 
            new Dictionary<SpriteFont, Dictionary<char, SpriteFont.Glyph>>();
        public static void DrawInto(MatrixSpriteBatch spriteBatch, SpriteFont font, string text, Vector2 position, Color color,
                                float rotation, Vector2 origin, Vector2 scale, SpriteEffects effect, float depth)
        {
            var flipAdjustment = Vector2.Zero;

            var flippedVert = (effect & SpriteEffects.FlipVertically) == SpriteEffects.FlipVertically;
            var flippedHorz = (effect & SpriteEffects.FlipHorizontally) == SpriteEffects.FlipHorizontally;

            if (flippedVert || flippedHorz)
            {
                Vector2 size;
                size = font.MeasureString(text);

                if (flippedHorz)
                {
                    origin.X *= -1;
                    flipAdjustment.X = -size.X;
                }

                if (flippedVert)
                {
                    origin.Y *= -1;
                    flipAdjustment.Y = font.LineSpacing - size.Y;
                }
            }

            // TODO: This looks excessive... i suspect we could do most
            // of this with simple vector math and avoid this much matrix work.

            Matrix transformation, temp;
            Matrix.CreateTranslation(-origin.X, -origin.Y, 0f, out transformation);
            Matrix.CreateScale((flippedHorz ? -scale.X : scale.X), (flippedVert ? -scale.Y : scale.Y), 1f, out temp);
            Matrix.Multiply(ref transformation, ref temp, out transformation);
            Matrix.CreateTranslation(flipAdjustment.X, flipAdjustment.Y, 0, out temp);
            Matrix.Multiply(ref temp, ref transformation, out transformation);
            Matrix.CreateRotationZ(rotation, out temp);
            Matrix.Multiply(ref transformation, ref temp, out transformation);
            Matrix.CreateTranslation(position.X, position.Y, 0f, out temp);
            Matrix.Multiply(ref transformation, ref temp, out transformation);

            // Get the default glyph here once.
            Microsoft.Xna.Framework.Graphics.SpriteFont.Glyph? defaultGlyph = null;
            Dictionary<char, SpriteFont.Glyph> _glyphs = null;
            if (!_glyphDictionaryMapping.ContainsKey(font))
            {
                _glyphDictionaryMapping.Add(font, font.GetGlyphs());
            }
            _glyphs = _glyphDictionaryMapping[font];
            
            if (font.DefaultCharacter.HasValue)
                defaultGlyph = _glyphs[font.DefaultCharacter.Value];

            var currentGlyph = Microsoft.Xna.Framework.Graphics.SpriteFont.Glyph.Empty;
            var offset = Vector2.Zero;
            var hasCurrentGlyph = false;
            var firstGlyphOfLine = true;

            for (var i = 0; i < text.Length; ++i)
            {
                var c = text[i];
                if (c == '\r')
                {
                    hasCurrentGlyph = false;
                    continue;
                }

                if (c == '\n')
                {
                    offset.X = 0;
                    offset.Y += font.LineSpacing;
                    hasCurrentGlyph = false;
                    firstGlyphOfLine = true;
                    continue;
                }

                if (hasCurrentGlyph)
                {
                    offset.X += font.Spacing + currentGlyph.Width + currentGlyph.RightSideBearing;
                }

                if (_glyphs.ContainsKey(c))
                {
                    currentGlyph = _glyphs[c];
                }
                else
                {
                    if (!defaultGlyph.HasValue)
                        throw new ArgumentException("Font contains unresolved characters");

                    currentGlyph = defaultGlyph.Value;
                }

                hasCurrentGlyph = true;

                // The first character on a line might have a negative left side bearing.
                // In this scenario, SpriteBatch/SpriteFont normally offset the text to the right,
                //  so that text does not hang off the left side of its rectangle.
                if (firstGlyphOfLine)
                {
                    offset.X = Math.Max(currentGlyph.LeftSideBearing, 0);
                    firstGlyphOfLine = false;
                }
                else
                {
                    offset.X += currentGlyph.LeftSideBearing;
                }

                var p = offset;

                if (flippedHorz)
                    p.X += currentGlyph.BoundsInTexture.Width;
                p.X += currentGlyph.Cropping.X;

                if (flippedVert)
                    p.Y += currentGlyph.BoundsInTexture.Height - font.LineSpacing;
                p.Y += currentGlyph.Cropping.Y;

                Vector2.Transform(ref p, ref transformation, out p);

                var destRect = new Vector4(p.X, p.Y,
                                            currentGlyph.BoundsInTexture.Width * scale.X,
                                            currentGlyph.BoundsInTexture.Height * scale.Y);
                
                spriteBatch.DrawInternal(
                    font.Texture, destRect, currentGlyph.BoundsInTexture,
                    color, rotation, Vector2.Zero, effect, depth, false);
            }

            // We need to flush if we're using Immediate sort mode.
            spriteBatch.FlushIfNeeded();
        }
    }
}
