using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGUI.Utilities;
using System;
using System.Collections.Generic;
using System.Text;

namespace MonoGUI.MSpriteBatch
{
    /// <summary>
    /// Helper class for drawing text strings and sprites in one or more optimized batches.
    /// </summary>
    public class MatrixSpriteBatch
    {
        readonly SpriteBatcher3D _batcher;

        static internal readonly byte[] Bytecode = LoadEffectResource(
#if DIRECTX
"Microsoft.Xna.Framework.Graphics.Effect.Resources.SpriteEffect.dx11.mgfxo"
#elif PSM
            "Microsoft.Xna.Framework.PSSuite.Graphics.Resources.SpriteEffect.cgx" //FIXME: This shader is totally incomplete
#else
            "Microsoft.Xna.Framework.Graphics.Effect.Resources.SpriteEffect.ogl.mgfxo"
#endif
);

        internal static byte[] LoadEffectResource(string name)
        {
#if WINRT
            var assembly = typeof(Effect).GetTypeInfo().Assembly;
#else
            var assembly = typeof(Effect).Assembly;
#endif
            var stream = assembly.GetManifestResourceStream(name);
            using (var ms = new System.IO.MemoryStream())
            {
                stream.CopyTo(ms);
                return ms.ToArray();
            }
        }

        SpriteSortMode _sortMode;
        BlendState _blendState;
        SamplerState _samplerState;
        DepthStencilState _depthStencilState;
        RasterizerState _rasterizerState;
        Effect _effect;
        bool _beginCalled;

        Effect _spriteEffect;
        readonly EffectParameter _matrixTransform;
        readonly EffectPass _spritePass;

        Matrix _matrix;
        Rectangle _tempRect = new Rectangle(0, 0, 0, 0);
        Vector2 _texCoordTL = new Vector2(0, 0);
        Vector2 _texCoordBR = new Vector2(0, 0);

        public Vector2 Position { get; private set; }
        public Vector2 Scale { get; private set; }
        public float Rotation { get; private set; }

        public float Depth { get; set; }
        private float _midPlane;
        private float _nearPlane;
        private float _farPlane;

        private Stack<Matrix> TransformationMatrixStack;
        private Stack<Matrix> TransformationMatrix3DStack;
        private Stack<DrawBeginInfo> BeginStack;
        private Stack<Rectangle> ClippingStack;
        private Dictionary<Texture2D, int> TextureToIdMapping;
        private int CurrentIdMapping = 0;
        private Matrix TransformationMatrix;
        private Matrix Transformation3DMatrix;

        private Stack<Color> ColorStack;
        private Color _totalColor = Color.White;
        public bool TotalColorNonWhite { get; set; }
        public Color TotalColor
        {
            get { return _totalColor; }
        }

        private GraphicsDevice GraphicsDevice;

        /// <summary>
        /// Creates a new instance of <see cref="SpriteBatch"/> class.
        /// </summary>
        /// <param name="graphicsDevice">The <see cref="GraphicsDevice"/>, which will be used for sprite rendering.</param>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="graphicsDevice"/> is null.</exception>
        public MatrixSpriteBatch(GraphicsDevice graphicsDevice)
        {
            if (graphicsDevice == null)
            {
                throw new ArgumentException("graphicsDevice");
            }

            this.GraphicsDevice = graphicsDevice;

            Depth = 100;

            // Use a custom SpriteEffect so we can control the transformation matrix
            _spriteEffect = new Effect(graphicsDevice, Bytecode);
            _matrixTransform = _spriteEffect.Parameters["MatrixTransform"];
            _spritePass = _spriteEffect.CurrentTechnique.Passes[0];

            _batcher = new SpriteBatcher3D(graphicsDevice);

            _beginCalled = false;
            TransformationMatrixStack = new Stack<Matrix>();
            TransformationMatrix3DStack = new Stack<Matrix>();
            BeginStack = new Stack<DrawBeginInfo>();
            ClippingStack = new Stack<Rectangle>();
            TextureToIdMapping = new Dictionary<Texture2D, int>();
            ColorStack = new Stack<Color>();
            UpdateTransformationMatrix();
            UpdateTransformation3DMatrix();
        }

        public void Begin(DrawBeginInfo beginInfo)
        {
            beginInfo.Restore(this);
        }

        /// <summary>
        /// Begins a new sprite and text batch with the specified render state.
        /// </summary>
        /// <param name="sortMode">The drawing order for sprite and text drawing. <see cref="SpriteSortMode.Deferred"/> by default.</param>
        /// <param name="blendState">State of the blending. Uses <see cref="BlendState.AlphaBlend"/> if null.</param>
        /// <param name="samplerState">State of the sampler. Uses <see cref="SamplerState.LinearClamp"/> if null.</param>
        /// <param name="depthStencilState">State of the depth-stencil buffer. Uses <see cref="DepthStencilState.None"/> if null.</param>
        /// <param name="rasterizerState">State of the rasterization. Uses <see cref="RasterizerState.CullCounterClockwise"/> if null.</param>
        /// <param name="effect">A custom <see cref="Effect"/> to override the default sprite effect. Uses default sprite effect if null.</param>
        /// <param name="transformMatrix">An optional matrix used to transform the sprite geometry. Uses <see cref="Matrix.Identity"/> if null.</param>
        /// <exception cref="InvalidOperationException">Thrown if <see cref="Begin"/> is called next time without previous <see cref="End"/>.</exception>
        /// <remarks>This method uses optional parameters.</remarks>
        /// <remarks>The <see cref="Begin"/> Begin should be called before drawing commands, and you cannot call it again before subsequent <see cref="End"/>.</remarks>
        public void Begin
        (
             SpriteSortMode sortMode = SpriteSortMode.Deferred,
             BlendState blendState = null,
             SamplerState samplerState = null,
             DepthStencilState depthStencilState = null,
             RasterizerState rasterizerState = null,
             Effect effect = null,
             Matrix? transformMatrix = null
        )
        {
            //BeginStack.Push(new DrawBeginInfo(_sortMode, _blendState, _samplerState, _depthStencilState, _rasterizerState, _effect, _matrix));
            FirstBegin(sortMode, blendState, samplerState, depthStencilState, rasterizerState, effect, transformMatrix);
        }

        internal void FirstBegin(DrawBeginInfo beg)
        {
            FirstBegin(beg.SortMode, beg.BlendState, beg.SamplerState, beg.DepthStencilState,
                beg.RasterizerState, beg.Effect, beg.TransformMatrix);
        }

        internal void FirstBegin
            (
             SpriteSortMode sortMode = SpriteSortMode.Deferred,
             BlendState blendState = null,
             SamplerState samplerState = null,
             DepthStencilState depthStencilState = null,
             RasterizerState rasterizerState = null,
             Effect effect = null,
             Matrix? transformMatrix = null
        )
        {
            if (_beginCalled)
                throw new InvalidOperationException("Begin cannot be called again until End has been successfully called.");

            // defaults
            _sortMode = sortMode;
            _blendState = blendState ?? BlendState.AlphaBlend;
            _samplerState = samplerState ?? SamplerState.LinearClamp;
            _depthStencilState = depthStencilState ?? DepthStencilState.None;
            _rasterizerState = rasterizerState ?? RasterizerState.CullCounterClockwise;
            _effect = effect;
            _matrix = transformMatrix ?? Matrix.Identity;

            // Setup things now so a user can change them.
            if (sortMode == SpriteSortMode.Immediate)
            {
                Setup();
            }

            _beginCalled = true;
            BeginStack.Push(new DrawBeginInfo(_sortMode, _blendState, _samplerState, _depthStencilState, _rasterizerState, _effect, _matrix));
        }

        /// <summary>
        /// Flushes all batched text and sprites to the screen.
        /// </summary>
        /// <remarks>This command should be called after <see cref="Begin"/> and drawing commands.</remarks>
        public void End()
        {
            End(true);
        }

        private void End(bool doStackPop)
        {
            if (doStackPop)
            {
                BeginStack.Pop();
            }
            FinalEnd();
        }

        internal void FinalEnd()
        {
            _beginCalled = false;

            if (_sortMode != SpriteSortMode.Immediate)
                Setup();
#if PSM   
            GraphicsDevice.BlendState = _blendState;
            _blendState.PlatformApplyState(GraphicsDevice);
#endif

            _batcher.DrawBatch(_sortMode, _effect);

        }

        void Setup()
        {
            var gd = GraphicsDevice;
            gd.BlendState = _blendState;
            gd.DepthStencilState = _depthStencilState;
            gd.RasterizerState = _rasterizerState;
            gd.SamplerStates[0] = _samplerState;

            // Setup the default sprite effect.
            var vp = gd.Viewport;
            Depth = Math.Max(vp.Width, vp.Height) * 2;
            float halfDepth = Depth / 2f;
            _nearPlane = halfDepth;
            _midPlane = Depth;
            _farPlane = Depth + halfDepth;
            Matrix projection;
#if PSM || DIRECTX
            //Matrix.CreateOrthographicOffCenter(0, vp.Width, vp.Height, 0, -1, 0, out projection);
            Matrix.CreatePerspectiveOffCenter(-vp.Width / 4f, vp.Width / 4f, vp.Height / 4f, -vp.Height / 4f, _nearPlane, _farPlane, out projection);
            projection = Matrix.Multiply(Matrix.CreateTranslation(-vp.Width / 2f, -vp.Height / 2f, 0f), projection);
#else
            // GL requires a half pixel offset to match DX.
            //Matrix.CreateOrthographicOffCenter(0, vp.Width, vp.Height, 0, 0, 1, out projection);
            Matrix.CreatePerspectiveOffCenter(-vp.Width / 4f, vp.Width / 4f, vp.Height / 4f, -vp.Height / 4f, _nearPlane, _farPlane, out projection);
            projection = Matrix.Multiply(Matrix.CreateTranslation(-vp.Width / 2f, -vp.Height / 2f, 0f), projection);
            projection.M41 += -0.5f * projection.M11;
            projection.M42 += -0.5f * projection.M22;
#endif
            Matrix.Multiply(ref _matrix, ref projection, out projection);

            _matrixTransform.SetValue(projection);
            _spritePass.Apply();
        }

        void CheckValid(Texture2D texture)
        {
            if (texture == null)
                throw new ArgumentNullException("texture");
            if (!_beginCalled)
                throw new InvalidOperationException("Draw was called, but Begin has not yet been called. Begin must be called successfully before you can call Draw.");
        }

        void CheckValid(SpriteFont spriteFont, string text)
        {
            if (spriteFont == null)
                throw new ArgumentNullException("spriteFont");
            if (text == null)
                throw new ArgumentNullException("text");
            if (!_beginCalled)
                throw new InvalidOperationException("DrawString was called, but Begin has not yet been called. Begin must be called successfully before you can call DrawString.");
        }

        void CheckValid(SpriteFont spriteFont, StringBuilder text)
        {
            if (spriteFont == null)
                throw new ArgumentNullException("spriteFont");
            if (text == null)
                throw new ArgumentNullException("text");
            if (!_beginCalled)
                throw new InvalidOperationException("DrawString was called, but Begin has not yet been called. Begin must be called successfully before you can call DrawString.");
        }

        /// <summary>
        /// Submit a sprite for drawing in the current batch.
        /// </summary>
        /// <param name="texture">A texture.</param>
        /// <param name="position">The drawing location on screen or null if <paramref name="destinationRectangle"> is used.</paramref></param>
        /// <param name="destinationRectangle">The drawing bounds on screen or null if <paramref name="position"> is used.</paramref></param>
        /// <param name="sourceRectangle">An optional region on the texture which will be rendered. If null - draws full texture.</param>
        /// <param name="origin">An optional center of rotation. Uses <see cref="Vector2.Zero"/> if null.</param>
        /// <param name="rotation">An optional rotation of this sprite. 0 by default.</param>
        /// <param name="scale">An optional scale vector. Uses <see cref="Vector2.One"/> if null.</param>
        /// <param name="color">An optional color mask. Uses <see cref="Color.White"/> if null.</param>
        /// <param name="effects">The optional drawing modificators. <see cref="SpriteEffects.None"/> by default.</param>
        /// <param name="layerDepth">An optional depth of the layer of this sprite. 0 by default.</param>
        /// <exception cref="InvalidOperationException">Throwns if both <paramref name="position"/> and <paramref name="destinationRectangle"/> been used.</exception>
        /// <remarks>This overload uses optional parameters. This overload requires only one of <paramref name="position"/> and <paramref name="destinationRectangle"/> been used.</remarks>
        public void Draw(Texture2D texture,
                Vector2? position = null,
                Rectangle? destinationRectangle = null,
                Rectangle? sourceRectangle = null,
                Vector2? origin = null,
                float rotation = 0f,
                Vector2? scale = null,
                Color? color = null,
                SpriteEffects effects = SpriteEffects.None,
                float layerDepth = 0f)
        {

            // Assign default values to null parameters here, as they are not compile-time constants
            if (!color.HasValue)
                color = Color.White;
            if (!origin.HasValue)
                origin = Vector2.Zero;
            if (!scale.HasValue)
                scale = Vector2.One;

            // If both drawRectangle and position are null, or if both have been assigned a value, raise an error
            if ((destinationRectangle.HasValue) == (position.HasValue))
            {
                throw new InvalidOperationException("Expected drawRectangle or position, but received neither or both.");
            }
            else if (position != null)
            {
                // Call Draw() using position
                Draw(texture, (Vector2)position, sourceRectangle, (Color)color, rotation, (Vector2)origin, (Vector2)scale, effects, layerDepth);
            }
            else
            {
                // Call Draw() using drawRectangle
                Draw(texture, (Rectangle)destinationRectangle, sourceRectangle, (Color)color, rotation, (Vector2)origin, effects, layerDepth);
            }
        }

        /// <summary>
        /// Submit a sprite for drawing in the current batch.
        /// </summary>
        /// <param name="texture">A texture.</param>
        /// <param name="position">The drawing location on screen.</param>
        /// <param name="sourceRectangle">An optional region on the texture which will be rendered. If null - draws full texture.</param>
        /// <param name="color">A color mask.</param>
        /// <param name="rotation">A rotation of this sprite.</param>
        /// <param name="origin">Center of the rotation. 0,0 by default.</param>
        /// <param name="scale">A scaling of this sprite.</param>
        /// <param name="effects">Modificators for drawing. Can be combined.</param>
        /// <param name="layerDepth">A depth of the layer of this sprite.</param>
        public void Draw(Texture2D texture,
                Vector2 position,
                Rectangle? sourceRectangle,
                Color color,
                float rotation,
                Vector2 origin,
                Vector2 scale,
                SpriteEffects effects,
                float layerDepth)
        {
            CheckValid(texture);

            //TODO make this color global
            Color modifiedColor = Color.White;
            if (TotalColorNonWhite || !color.Equals(Color.White))
            {
                ColorUtils.Multiply(color, TotalColor, ref modifiedColor);
            }


            var x = Position.X + position.X * Scale.X;
            var y = Position.Y + position.Y * Scale.Y;
            var w = texture.Width * scale.X * Scale.X;
            var h = texture.Height * scale.Y * Scale.Y;
            if (sourceRectangle.HasValue)
            {
                w = sourceRectangle.Value.Width * scale.X * Scale.X;
                h = sourceRectangle.Value.Height * scale.Y * Scale.Y;
            }

            DrawInternal(texture,
                new Vector4(x, y, w, h),
                sourceRectangle,
                modifiedColor,
                rotation,
                origin * scale,
                effects,
                layerDepth,
                true);
        }

        /// <summary>
        /// Submit a sprite for drawing in the current batch.
        /// </summary>
        /// <param name="texture">A texture.</param>
        /// <param name="position">The drawing location on screen.</param>
        /// <param name="sourceRectangle">An optional region on the texture which will be rendered. If null - draws full texture.</param>
        /// <param name="color">A color mask.</param>
        /// <param name="rotation">A rotation of this sprite.</param>
        /// <param name="origin">Center of the rotation. 0,0 by default.</param>
        /// <param name="scale">A scaling of this sprite.</param>
        /// <param name="effects">Modificators for drawing. Can be combined.</param>
        /// <param name="layerDepth">A depth of the layer of this sprite.</param>
        public void Draw(Texture2D texture,
                Vector2 position,
                Rectangle? sourceRectangle,
                Color color,
                float rotation,
                Vector2 origin,
                float scale,
                SpriteEffects effects,
                float layerDepth)
        {
            CheckValid(texture);


            Color modifiedColor = Color.White;
            if (TotalColorNonWhite || !color.Equals(Color.White))
            {
                ColorUtils.Multiply(color, TotalColor, ref modifiedColor);
            }

            var x = Position.X + position.X * Scale.X;
            var y = Position.Y + position.Y * Scale.Y;
            var w = texture.Width * scale * Scale.X;
            var h = texture.Height * scale * Scale.Y;
            if (sourceRectangle.HasValue)
            {
                w = sourceRectangle.Value.Width * scale * Scale.X;
                h = sourceRectangle.Value.Height * scale * Scale.Y;
            }

            DrawInternal(texture,
                new Vector4(x, y, w, h),
                sourceRectangle,
                modifiedColor,
                rotation,
                origin * scale,
                effects,
                layerDepth,
                true);
        }

        /// <summary>
        /// Submit a sprite for drawing in the current batch.
        /// </summary>
        /// <param name="texture">A texture.</param>
        /// <param name="destinationRectangle">The drawing bounds on screen.</param>
        /// <param name="sourceRectangle">An optional region on the texture which will be rendered. If null - draws full texture.</param>
        /// <param name="color">A color mask.</param>
        /// <param name="rotation">A rotation of this sprite.</param>
        /// <param name="origin">Center of the rotation. 0,0 by default.</param>
        /// <param name="effects">Modificators for drawing. Can be combined.</param>
        /// <param name="layerDepth">A depth of the layer of this sprite.</param>
        public void Draw(Texture2D texture,
            Rectangle destinationRectangle,
            Rectangle? sourceRectangle,
            Color color,
            float rotation,
            Vector2 origin,
            SpriteEffects effects,
            float layerDepth)
        {
            CheckValid(texture);

            Color modifiedColor = Color.White;
            if (TotalColorNonWhite || !color.Equals(Color.White))
            {
                ColorUtils.Multiply(color, TotalColor, ref modifiedColor);
            }

            DrawInternal(texture,
                  new Vector4(Position.X + destinationRectangle.X * Scale.X,
                              Position.Y + destinationRectangle.Y * Scale.Y,
                              destinationRectangle.Width * Scale.X,
                              destinationRectangle.Height * Scale.Y),
                  sourceRectangle,
                  modifiedColor,
                  rotation,
                  new Vector2(origin.X * ((float)destinationRectangle.Width / (float)((sourceRectangle.HasValue && sourceRectangle.Value.Width != 0) ? sourceRectangle.Value.Width : texture.Width)),
                                    origin.Y * ((float)destinationRectangle.Height) / (float)((sourceRectangle.HasValue && sourceRectangle.Value.Height != 0) ? sourceRectangle.Value.Height : texture.Height)),
                  effects,
                  layerDepth,
                  true);
        }

        internal void DrawInternal(Texture2D texture,
            Vector4 destinationRectangle,
            Rectangle? sourceRectangle,
            Color color,
            float rotation,
            Vector2 origin,
            SpriteEffects effect,
            float depth,
            bool autoFlush)
        {
            var item = _batcher.CreateBatchItem();

            item.Depth = -_midPlane;//depth;
            item.Texture = texture;
            //if (!TextureToIdMapping.ContainsKey(item.Texture))
            //{
            //    TextureToIdMapping.Add(texture, CurrentIdMapping);
            //    CurrentIdMapping++;
            //}
            //item.TextureID = TextureToIdMapping[texture];

            if (sourceRectangle.HasValue)
            {
                _tempRect = sourceRectangle.Value;
            }
            else
            {
                _tempRect.X = 0;
                _tempRect.Y = 0;
                _tempRect.Width = texture.Width;
                _tempRect.Height = texture.Height;
            }

            _texCoordTL.X = _tempRect.X / (float)texture.Width;
            _texCoordTL.Y = _tempRect.Y / (float)texture.Height;
            _texCoordBR.X = (_tempRect.X + _tempRect.Width) / (float)texture.Width;
            _texCoordBR.Y = (_tempRect.Y + _tempRect.Height) / (float)texture.Height;

            if ((effect & SpriteEffects.FlipVertically) != 0)
            {
                var temp = _texCoordBR.Y;
                _texCoordBR.Y = _texCoordTL.Y;
                _texCoordTL.Y = temp;
            }
            if ((effect & SpriteEffects.FlipHorizontally) != 0)
            {
                var temp = _texCoordBR.X;
                _texCoordBR.X = _texCoordTL.X;
                _texCoordTL.X = temp;
            }
            if (TransformationMatrix3DStack.Count > 0)
            {
                item.Set(destinationRectangle.X,
                    destinationRectangle.Y,
                    -origin.X,
                    -origin.Y,
                    destinationRectangle.Z,
                    destinationRectangle.W,
                    (float)Math.Sin(rotation),
                    (float)Math.Cos(rotation),
                    color,
                    _texCoordTL,
                    _texCoordBR, Transformation3DMatrix);
            }
            else
            {
                item.Set(destinationRectangle.X,
                    destinationRectangle.Y,
                    -origin.X,
                    -origin.Y,
                    destinationRectangle.Z,
                    destinationRectangle.W,
                    (float)Math.Sin(rotation),
                    (float)Math.Cos(rotation),
                    color,
                    _texCoordTL,
                    _texCoordBR);
            }


            if (autoFlush)
            {
                FlushIfNeeded();
            }
        }

        // Mark the end of a draw operation for Immediate SpriteSortMode.
        internal void FlushIfNeeded()
        {
            if (_sortMode == SpriteSortMode.Immediate)
            {
                _batcher.DrawBatch(_sortMode, _effect);
            }
        }

        /// <summary>
        /// Submit a sprite for drawing in the current batch.
        /// </summary>
        /// <param name="texture">A texture.</param>
        /// <param name="position">The drawing location on screen.</param>
        /// <param name="sourceRectangle">An optional region on the texture which will be rendered. If null - draws full texture.</param>
        /// <param name="color">A color mask.</param>
        public void Draw(Texture2D texture, Vector2 position, Rectangle? sourceRectangle, Color color)
        {
            Draw(texture, position, sourceRectangle, color, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
        }

        /// <summary>
        /// Submit a sprite for drawing in the current batch.
        /// </summary>
        /// <param name="texture">A texture.</param>
        /// <param name="destinationRectangle">The drawing bounds on screen.</param>
        /// <param name="sourceRectangle">An optional region on the texture which will be rendered. If null - draws full texture.</param>
        /// <param name="color">A color mask.</param>
        public void Draw(Texture2D texture, Rectangle destinationRectangle, Rectangle? sourceRectangle, Color color)
        {
            Draw(texture, destinationRectangle, sourceRectangle, color, 0, Vector2.Zero, SpriteEffects.None, 0f);
        }

        /// <summary>
        /// Submit a sprite for drawing in the current batch.
        /// </summary>
        /// <param name="texture">A texture.</param>
        /// <param name="position">The drawing location on screen.</param>
        /// <param name="color">A color mask.</param>
        public void Draw(Texture2D texture, Vector2 position, Color color)
        {
            Draw(texture, position, null, color);
        }

        /// <summary>
        /// Submit a sprite for drawing in the current batch.
        /// </summary>
        /// <param name="texture">A texture.</param>
        /// <param name="destinationRectangle">The drawing bounds on screen.</param>
        /// <param name="color">A color mask.</param>
        public void Draw(Texture2D texture, Rectangle destinationRectangle, Color color)
        {
            Draw(texture, destinationRectangle, null, color);
        }

        /// <summary>
        /// Submit a text string of sprites for drawing in the current batch.
        /// </summary>
        /// <param name="spriteFont">A font.</param>
        /// <param name="text">The text which will be drawn.</param>
        /// <param name="position">The drawing location on screen.</param>
        /// <param name="color">A color mask.</param>
        public void DrawString(SpriteFont spriteFont, string text, Vector2 position, Color color)
        {
            CheckValid(spriteFont, text);
            float x = Position.X + (position.X * Scale.X);
            float y = Position.Y + (position.Y * Scale.Y);
            var source = text;
            SpriteFontHelper.DrawInto(this, spriteFont, text, new Vector2(x, y), color, 0, Vector2.Zero, Vector2.One, SpriteEffects.None, 0f);
        }

        /// <summary>
        /// Submit a text string of sprites for drawing in the current batch.
        /// </summary>
        /// <param name="spriteFont">A font.</param>
        /// <param name="text">The text which will be drawn.</param>
        /// <param name="position">The drawing location on screen.</param>
        /// <param name="color">A color mask.</param>
        /// <param name="rotation">A rotation of this string.</param>
        /// <param name="origin">Center of the rotation. 0,0 by default.</param>
        /// <param name="scale">A scaling of this string.</param>
        /// <param name="effects">Modificators for drawing. Can be combined.</param>
        /// <param name="layerDepth">A depth of the layer of this string.</param>
        public void DrawString(
            SpriteFont spriteFont, string text, Vector2 position, Color color,
            float rotation, Vector2 origin, float scale, SpriteEffects effects, float layerDepth)
        {
            //CheckValid(spriteFont, text);

            //var scaleVec = new Vector2(scale, scale);
            //var source = new SpriteFont.CharacterSource(text);
            //spriteFont.DrawInto(this, ref source, position, color, rotation, origin, scaleVec, effects, layerDepth);
        }

        /// <summary>
        /// Submit a text string of sprites for drawing in the current batch.
        /// </summary>
        /// <param name="spriteFont">A font.</param>
        /// <param name="text">The text which will be drawn.</param>
        /// <param name="position">The drawing location on screen.</param>
        /// <param name="color">A color mask.</param>
        /// <param name="rotation">A rotation of this string.</param>
        /// <param name="origin">Center of the rotation. 0,0 by default.</param>
        /// <param name="scale">A scaling of this string.</param>
        /// <param name="effects">Modificators for drawing. Can be combined.</param>
        /// <param name="layerDepth">A depth of the layer of this string.</param>
        public void DrawString(
            SpriteFont spriteFont, string text, Vector2 position, Color color,
            float rotation, Vector2 origin, Vector2 scale, SpriteEffects effects, float layerDepth)
        {
            //CheckValid(spriteFont, text);

            //var source = new SpriteFont.CharacterSource(text);
            //spriteFont.DrawInto(this, ref source, position, color, rotation, origin, scale, effects, layerDepth);
        }

        /// <summary>
        /// Submit a text string of sprites for drawing in the current batch.
        /// </summary>
        /// <param name="spriteFont">A font.</param>
        /// <param name="text">The text which will be drawn.</param>
        /// <param name="position">The drawing location on screen.</param>
        /// <param name="color">A color mask.</param>
        public void DrawString(SpriteFont spriteFont, StringBuilder text, Vector2 position, Color color)
        {
            //CheckValid(spriteFont, text);

            //var source = new SpriteFont.CharacterSource(text);
            //spriteFont.DrawInto(this, ref source, position, color, 0, Vector2.Zero, Vector2.One, SpriteEffects.None, 0f);
        }

        /// <summary>
        /// Submit a text string of sprites for drawing in the current batch.
        /// </summary>
        /// <param name="spriteFont">A font.</param>
        /// <param name="text">The text which will be drawn.</param>
        /// <param name="position">The drawing location on screen.</param>
        /// <param name="color">A color mask.</param>
        /// <param name="rotation">A rotation of this string.</param>
        /// <param name="origin">Center of the rotation. 0,0 by default.</param>
        /// <param name="scale">A scaling of this string.</param>
        /// <param name="effects">Modificators for drawing. Can be combined.</param>
        /// <param name="layerDepth">A depth of the layer of this string.</param>
        public void DrawString(
            SpriteFont spriteFont, StringBuilder text, Vector2 position, Color color,
            float rotation, Vector2 origin, float scale, SpriteEffects effects, float layerDepth)
        {
            //CheckValid(spriteFont, text);

            //var scaleVec = new Vector2(scale, scale);
            //var source = new SpriteFont.CharacterSource(text);
            //spriteFont.DrawInto(this, ref source, position, color, rotation, origin, scaleVec, effects, layerDepth);
        }

        /// <summary>
        /// Submit a text string of sprites for drawing in the current batch.
        /// </summary>
        /// <param name="spriteFont">A font.</param>
        /// <param name="text">The text which will be drawn.</param>
        /// <param name="position">The drawing location on screen.</param>
        /// <param name="color">A color mask.</param>
        /// <param name="rotation">A rotation of this string.</param>
        /// <param name="origin">Center of the rotation. 0,0 by default.</param>
        /// <param name="scale">A scaling of this string.</param>
        /// <param name="effects">Modificators for drawing. Can be combined.</param>
        /// <param name="layerDepth">A depth of the layer of this string.</param>
        public void DrawString(
            SpriteFont spriteFont, StringBuilder text, Vector2 position, Color color,
            float rotation, Vector2 origin, Vector2 scale, SpriteEffects effects, float layerDepth)
        {
            //CheckValid(spriteFont, text);

            //var source = new SpriteFont.CharacterSource(text);
            //spriteFont.DrawInto(this, ref source, position, color, rotation, origin, scale, effects, layerDepth);
        }

        public void HaltDraw()
        {
            End(false);
        }

        public void ResumeDraw()
        {
            BeginStack.Pop().Restore(this);
        }

        public DrawBeginInfo ModifiableBeginCall()
        {
            if (BeginStack.Count > 0)
            {
                return BeginStack.Peek().Clone();
            }
            return null;
        }

        public void PushMatrix()
        {
            //TODO switching to actual opengl style matrix might have more problems, look into this
            TransformationMatrixStack.Push(TransformationMatrix);
            //UpdateTransformationMatrix();
        }

        public Matrix PopMatrix()
        {
            Matrix retVal = TransformationMatrixStack.Pop();
            if (TransformationMatrixStack.Count == 0)
            {
                TransformationMatrix = Matrix.Identity;
            }
            else
            {
                TransformationMatrix = TransformationMatrixStack.Peek();
            }
            UpdateTransformationMatrix();
            return retVal;
        }

        public void LoadMatrix(Matrix m)
        {
            TransformationMatrix = m;
            UpdateTransformationMatrix();
        }

        public void Push3DMatrix(Matrix m)
        {
            TransformationMatrix3DStack.Push(m);
            UpdateTransformation3DMatrix();
        }

        public Matrix Pop3DMatrix()
        {
            Matrix retVal = TransformationMatrix3DStack.Pop();
            UpdateTransformation3DMatrix();
            return retVal;
        }

        private void UpdateTransformationMatrix()
        {
            Vector3 position3, scale3;
            Quaternion rotationQ;

            TransformationMatrix.Decompose(out scale3, out rotationQ, out position3);

            Vector2 direction = Vector2.Transform(Vector2.UnitX, rotationQ);
            Rotation = (float)Math.Atan2((double)(direction.Y), (double)(direction.X));
            Position = new Vector2(position3.X, position3.Y);
            Scale = new Vector2(scale3.X, scale3.Y);
        }

        private void UpdateTransformation3DMatrix()
        {
            Transformation3DMatrix = Matrix.Identity;

            foreach (Matrix m in TransformationMatrix3DStack)
            {
                Transformation3DMatrix *= m;
            }
        }

        /// <summary>
        /// This must be called while spritebatch.begin is open
        /// </summary>
        /// <param name="rect"></param>
        /// <param name="doClip"></param>
        public void PushClippingRectangle(Rectangle rect, bool doClip)
        {
            HaltDraw();
            DrawBeginInfo mod = ModifiableBeginCall();
            RasterizerState rast = mod.RasterizerState;
            if (rast == null)
            {
                rast = new RasterizerState();
            }
            rast.ScissorTestEnable = doClip;
            mod.RasterizerState = rast;
            Rectangle curRect = GraphicsDevice.ScissorRectangle;
            int x = (int)(Position.X + (rect.X * Scale.X));
            int y = (int)(Position.Y + (rect.Y * Scale.Y));
            int width = (int)(Scale.X * rect.Width);
            int height = (int)(Scale.Y * rect.Height);

            Rectangle newDestination = new Rectangle(x, y, width, height);
            Rectangle intersected = Rectangle.Intersect(curRect, newDestination);
            GraphicsDevice.ScissorRectangle = intersected;
            ClippingStack.Push(curRect);
            Begin(mod);
        }

        public void PopClippingRectangle()
        {
            End();
            GraphicsDevice.ScissorRectangle = ClippingStack.Pop();
            ResumeDraw();
        }

        public void PushColor(Color color)
        {
            ColorStack.Push(color);
            ColorUtils.Multiply(_totalColor, color, ref _totalColor);
            if (!_totalColor.Equals(Color.White))
            {
                TotalColorNonWhite = true;
            }
            else
            {
                TotalColorNonWhite = false;
            }
        }

        public Color PopColor()
        {
            Color retVal = ColorStack.Pop();
            _totalColor = Color.White;
            foreach (Color c in ColorStack)
            {
                ColorUtils.Multiply(_totalColor, c, ref _totalColor);
            }
            if (!_totalColor.Equals(Color.White))
            {
                TotalColorNonWhite = true;
            }
            else
            {
                TotalColorNonWhite = false;
            }
            return retVal;
        }
    }
}

