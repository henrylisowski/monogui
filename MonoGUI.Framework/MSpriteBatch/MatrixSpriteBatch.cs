﻿//using Microsoft.Xna.Framework;
//using Microsoft.Xna.Framework.Graphics;
//using MonoGUI.Utilities;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//namespace MonoGUI.MSpriteBatch
//{
//    public class MatrixSpriteBatch
//    {
//        private SpriteBatch SpriteBatch;
//        private Stack<Matrix> TransformationMatrixStack;
//        private Stack<DrawBeginInfo> BeginStack;
//        private Stack<Rectangle> ClippingStack;

//        public enum RectangleDrawPrecision { IntClamp, Float };
//        public RectangleDrawPrecision DrawPrecision;
        
        
//        private Matrix TransformationMatrix;

//        public Vector2 Position { get; private set; }
//        public Vector2 Scale { get; private set; }
//        public float Rotation { get; private set; }

//        private Stack<Color> ColorStack;
//        private Color _totalColor = Color.White;
//        public Color TotalColor
//        {
//            get { return _totalColor; }
//        }

//        public GraphicsDevice GraphicsDevice
//        {
//            get { return SpriteBatch.GraphicsDevice; }
//        }

//        public MatrixSpriteBatch(SpriteBatch SpriteBatch)
//        {
//            this.SpriteBatch = SpriteBatch;
            
//            TransformationMatrixStack = new Stack<Matrix>();
//            BeginStack = new Stack<DrawBeginInfo>();
//            ClippingStack = new Stack<Rectangle>();
//            ColorStack = new Stack<Color>();
//            DrawPrecision = RectangleDrawPrecision.IntClamp;
//            UpdateTransformationMatrix();
//        }

//        public void Begin()
//        {
//            SpriteBatch.Begin();
//            BeginStack.Push(new DrawBeginInfo());
//        }

//        public void Begin(SpriteSortMode sortMode, BlendState blendState)
//        {
//            SpriteBatch.Begin(sortMode, blendState);
//            BeginStack.Push(new DrawBeginInfo(sortMode, blendState));
//        }

//        public void Begin(SpriteSortMode sortMode, BlendState blendState, SamplerState samplerState, 
//            DepthStencilState depthStencilState, RasterizerState rasterizerState)
//        {
//            SpriteBatch.Begin(sortMode, blendState, samplerState, depthStencilState, rasterizerState);
//            BeginStack.Push(new DrawBeginInfo(sortMode, blendState, samplerState, depthStencilState, rasterizerState));
//        }

//        public void Begin(SpriteSortMode sortMode, BlendState blendState, SamplerState samplerState,
//            DepthStencilState depthStencilState, RasterizerState rasterizerState, Effect effect)
//        {
//            SpriteBatch.Begin(sortMode, blendState, samplerState, depthStencilState, rasterizerState, effect);
//            BeginStack.Push(new DrawBeginInfo(sortMode, blendState, samplerState, depthStencilState, rasterizerState, effect));
//        }

//        public void Begin(SpriteSortMode sortMode, BlendState blendState, SamplerState samplerState,
//            DepthStencilState depthStencilState, RasterizerState rasterizerState, Effect effect, Matrix transformMatrix)
//        {
//            SpriteBatch.Begin(sortMode, blendState, samplerState, depthStencilState, rasterizerState, effect, transformMatrix);
//            BeginStack.Push(new DrawBeginInfo(sortMode, blendState, samplerState, depthStencilState, rasterizerState, effect, transformMatrix));
//        }

//        public void Begin(DrawBeginInfo beginInfo)
//        {
//            beginInfo.Restore(this);
//            //Note: All other begin methods take care of pushing this beginInfo
//        }

//        public void End()
//        {
//            SpriteBatch.End();
//            BeginStack.Pop();
//        }

//        public void HaltDraw()
//        {
//            SpriteBatch.End();
//        }

//        public void ResumeDraw()
//        {
//            BeginStack.Pop().Restore(this);
//        }

//        public DrawBeginInfo ModifiableBeginCall()
//        {
//            if (BeginStack.Count > 0)
//            {
//                return BeginStack.Peek().Clone();
//            }
//            return null;
//        }

//        public void PushMatrix(Matrix m)
//        {
//            TransformationMatrixStack.Push(m);
//            UpdateTransformationMatrix();
//        }

//        public Matrix PopMatrix()
//        {
//            Matrix retVal = TransformationMatrixStack.Pop();
//            UpdateTransformationMatrix();
//            return retVal;
//        }

//        /// <summary>
//        /// This must be called while spritebatch.begin is open
//        /// </summary>
//        /// <param name="rect"></param>
//        /// <param name="doClip"></param>
//        public void PushClippingRectangle(Rectangle rect, bool doClip)
//        {
//            HaltDraw();
//            DrawBeginInfo mod = ModifiableBeginCall();
//            RasterizerState rast = mod.RasterizerState;
//            if (rast == null)
//            {
//                rast = new RasterizerState();
//            }
//            rast.ScissorTestEnable = doClip;
//            mod.RasterizerState = rast;
//            Rectangle curRect = SpriteBatch.GraphicsDevice.ScissorRectangle;
//            int x = (int)(Position.X + (rect.X * Scale.X));
//            int y = (int)(Position.Y + (rect.Y * Scale.Y));
//            int width = (int)(Scale.X * rect.Width);
//            int height = (int)(Scale.Y * rect.Height);

//            Rectangle newDestination = new Rectangle(x, y, width, height);
//            Rectangle intersected = Rectangle.Intersect(curRect, newDestination);
//            SpriteBatch.GraphicsDevice.ScissorRectangle = intersected;
//            ClippingStack.Push(curRect);
//            Begin(mod);
//        }

//        public void PopClippingRectangle()
//        {
//            End();
//            SpriteBatch.GraphicsDevice.ScissorRectangle = ClippingStack.Pop();
//            ResumeDraw();
//        }

//        public void PushColor(Color color)
//        {
//            ColorStack.Push(color);
//            _totalColor = ColorUtils.Multiply(_totalColor, color);
//        }

//        public Color PopColor()
//        {
//            Color retVal = ColorStack.Pop();
//            _totalColor = Color.White;
//            foreach (Color c in ColorStack)
//            {
//                _totalColor = ColorUtils.Multiply(_totalColor, c);
//            }
//            return retVal;
//        }

//        public void Draw(Texture2D texture, Rectangle destination, Color color)
//        {
//            if (DrawPrecision == RectangleDrawPrecision.IntClamp)
//            {
//                int x = (int)(Position.X + (destination.X * Scale.X));
//                int y = (int)(Position.Y + (destination.Y * Scale.Y));
//                int width = (int)(Scale.X * destination.Width);
//                int height = (int)(Scale.Y * destination.Height);
//                Color modifiedColor = ColorUtils.Multiply(color, TotalColor);

//                Rectangle newDestination = new Rectangle(x, y, width, height);
//                SpriteBatch.Draw(texture, newDestination, null, modifiedColor, Rotation, Vector2.Zero, SpriteEffects.None, 0f);
//            }
//            else if(DrawPrecision == RectangleDrawPrecision.Float)
//            {
//                float x = (Position.X + (destination.X * Scale.X));
//                float y = (Position.Y + (destination.Y * Scale.Y));
//                float width = (Scale.X * destination.Width);
//                float height = (Scale.Y * destination.Height);
//                Vector2 vDestination = new Vector2(x, y);
//                Vector2 vScale = new Vector2(width, height) / new Vector2(texture.Width, texture.Height);
//                Draw(texture, vDestination, null, color, 0f, Vector2.Zero, vScale, SpriteEffects.None, 0f);
//            }
//        }

//        public void Draw(Texture2D texture, Vector2 destination, Color color)
//        {
//            float x = (Position.X + (destination.X * Scale.X));
//            float y = (Position.Y + (destination.Y * Scale.Y));
//            float scaleX = (Scale.X);
//            float scaleY = (Scale.Y);
//            Color modifiedColor = ColorUtils.Multiply(color, TotalColor);

//            Vector2 positionVector = new Vector2(x, y);
//            Vector2 scaleVector = new Vector2(scaleX, scaleY);

//            SpriteBatch.Draw(texture, positionVector, null, modifiedColor, Rotation, Vector2.Zero, scaleVector, SpriteEffects.None, 0f);
//        }

//        public void Draw(Texture2D texture, Rectangle destination, Nullable<Rectangle> source, Color color)
//        {
//            if (DrawPrecision == RectangleDrawPrecision.IntClamp)
//            {
//                int x = (int)(Position.X + (destination.X * Scale.X));
//                int y = (int)(Position.Y + (destination.Y * Scale.Y));
//                int width = (int)(Scale.X * destination.Width);
//                int height = (int)(Scale.Y * destination.Height);
//                Color modifiedColor = ColorUtils.Multiply(color, TotalColor);

//                Rectangle newDestination = new Rectangle(x, y, width, height);
//                SpriteBatch.Draw(texture, newDestination, source, modifiedColor, Rotation, Vector2.Zero, SpriteEffects.None, 0f);
//            }
//            else if (DrawPrecision == RectangleDrawPrecision.Float)
//            {
//                float x = (Position.X + (destination.X * Scale.X));
//                float y = (Position.Y + (destination.Y * Scale.Y));
//                float width = (Scale.X * destination.Width);
//                float height = (Scale.Y * destination.Height);
//                Vector2 vDestination = new Vector2(x, y);
//                Vector2 vScale = new Vector2(width, height) / 
//                    (source==null?new Vector2(texture.Width, texture.Height):new Vector2(source.Value.Width, source.Value.Height));
//                Draw(texture, vDestination, source, color, 0f, Vector2.Zero, vScale, SpriteEffects.None, 0f);
//            }
//        }

//        public void Draw(Texture2D texture, Vector2 destination, Nullable<Rectangle> source, Color color)
//        {
//            float x = (Position.X + (destination.X * Scale.X));
//            float y = (Position.Y + (destination.Y * Scale.Y));
//            float scaleX = (Scale.X);
//            float scaleY = (Scale.Y);
//            Color modifiedColor = ColorUtils.Multiply(color, TotalColor);

//            Vector2 positionVector = new Vector2(x, y);
//            Vector2 scaleVector = new Vector2(scaleX, scaleY);

//            SpriteBatch.Draw(texture, positionVector, source, modifiedColor, Rotation, Vector2.Zero, scaleVector, SpriteEffects.None, 0f);
//        }

//        public void Draw(Texture2D texture, Rectangle destination, Nullable<Rectangle> source, Color color,
//            Single rotation, Vector2 origin, SpriteEffects effects, Single layer)
//        {
//            if (DrawPrecision == RectangleDrawPrecision.IntClamp)
//            {
//                int x = (int)(Position.X + (destination.X * Scale.X));
//                int y = (int)(Position.Y + (destination.Y * Scale.Y));
//                int width = (int)(Scale.X * destination.Width);
//                int height = (int)(Scale.Y * destination.Height);
//                Color modifiedColor = ColorUtils.Multiply(color, TotalColor);

//                Rectangle newDestination = new Rectangle(x, y, width, height);
//                SpriteBatch.Draw(texture, newDestination, source, modifiedColor, Rotation + rotation, origin, effects, layer);
//            }
//            else if (DrawPrecision == RectangleDrawPrecision.Float)
//            {
//                float x = (Position.X + (destination.X * Scale.X));
//                float y = (Position.Y + (destination.Y * Scale.Y));
//                float width = (Scale.X * destination.Width);
//                float height = (Scale.Y * destination.Height);
//                Vector2 vDestination = new Vector2(x, y);
//                Vector2 vScale = new Vector2(width, height) / 
//                    (source == null ? new Vector2(texture.Width, texture.Height) : new Vector2(source.Value.Width, source.Value.Height));
//                Draw(texture, vDestination, source, color, Rotation + rotation, origin, vScale, effects, layer);
//            }
            
//        }

//        public void Draw(Texture2D texture, Vector2 destination, Nullable<Rectangle> source, Color color,
//            Single rotation, Vector2 origin, float scale, SpriteEffects effects, Single layer)
//        {
//            float x = (Position.X + (destination.X * Scale.X));
//            float y = (Position.Y + (destination.Y * Scale.Y));
//            float scaleX = (Scale.X * scale);
//            float scaleY = (Scale.Y * scale);
//            Color modifiedColor = ColorUtils.Multiply(color, TotalColor);

//            Vector2 positionVector = new Vector2(x, y);
//            Vector2 scaleVector = new Vector2(scaleX, scaleY);

//            SpriteBatch.Draw(texture, positionVector, source, modifiedColor, Rotation + rotation, origin, scaleVector, effects, layer);
//        }

//        public void Draw(Texture2D texture, Vector2 destination, Nullable<Rectangle> source, Color color,
//            Single rotation, Vector2 origin, Vector2 scale, SpriteEffects effects, Single layer)
//        {
//            float x = (Position.X + (destination.X * Scale.X));
//            float y = (Position.Y + (destination.Y * Scale.Y));
//            float scaleX = (Scale.X * scale.X);
//            float scaleY = (Scale.Y * scale.Y);
//            Color modifiedColor = ColorUtils.Multiply(color, TotalColor);

//            Vector2 positionVector = new Vector2(x, y);
//            Vector2 scaleVector = new Vector2(scaleX, scaleY);

//            SpriteBatch.Draw(texture, positionVector, source, modifiedColor, Rotation + rotation, origin, scaleVector, effects, layer);
//        }

//        public void DrawString(SpriteFont spriteFont, string text, Vector2 position, Color color)
//        {
//            float x = Position.X + (position.X * Scale.X);
//            float y = Position.Y + (position.Y * Scale.Y);
//            Color modifiedColor = ColorUtils.Multiply(color, TotalColor);
//            SpriteBatch.DrawString(spriteFont, text, new Vector2(x, y), modifiedColor, 0f, Vector2.Zero, Scale, SpriteEffects.None, 0f);
//        }

//        public void DrawString(SpriteFont spriteFont, StringBuilder text, Vector2 position, Color color)
//        {
//            float x = Position.X + (position.X * Scale.X);
//            float y = Position.Y + (position.Y * Scale.Y);
//            Color modifiedColor = ColorUtils.Multiply(color, TotalColor);
//            SpriteBatch.DrawString(spriteFont, text, new Vector2(x, y), modifiedColor, 0f, Vector2.Zero, Scale, SpriteEffects.None, 0f);
//        }

//        public void DrawString(SpriteFont spriteFont, string text, Vector2 position, Color color, float rotation, Vector2 origin, 
//            float scale, SpriteEffects effects, float depth)
//        {
//            float x = Position.X + (position.X * Scale.X);
//            float y = Position.Y + (position.Y * Scale.Y);
//            Color modifiedColor = ColorUtils.Multiply(color, TotalColor);
//            SpriteBatch.DrawString(spriteFont, text, new Vector2(x, y), modifiedColor, rotation, origin, scale * Scale, effects, depth);
//        }

//        private void UpdateTransformationMatrix()
//        {
//            TransformationMatrix = Matrix.Identity;
            
//            foreach (Matrix m in TransformationMatrixStack)
//            {
//                TransformationMatrix *= m;
//            }

//            Vector3 position3, scale3;
//            Quaternion rotationQ;

//            TransformationMatrix.Decompose(out scale3, out rotationQ, out position3);

//            Vector2 direction = Vector2.Transform(Vector2.UnitX, rotationQ);
//            Rotation = (float)Math.Atan2((double)(direction.Y), (double)(direction.X));
//            Position = new Vector2(position3.X, position3.Y);
//            Scale = new Vector2(scale3.X, scale3.Y);
//        }
//    }
//}
