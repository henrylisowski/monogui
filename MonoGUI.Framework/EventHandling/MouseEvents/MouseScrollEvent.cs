using Microsoft.Xna.Framework;
using MonoGUI.Manager.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace MonoGUI.EventHandling.MouseEvents
{
    public class MouseScrollEvent : EventArgs
    {
        public float ScrollAmount { get; private set; }

        public MouseScrollEvent(float ScrollAmount)
        {
            this.ScrollAmount = ScrollAmount;
        }
    }
}
