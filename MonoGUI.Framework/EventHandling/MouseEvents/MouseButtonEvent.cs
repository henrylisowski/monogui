﻿using MonoGUI.Manager.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.EventHandling.MouseEvents
{
    public class MouseButtonEvent : EventArgs
    {
        public MouseEvent MouseEvent { get; private set; }
        public MouseButton Button { get; private set; }

        public MouseButtonEvent(MouseEvent MouseEvent, MouseButton Button)
        {
            this.MouseEvent = MouseEvent;
            this.Button = Button;
        }

        public void NewValues(MouseEvent MouseEvent, MouseButton Button)
        {
            this.MouseEvent = MouseEvent;
            this.Button = Button;
        }
    }
}
