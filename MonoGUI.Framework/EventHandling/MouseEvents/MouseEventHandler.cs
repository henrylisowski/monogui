﻿using Microsoft.Xna.Framework;
using MonoGUI.Manager.Input;
using MonoGUI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.EventHandling.MouseEvents
{
    public class MouseEventHandler : InputEventHandler
    {
        private bool previousInBox;
        private Dictionary<MouseButton, bool> buttonStatus;

        public delegate void MouseMovedEvent(MouseEvent mouseEvent);
        public delegate void MouseButtonEvent(MouseEvent mouseEvent, MouseButton b);

        public MouseMovedEvent MouseEntered { private get; set; }
        public MouseMovedEvent MouseExited { private get; set; }
        public MouseMovedEvent MouseMoved { private get; set; }
        public MouseButtonEvent MousePressed { private get; set; }
        public MouseButtonEvent MouseReleased { private get; set; }
        public MouseButtonEvent MouseClicked { private get; set; }

        public MouseEventHandler()
        {
            previousInBox = false;
            buttonStatus = new Dictionary<MouseButton, bool>();
            foreach (MouseButton b in EnumUtils.MouseButton)
            {
                buttonStatus.Add(b, false);
            }

            MouseEntered = EmptyMouseMovedFunction;
            MouseExited = EmptyMouseMovedFunction;
            MouseMoved = EmptyMouseMovedFunction;
            MousePressed = EmptyMouseButtonFunction;
            MouseReleased = EmptyMouseButtonFunction;
            MouseClicked = EmptyMouseButtonFunction;
        }
        public override void Update(InputManager input, ScreenItems.ParentNode parent)
        {
            if (!input.MouseTouchHandled)
            {
                MouseEvent mouseEvent = GetEvent(input, parent);

                if (mouseEvent.Delta.X == 0 && mouseEvent.Delta.Y == 0)
                {
                    MouseMoved(mouseEvent);
                }
                Vector2 dimensions = parent.RelativeDimensions;
                bool mouseInBox = mouseEvent.Position.X > 0 && mouseEvent.Position.Y > 0 &&
                    mouseEvent.Position.X < dimensions.X && mouseEvent.Position.Y < dimensions.Y;
                if (mouseInBox)
                {
                    if (!previousInBox)
                    {
                        MouseEntered(mouseEvent);
                    }
                    previousInBox = true;
                }
                else
                {
                    if (previousInBox)
                    {
                        MouseExited(mouseEvent);
                    }
                    previousInBox = false;
                }
                foreach (MouseButton b in EnumUtils.MouseButton)
                {
                    bool prevStatus = buttonStatus[b];
                    if (mouseInBox)
                    {
                        if (input.MousePress(b))
                        {
                            buttonStatus[b] = true;
                            MousePressed(mouseEvent, b);
                        }
                        else if (input.MouseRelease(b))
                        {
                            MouseReleased(mouseEvent, b);
                            if (prevStatus)
                            {
                                MouseClicked(mouseEvent, b);
                            }
                        }
                    }
                    if (input.MouseRelease(b))
                    {
                        buttonStatus[b] = false;
                    }
                }
            }
            
        }

        private void EmptyMouseMovedFunction(MouseEvent mouseEvent)
        {

        }

        private void EmptyMouseButtonFunction(MouseEvent mouseEvent, MouseButton b)
        {

        }

        private MouseEvent GetEvent(InputManager input, ScreenItems.ParentNode parent)
        {
            Matrix inverse = parent.AbsoluteInverseAnimatedTransformation;

            Vector3 position3, scale3;
            Quaternion rotationQ;

            inverse.Decompose(out scale3, out rotationQ, out position3);
            Vector2 direction = Vector2.Transform(Vector2.UnitX, rotationQ);
            float Rotation = (float)Math.Atan2((double)(direction.Y), (double)(direction.X));

            Matrix deltaTransform = Matrix.CreateScale(scale3) * Matrix.CreateRotationZ(Rotation);

            Vector2 transformedPosition = input.MousePosition(inverse);
            MouseEvent mouseEvent = new MouseEvent(Vector2.Transform(input.MousePosition(), inverse),
                Vector2.Transform(input.MouseDelta(), deltaTransform));
            return mouseEvent;
        }
    }
}
