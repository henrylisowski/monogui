﻿using MonoGUI.Manager.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.EventHandling.MouseEvents
{
    public class MouseHoldEvent : EventArgs
    {
        public MouseEvent MouseEvent { get; private set; }
        public MouseButton Button { get; private set; }
        public float ElapsedTime { get; private set; }

        public MouseHoldEvent(MouseEvent MouseEvent, MouseButton Button, float ElapsedTime)
        {
            this.MouseEvent = MouseEvent;
            this.Button = Button;
            this.ElapsedTime = ElapsedTime;
        }
    }
}
