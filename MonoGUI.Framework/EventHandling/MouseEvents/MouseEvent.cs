﻿using Microsoft.Xna.Framework;
using MonoGUI.Manager.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace MonoGUI.EventHandling.MouseEvents
{
    public class MouseEvent : EventArgs
    {
        public Vector2 Position { get; private set; }
        public Vector2 Delta { get; private set; }

        public MouseEvent(Vector2 Position, Vector2 Delta)
        {
            this.Position = Position;
            this.Delta = Delta;
        }

        public void NewValues(Vector2 Position, Vector2 Delta)
        {
            this.Position = Position;
            this.Delta = Delta;
        }
    }
}
