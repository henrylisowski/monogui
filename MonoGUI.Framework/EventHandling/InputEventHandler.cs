﻿using MonoGUI.Manager.Input;
using MonoGUI.ScreenItems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.EventHandling
{
    public abstract class InputEventHandler
    {
        public abstract void Update(InputManager input, ParentNode parent);
    }
}
