﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input.Touch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.EventHandling.TouchEvents
{
    public class TouchEvent : EventArgs
    {
        public int Id { get; private set; }
        public Vector2 Position { get; private set; }
        public Vector2 Delta { get; private set; }
        public float Pressure { get; private set; }
        public TouchLocationState State { get; private set; }

        public TouchEvent(int Id, Vector2 Position, Vector2 Delta, float Pressure, TouchLocationState State)
        {
            this.Id = Id;
            this.Position = Position;
            this.Delta = Delta;
            this.Pressure = Pressure;
            this.State = State;
        }

        public void NewValues(int Id, Vector2 Position, Vector2 Delta, float Pressure, TouchLocationState State)
        {
            this.Id = Id;
            this.Position = Position;
            this.Delta = Delta;
            this.Pressure = Pressure;
            this.State = State;
        }
    }
}
