﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input.Touch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.EventHandling.TouchEvents
{
    public class TouchHoldEvent : EventArgs
    {
        public TouchEvent Touch { get; private set; }
        public float HoldDuration { get; private set; }

        public TouchHoldEvent(TouchEvent Touch, float HoldDuration)
        {
            this.Touch = Touch;
            this.HoldDuration = HoldDuration;
        }
    }
}
