﻿using Microsoft.Xna.Framework;
using MonoGUI.Manager.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.EventHandling.TouchEvents
{
    public class TouchEventHandler : InputEventHandler
    {
        private bool previousInBox;
        private Dictionary<int, bool> idStatus;

        public delegate void TouchEventResponse(TouchEvent ev);

        public TouchEventResponse TouchEntered { private get; set; }
        public TouchEventResponse TouchExited { private get; set; }
        public TouchEventResponse TouchMoved { private get; set; }
        public TouchEventResponse TouchPressed { private get; set; }
        public TouchEventResponse TouchReleased { private get; set; }
        public TouchEventResponse TouchClicked { private get; set; }

        public TouchEventHandler()
        {
            previousInBox = false;
            idStatus = new Dictionary<int, bool>();

            TouchEntered = EmptyTouchFunction;
            TouchExited = EmptyTouchFunction;
            TouchMoved = EmptyTouchFunction;
            TouchPressed = EmptyTouchFunction;
            TouchReleased = EmptyTouchFunction;
            TouchClicked = EmptyTouchFunction;
        }
        public override void Update(InputManager input, ScreenItems.ParentNode parent)
        {
            if (!input.MouseTouchHandled)
            {
                foreach (TouchPointState unmodifiedState in input.TouchStates())
                {
                    TouchEvent touchState = GetEvent(unmodifiedState, parent);

                    if (touchState.State == Microsoft.Xna.Framework.Input.Touch.TouchLocationState.Moved)
                    {
                        TouchMoved(touchState);
                    }
                    Vector2 dimensions = parent.RelativeDimensions;
                    bool mouseInBox = touchState.Position.X > 0 && touchState.Position.Y > 0 &&
                        touchState.Position.X < dimensions.X && touchState.Position.Y < dimensions.Y;
                    if (mouseInBox)
                    {
                        if (!previousInBox)
                        {
                            TouchEntered(touchState);
                        }
                        previousInBox = true;
                    }
                    else
                    {
                        if (previousInBox)
                        {
                            TouchExited(touchState);
                        }
                        previousInBox = false;
                    }

                    if (touchState.State == Microsoft.Xna.Framework.Input.Touch.TouchLocationState.Pressed)
                    {
                        if (mouseInBox)
                        {
                            idStatus[touchState.Id] = true;
                            TouchPressed(touchState);
                        }
                        else
                        {
                            idStatus[touchState.Id] = false;
                        }
                    }
                    if (touchState.State == Microsoft.Xna.Framework.Input.Touch.TouchLocationState.Released)
                    {
                        if (mouseInBox)
                        {
                            TouchReleased(touchState);
                            if (idStatus.ContainsKey(touchState.Id))
                            {
                                if (idStatus[touchState.Id] == true)
                                {
                                    TouchClicked(touchState);
                                }
                            }
                        }
                        idStatus[touchState.Id] = false;
                    }
                    
                    
                }
                
            }
            
        }

        private void EmptyTouchFunction(TouchEvent state)
        {

        }

        private TouchEvent GetEvent(TouchPointState state, ScreenItems.ParentNode parent)
        {
            Matrix inverse = parent.AbsoluteInverseAnimatedTransformation;

            Vector3 position3, scale3;
            Quaternion rotationQ;

            inverse.Decompose(out scale3, out rotationQ, out position3);
            Vector2 direction = Vector2.Transform(Vector2.UnitX, rotationQ);
            float Rotation = (float)Math.Atan2((double)(direction.Y), (double)(direction.X));

            Matrix deltaTransform = Matrix.CreateScale(scale3) * Matrix.CreateRotationZ(Rotation);

            Vector2 transformedPosition = Vector2.Transform(state.Position, inverse);

            //TODO support delta!
            Vector2 transformedDelta = Vector2.Transform(Vector2.Zero, deltaTransform);
            TouchEvent retVal = new TouchEvent(state.Id, transformedPosition, transformedDelta, state.Pressure, state.State);
            
            return retVal;
        }
    }
}
