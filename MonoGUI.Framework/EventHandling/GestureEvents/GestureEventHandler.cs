﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input.Touch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.EventHandling.GestureEvents
{
    public class GestureEventHandler : InputEventHandler
    {
        public delegate void GestureFiredEvent(GestureEvent sample);
        public delegate void GestureCompletedEvent(GestureEvent finishedGesture);

        public GestureFiredEvent TapEvent { private get; set; }
        public GestureFiredEvent DoubleTapEvent { private get; set; }
        public GestureFiredEvent HoldEvent { private get; set; }
        public GestureFiredEvent HorizontalDragEvent { private get; set; }
        public GestureFiredEvent VerticalDragEvent { private get; set; }
        public GestureFiredEvent FreeDragEvent { private get; set; }
        public GestureFiredEvent PinchEvent { private get; set; }
        public GestureFiredEvent FlickEvent { private get; set; }
        public GestureCompletedEvent DragCompletedEvent { private get; set; }
        public GestureCompletedEvent PinchCompletedEvent { private get; set; }

        private GestureEvent RunningDragEvent;
        private GestureEvent RunningPinchEvent;

        public GestureEventHandler()
        {
            TapEvent = EmptyGestureEvent;
            DoubleTapEvent = EmptyGestureEvent;
            HoldEvent = EmptyGestureEvent;
            HorizontalDragEvent = EmptyGestureEvent;
            VerticalDragEvent = EmptyGestureEvent;
            FreeDragEvent = EmptyGestureEvent;
            PinchEvent = EmptyGestureEvent;
            FlickEvent = EmptyGestureEvent;
            DragCompletedEvent = EmptyGestureCompletedEvent;
            PinchCompletedEvent = EmptyGestureCompletedEvent;

            RunningDragEvent = null;
            RunningPinchEvent = null;
        }

        public override void Update(Manager.Input.InputManager input, ScreenItems.ParentNode parent)
        {
            List<GestureSample> curGestures = input.CurrentGestureSamples;
            Dictionary<GestureType, ISet<GestureSample>> sortedGestures = new Dictionary<GestureType, ISet<GestureSample>>();
            if (!input.MouseTouchHandled)
            {
                foreach (GestureSample sample in curGestures)
                {
                    if (!sortedGestures.ContainsKey(sample.GestureType))
                    {
                        sortedGestures.Add(sample.GestureType, new HashSet<GestureSample>());
                    }
                    sortedGestures[sample.GestureType].Add(sample);
                }
                if (sortedGestures.ContainsKey(GestureType.Tap))
                {
                    foreach (GestureSample sample in sortedGestures[GestureType.Tap])
                    {
                        GestureEvent ev = scaledGesture(sample, parent);
                        if (ev.StartPosition1.X > 0 && ev.StartPosition1.X < parent.RelativeDimensions.X &&
                            ev.StartPosition1.Y > 0 && ev.StartPosition1.Y < parent.RelativeDimensions.Y)
                        {
                            TapEvent(ev);
                        }

                    }
                }
                if (sortedGestures.ContainsKey(GestureType.DoubleTap))
                {
                    foreach (GestureSample sample in sortedGestures[GestureType.DoubleTap])
                    {
                        GestureEvent ev = scaledGesture(sample, parent);
                        if (ev.StartPosition1.X > 0 && ev.StartPosition1.X < parent.RelativeDimensions.X &&
                            ev.StartPosition1.Y > 0 && ev.StartPosition1.Y < parent.RelativeDimensions.Y)
                        {
                            DoubleTapEvent(ev);
                        }
                    }
                }
                if (sortedGestures.ContainsKey(GestureType.Hold))
                {
                    foreach (GestureSample sample in sortedGestures[GestureType.Hold])
                    {
                        GestureEvent ev = scaledGesture(sample, parent);
                        if (ev.StartPosition1.X > 0 && ev.StartPosition1.X < parent.RelativeDimensions.X &&
                            ev.StartPosition1.Y > 0 && ev.StartPosition1.Y < parent.RelativeDimensions.Y)
                        {
                            HoldEvent(ev);
                        }
                    }
                }
                if (sortedGestures.ContainsKey(GestureType.Flick))
                {
                    foreach (GestureSample sample in sortedGestures[GestureType.Flick])
                    {
                        GestureEvent ev = scaledGesture(sample, parent);
                        FlickEvent(ev);
                    }
                }
                if (sortedGestures.ContainsKey(GestureType.HorizontalDrag))
                {
                    foreach (GestureSample sample in sortedGestures[GestureType.HorizontalDrag])
                    {
                        GestureEvent ev = scaledGesture(sample, parent);
                        if ((RunningDragEvent == null && ev.StartPosition1.X > 0 && ev.StartPosition1.X < parent.RelativeDimensions.X &&
                            ev.StartPosition1.Y > 0 && ev.StartPosition1.Y < parent.RelativeDimensions.Y) || RunningDragEvent != null)
                        {
                            HorizontalDragEvent(ev);
                            if (RunningDragEvent == null)
                            {
                                RunningDragEvent = ev;
                            }
                            else
                            {
                                RunningDragEvent.AddGestureIteration(ev);
                            }
                        }

                    }
                }
                if (sortedGestures.ContainsKey(GestureType.VerticalDrag))
                {
                    foreach (GestureSample sample in sortedGestures[GestureType.VerticalDrag])
                    {
                        GestureEvent ev = scaledGesture(sample, parent);
                        if ((RunningDragEvent == null && ev.StartPosition1.X > 0 && ev.StartPosition1.X < parent.RelativeDimensions.X &&
                            ev.StartPosition1.Y > 0 && ev.StartPosition1.Y < parent.RelativeDimensions.Y) || RunningDragEvent != null)
                        {
                            VerticalDragEvent(ev);
                            if (RunningDragEvent == null)
                            {
                                RunningDragEvent = ev;
                            }
                            else
                            {
                                RunningDragEvent.AddGestureIteration(ev);
                            }
                        }
                    }
                }
                if (sortedGestures.ContainsKey(GestureType.FreeDrag))
                {
                    foreach (GestureSample sample in sortedGestures[GestureType.FreeDrag])
                    {
                        GestureEvent ev = scaledGesture(sample, parent);
                        if ((RunningDragEvent == null && ev.StartPosition1.X > 0 && ev.StartPosition1.X < parent.RelativeDimensions.X &&
                            ev.StartPosition1.Y > 0 && ev.StartPosition1.Y < parent.RelativeDimensions.Y) || RunningDragEvent != null)
                        {
                            FreeDragEvent(ev);
                            if (RunningDragEvent == null)
                            {
                                RunningDragEvent = ev;
                            }
                            else
                            {
                                RunningDragEvent.AddGestureIteration(ev);
                            }
                        }
                    }
                }
                if (sortedGestures.ContainsKey(GestureType.Pinch))
                {
                    foreach (GestureSample sample in sortedGestures[GestureType.Pinch])
                    {
                        GestureEvent ev = scaledGesture(sample, parent);
                        if ((RunningPinchEvent == null && ev.StartPosition1.X > 0 && ev.StartPosition1.X < parent.RelativeDimensions.X &&
                            ev.StartPosition1.Y > 0 && ev.StartPosition1.Y < parent.RelativeDimensions.Y) || RunningPinchEvent != null)
                        {
                            PinchEvent(ev);
                            if (RunningPinchEvent == null)
                            {
                                RunningPinchEvent = ev;
                            }
                            else
                            {
                                RunningPinchEvent.AddGestureIteration(ev);
                            }
                        }
                    }
                }
                if (sortedGestures.ContainsKey(GestureType.DragComplete))
                {
                    DragCompletedEvent(RunningDragEvent);
                    RunningDragEvent = null;
                }
                if (sortedGestures.ContainsKey(GestureType.PinchComplete))
                {
                    PinchCompletedEvent(RunningPinchEvent);
                    RunningPinchEvent = null;
                }
            }
        }

        private void EmptyGestureEvent(GestureEvent sample) { }
        private void EmptyGestureCompletedEvent(GestureEvent finishedGesture) { }
        private GestureEvent scaledGesture(GestureSample sample, ScreenItems.ParentNode parent)
        {
            Matrix inverse = parent.AbsoluteInverseAnimatedTransformation;

            Vector3 position3, scale3;
            Quaternion rotationQ;

            inverse.Decompose(out scale3, out rotationQ, out position3);
            Vector2 direction = Vector2.Transform(Vector2.UnitX, rotationQ);
            float Rotation = (float)Math.Atan2((double)(direction.Y), (double)(direction.X));

            Matrix deltaTransform = Matrix.CreateScale(scale3) * Matrix.CreateRotationZ(Rotation);
           
            GestureEvent retVal = new GestureEvent(sample);
            retVal.StartPosition1 = Vector2.Transform(sample.Position, inverse);
            retVal.StartPosition2 = Vector2.Transform(sample.Position2, inverse);
            retVal.Delta1 = Vector2.Transform(sample.Delta, deltaTransform);
            retVal.Delta2 = Vector2.Transform(sample.Delta2, deltaTransform);
            return retVal;
        }
    }
}
