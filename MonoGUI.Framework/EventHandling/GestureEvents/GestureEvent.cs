﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input.Touch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.EventHandling.GestureEvents
{
    public class GestureEvent : EventArgs
    {
        public GestureType GestureType { get; private set; }
        public Vector2 StartPosition1 { get; set; }
        public Vector2 StartPosition2 { get; set; }
        public Vector2 Delta1 { get; set; }
        public Vector2 Delta2 { get; set; }
        public TimeSpan TimeStamp { get; private set; }

        public GestureEvent(GestureSample sample)
        {
            GestureType = sample.GestureType;
            StartPosition1 = sample.Position;
            StartPosition2 = sample.Position2;
            Delta1 = sample.Delta;
            Delta2 = sample.Delta2;
            TimeStamp = sample.Timestamp;
        }

        public void AddGestureIteration(GestureEvent sample)
        {
            if (GestureType == sample.GestureType)
            {
                Delta1 += sample.Delta1;
                Delta2 += sample.Delta2;
                TimeStamp += (sample.TimeStamp - TimeStamp);
            }
        }
    }
}
