﻿using Microsoft.Xna.Framework;
using MonoGUI.Themes.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Themes
{
    public class PanelTheme : IComponentTheme
    {
        public List<DrawCommand> GetDrawCommands(string drawName, Microsoft.Xna.Framework.Vector2 Position, Microsoft.Xna.Framework.Vector2 Dimensions, float componentLifetime)
        {
            throw new NotImplementedException();
        }

        public Microsoft.Xna.Framework.Rectangle? GetThemeLocation(string locationName, Microsoft.Xna.Framework.Vector2 Position, Microsoft.Xna.Framework.Vector2 Dimensions, float componentLifetime)
        {
            throw new NotImplementedException();
        }

        public DrawCommand GetComponentThemeTextureAreaPair(string tapName, Microsoft.Xna.Framework.Vector2 Position, Microsoft.Xna.Framework.Vector2 Dimensions, float componentLifetime)
        {
            throw new NotImplementedException();
        }

        public string GetComponentString(string key)
        {
            throw new NotImplementedException();
        }

        public Color GetComponentColor(string colorName)
        {
            return Color.White;
        }

        public void LoadContent(Microsoft.Xna.Framework.Content.ContentManager Content)
        {
            throw new NotImplementedException();
        }

        public Microsoft.Xna.Framework.Point GetMinimumDimensions()
        {
            throw new NotImplementedException();
        }
    }
}
