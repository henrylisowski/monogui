﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGUI.Skinning.SkinElements;
using MonoGUI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Themes
{
    public class DrawCommand
    {
        public Texture2D SourceImage { get; private set; }
        public string SourceImageName { get; private set; }
        public Rectangle SourceRectangle { get; private set; }
        public RectangleFloat DestinationRectangle { get; private set; }
        private Vector2 _scale;
        public Vector2 Scale
        {
            get
            {
                return _scale;
            }
        }
        public SpriteEffects Effects { get; private set; }
        public TextureElement SourceElement { get; private set; }

        public DrawCommand(Texture2D SourceImage, string SourceImageName, Rectangle SourceRectangle, 
            SpriteEffects Effects, RectangleFloat DestinationRectangle, TextureElement SourceElement)
        {
            this.SourceImage = SourceImage;
            this.SourceImageName = SourceImageName;
            this.SourceRectangle = SourceRectangle;
            this.Effects = Effects;
            this.DestinationRectangle = DestinationRectangle;
            this.SourceElement = SourceElement;
            CalculateScale();
        }

        public void UpdateSource(float componentLifetime)
        {
            if (SourceElement.SingleFrameAnimation)
            {
                return;
            }
            Rectangle curSource = SourceRectangle;
            SourceRectangle = SourceElement.CurrentSourceBox(componentLifetime);
            if (curSource.X != SourceRectangle.X || curSource.Y != SourceRectangle.Y ||
                curSource.Width != SourceRectangle.Width || curSource.Height != SourceRectangle.Height)
            {
                CalculateScale();
            }
        }

        public void CalculateScale()
        {
            _scale = DestinationRectangle.Dimensions / new Vector2(SourceRectangle.Width, SourceRectangle.Height);
        }

        public DrawCommand Clone()
        {
            return new DrawCommand(SourceImage, SourceImageName, SourceRectangle, Effects, DestinationRectangle, SourceElement);
        }
    }
}
