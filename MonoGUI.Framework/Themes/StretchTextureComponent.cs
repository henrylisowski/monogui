﻿using Microsoft.Xna.Framework;
using MonoGUI.Themes.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Themes
{
    public class StretchTextureComponent : IComponentTheme
    {
        public TextureAreaPair Texture { get; set; }
        
        public List<DrawCommand> GetDrawCommands(string drawName, Microsoft.Xna.Framework.Vector2 Position, Microsoft.Xna.Framework.Vector2 Dimensions, float componentLifetime)
        {
            List<DrawCommand> retVal = new List<DrawCommand>();
            Vector2 scale = Dimensions / new Vector2(Texture.DestinationBox.Width, Texture.DestinationBox.Height);
            //retVal.Add(new DrawCommand(Texture.SourceTexture, Texture.SourceTextureName,
            //    Texture.CurrentSourceBox(componentLifetime), Texture.Effect,
            //    new Microsoft.Xna.Framework.Vector2(Texture.DestinationBox.X, Texture.DestinationBox.Y) * scale + Position,
            //    new Microsoft.Xna.Framework.Vector2(Texture.DestinationBox.Width, Texture.DestinationBox.Height) * scale));
            return retVal;
        }

        public Microsoft.Xna.Framework.Rectangle? GetThemeLocation(string locationName, Microsoft.Xna.Framework.Vector2 Position, Microsoft.Xna.Framework.Vector2 Dimensions, float componentLifetime)
        {
            return null;
        }

        public DrawCommand GetComponentThemeTextureAreaPair(string tapName, Microsoft.Xna.Framework.Vector2 Position, Microsoft.Xna.Framework.Vector2 Dimensions, float componentLifetime)
        {
            return null;
        }

        public string GetComponentString(string key)
        {
            return null;
        }

        Color GetComponentColor(string colorName)
        {
            return Color.White;
        }

        public void LoadContent(Microsoft.Xna.Framework.Content.ContentManager Content)
        {
            Texture.LoadContent(Content);
        }

        public Microsoft.Xna.Framework.Point GetMinimumDimensions()
        {
            return new Point();
        }


        Color IComponentTheme.GetComponentColor(string colorName)
        {
            return Color.White;
        }
    }
}
