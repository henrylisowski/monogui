﻿using Microsoft.Xna.Framework.Content;
using MonoGUI.Animations;
using MonoGUI.ScreenItems;
using MonoGUI.Themes.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Themes
{
    public class ConstantStateComponentThemeStateContainer : IComponentThemeStateContainer
    {
        public IComponentTheme Theme { get; set; }
        [ContentSerializerAttribute(Optional = true)]
        public Dictionary<string, List<IModifier>> ThemeModifiers { get; set; }

        public ConstantStateComponentThemeStateContainer()
        {
            ThemeModifiers = new Dictionary<string, List<IModifier>>();
        }

        public void LoadContent(Microsoft.Xna.Framework.Content.ContentManager Content)
        {
            Theme.LoadContent(Content);
        }

        public IComponentTheme GetProperStateComponentTheme(ScreenItemVisualStates visualState)
        {
            return Theme;
        }

        public Dictionary<string, List<Animations.IModifier>> GetProperStateModifiers(ScreenItemVisualStates visualState)
        {
            return ThemeModifiers;
        }
    }
}
