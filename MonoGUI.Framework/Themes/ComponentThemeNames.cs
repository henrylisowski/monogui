﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Themes
{
    public static class ComponentThemeNames
    {
        public static string MonoGUIButtonTheme { get { return "MonoGUIButton_Theme"; } }
        public static string MonoGUIProgressBarTheme { get { return "MonoGUIProgressBar_Theme"; } }
        public static string MonoGUILabelTheme { get { return "MonoGUILabel_Theme"; } }
        public static string MonoGUICheckboxTheme { get { return "MonoGUICheckbox_Theme"; } }
        public static string MonoGUIRadioButtonTheme { get { return "MonoGUIRadioButton_Theme"; } }
    }
}
