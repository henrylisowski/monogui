﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using MonoGUI.Themes.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Themes
{
    public class CheckboxTheme : IComponentTheme
    {
        public IComponentTheme CheckedBoxSquare { get; set; }
        public IComponentTheme UncheckedBoxSquare { get; set; }
        public IComponentTheme Check { get; set; }

        [ContentSerializer(Optional = true)]
        public IComponentTheme Background { get; set; }
        [ContentSerializer(Optional = true)]
        public Color? DefaultColor { get; set; }
        [ContentSerializer(Optional = true)]
        public Color? CheckboxCheckedColor { get; set; }
        [ContentSerializer(Optional = true)]
        public Color? CheckboxUncheckedColor { get; set; }
        [ContentSerializer(Optional = true)]
        public Color? CheckboxCheckColor { get; set; }

        public Rectangle ImageLocation { get; set; }
        public Rectangle CheckLocation { get; set; }
        public Rectangle ClickableLocation { get; set; }
        public Rectangle TextLocation { get; set; }

        public string FontName { get; set; }

        public List<DrawCommand> GetDrawCommands(string drawName, Microsoft.Xna.Framework.Vector2 Position, Microsoft.Xna.Framework.Vector2 Dimensions, float componentLifetime)
        {
            List<DrawCommand> retVal = new List<DrawCommand>();
            float scale = scaleFactor(Dimensions);
            Vector2 updatedImagePosition = Position + (new Vector2(ImageLocation.X, ImageLocation.Y) * scale);
            Vector2 udpatedImageDimensions = new Vector2(ImageLocation.Width, ImageLocation.Height) * scale;

            Vector2 checkPosition = Position + (new Vector2(CheckLocation.X, CheckLocation.Y) * scale);
            Vector2 checkDimensions = new Vector2(CheckLocation.Width, CheckLocation.Height) * scale;

            if (drawName.Equals(ComponentThemeDrawCommandNames.MonoGUICheckboxCheck))
            {
                retVal.AddRange(Check.GetDrawCommands(drawName, checkPosition, checkDimensions, componentLifetime));
            }
            else if (drawName.Equals(ComponentThemeDrawCommandNames.MonoGUICheckboxChecked))
            {
                retVal.AddRange(CheckedBoxSquare.GetDrawCommands(drawName, updatedImagePosition, udpatedImageDimensions, componentLifetime));
            }
            else if (drawName.Equals(ComponentThemeDrawCommandNames.MonoGUICheckboxUnchecked))
            {
                retVal.AddRange(UncheckedBoxSquare.GetDrawCommands(drawName, updatedImagePosition, udpatedImageDimensions, componentLifetime));
            }
            else if (drawName.Equals(ComponentThemeDrawCommandNames.MonoGUIBackground) && Background != null)
            {
                retVal.AddRange(Background.GetDrawCommands(drawName, Position, Dimensions, componentLifetime));
            }

            return retVal;
        }

        public Microsoft.Xna.Framework.Rectangle? GetThemeLocation(string locationName, Microsoft.Xna.Framework.Vector2 Position, Microsoft.Xna.Framework.Vector2 Dimensions, float componentLifetime)
        {
            float scale = scaleFactor(Dimensions);
            if (locationName.Equals(ComponentThemeLocationNames.MonoGUICheckboxClickable))
            {
                return new Rectangle((int)(ClickableLocation.X * scale + Position.X), (int)(ClickableLocation.Y * scale + Position.Y), 
                    (int)(ClickableLocation.Width * scale), (int)(ClickableLocation.Height * scale));
            }
            if (locationName.Equals(ComponentThemeLocationNames.MonoGUICheckboxText))
            {
                return new Rectangle((int)(TextLocation.X * scale + Position.X), (int)(TextLocation.Y * scale + Position.Y),
                    (int)(TextLocation.Width * scale), (int)(TextLocation.Height * scale));
            }

            return null;
        }

        public DrawCommand GetComponentThemeTextureAreaPair(string tapName, Microsoft.Xna.Framework.Vector2 Position, Microsoft.Xna.Framework.Vector2 Dimensions, float componentLifetime)
        {
            return null;
        }

        public Color GetComponentColor(string colorName)
        {
            if (colorName.Equals(ComponentThemeColorNames.MonoGUIDefaultColor) && DefaultColor != null)
            {
                return DefaultColor.Value;
            }
            if (colorName.Equals(ComponentThemeColorNames.MonoGUICheckboxCheckColor) && CheckboxCheckColor != null)
            {
                return CheckboxCheckColor.Value;
            }
            if (colorName.Equals(ComponentThemeColorNames.MonoGUICheckboxUncheckedColor) && CheckboxUncheckedColor != null)
            {
                return CheckboxUncheckedColor.Value;
            }
            if (colorName.Equals(ComponentThemeColorNames.MonoGUICheckboxCheckedColor) && CheckboxCheckedColor != null)
            {
                return CheckboxCheckedColor.Value;
            }
            return Color.White;
        }

        public string GetComponentString(string key)
        {
            if (key.Equals(ComponentThemeStringNames.FontName))
            {
                return FontName;
            }
            return null;
        }

        public void LoadContent(Microsoft.Xna.Framework.Content.ContentManager Content)
        {
            CheckedBoxSquare.LoadContent(Content);
            UncheckedBoxSquare.LoadContent(Content);
            Check.LoadContent(Content);
            if (Background != null)
            {
                Background.LoadContent(Content);
            }
        }

        public Microsoft.Xna.Framework.Point GetMinimumDimensions()
        {
            return new Point();
        }

        private float scaleFactor(Vector2 Dimensions)
        {
            //TODO right now this only takes into account the image height, not the text height
            float height = ImageLocation.Height;
            return Dimensions.Y / height;
        }
    }
}
