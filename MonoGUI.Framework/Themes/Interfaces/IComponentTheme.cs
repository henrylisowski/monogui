﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using MonoGUI.ScreenItems.CompositionElements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Themes.Interfaces
{
    public interface IComponentTheme
    {
        List<DrawCommand> GetDrawCommands(string drawName, Vector2 Position, Vector2 Dimensions, float componentLifetime);
        Rectangle? GetThemeLocation(string locationName, Vector2 Position, Vector2 Dimensions, float componentLifetime);
        DrawCommand GetComponentThemeTextureAreaPair(string tapName, Vector2 Position, Vector2 Dimensions, float componentLifetime);
        string GetComponentString(string key);
        Color GetComponentColor(string colorName);

        void LoadContent(ContentManager Content);
        Point GetMinimumDimensions();
        
    }
}
