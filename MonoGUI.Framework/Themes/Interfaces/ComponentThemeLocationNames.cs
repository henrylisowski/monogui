﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Themes.Interfaces
{
    public static class ComponentThemeLocationNames
    {
        public static string MonoGUIContainerLocation { get { return "MonoGUI_Container_Location"; } }

        public static string MonoGUICheckboxClickable { get { return "MonoGUI_Checkbox_Clickable"; } }
        public static string MonoGUICheckboxText { get { return "MonoGUI_Checkbox_Text"; } }
        public static string MonoGUICheckboxImage { get { return "MonoGUI_Checkbox_Image"; } }
    }
}
