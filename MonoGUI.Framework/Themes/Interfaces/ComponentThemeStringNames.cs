﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Themes.Interfaces
{
    public static class ComponentThemeStringNames
    {
        public static string FontName { get { return "MonoGUI_FontName"; } }
    }
}
