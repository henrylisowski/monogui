﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using MonoGUI.Animations;
using MonoGUI.ScreenItems;
using MonoGUI.ScreenItems.CompositionElements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Themes.Interfaces
{
    public interface IComponentThemeStateContainer
    {
        void LoadContent(ContentManager Content);
        IComponentTheme GetProperStateComponentTheme(ScreenItemVisualStates visualState);
        Dictionary<string, List<IModifier>> GetProperStateModifiers(ScreenItemVisualStates visualState);
    }
}
