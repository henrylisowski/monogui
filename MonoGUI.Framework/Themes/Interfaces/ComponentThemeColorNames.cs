using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Themes.Interfaces
{
    public static class ComponentThemeColorNames
    {
        public static string MonoGUIDefaultColor { get { return "MonoGUI_Default_Color"; } }
        public static string MonoGUICheckboxCheckColor { get { return "MonoGUI_Checkbox_Check_Color"; } }
        public static string MonoGUICheckboxUncheckedColor { get { return "MonoGUI_Checkbox_Unchecked_Color"; } }
        public static string MonoGUICheckboxCheckedColor { get { return "MonoGUI_Checkbox_Checked_Color"; } }
    }
}
