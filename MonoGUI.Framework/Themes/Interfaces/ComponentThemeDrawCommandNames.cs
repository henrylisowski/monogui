﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Themes.Interfaces
{
    public static class ComponentThemeDrawCommandNames
    {
        public static string MonoGUIForeground { get { return "MonoGUI_Foreground_Draw"; } }
        public static string MonoGUIBackground { get { return "MonoGUI_Background_Draw"; } }

        public static string MonoGUICheckboxChecked { get { return "MonoGUI_Checkbox_Checked"; } }
        public static string MonoGUICheckboxUnchecked { get { return "MonoGUI_Checkbox_Unchecked"; } }
        public static string MonoGUICheckboxCheck { get { return "MonoGUI_Checkbox_Check"; } }
    }
}
