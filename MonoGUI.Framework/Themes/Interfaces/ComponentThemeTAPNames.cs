﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Themes.Interfaces
{
    public static class ComponentThemeTAPNames
    {
        public static string ProgressBarGradientStencil { get { return "MonoGUI_Progress_Bar_Gradient_Stencil"; } }
        public static string ProgressBarFiller { get { return "MonoGUI_Progress_Bar_Filler"; } }
        public static string ProgressBarStencilClipping { get { return "MonoGUI_Progress_Bar_Stencil_Clipping"; } }
        public static string ProgressBarSecondaryGradientStencil { get { return "MonoGUI_Progress_Bar_Secondary_Gradient_Stencil"; } }
        public static string ProgressBarSecondaryFiller { get { return "MonoGUI_Progress_Bar_Secondary_Filler"; } }
        public static string ProgressBarSecondaryStencilClipping { get { return "MonoGUI_Progress_Bar_Secondary_Stencil_Clipping"; } }
    }
}
