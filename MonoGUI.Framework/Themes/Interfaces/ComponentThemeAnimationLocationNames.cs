﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Themes.Interfaces
{
    public static class ComponentThemeAnimationLocationNames
    {
        public static string DefaultAnimationLocation { get { return "MonoGUI_Default"; } }

        public static string MonoGUICheckboxAnimation { get { return "MonoGUI_Checkbox_Animation"; } }
        public static string MonoGUICheckboxCheckAnimation { get { return "MonoGUI_Checkbox_Check_Animation"; } }
    }
}
