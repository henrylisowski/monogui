﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace MonoGUI.Themes
{
    public class TextureAreaPair
    {
        [ContentSerializerIgnore]
        public Texture2D SourceTexture { get; set; }

        public List<Rectangle> SourceBoxes { get; set; }
        public Rectangle DestinationBox { get; set; }
        public string SourceTextureName { get; set; }
        public SpriteEffects Effect { get; set; }
        public float FrameTime { get; set; }

        public TextureAreaPair()
        {
            
        }

        public TextureAreaPair(string SourceTextureName, Rectangle SourceBox, Rectangle DestinationBox, SpriteEffects Effect)
        {
            this.SourceTextureName = SourceTextureName;
            this.SourceBoxes = new List<Rectangle>();
            this.SourceBoxes.Add(SourceBox);
            this.DestinationBox = DestinationBox;
            this.Effect = Effect;
            this.FrameTime = 1f;
        }

        public TextureAreaPair(string SourceTextureName, List<Rectangle> SourceBoxes, 
            Rectangle DestinationBox, SpriteEffects Effect, float FrameTime)
        {
            this.SourceTextureName = SourceTextureName;
            this.SourceBoxes = SourceBoxes;
            this.DestinationBox = DestinationBox;
            this.Effect = Effect;
            this.FrameTime = FrameTime;
        }

        public Rectangle CurrentSourceBox(float componentLifetime)
        {
            int numFrames = (int)(componentLifetime / FrameTime);
            if (numFrames < SourceBoxes.Count)
            {
                return (SourceBoxes[numFrames]);
            }
            else
            {
                return SourceBoxes[numFrames % SourceBoxes.Count];
            }
        }

        //public Rectangle CurrentDestinationBox(float componentLifetime)
        //{
        //    int numFrames = (int)(componentLifetime % FrameTime);
        //    if (numFrames < DestinationBoxes.Count)
        //    {
        //        return (DestinationBoxes[numFrames]);
        //    }
        //    else
        //    {
        //        return DestinationBoxes[numFrames % DestinationBoxes.Count];
        //    }
        //}

        public void LoadContent(ContentManager Content)
        {
            SourceTexture = Content.Load<Texture2D>(SourceTextureName);
        }

        public override string ToString()
        {
            return SourceTextureName + " " + SourceBoxes + " " + DestinationBox;
        }
    }
}
