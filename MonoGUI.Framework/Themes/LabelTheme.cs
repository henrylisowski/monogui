﻿using Microsoft.Xna.Framework;
using MonoGUI.Themes.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Themes
{
    public class LabelTheme : IComponentTheme
    {
        public string FontName { get; set; }


        public List<DrawCommand> GetDrawCommands(string drawName, Microsoft.Xna.Framework.Vector2 Position, Microsoft.Xna.Framework.Vector2 Dimensions, float componentLifetime)
        {
            return new List<DrawCommand>();
        }

        public Microsoft.Xna.Framework.Rectangle? GetThemeLocation(string locationName, Microsoft.Xna.Framework.Vector2 Position, Microsoft.Xna.Framework.Vector2 Dimensions, float componentLifetime)
        {
            return null;
        }

        public DrawCommand GetComponentThemeTextureAreaPair(string tapName, Microsoft.Xna.Framework.Vector2 Position, Microsoft.Xna.Framework.Vector2 Dimensions, float componentLifetime)
        {
            return null;
        }

        public string GetComponentString(string key)
        {
            if (key.Equals(ComponentThemeStringNames.FontName))
            {
                return FontName;
            }
            return null;
        }

        public Color GetComponentColor(string colorName)
        {
            return Color.White;
        }

        public void LoadContent(Microsoft.Xna.Framework.Content.ContentManager Content)
        {
            
        }

        public Microsoft.Xna.Framework.Point GetMinimumDimensions()
        {
            return new Microsoft.Xna.Framework.Point();
        }

    }
}
