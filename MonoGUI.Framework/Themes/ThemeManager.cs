﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using MonoGUI.Animations;
using MonoGUI.ScreenItems;
using MonoGUI.ScreenItems.CompositionElements;
using MonoGUI.Skinning.Fonts;
using MonoGUI.Themes.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Themes
{
    public class ThemeManager
    {
        private Theme activeTheme;
        private Theme defaultTheme;
        public Theme ActiveTheme
        {
            get
            {
                return activeTheme;
            }
            set
            {
                activeTheme = value;
            }
        }

        public void InitializeDefaultTheme(ContentManager Content)
        {
            defaultTheme = Content.Load<Theme>("defaultTheme");
            defaultTheme.LoadContent(Content);
            activeTheme = defaultTheme;
        }

        public void LoadTheme(string themeName, ContentManager Content)
        {
            activeTheme = Content.Load<Theme>(themeName);
            activeTheme.LoadContent(Content);
        }

        public IComponentTheme GetComponentTheme(string componentName, string themeName, ScreenItemVisualStates visualState)
        {
            IComponentTheme activeThemeAttempt = activeTheme.GetComponentTheme(componentName, themeName, visualState);
            if (activeThemeAttempt == null)
            {
                activeThemeAttempt = defaultTheme.GetComponentTheme(componentName, "default", visualState);
            }
            return activeThemeAttempt;
        }

        public Dictionary<string, List<IModifier>> GetVisualStateModifiers(string componentName, string themeName, ScreenItemVisualStates visualState)
        {
            if (activeTheme == null)
            {
                return new Dictionary<string, List<IModifier>>();
            }
            return activeTheme.GetVisualStateModifiers(componentName, themeName, visualState);
        }

        public SpriteFont GetFont(string fontName, int size, FontRoundMethod round)
        {
            return ActiveTheme.FontManager.GetClosestFont(fontName, size, round);
        }

        public ThemeManager()
        {

        }
    }
}
