﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Themes
{
    public enum StretchOptions
    {
        FillWidth, FillHeight, Stretch
    }
}
