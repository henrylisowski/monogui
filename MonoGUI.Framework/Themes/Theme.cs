﻿using Microsoft.Xna.Framework.Content;
using MonoGUI.Animations;
using MonoGUI.ScreenItems;
using MonoGUI.ScreenItems.CompositionElements;
using MonoGUI.Skinning.Fonts;
using MonoGUI.Themes.Box;
using MonoGUI.Themes.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Themes
{
    public class Theme
    {
        public string ThemeName { get; set; }
        public List<string> LinkedTextures { get; set; }
        public FontManager FontManager { get; set; }

        /// <summary>
        /// Themes are mapped primarily by the component string name, then the name of the theme variant you want
        /// </summary>
        [ContentSerializerAttribute(Optional = true)]
        public Dictionary<string, Dictionary<string, IComponentThemeStateContainer>> ComponentThemes { get; set; }

        public Theme()
        {
            LinkedTextures = new List<string>();
            FontManager = new FontManager();
            ComponentThemes = new Dictionary<string, Dictionary<string, IComponentThemeStateContainer>>();
        }

        public void LoadContent(ContentManager Content)
        {
            FontManager.LoadContent(Content);
            if (ComponentThemes != null)
            {
                foreach (Dictionary<string, IComponentThemeStateContainer> d in ComponentThemes.Values)
                {
                    foreach (IComponentThemeStateContainer cont in d.Values)
                    {
                        cont.LoadContent(Content);
                    }
                }
                
            }
        }

        public IComponentTheme GetComponentTheme(string componentName, string themeName, ScreenItemVisualStates visualState)
        {
            if (ComponentThemes != null)
            {
                if (ComponentThemes.ContainsKey(componentName))
                {
                    Dictionary<string, IComponentThemeStateContainer> StatesForComponent = ComponentThemes[componentName];
                    if (StatesForComponent.ContainsKey(themeName))
                    {
                        IComponentThemeStateContainer stateContainer = StatesForComponent[themeName];
                        return stateContainer.GetProperStateComponentTheme(visualState);
                    }
                }

                
            }
            return null;
        }

        public Dictionary<string, List<IModifier>> GetVisualStateModifiers(string componentName, string themeName, ScreenItemVisualStates visualState)
        {
            if (ComponentThemes != null)
            {
                if (componentName != null && ComponentThemes.ContainsKey(componentName))
                {
                    Dictionary<string, IComponentThemeStateContainer> StatesForComponent = ComponentThemes[componentName];
                    if (StatesForComponent.ContainsKey(themeName))
                    {
                        IComponentThemeStateContainer stateContainer = StatesForComponent[themeName];
                        return stateContainer.GetProperStateModifiers(visualState);
                    }
                }


            }
            return new Dictionary<string,List<IModifier>>();
        }
    }
}
