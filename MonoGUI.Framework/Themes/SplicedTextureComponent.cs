﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using MonoGUI.Themes.Box;
using MonoGUI.Themes.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Themes
{
    public class SplicedTextureComponent : IComponentTheme
    {
        [ContentSerializer(Optional = true)]
        public TextureAreaPair MainBackground { get; set; }

        public BoxBorder TopBorder { get; set; }
        public BoxBorder BottomBorder { get; set; }
        public BoxBorder LeftBorder { get; set; }
        public BoxBorder RightBorder { get; set; }
        [ContentSerializerIgnore]
        public HashSet<BoxBorder> AllBorders
        {
            get
            {
                HashSet<BoxBorder> retVal = new HashSet<BoxBorder>();
                retVal.Add(TopBorder);
                retVal.Add(BottomBorder);
                retVal.Add(LeftBorder);
                retVal.Add(RightBorder);
                return retVal;
            }
        }

        public BoxCorner TopLeftCorner { get; set; }
        public BoxCorner TopRightCorner { get; set; }
        public BoxCorner BottomLeftCorner { get; set; }
        public BoxCorner BottomRightCorner { get; set; }
        [ContentSerializerIgnore]
        public HashSet<BoxCorner> AllCorners
        {
            get
            {
                HashSet<BoxCorner> retVal = new HashSet<BoxCorner>();
                retVal.Add(TopLeftCorner);
                retVal.Add(TopRightCorner);
                retVal.Add(BottomLeftCorner);
                retVal.Add(BottomRightCorner);
                return retVal;
            }
        }

        public List<DrawCommand> GetDrawCommands(string drawName, Vector2 Position, Vector2 Dimensions, float componentLifetime)
        {
            List<DrawCommand> retVal = new List<DrawCommand>();

            if (drawName.Equals(ComponentThemeDrawCommandNames.MonoGUIForeground))
            {
                int topWidth = TopRightCorner.Corner.DestinationBox.Right - TopLeftCorner.Corner.DestinationBox.Left;
                int bottomWidth = BottomRightCorner.Corner.DestinationBox.Right - BottomLeftCorner.Corner.DestinationBox.Left;
                int leftHeight = BottomLeftCorner.Corner.DestinationBox.Bottom - TopLeftCorner.Corner.DestinationBox.Top;
                int rightHeight = BottomRightCorner.Corner.DestinationBox.Bottom - TopRightCorner.Corner.DestinationBox.Top;

                float topWidthIncrease = Dimensions.X - topWidth;
                float bottomWidthIncrease = Dimensions.X - bottomWidth;
                float leftHeightIncrease = Dimensions.Y - leftHeight;
                float rightHeightIncrease = Dimensions.Y - rightHeight;

                retVal.AddRange(TopBorder.GetDrawCommands(Position, topWidthIncrease, componentLifetime));
                retVal.AddRange(LeftBorder.GetDrawCommands(Position, leftHeightIncrease, componentLifetime));
                retVal.AddRange(RightBorder.GetDrawCommands(Position + new Vector2(topWidthIncrease, 0), rightHeightIncrease, componentLifetime));
                retVal.AddRange(BottomBorder.GetDrawCommands(Position + new Vector2(0, leftHeightIncrease), bottomWidthIncrease, componentLifetime));

                retVal.Add(TopLeftCorner.GetDrawCommand(Position, componentLifetime));
                retVal.Add(TopRightCorner.GetDrawCommand(Position + new Vector2(topWidthIncrease, 0), componentLifetime));
                retVal.Add(BottomLeftCorner.GetDrawCommand(Position + new Vector2(0, leftHeightIncrease), componentLifetime));
                retVal.Add(BottomRightCorner.GetDrawCommand(Position + new Vector2(bottomWidthIncrease, rightHeightIncrease), componentLifetime));
            }
            else if (drawName.Equals(ComponentThemeDrawCommandNames.MonoGUIBackground))
            {

            }

            return retVal;
        }

        Color GetComponentColor(string colorName)
        {
            return Color.White;
        }

        public void LoadContent(ContentManager Content)
        {
            foreach (BoxBorder b in AllBorders)
            {
                b.LoadContent(Content);
            }
            foreach (BoxCorner b in AllCorners)
            {
                b.LoadContent(Content);
            }
        }


        public Point GetMinimumDimensions()
        {
            int minX = int.MaxValue, minY = int.MaxValue, maxX = int.MinValue, maxY = int.MinValue;

            foreach (BoxCorner corner in AllCorners)
            {
                minX = Math.Min(minX, corner.Corner.DestinationBox.X);
                minY = Math.Min(minY, corner.Corner.DestinationBox.Y);
                maxX = Math.Max(maxX, corner.Corner.DestinationBox.Right);
                maxY = Math.Max(maxX, corner.Corner.DestinationBox.Bottom);
            }
            //TODO do borders

            maxX -= minX;
            maxY -= minY;

            return new Point(maxX, maxY);
        }

        public Rectangle? GetThemeLocation(string locationName, Vector2 Position, Vector2 Dimensions, float componentLifetime)
        {
            //TODO THIS
            return null;
        }

        public DrawCommand GetComponentThemeTextureAreaPair(string tapName, Vector2 Position, Vector2 Dimensions, float componentLifetime)
        {
            //TODO THIS
            return null;
        }

        public string GetComponentString(string key)
        {
            //TODO THIS
            return null;
        }




        Color IComponentTheme.GetComponentColor(string colorName)
        {
            return Color.White;
        }
    }
}
