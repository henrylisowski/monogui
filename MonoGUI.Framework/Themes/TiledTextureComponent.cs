﻿using Microsoft.Xna.Framework;
using MonoGUI.Themes.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Themes
{
    public class TiledTextureComponent : IComponentTheme
    {
        public TextureAreaPair Texture { get; set; }

        public List<DrawCommand> GetDrawCommands(string drawName, Microsoft.Xna.Framework.Vector2 Position, Microsoft.Xna.Framework.Vector2 Dimensions, float componentLifetime)
        {
            List<DrawCommand> retVal = new List<DrawCommand>();
            if (Texture != null)
            {
                Rectangle destBox = Texture.DestinationBox;
                Rectangle srcBox = Texture.CurrentSourceBox(componentLifetime);
                for (int i = 0; i < Dimensions.X; i += destBox.Width)
                {
                    int remainingWidth = (int)Dimensions.X - i;
                    int destinationWidth = 0;
                    float sourceWidthMultiplier = 0f;
                    if (destBox.Width < remainingWidth)
                    {
                        //Will need to tile the width at least once more, use full width
                        destinationWidth = destBox.Width;
                        sourceWidthMultiplier = 1f;
                    }
                    else
                    {
                        //Will need to cut down on the width since it's too big
                        sourceWidthMultiplier = (float)remainingWidth / destBox.Width;
                        destinationWidth = (int)(destBox.Width * sourceWidthMultiplier);
                    }
                    for (int j = 0; j < Dimensions.Y; j += destBox.Height)
                    {
                        int remainingHeight = (int)Dimensions.Y - j;
                        int destinationHeight = 0;
                        float sourceHeightMultiplier = 0f;
                        if (destBox.Height < remainingHeight)
                        {
                            destinationHeight = destBox.Height;
                            sourceHeightMultiplier = 1f;
                        }
                        else
                        {
                            sourceHeightMultiplier = (float)remainingHeight / destBox.Height;
                            destinationHeight = (int)(destBox.Height * sourceHeightMultiplier);
                        }
                        Rectangle sourceRectangle = new Rectangle(srcBox.X, srcBox.Y, 
                            (int)(srcBox.Width * sourceWidthMultiplier), (int)(srcBox.Height * sourceHeightMultiplier));
                        Vector2 destPosition = new Vector2(i, j);
                        Vector2 destDimensions = new Vector2(destinationWidth, destinationHeight);
                        //retVal.Add(new DrawCommand(Texture.SourceTexture, Texture.SourceTextureName, sourceRectangle, Texture.Effect,
                        //    destPosition, destDimensions));
                    }
                }
            }
            
            return retVal;
        }

        public Microsoft.Xna.Framework.Rectangle? GetThemeLocation(string locationName, Microsoft.Xna.Framework.Vector2 Position, Microsoft.Xna.Framework.Vector2 Dimensions, float componentLifetime)
        {
            return null;
        }

        public DrawCommand GetComponentThemeTextureAreaPair(string tapName, Microsoft.Xna.Framework.Vector2 Position, Microsoft.Xna.Framework.Vector2 Dimensions, float componentLifetime)
        {
            return null;
        }

        public string GetComponentString(string key)
        {
            return null;
        }

        Color GetComponentColor(string colorName)
        {
            return Color.White;
        }

        public void LoadContent(Microsoft.Xna.Framework.Content.ContentManager Content)
        {
            if (Texture != null)
            {
                Texture.LoadContent(Content);
            }
        }

        public Microsoft.Xna.Framework.Point GetMinimumDimensions()
        {
            return Point.Zero;
        }


        Color IComponentTheme.GetComponentColor(string colorName)
        {
            return Color.White;
        }
    }
}
