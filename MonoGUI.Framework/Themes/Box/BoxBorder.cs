﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Themes.Box
{
    public class BoxBorder
    {
        public BoxBorderStyle Style;
        public BoxBorderDirection Direction;

        [ContentSerializer(Optional = true)]
        public TextureAreaPair StretchBorder { get; set; }

        [ContentSerializer(Optional = true)]
        public TextureAreaPair TileBorder { get; set; }

        [ContentSerializer(Optional = true)]
        public TextureAreaPair GemstoneCenterBorder { get; set; }
        [ContentSerializer(Optional = true)]
        public TextureAreaPair GemstoneFirstBorder { get; set; }
        [ContentSerializer(Optional = true)]
        public TextureAreaPair GemstoneLastBorder { get; set; }

        public Point MinimumDimensions
        {
            get
            {
                switch (Direction)
                {
                    case BoxBorderDirection.Horizontal:
                        switch (Style)
                        {
                            case BoxBorderStyle.Gemstone:

                                break;
                            case BoxBorderStyle.Stretch:

                                break;
                            case BoxBorderStyle.Tile:

                                break;
                        }
                        break;
                    case BoxBorderDirection.Vertical:
                        switch (Style)
                        {
                            case BoxBorderStyle.Gemstone:

                                break;
                            case BoxBorderStyle.Stretch:

                                break;
                            case BoxBorderStyle.Tile:

                                break;
                        }
                        break;
                }

                return new Point();
            }
        }

        public BoxBorder()
        {

        }

        public BoxBorder(BoxBorderStyle Style, BoxBorderDirection Direction)
        {
            this.Style = Style;
            this.Direction = Direction;
        }

        public void LoadContent(ContentManager Content)
        {
            if (StretchBorder != null)
            {
                StretchBorder.LoadContent(Content);
            }
            if (TileBorder != null)
            {
                TileBorder.LoadContent(Content);
            }
            if (GemstoneFirstBorder != null)
            {
                GemstoneFirstBorder.LoadContent(Content);
            }
            if (GemstoneCenterBorder != null)
            {
                GemstoneCenterBorder.LoadContent(Content);
            }
            if (GemstoneLastBorder != null)
            {
                GemstoneLastBorder.LoadContent(Content);
            }
        }

        public List<DrawCommand> GetDrawCommands(Vector2 Position, float increase, float componentLifetime)
        {
            List<DrawCommand> retVal = new List<DrawCommand>();

            if (Direction == BoxBorderDirection.Horizontal)
            {
                switch (Style)
                {
                    case BoxBorderStyle.Gemstone:
                        float halfIncrease = increase / 2;
                        if (GemstoneCenterBorder != null)
                        {
                            Vector2 dimensions = new Vector2(GemstoneCenterBorder.DestinationBox.Width, GemstoneCenterBorder.DestinationBox.Height);
                            Vector2 pos = new Vector2(GemstoneCenterBorder.DestinationBox.X + halfIncrease, GemstoneCenterBorder.DestinationBox.Y);
                            //retVal.Add(new DrawCommand(GemstoneCenterBorder.SourceTexture, GemstoneCenterBorder.SourceTextureName, GemstoneCenterBorder.CurrentSourceBox(componentLifetime),
                            //    GemstoneCenterBorder.Effect, Position + pos, dimensions));
                        }
                        if (GemstoneFirstBorder != null)
                        {
                            Vector2 dimensions = new Vector2(GemstoneFirstBorder.DestinationBox.Width, GemstoneFirstBorder.DestinationBox.Height);
                            dimensions += new Vector2(halfIncrease, 0);
                            Vector2 pos = new Vector2(GemstoneFirstBorder.DestinationBox.X, GemstoneFirstBorder.DestinationBox.Y);
                            //retVal.Add(new DrawCommand(GemstoneFirstBorder.SourceTexture, GemstoneFirstBorder.SourceTextureName, GemstoneFirstBorder.CurrentSourceBox(componentLifetime),
                            //    GemstoneFirstBorder.Effect, Position + pos, dimensions));
                        }
                        if (GemstoneLastBorder != null)
                        {
                            Vector2 dimensions = new Vector2(GemstoneLastBorder.DestinationBox.Width, GemstoneLastBorder.DestinationBox.Height);
                            dimensions += new Vector2(halfIncrease, 0);
                            Vector2 pos = new Vector2(GemstoneLastBorder.DestinationBox.X + halfIncrease, GemstoneLastBorder.DestinationBox.Y);
                            //retVal.Add(new DrawCommand(GemstoneLastBorder.SourceTexture, GemstoneLastBorder.SourceTextureName, GemstoneLastBorder.CurrentSourceBox(componentLifetime),
                            //    GemstoneLastBorder.Effect, Position + pos, dimensions));
                        }
                        break;
                    case BoxBorderStyle.Stretch:
                        if (StretchBorder != null)
                        {
                            Vector2 dimensions = new Vector2(StretchBorder.DestinationBox.Width, StretchBorder.DestinationBox.Height);
                            dimensions += new Vector2(increase, 0);
                            Vector2 pos = new Vector2(StretchBorder.DestinationBox.X, StretchBorder.DestinationBox.Y);
                            //retVal.Add(new DrawCommand(StretchBorder.SourceTexture, StretchBorder.SourceTextureName,
                            //    StretchBorder.CurrentSourceBox(componentLifetime), StretchBorder.Effect, Position + pos, dimensions));
                        }
                        break;
                    case BoxBorderStyle.Tile:
                        if (TileBorder != null)
                        {
                            float expectedHeight = TileBorder.DestinationBox.Height;
                            float textureHeight = TileBorder.CurrentSourceBox(componentLifetime).Height;
                            float scale = expectedHeight / textureHeight;
                            float destinationWidth = TileBorder.CurrentSourceBox(componentLifetime).Width * scale;

                            int numTiles = (int)((TileBorder.DestinationBox.Width + increase) / destinationWidth);


                            //Odd Number of tiles
                            float currentXPosition = TileBorder.DestinationBox.X;
                            for (int i = 0; i < numTiles / 2; i++)
                            {
                                //retVal.Add(new DrawCommand(TileBorder.SourceTexture, TileBorder.SourceTextureName, TileBorder.CurrentSourceBox(componentLifetime), TileBorder.Effect,
                                //    new Vector2(currentXPosition, TileBorder.DestinationBox.Y) + Position,
                                //    new Vector2(destinationWidth, TileBorder.DestinationBox.Height)));

                                currentXPosition += destinationWidth;
                            }

                            float totalFullCovered = destinationWidth * (numTiles - 1);
                            float fill = (TileBorder.DestinationBox.Width + increase) - totalFullCovered;

                            //retVal.Add(new DrawCommand(TileBorder.SourceTexture, TileBorder.SourceTextureName, TileBorder.CurrentSourceBox(componentLifetime), TileBorder.Effect,
                            //        new Vector2(currentXPosition, TileBorder.DestinationBox.Y) + Position,
                            //        new Vector2(fill, TileBorder.DestinationBox.Height)));
                            currentXPosition += fill;
                            for (int i = numTiles / 2 + 1; i < numTiles; i++)
                            {
                                //retVal.Add(new DrawCommand(TileBorder.SourceTexture, TileBorder.SourceTextureName, TileBorder.CurrentSourceBox(componentLifetime), TileBorder.Effect,
                                //    new Vector2(currentXPosition, TileBorder.DestinationBox.Y) + Position,
                                //    new Vector2(destinationWidth, TileBorder.DestinationBox.Height)));

                                currentXPosition += destinationWidth;
                            }
                            
                        }
                        break;
                }
            }
            else if (Direction == BoxBorderDirection.Vertical)
            {
                switch (Style)
                {
                    case BoxBorderStyle.Gemstone:
                        float halfIncrease = increase / 2;
                        if (GemstoneCenterBorder != null)
                        {
                            Vector2 dimensions = new Vector2(GemstoneCenterBorder.DestinationBox.Width, GemstoneCenterBorder.DestinationBox.Height);
                            Vector2 pos = new Vector2(GemstoneCenterBorder.DestinationBox.X, GemstoneCenterBorder.DestinationBox.Y + halfIncrease);
                            //retVal.Add(new DrawCommand(GemstoneCenterBorder.SourceTexture, GemstoneCenterBorder.SourceTextureName, GemstoneCenterBorder.CurrentSourceBox(componentLifetime),
                            //    GemstoneCenterBorder.Effect, Position + pos, dimensions));
                        }
                        if (GemstoneFirstBorder != null)
                        {
                            Vector2 dimensions = new Vector2(GemstoneFirstBorder.DestinationBox.Width, GemstoneFirstBorder.DestinationBox.Height);
                            dimensions += new Vector2(0, halfIncrease);
                            Vector2 pos = new Vector2(GemstoneFirstBorder.DestinationBox.X, GemstoneFirstBorder.DestinationBox.Y);
                            //retVal.Add(new DrawCommand(GemstoneFirstBorder.SourceTexture, GemstoneFirstBorder.SourceTextureName, GemstoneFirstBorder.CurrentSourceBox(componentLifetime),
                            //    GemstoneFirstBorder.Effect, Position + pos, dimensions));
                        }
                        if (GemstoneLastBorder != null)
                        {
                            Vector2 dimensions = new Vector2(GemstoneLastBorder.DestinationBox.Width, GemstoneLastBorder.DestinationBox.Height);
                            dimensions += new Vector2(0, halfIncrease);
                            Vector2 pos = new Vector2(GemstoneLastBorder.DestinationBox.X, GemstoneLastBorder.DestinationBox.Y + halfIncrease);
                            //retVal.Add(new DrawCommand(GemstoneLastBorder.SourceTexture, GemstoneLastBorder.SourceTextureName, GemstoneLastBorder.CurrentSourceBox(componentLifetime),
                            //    GemstoneLastBorder.Effect, Position + pos, dimensions));
                        }
                        break;
                    case BoxBorderStyle.Stretch:
                        if (StretchBorder != null)
                        {
                            Vector2 dimensions = new Vector2(StretchBorder.DestinationBox.Width, StretchBorder.DestinationBox.Height);
                            dimensions += new Vector2(0, increase);
                            Vector2 pos = new Vector2(StretchBorder.DestinationBox.X, StretchBorder.DestinationBox.Y);
                            //retVal.Add(new DrawCommand(StretchBorder.SourceTexture, StretchBorder.SourceTextureName, 
                            //    StretchBorder.CurrentSourceBox(componentLifetime), StretchBorder.Effect, Position + pos, dimensions));
                        }
                        break;
                    case BoxBorderStyle.Tile:
                        if (TileBorder != null)
                        {
                            float expectedWidth = TileBorder.DestinationBox.Width;
                            float textureWidth = TileBorder.CurrentSourceBox(componentLifetime).Width;
                            float scale = expectedWidth / textureWidth;
                            float destinationHeight = TileBorder.CurrentSourceBox(componentLifetime).Height * scale;

                            int numTiles = (int)((TileBorder.DestinationBox.Height + increase) / destinationHeight);


                            //Odd Number of tiles
                            float currentYPosition = TileBorder.DestinationBox.Y;
                            for (int i = 0; i < numTiles / 2; i++)
                            {
                                //retVal.Add(new DrawCommand(TileBorder.SourceTexture, TileBorder.SourceTextureName, TileBorder.CurrentSourceBox(componentLifetime), TileBorder.Effect,
                                //    new Vector2(TileBorder.DestinationBox.X, currentYPosition) + Position,
                                //    new Vector2(TileBorder.DestinationBox.Width, destinationHeight)));

                                currentYPosition += destinationHeight;
                            }

                            float totalFullCovered = destinationHeight * (numTiles - 1);
                            float fill = (TileBorder.DestinationBox.Height + increase) - totalFullCovered;

                            //retVal.Add(new DrawCommand(TileBorder.SourceTexture, TileBorder.SourceTextureName, TileBorder.CurrentSourceBox(componentLifetime), TileBorder.Effect,
                            //        new Vector2(TileBorder.DestinationBox.X, currentYPosition) + Position,
                            //        new Vector2(TileBorder.DestinationBox.Width, fill)));

                            currentYPosition += fill;
                            for (int i = numTiles / 2 + 1; i < numTiles; i++)
                            {
                                //retVal.Add(new DrawCommand(TileBorder.SourceTexture, TileBorder.SourceTextureName, TileBorder.CurrentSourceBox(componentLifetime), TileBorder.Effect,
                                //    new Vector2(TileBorder.DestinationBox.X, currentYPosition) + Position,
                                //    new Vector2(TileBorder.DestinationBox.Width, destinationHeight)));

                                currentYPosition += destinationHeight;
                            }
                            
                        }
                        break;
                }
            }

            return retVal;
        }
    }
}
