﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Themes.Box
{
    public enum BoxBorderStyle
    {
        Stretch, Tile, Gemstone
    }
}
