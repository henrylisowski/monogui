﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Themes.Box
{
    public class BoxCorner
    {
        public TextureAreaPair Corner { get; set; }
        public BoxCorner()
        {
            Corner = new TextureAreaPair();
        }

        public BoxCorner(TextureAreaPair Corner)
        {
            this.Corner = Corner;
        }

        public DrawCommand GetDrawCommand(Vector2 Position, float componentLifetime)
        {
            return null;
            //return new DrawCommand(Corner.SourceTexture, Corner.SourceTextureName, Corner.CurrentSourceBox(componentLifetime),
            //    Corner.Effect, Position + new Vector2(Corner.DestinationBox.X, Corner.DestinationBox.Y),
            //    new Vector2(Corner.DestinationBox.Width, Corner.DestinationBox.Height));
        }

        public void LoadContent(ContentManager Content)
        {
            if (Corner != null)
            {
                Corner.LoadContent(Content);
            }
        }
    }
}
