﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using MonoGUI.Themes.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Themes.ProgressBar
{
    public class OneSizeProgressBarTheme : IComponentTheme
    {
        public TextureAreaPair MainForeground { get; set; }
        [ContentSerializer(Optional = true)]
        public TextureAreaPair MainBackground { get; set; }

        public TextureAreaPair FillStencil { get; set; }
        public TextureAreaPair FillTexture { get; set; }
        [ContentSerializer(Optional = true)]
        public TextureAreaPair FillStencilClipping { get; set; }
        
        [ContentSerializer(Optional=true)]
        public TextureAreaPair SecondaryFillStencil { get; set; }
        [ContentSerializer(Optional=true)]
        public TextureAreaPair SecondaryFillTexture { get; set; }
        [ContentSerializer(Optional = true)]
        public TextureAreaPair SecondaryFillStencilClipping { get; set; }

        public List<DrawCommand> GetDrawCommands(string drawName, Vector2 Position, Vector2 Dimensions, float componentLifetime)
        {
            List<DrawCommand> retVal = new List<DrawCommand>();
            if (drawName.Equals(ComponentThemeDrawCommandNames.MonoGUIForeground))
            {
                Vector2 scale = Dimensions / new Vector2(MainForeground.DestinationBox.Width, MainForeground.DestinationBox.Height);
                //retVal.Add(new DrawCommand(MainForeground.SourceTexture, MainForeground.SourceTextureName,
                //    MainForeground.CurrentSourceBox(componentLifetime), MainForeground.Effect,
                //    new Microsoft.Xna.Framework.Vector2(MainForeground.DestinationBox.X, MainForeground.DestinationBox.Y) * scale + Position,
                //    new Microsoft.Xna.Framework.Vector2(MainForeground.DestinationBox.Width, MainForeground.DestinationBox.Height) * scale));
            }
            else if (drawName.Equals(ComponentThemeDrawCommandNames.MonoGUIBackground))
            {
                if (MainBackground != null)
                {
                    Vector2 scale = Dimensions / new Vector2(MainForeground.DestinationBox.Width, MainForeground.DestinationBox.Height);
                    //retVal.Add(new DrawCommand(MainBackground.SourceTexture, MainBackground.SourceTextureName,
                    //    MainBackground.CurrentSourceBox(componentLifetime), MainBackground.Effect,
                    //    new Microsoft.Xna.Framework.Vector2(MainBackground.DestinationBox.X, MainBackground.DestinationBox.Y) * scale + Position,
                    //    new Microsoft.Xna.Framework.Vector2(MainBackground.DestinationBox.Width, MainBackground.DestinationBox.Height) * scale));
                }
            }
            return retVal;
        }

        public Microsoft.Xna.Framework.Rectangle? GetThemeLocation(string locationName, Microsoft.Xna.Framework.Vector2 Position, Microsoft.Xna.Framework.Vector2 Dimensions, float componentLifetime)
        {
            return null;
        }


        public string GetComponentString(string key)
        {
            return null;
        }

        public Color GetComponentColor(string colorName)
        {
            return Color.White;
        }

        public DrawCommand GetComponentThemeTextureAreaPair(string tapName, Microsoft.Xna.Framework.Vector2 Position, Microsoft.Xna.Framework.Vector2 Dimensions, float componentLifetime)
        {
            TextureAreaPair active = null;
            if (tapName.Equals(ComponentThemeTAPNames.ProgressBarFiller))
            {
                active = FillTexture;
            }
            if (tapName.Equals(ComponentThemeTAPNames.ProgressBarGradientStencil))
            {
                active = FillStencil;
            }
            if (tapName.Equals(ComponentThemeTAPNames.ProgressBarSecondaryFiller))
            {
                active = SecondaryFillTexture;
            }
            if (tapName.Equals(ComponentThemeTAPNames.ProgressBarSecondaryGradientStencil))
            {
                active = SecondaryFillStencil;
            }
            if (tapName.Equals(ComponentThemeTAPNames.ProgressBarStencilClipping))
            {
                active = FillStencilClipping;
            }
            if (tapName.Equals(ComponentThemeTAPNames.ProgressBarSecondaryStencilClipping))
            {
                active = SecondaryFillStencilClipping;
            }

            if (active != null)
            {
                Vector2 scale = Dimensions / new Vector2(MainForeground.DestinationBox.Width, MainForeground.DestinationBox.Height);
                Vector2 pos = new Vector2(active.DestinationBox.X, active.DestinationBox.Y) * scale + Position;
                Vector2 dims = new Vector2(active.DestinationBox.Width, active.DestinationBox.Height) * scale;
                //return new DrawCommand(active.SourceTexture, active.SourceTextureName, active.CurrentSourceBox(componentLifetime), active.Effect,
                //    pos, dims);
            }

            return null;
        }

        public void LoadContent(Microsoft.Xna.Framework.Content.ContentManager Content)
        {
            MainForeground.LoadContent(Content);
            if (MainBackground != null)
            {
                MainBackground.LoadContent(Content);
            }

            FillTexture.LoadContent(Content);
            FillStencil.LoadContent(Content);
            if(FillStencilClipping != null)
            {
                FillStencilClipping.LoadContent(Content);
            }

            if (SecondaryFillTexture != null)
            {
                SecondaryFillTexture.LoadContent(Content);
            }
            if(SecondaryFillStencil != null)
            {
                SecondaryFillStencil.LoadContent(Content);
            }
            if (SecondaryFillStencilClipping != null)
            {
                SecondaryFillStencilClipping.LoadContent(Content);
            }


        }

        public Microsoft.Xna.Framework.Point GetMinimumDimensions()
        {
            return new Microsoft.Xna.Framework.Point((int)MainForeground.DestinationBox.Width, (int)MainForeground.DestinationBox.Height);
        }


        
    }
}
