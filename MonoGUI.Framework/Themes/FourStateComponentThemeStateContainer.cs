﻿using Microsoft.Xna.Framework.Content;
using MonoGUI.Animations;
using MonoGUI.ScreenItems;
using MonoGUI.Themes.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Themes
{
    public class FourStateComponentThemeStateContainer : IComponentThemeStateContainer
    {
        [ContentSerializerAttribute(Optional = true)]
        public IComponentTheme ActiveTheme { get; set; }
        [ContentSerializerAttribute(Optional = true)]
        public IComponentTheme FocussedTheme { get; set; }
        [ContentSerializerAttribute(Optional = true)]
        public IComponentTheme DisabledTheme { get; set; }
        [ContentSerializerAttribute(Optional = true)]
        public IComponentTheme PressedTheme { get; set; }
        [ContentSerializerAttribute(Optional = true)]
        public IComponentTheme MouseDirectlyOverTheme { get; set; }

        [ContentSerializerAttribute(Optional = true)]
        public Dictionary<string, List<IModifier>> ActiveModifiers { get; set; }
        [ContentSerializerAttribute(Optional = true)]
        public Dictionary<string, List<IModifier>> FocussedModifiers { get; set; }
        [ContentSerializerAttribute(Optional = true)]
        public Dictionary<string, List<IModifier>> DisabledModifiers { get; set; }
        [ContentSerializerAttribute(Optional = true)]
        public Dictionary<string, List<IModifier>> PressedModifiers { get; set; }
        [ContentSerializerAttribute(Optional = true)]
        public Dictionary<string, List<IModifier>> MouseDirectlyOverModifiers { get; set; }

        public FourStateComponentThemeStateContainer()
        {
            ActiveModifiers = new Dictionary<string,List<IModifier>>();
            FocussedModifiers = new Dictionary<string,List<IModifier>>();
            DisabledModifiers = new Dictionary<string,List<IModifier>>();
            PressedModifiers = new Dictionary<string,List<IModifier>>();
            MouseDirectlyOverModifiers = new Dictionary<string, List<IModifier>>();
        }

        public void LoadContent(ContentManager Content)
        {
            if (ActiveTheme != null)
            {
                ActiveTheme.LoadContent(Content);
            }
            if (FocussedTheme != null)
            {
                FocussedTheme.LoadContent(Content);
            }
            if (DisabledTheme != null)
            {
                DisabledTheme.LoadContent(Content);
            }
            if (PressedTheme != null)
            {
                PressedTheme.LoadContent(Content);
            }
            if (MouseDirectlyOverTheme != null)
            {
                MouseDirectlyOverTheme.LoadContent(Content);
            }
        }

        public IComponentTheme GetProperStateComponentTheme(ScreenItemVisualStates visualState)
        {
            if (visualState == ScreenItemVisualStates.Disabled && DisabledTheme != null)
            {
                return DisabledTheme;
            }

            if (visualState == ScreenItemVisualStates.Hover && MouseDirectlyOverTheme != null)
            {
                return MouseDirectlyOverTheme;
            }

            if (visualState == ScreenItemVisualStates.Focussed && FocussedTheme != null)
            {
                return FocussedTheme;
            }

            if (visualState == ScreenItemVisualStates.Pressed && PressedTheme != null)
            {
                return PressedTheme;
            }

            return ActiveTheme;
        }


        public Dictionary<string, List<Animations.IModifier>> GetProperStateModifiers(ScreenItemVisualStates visualState)
        {
            if (visualState == ScreenItemVisualStates.Disabled)
            {
                return DisabledModifiers;
            }

            if (visualState == ScreenItemVisualStates.Hover)
            {
                return MouseDirectlyOverModifiers;
            }

            if (visualState == ScreenItemVisualStates.Focussed)
            {
                return FocussedModifiers;
            }

            if (visualState == ScreenItemVisualStates.Pressed)
            {
                return PressedModifiers;
            }

            return ActiveModifiers;
        }
    }
}
