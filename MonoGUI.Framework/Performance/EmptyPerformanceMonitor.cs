﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Performance
{
    public class EmptyPerformanceMonitor : IPerformanceMonitor
    {
        public void StartTimer(string name)
        {
            
        }

        public void StopTimer(string name)
        {
            
        }

        public void AddMethodCall(string name)
        {
            
        }

        public SinglePerformanceCall PerformanceValue(string name)
        {
            return new SinglePerformanceCall(0);
        }

        public SinglePerformanceCall AveragePerformanceValue(string name)
        {
            return new SinglePerformanceCall(0);
        }

        public int MethodCalls(string name)
        {
            return 0;
        }

        public float AverageMethodCalls(string name)
        {
            return 0;
        }

        public void StartNew()
        {
            
        }

        public void StopSampling()
        {
            
        }

        public void StartUpdate()
        {
            
        }

        public void StopUpdate()
        {
            
        }

        public void StartDraw()
        {
            
        }

        public void StopDraw()
        {
            
        }

        public void AddDrawCall()
        {
            
        }

        public void AddDrawBatchCall()
        {
            
        }

        public void AddTextureSwap()
        {
            
        }

        public void AddRequestResize()
        {
            
        }
    }
}
