﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace MonoGUI.Performance
{
    public class PerformanceMonitor : IPerformanceMonitor
    {
        public const string UPDATE_LOOP_NAME = "_update_loop";
        public const string DRAW_LOOP_NAME = "_draw_loop";
        public const string DRAW_CALLS = "_draw_calls";
        public const string DRAW_BATCH_CALLED = "_draw_batch_calls";
        public const string DRAW_TEXTURE_SWAP = "_draw_texture_swap";
        public const string REQUEST_RESIZE = "_request_resize";
        public const int SAMPLE_AMOUNT = 30;

        private Dictionary<string, List<Stopwatch>> _stopwatchMap;
        private Dictionary<string, SinglePerformanceCall> _timerCallMap;
        private Dictionary<string, int> _methodCallMap;

        private Dictionary<string, LinkedList<SinglePerformanceCall>> _timerSampleMap;
        private Dictionary<string, List<int>> _timerMethodCallMap;
        private List<string> _timerSampleWritten;
        private List<string> _timerMethodWritten;

        private bool _sampling;

        public PerformanceMonitor()
        {
            _stopwatchMap = new Dictionary<string, List<Stopwatch>>();
            _timerCallMap = new Dictionary<string, SinglePerformanceCall>();
            _methodCallMap = new Dictionary<string, int>();
            _timerSampleMap = new Dictionary<string, LinkedList<SinglePerformanceCall>>();
            _timerSampleWritten = new List<string>();
            _timerMethodCallMap = new Dictionary<string, List<int>>();
            _timerMethodWritten = new List<string>();
            _sampling = false;
        }

        public void StartTimer(string name)
        {
            if (_sampling)
            {
                if (!_stopwatchMap.ContainsKey(name))
                {
                    _stopwatchMap.Add(name, new List<Stopwatch>());
                }
                Stopwatch s = new Stopwatch();
                s.Start();
                _stopwatchMap[name].Add(s);
            }
        }

        public void StopTimer(string name)
        {
            if (_sampling)
            {
                if (_stopwatchMap.ContainsKey(name))
                {
                    List<Stopwatch> stopwatches = _stopwatchMap[name];
                    if (stopwatches.Count > 0)
                    {
                        Stopwatch s = stopwatches[stopwatches.Count - 1];
                        stopwatches.Remove(s);
                        s.Stop();
                        SinglePerformanceCall toAdd = null;
                        if (_timerCallMap.ContainsKey(name))
                        {
                            _timerCallMap[name].AddCall(s.Elapsed.TotalSeconds);
                            toAdd = _timerCallMap[name];
                        }
                        else
                        {
                            toAdd = new SinglePerformanceCall(s.Elapsed.TotalSeconds);
                            _timerCallMap.Add(name, toAdd);
                        }

                        if (_timerSampleWritten.Contains(name))
                        {
                            //new queue already added, need to add another iteration
                            //_timerSampleMap[name]Add(new SinglePerformanceCall(s.Elapsed.TotalSeconds));
                        }
                        else
                        {
                            _timerSampleWritten.Add(name);
                            //need to add another queue, remove if we're above sample count
                            if (!_timerSampleMap.ContainsKey(name))
                            {
                                _timerSampleMap.Add(name, new LinkedList<SinglePerformanceCall>());
                            }

                            _timerSampleMap[name].AddLast(toAdd);
                            if (_timerSampleMap[name].Count > SAMPLE_AMOUNT)
                            {
                                _timerSampleMap[name].RemoveFirst();
                            }
                        }
                    }
                }
            }
        }

        public void AddMethodCall(string name)
        {
            if (_sampling)
            {
                if (!_methodCallMap.ContainsKey(name))
                {
                    _methodCallMap.Add(name, 0);
                }
                _methodCallMap[name] = _methodCallMap[name] + 1;

                bool marked = true;
                if (!_timerSampleWritten.Contains(name))
                {
                    _timerSampleWritten.Add(name);
                    marked = false;
                }

                if (marked)
                {
                    _timerMethodCallMap[name][_timerMethodCallMap[name].Count - 1] = _methodCallMap[name];
                }
                else
                {
                    if (!_timerMethodCallMap.ContainsKey(name))
                    {
                        _timerMethodCallMap.Add(name, new List<int>());
                    }
                    _timerMethodCallMap[name].Add(_methodCallMap[name]);
                    if (_timerMethodCallMap[name].Count > SAMPLE_AMOUNT)
                    {
                        _timerMethodCallMap[name].RemoveAt(0);
                    }

                }
            }
        }

        public SinglePerformanceCall PerformanceValue(string name)
        {
            if (_timerCallMap.ContainsKey(name))
            {
                return _timerCallMap[name];
            }
            return new SinglePerformanceCall(0);
        }

        public SinglePerformanceCall AveragePerformanceValue(string name)
        {
            if (_timerSampleMap.ContainsKey(name))
            {
                SinglePerformanceCall retVal = new SinglePerformanceCall(_timerSampleMap[name].ToList());
                return retVal;
            }
            return new SinglePerformanceCall(0);
        }

        public int MethodCalls(string name)
        {
            if (_methodCallMap.ContainsKey(name))
            {
                return _methodCallMap[name];
            }
            return 0;
        }

        public float AverageMethodCalls(string name)
        {
            if (_timerMethodCallMap.ContainsKey(name))
            {
                float retVal = 0f;
                foreach (int i in _timerMethodCallMap[name])
                {
                    retVal += i;
                }
                if (_timerMethodCallMap[name].Count > 0)
                {
                    retVal /= (float)_timerMethodCallMap[name].Count;
                }
                return retVal;
            }
            return 0f;
        }

        public void StartNew()
        {
            foreach (List<Stopwatch> l in _stopwatchMap.Values)
            {
                l.Clear();
            }
            _timerCallMap.Clear();
            _methodCallMap.Clear();
            _timerSampleWritten.Clear();
            _timerMethodWritten.Clear();
            _sampling = true;
        }

        public void StopSampling()
        {
            _sampling = false;
        }

        public void StartUpdate()
        {
            StartTimer(UPDATE_LOOP_NAME);
        }

        public void StopUpdate()
        {
            StopTimer(UPDATE_LOOP_NAME);
        }

        public void StartDraw()
        {
            StartTimer(DRAW_LOOP_NAME);
        }

        public void StopDraw()
        {
            StopTimer(DRAW_LOOP_NAME);
        }

        public void AddDrawCall()
        {
            AddMethodCall(DRAW_CALLS);
        }

        public void AddDrawBatchCall()
        {
            AddMethodCall(DRAW_BATCH_CALLED);
        }

        public void AddTextureSwap()
        {
            AddMethodCall(DRAW_TEXTURE_SWAP);
        }

        public void AddRequestResize()
        {
            AddMethodCall(REQUEST_RESIZE);
        }
    }

    public class SinglePerformanceCall
    {
        public List<double> AllPerformanceCalls { get; private set; }
        public double MinValue { get; private set; }
        public double MaxValue { get; private set; }
        public double AverageValue { get; private set; }
        public double TotalValue { get; private set; }
        public int NumberOfValues { get; private set; }

        public SinglePerformanceCall(double InitialValue)
        {
            AllPerformanceCalls = new List<double>();
            AllPerformanceCalls.Add(InitialValue);
            MinValue = InitialValue;
            MaxValue = InitialValue;
            AverageValue = InitialValue;
            TotalValue = InitialValue;
            NumberOfValues = 1;
        }

        public SinglePerformanceCall(List<SinglePerformanceCall> PerformanceCallsToAverage)
        {
            MinValue = 0;
            MaxValue = 0;
            AverageValue = 0;
            TotalValue = 0;
            NumberOfValues = 0;

            foreach (SinglePerformanceCall c in PerformanceCallsToAverage)
            {
                MinValue += c.MinValue;
                MaxValue += c.MaxValue;
                AverageValue += c.AverageValue;
                TotalValue += c.TotalValue;
                NumberOfValues += c.NumberOfValues;
            }

            MinValue /= PerformanceCallsToAverage.Count;
            MaxValue /= PerformanceCallsToAverage.Count;
            AverageValue /= PerformanceCallsToAverage.Count;
            TotalValue /= PerformanceCallsToAverage.Count;
            NumberOfValues = (int)(NumberOfValues / (float)PerformanceCallsToAverage.Count);
        }

        public void AddCall(double value)
        {
            AllPerformanceCalls.Add(value);
            NumberOfValues++;
            if (value < MinValue)
            {
                MinValue = value;
            }
            if (value > MaxValue)
            {
                MaxValue = value;
            }
            TotalValue += value;
            AverageValue = TotalValue / NumberOfValues;
        }
    }
}
