﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGUI.Manager;
using MonoGUI.ScreenItems.Containers.CanvasContainer;
using MonoGUI.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Performance
{
    public class PerformanceMonitorOverlay : CanvasFrame
    {
        private IPerformanceMonitor _monitor;
        private Texture2D _pixelTexture;
        private Label _elapsedTime;
        private Label _drawTime;
        private Label _drawCalls;
        private Label _drawBatchCalls;
        private Label _drawTextureSwap;
        private Label _resizeCount;
        private Icon _updateBar;
        public PerformanceMonitorOverlay(ScreenManager Manager)
            : base(Manager)
        {
            Width = ScreenItems.ItemSizeOptions.FillParent;
            Height = ScreenItems.ItemSizeOptions.FillParent;

            _elapsedTime = new Label(Manager, "");
            _elapsedTime.Width = ScreenItems.ItemSizeOptions.Percentage;
            _elapsedTime.Height = ScreenItems.ItemSizeOptions.Percentage;
            _elapsedTime.PreferredDimensions = new Microsoft.Xna.Framework.Vector2(0.5f, 0.1f);

            _drawTime = new Label(Manager, "");
            _drawTime.Width = ScreenItems.ItemSizeOptions.Percentage;
            _drawTime.Height = ScreenItems.ItemSizeOptions.Percentage;
            _drawTime.PreferredDimensions = new Microsoft.Xna.Framework.Vector2(0.5f, 0.1f);
            _drawTime.HorizontalAlignment = ScreenItems.AlignH.Right;

            _drawCalls = new Label(Manager, "");
            _drawCalls.Width = ScreenItems.ItemSizeOptions.Percentage;
            _drawCalls.Height = ScreenItems.ItemSizeOptions.Percentage;
            _drawCalls.PreferredDimensions = new Vector2(0.5f, 0.1f);
            _drawCalls.PreferredPosition = new Vector2(0, 0.1f);
            _drawCalls.VerticalPositionType = ScreenItems.ItemPositionOptions.Percentage;

            _drawBatchCalls = new Label(Manager, "");
            _drawBatchCalls.Width = ScreenItems.ItemSizeOptions.Percentage;
            _drawBatchCalls.Height = ScreenItems.ItemSizeOptions.Percentage;
            _drawBatchCalls.PreferredDimensions = new Vector2(0.5f, 0.1f);
            _drawBatchCalls.PreferredPosition = new Vector2(0, 0.2f);
            _drawBatchCalls.VerticalPositionType = ScreenItems.ItemPositionOptions.Percentage;

            _drawTextureSwap = new Label(Manager, "");
            _drawTextureSwap.Width = ScreenItems.ItemSizeOptions.Percentage;
            _drawTextureSwap.Height = ScreenItems.ItemSizeOptions.Percentage;
            _drawTextureSwap.PreferredDimensions = new Vector2(0.5f, 0.1f);
            _drawTextureSwap.PreferredPosition = new Vector2(0, 0.3f);
            _drawTextureSwap.VerticalPositionType = ScreenItems.ItemPositionOptions.Percentage;

            _resizeCount = new Label(Manager, "");
            _resizeCount.Width = ScreenItems.ItemSizeOptions.Percentage;
            _resizeCount.Height = ScreenItems.ItemSizeOptions.Percentage;
            _resizeCount.PreferredDimensions = new Vector2(0.5f, 0.1f);
            _resizeCount.PreferredPosition = new Vector2(0, 0.4f);
            _resizeCount.VerticalPositionType = ScreenItems.ItemPositionOptions.Percentage;

            DrawColor = new Color(1f, 1f, 1f, 0.2f);
        }

        public override void LoadContent(Microsoft.Xna.Framework.Content.ContentManager Content)
        {
            base.LoadContent(Content);
            _pixelTexture = new Texture2D(Manager.Graphics.GraphicsDevice, 1, 1);
            Color[] c = new Color[] { Color.White };
            _pixelTexture.SetData<Color>(c);

            _updateBar = new Icon(Manager, _pixelTexture);
            _updateBar.Height = ScreenItems.ItemSizeOptions.Percentage;
            _updateBar.Width = ScreenItems.ItemSizeOptions.FillParent;
            _updateBar.PreferredDimensions = new Vector2(1f, 0.1f);
            _updateBar.DrawColor = Color.Red;
            AddChild(_updateBar);
            AddChild(_elapsedTime);
            AddChild(_drawTime);
            AddChild(_drawCalls);
            AddChild(_drawBatchCalls);
            AddChild(_drawTextureSwap);
            AddChild(_resizeCount);
        }

        public override void Update(Microsoft.Xna.Framework.GameTime gameTime)
        {
            base.Update(gameTime);
            _monitor = PF.Instance;
            SinglePerformanceCall updateCalls = _monitor.AveragePerformanceValue(PerformanceMonitor.UPDATE_LOOP_NAME);
            SinglePerformanceCall drawCalls = _monitor.AveragePerformanceValue(PerformanceMonitor.DRAW_LOOP_NAME);
            _elapsedTime.SetText(updateCalls.TotalValue.ToString("0.0000000") + " " + updateCalls.NumberOfValues, false);
            _drawTime.SetText(drawCalls.TotalValue.ToString("0.0000000") + " " + drawCalls.NumberOfValues, false);

            float drawCall = _monitor.AverageMethodCalls(PerformanceMonitor.DRAW_CALLS);
            _drawCalls.SetText("Draw Calls: " + drawCall, false);

            float drawCallBatch = _monitor.AverageMethodCalls(PerformanceMonitor.DRAW_BATCH_CALLED);
            _drawBatchCalls.SetText("Draw Batch Calls: " + drawCallBatch, false);

            float drawCallTexture = _monitor.AverageMethodCalls(PerformanceMonitor.DRAW_TEXTURE_SWAP);
            _drawTextureSwap.SetText("Draw Textures: " + drawCallTexture, false);

            float resizes = _monitor.AverageMethodCalls(PerformanceMonitor.REQUEST_RESIZE);
            _resizeCount.SetText("Resizes: " + resizes, false);

            double totalLoopTime = updateCalls.TotalValue + drawCalls.TotalValue;
            double updatePercentage = updateCalls.TotalValue / totalLoopTime;

            _updateBar.RelativeDimensions = new Vector2(
                RelativeDimensions.X * (float)updatePercentage, _updateBar.RelativeDimensions.Y);
        }
    }
}
