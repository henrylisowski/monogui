﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Performance
{
    public interface IPerformanceMonitor
    {
        void StartTimer(string name);
        void StopTimer(string name);
        void AddMethodCall(string name);
        SinglePerformanceCall PerformanceValue(string name);
        SinglePerformanceCall AveragePerformanceValue(string name);
        int MethodCalls(string name);
        float AverageMethodCalls(string name);
        void StartNew();
        void StopSampling();
        void StartUpdate();
        void StopUpdate();
        void StartDraw();
        void StopDraw();
        void AddDrawCall();
        void AddDrawBatchCall();
        void AddTextureSwap();
        void AddRequestResize();
    }
}
