﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Performance
{
    public static class PF
    {
        private static IPerformanceMonitor _instance;
        public static IPerformanceMonitor Instance
        {
            get
            {
                if (_instance == null)
                {
                    switch (_currentPerformanceMode)
                    {
                        case PerformanceMode.Off:
                            _instance = new EmptyPerformanceMonitor();
                            break;
                        case PerformanceMode.On:
                            _instance = new PerformanceMonitor();
                            break;
                    }
                }
                return _instance;
            }
        }

        private static PerformanceMode _currentPerformanceMode;
        public static PerformanceMode CurrentPerformanceMode { get { return _currentPerformanceMode; } }
        public static void SetPerformanceMode(PerformanceMode Mode)
        {
            if (_currentPerformanceMode != Mode)
            {
                _currentPerformanceMode = Mode;
                switch (_currentPerformanceMode)
                {
                    case PerformanceMode.Off:
                        _instance = new EmptyPerformanceMonitor();
                        break;
                    case PerformanceMode.On:
                        _instance = new PerformanceMonitor();
                        break;
                }
            }
        }

        public static void TogglePerformanceMode()
        {
            if (_currentPerformanceMode == PerformanceMode.On)
            {
                SetPerformanceMode(PerformanceMode.Off);
            }
            else if (_currentPerformanceMode == PerformanceMode.Off)
            {
                SetPerformanceMode(PerformanceMode.On);
            }
        }

        public enum PerformanceMode { Off, On };
    }
}
