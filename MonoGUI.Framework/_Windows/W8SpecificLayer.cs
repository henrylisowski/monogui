﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGUI.Manager;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Store;
using Windows.Data.Xml.Dom;
using Windows.Storage;

namespace MonoGUI_W8.Manager
{
    public class W8SpecificLayer : PlatformSpecificLayer
    {
        public override Microsoft.Xna.Framework.Graphics.Texture2D loadTexture2DFromFile(GraphicsDevice graphicsDevice, string fileName)
        {
            //TODO THIS REALLY NEEDS TO BE TESTED!!!!!!!!!!
            Texture2D image;
            var task = Task.Run<Stream>(() => openStream(fileName));

            task.Wait();
            Stream s = task.Result;
            using (s)
            {
                image = Texture2D.FromStream(graphicsDevice, s);
            }
            return image;
        }

        private async Task<Stream> openStream(String fileName){
            StorageFile f = await StorageFile.GetFileFromPathAsync(fileName);
            Stream s = (await f.OpenReadAsync()).AsStream();
            return s;
        }



        public override T XMLDeserialize<T>(string path)
        {
            var task = Task.Run<T>(() => deserialize<T>(path, null));
            task.Wait();
            return task.Result;
        }


        public override T XMLDeserialize<T>(string path, string[] overrides)
        {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            for (int i = 0; i < overrides.Length; i++)
            {
                string attrib = overrides[i];
                XmlAttributes attribs = new XmlAttributes();
                attribs.XmlIgnore = true;
                attribs.XmlElements.Add(new XmlElementAttribute(attrib));
                ov.Add(typeof(T), attrib, attribs);
            }
            
            var task = Task.Run<T>(() => deserialize<T>(path, ov));
            task.Wait();
            return task.Result;
        }

        
        //TODO NEED TO FIND A BETTER WAY TO DETERMINE LOCATION (Install, Roaming, Local) ON WINDOWS 8
        private async Task<T> deserialize<T>(string path, XmlAttributeOverrides overrides)
        {
            T retval = default(T);
            //StorageFolder folder = await StorageFolder.GetFolderFromPathAsync(path);
            StorageFile file = await StorageFile.GetFileFromPathAsync(path);
            
            using (Stream read = await file.OpenStreamForReadAsync())
            {
                XmlSerializer ser = null;
                if (overrides == null)
                {
                    ser = new XmlSerializer(typeof(T));
                }
                else
                {
                    ser = new XmlSerializer(typeof(T), overrides);
                }
                
                retval = (T)ser.Deserialize(read);
            }

            return retval;
        }

        public override void XMLSerialize<T>(string path, T obj)
        {
            var task = Task.Run<T>(() => serialize<T>(path, obj, null));
            task.Wait();

        }

        public override void XMLSerialize<T>(string path, T obj, string[] overrides)
        {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            for (int i = 0; i < overrides.Length; i++)
            {
                string attrib = overrides[i];
                XmlAttributes attribs = new XmlAttributes();
                attribs.XmlIgnore = true;
                attribs.XmlElements.Add(new XmlElementAttribute(attrib));
                ov.Add(typeof(T), attrib, attribs);
            }

            var task = Task.Run<T>(() => serialize<T>(path, obj, ov));
            task.Wait();
        }


        private async Task<T> serialize<T>(string path, T obj, XmlAttributeOverrides overrides)
        {
            StorageFolder folder = ApplicationData.Current.RoamingFolder;
            using (Stream write = await folder.OpenStreamForWriteAsync(path, CreationCollisionOption.ReplaceExisting))
            {
                XmlSerializer ser = null;
                if (overrides == null)
                {
                    ser = new XmlSerializer(typeof(T));
                }
                else
                {
                    ser = new XmlSerializer(typeof(T), overrides);
                }
                ser.Serialize(write, obj);
            }
            return default(T);
        }

        public override void StoreStringValue(string key, string value)
        {
            Windows.Storage.ApplicationDataContainer roamingSettings = Windows.Storage.ApplicationData.Current.RoamingSettings;
            roamingSettings.Values[key] = value;
        }

        public override string RetrieveStringValue(string key)
        {
            Windows.Storage.ApplicationDataContainer roamingSettings = Windows.Storage.ApplicationData.Current.RoamingSettings;
            return (string)roamingSettings.Values[key];
        }

        public override IEnumerable<string> EnumerateValueKeys()
        {
            Windows.Storage.ApplicationDataContainer roamingSettings = Windows.Storage.ApplicationData.Current.RoamingSettings;
            return roamingSettings.Values.Keys;
        }

        public override IEnumerable<string> EnumerateDirectories(string path)
        {

            var task = Task.Run<IEnumerable<string>>(() => enumerateDirectories(path));

            task.Wait();
            
            return task.Result;
        }

        private async Task<IEnumerable<string>> enumerateDirectories(string path)
        {
            StorageFolder inst = Package.Current.InstalledLocation;
            string newPath = Path.Combine(inst.Path, path);
            var folder = await StorageFolder.GetFolderFromPathAsync(newPath);
            var folders = await folder.GetFoldersAsync();
            List<string> retVal = new List<string>();
            foreach (var f in folders)
            {
                retVal.Add(f.Path);
            }
            return retVal;
        }

        public override IEnumerable<string> EnumerateFiles(string path)
        {
            var task = Task.Run<IEnumerable<string>>(() => enumerateFiles(path));

            task.Wait();
            
            return task.Result;
        }

        private async Task<IEnumerable<string>> enumerateFiles(string path)
        {
            StorageFolder inst = Package.Current.InstalledLocation;
            string newPath = Path.Combine(inst.Path, path);
            var folder = await StorageFolder.GetFolderFromPathAsync(newPath);
            var folders = await folder.GetFilesAsync();
            List<string> retVal = new List<string>();
            foreach (var f in folders)
            {
                retVal.Add(f.Path);
            }
            return retVal;
        }

        public override bool IAPSupported()
        {
            return true;
        }

        public override bool IAPIsDurableItemPurchased(string key)
        {
            LicenseInformation license;
#if DEBUG
            license = Windows.ApplicationModel.Store.CurrentAppSimulator.LicenseInformation;
#else
            license = Windows.ApplicationModel.Store.CurrentApp.LicenseInformation;
#endif
            return license.ProductLicenses[key].IsActive;
        }

        public override IEnumerable<string> IAPEnumerateLicenses()
        {
            LicenseInformation license;
#if DEBUG
            license = Windows.ApplicationModel.Store.CurrentAppSimulator.LicenseInformation;
#else
            license = Windows.ApplicationModel.Store.CurrentApp.LicenseInformation;
#endif
            return license.ProductLicenses.Keys;
        }

        private delegate void emptyMethod();
        public override void IAPPurchaseDurableItem(string key, Action<string> callBack)
        {
            iapPurchaseDurableItem(key, callBack);
        }

        private async void iapPurchaseDurableItem(string key, Action<string> callBack)
        {
            string receipt = "";
            try
            {
#if DEBUG
                receipt = await CurrentAppSimulator.RequestProductPurchaseAsync(key, true);
#else
                receipt = await CurrentApp.RequestProductPurchaseAsync(key, true);
#endif
            }
            catch(Exception)
            {
            
            }

            callBack(receipt);
        }

        public override bool IAPVerifyReceipt(string receipt, string purchasedId)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(receipt);
                IXmlNode receiptNode = doc.SelectSingleNode("Receipt");
                IXmlNode productReceiptNode = receiptNode.SelectSingleNode("ProductReceipt");
                IXmlNode id = productReceiptNode.Attributes.GetNamedItem("ProductId");
                string idString = (string)id.NodeValue;
                return idString.Equals(purchasedId);
            }
            catch (Exception)
            {

            }
            return false;
            
        }

        public override void LaunchWebsite(string url)
        {
            var uri = new Uri(url);
            Windows.System.Launcher.LaunchUriAsync(uri);
        }

        public override void LaunchRateAppScreen()
        {
            string packageName = Package.Current.Id.FamilyName;
            Windows.System.Launcher.LaunchUriAsync(new Uri("ms-windows-store:PDP?PFN="+packageName));
        }

        public override void SendEmail(string emailAdress)
        {
            var mailto = new Uri("mailto:" + emailAdress);
            Windows.System.Launcher.LaunchUriAsync(mailto);
        }

        public override void GoogleAnalyticsSendView(string screenName)
        {
            //GoogleAnalytics.EasyTracker.GetTracker().SendView(screenName);
        }

        public override void GoogleAnalyticsSendEvent(string category, string action, string label, int value)
        {
            //GoogleAnalytics.EasyTracker.GetTracker().SendEvent(category, action, label, value);
        }

        public override T ProtoDeserialize<T>(string path)
        {
            throw new NotImplementedException();
        }

        public override void ProtoSerialize<T>(string path, T obj)
        {
            throw new NotImplementedException();
        }

        public override void LogInfo(string message)
        {
            System.Diagnostics.Debug.WriteLine("Info: " + message);
        }

        public override void LogWarning(string message)
        {
            System.Diagnostics.Debug.WriteLine("Warning: " + message);
        }

        public override void LogError(string message)
        {
            System.Diagnostics.Debug.WriteLine("ERROR: " + message);
        }

        public override string CurrentDirectory()
        {
            return String.Empty;// return Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
        }
    }
}
