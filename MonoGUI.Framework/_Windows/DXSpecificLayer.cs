﻿using Microsoft.Xna.Framework.Graphics;
using MonoGUI.Manager;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml.Serialization;
using System.Windows;

namespace MonoGUI_DX.Manager
{
    public class DXSpecificLayer : PlatformSpecificLayer
    {
        private Dictionary<string, string> _mockStringStorage = new Dictionary<string, string>();
        private Dictionary<string, int> _mockIntStorage = new Dictionary<string, int>();
        private Dictionary<string, bool> _mockBoolStorage = new Dictionary<string, bool>();
        private Dictionary<string, float> _mockFloatStorage = new Dictionary<string, float>();

        public override Microsoft.Xna.Framework.Graphics.Texture2D loadTexture2DFromFile(GraphicsDevice graphicsDevice, string fileName)
        {
            Texture2D image;
            using (FileStream stream = File.OpenRead(fileName))
            {
                image = Texture2D.FromStream(graphicsDevice, stream);
            }
            return image;
        }

        public override T XMLDeserialize<T>(string path, FolderTarget target)
        {
            T retVal;
            using (Stream wri = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.None))
            {
                XmlSerializer ser = new XmlSerializer(typeof(T));
                retVal = (T)ser.Deserialize(wri);
            }
            return retVal;
        }

        public override void XMLSerialize<T>(string path, T obj, FolderTarget target)
        {
            using (Stream wri = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None))
            {
                XmlSerializer ser = new XmlSerializer(typeof(T));
                ser.Serialize(wri, obj);
                wri.SetLength(wri.Position);
            }
        }

        public override T XMLDeserialize<T>(string path, string[] overrides, PlatformSpecificLayer.FolderTarget target)
        {
            return XMLDeserialize<T>(path, target);
        }

        public override void XMLSerialize<T>(string path, T obj, string[] overrides, PlatformSpecificLayer.FolderTarget target)
        {
            XMLSerialize<T>(path, obj, target);
        }


        public override T ProtoDeserialize<T>(string path, FolderTarget target)
        {
            using (var f = File.OpenRead(path))
            {
                return Serializer.Deserialize<T>(f);
            }
        }

        public override string ProtoSerialize<T>(string path, T obj, FolderTarget target)
        {
            using (var f = File.Create(path))
            {
                Serializer.Serialize<T>(f, obj);
            }
            return path;
        }

        public override void LogInfo(string message)
        {
            Console.WriteLine("INFO: " + message);
        }

        public override void LogWarning(string message)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("WARNING: " + message);
            Console.ResetColor();
        }

        public override void LogError(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("ERROR: " + message);
            Console.ResetColor();
        }


        public override void StoreStringValue(string key, string value)
        {
            if (!_mockStringStorage.ContainsKey(key))
            {
                _mockStringStorage.Add(key, value);
            }
            _mockStringStorage[key] = value;
        }

        public override string RetrieveStringValue(string key, string defaultValue)
        {
            if (_mockStringStorage.ContainsKey(key))
            {
                return _mockStringStorage[key];
            }
            return defaultValue;
        }

        public override void StoreIntValue(string key, int value)
        {
            if (!_mockIntStorage.ContainsKey(key))
            {
                _mockIntStorage.Add(key, value);
            }
            _mockIntStorage[key] = value;
        }

        public override int RetrieveIntValue(string key, int defaultValue)
        {
            if (_mockIntStorage.ContainsKey(key))
            {
                return _mockIntStorage[key];
            }
            return defaultValue;
        }

        public override void StoreBoolValue(string key, bool value)
        {
            if (!_mockBoolStorage.ContainsKey(key))
            {
                _mockBoolStorage.Add(key, value);
            }
            _mockBoolStorage[key] = value;
        }

        public override bool RetrieveBoolValue(string key, bool defaultValue)
        {
            if (_mockBoolStorage.ContainsKey(key))
            {
                return _mockBoolStorage[key];
            }
            return defaultValue;
        }

        public override void StoreFloatValue(string key, float value)
        {
            if (!_mockFloatStorage.ContainsKey(key))
            {
                _mockFloatStorage.Add(key, value);
            }
            _mockFloatStorage[key] = value;
        }

        public override float RetrieveFloatValue(string key, float defaultValue)
        {
            if (_mockFloatStorage.ContainsKey(key))
            {
                return _mockFloatStorage[key];
            }
            return defaultValue;
        }

        public override IEnumerable<string> EnumerateValueKeys()
        {
            return new List<string>();
        }

        public override string CurrentDirectory()
        {
            return Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
        }

        public override IEnumerable<string> EnumerateDirectories(string path, FolderTarget folder)
        {
            if (String.IsNullOrEmpty(path))
            {
                path = CurrentDirectory();
            }
            return Directory.EnumerateDirectories(path);
        }

        public override IEnumerable<string> EnumerateFiles(string path, FolderTarget folder)
        {
            if (String.IsNullOrEmpty(path))
            {
                path = CurrentDirectory();
            }
            return Directory.EnumerateFiles(path);
        }

        public override bool IAPSupported()
        {
            //throw new NotImplementedException();
            return true;
        }

        public override bool IAPIsDurableItemPurchased(string key)
        {
           // throw new NotImplementedException();
            return true;
        }

        public override void IAPPurchaseDurableItem(string key, Action<string> callBack)
        {
            // new NotImplementedException();
        }

        public override bool IAPVerifyReceipt(string receipt, string purchasedId)
        {
            //throw new NotImplementedException();
            return true;
        }

        public override void IAPConsumeConsumeable(string key)
        {
            throw new NotImplementedException();
        }

        public override IEnumerable<string> IAPEnumerateLicenses()
        {
            throw new NotImplementedException();
           
        }

        public override void LaunchWebsite(string url)
        {
            //throw new NotImplementedException();
            
        }

        public override void LaunchRateAppScreen()
        {
            //throw new NotImplementedException();
       
        }

        public override void SendEmail(string emailAdress)
        {
            //throw new NotImplementedException();
            
        }

        public override void GoogleAnalyticsSendView(string screenName)
        {
            //throw new NotImplementedException();
            
        }

        public override void GoogleAnalyticsSendEvent(string category, string action, string label, int value)
        {
           // throw new NotImplementedException();
            
        }

        public override bool HasFilePicker()
        {
            return true;
        }
        public override void LaunchFilePicker(Action<string[]> callBackFile, bool doMultiselect)
        {
            var FD = new System.Windows.Forms.OpenFileDialog();
            FD.Multiselect = doMultiselect;
            FD.Filter = "Image files (*.png)|*.png";
            if (FD.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                callBackFile(FD.FileNames);
            }
        }

        public override string FileNameWithoutExtension(string fileName)
        {
            return Path.GetFileNameWithoutExtension(fileName);
        }






        public override void ThreadingQueueWorkerThread(Delegate method, params Object[] args)
        {
            ThreadPool.QueueUserWorkItem(new WaitCallback(
                delegate(object state)
                {
                    method.DynamicInvoke(args);
                }
                ));
        }

        public override void ThreadingQueueUIThread(Delegate method, params Object[] args)
        {
            ScreenManager.QueueUIThreadCallback(new MonoGUI.Threading.ThreadCallbackMethod(method, args));
        }
    }
}
