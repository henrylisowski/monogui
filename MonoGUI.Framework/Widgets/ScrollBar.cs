﻿using Microsoft.Xna.Framework;
using MonoGUI.Animations;
using MonoGUI.Manager;
using MonoGUI.ScreenItems;
using MonoGUI.ScreenItems.Containers.CanvasContainer;
using MonoGUI.Skinning;
using MonoGUI.Skinning.SkinElements;
using MonoGUI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace MonoGUI.Widgets
{
    public class ScrollBar : DefinedAreasCanvasFrame
    {
        #region Skinned Properties
        [SkinnedProperty]
        public string DownButtonSkinVariant
        {
            get { return _downButton.SkinVariant; }
            set { _downButton.SkinVariant = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); _downButton.ApplySkin(); }
        }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> DownButtonArea
        {
            get { return GetAreaDefinition(_downButton); }
            set { SetAreaDefinition(_downButton, value); SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }
        [SkinnedProperty]
        public string UpButtonSkinVariant
        {
            get { return _upButton.SkinVariant; }
            set { _upButton.SkinVariant = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); _upButton.ApplySkin(); }
        }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> UpButtonArea
        {
            get { return GetAreaDefinition(_upButton); }
            set { SetAreaDefinition(_upButton, value); SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }
        [SkinnedProperty]
        public string LeftButtonSkinVariant
        {
            get { return _leftButton.SkinVariant; }
            set { _leftButton.SkinVariant = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); _leftButton.ApplySkin(); }
        }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> LeftButtonArea
        {
            get { return GetAreaDefinition(_leftButton); }
            set { SetAreaDefinition(_leftButton, value); SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }
        [SkinnedProperty]
        public string RightButtonSkinVariant
        {
            get { return _rightButton.SkinVariant; }
            set { _rightButton.SkinVariant = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); _rightButton.ApplySkin(); }
        }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> RightButtonArea
        {
            get { return GetAreaDefinition(_rightButton); }
            set { SetAreaDefinition(_rightButton, value); SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }
        [SkinnedProperty]
        public string HorizontalScrollBackgroundVariant
        {
            get { return _horizontalScrollBackground.SkinVariant; }
            set { _horizontalScrollBackground.SkinVariant = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); 
                _horizontalScrollBackground.ApplySkin(); }
        }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> HorizontalScrollBackgroundArea
        {
            get { return GetAreaDefinition(_horizontalScrollBackground); }
            set { SetAreaDefinition(_horizontalScrollBackground, value); SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }
        [SkinnedProperty]
        public string HorizontalScrollKnobVariant
        {
            get { return _horizontalScrollKnob.SkinVariant; }
            set { _horizontalScrollKnob.SkinVariant = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name);
                _horizontalScrollKnob.ApplySkin(); }
        }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> HorizontalScrollKnobArea
        {
            get { return GetAreaDefinition(_horizontalScrollKnob); }
            set { SetAreaDefinition(_horizontalScrollKnob, value); SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }
        [SkinnedProperty]
        public string VerticalScrollBackgroundVariant
        {
            get { return _verticalScrollBackground.SkinVariant; }
            set { _verticalScrollBackground.SkinVariant = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); 
                _verticalScrollBackground.ApplySkin(); }
        }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> VerticalScrollBackgroundArea
        {
            get { return GetAreaDefinition(_verticalScrollBackground); }
            set { SetAreaDefinition(_verticalScrollBackground, value); SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }
        [SkinnedProperty]
        public string VerticalScrollKnobVariant
        {
            get { return _verticalScrollKnob.SkinVariant; }
            set { _verticalScrollKnob.SkinVariant = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); 
                _verticalScrollKnob.ApplySkin(); }
        }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> VerticalScrollKnobArea
        {
            get { return GetAreaDefinition(_verticalScrollKnob); }
            set { SetAreaDefinition(_verticalScrollKnob, value); SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }
        #endregion

        private int _horizontalScrollSize;
        public int HorizontalScrollSize
        {
            get { return _horizontalScrollSize; }
            set
            {
                if (HorizontalPosition + HorizontalKnobSize > value)
                {
                    PlatformSpecificLayer.Instance.LogWarning("Scroll Size is too small for current knob and position");
                }
                else
                {
                    _horizontalScrollSize = value;
                    _scrollCanvasFrame.PreferredDimensions = new Vector2(value, _scrollCanvasFrame.PreferredDimensions.Y);
                }
            }
        }

        private int _horizontalKnobSize;
        public int HorizontalKnobSize
        {
            get { return _horizontalKnobSize; }
            set
            {
                if (HorizontalPosition + value > HorizontalScrollSize)
                {
                    PlatformSpecificLayer.Instance.LogWarning("Knob Size is too big for current scroll size and position");
                }
                else
                {
                    _horizontalKnobSize = value;
                }
            }
        }
        private int _horizontalPosition;
        public int HorizontalPosition
        {
            get { return _horizontalPosition; }
            set { _horizontalPosition = MathHelper.Clamp(value, 0, HorizontalScrollSize - HorizontalKnobSize); }
        }
        public bool UseHorizontalScrollBar { get; set; }
        public int HorizontalIncrement { get; set; }
        private int _verticalScrollSize;
        public int VerticalScrollSize
        {
            get { return _verticalScrollSize; }
            set
            {
                if (VerticalPosition + VerticalKnobSize > value)
                {
                    PlatformSpecificLayer.Instance.LogWarning("Scroll Size is too small for current knob and position");
                }
                else
                {
                    _verticalScrollSize = value;
                    _scrollCanvasFrame.PreferredDimensions = new Vector2(_scrollCanvasFrame.PreferredDimensions.X, value);
                }
            }
        }

        private int _verticalKnobSize;
        public int VerticalKnobSize
        {
            get { return _verticalKnobSize; }
            set
            {
                if (VerticalPosition + value > VerticalScrollSize)
                {
                    PlatformSpecificLayer.Instance.LogWarning("Knob Size is too big for current scroll size and position");
                }
                else
                {
                    _verticalKnobSize = value;
                }
            }
        }
        private int _verticalPosition;
        public int VerticalPosition
        {
            get { return _verticalPosition; }
            set { _verticalPosition = MathHelper.Clamp(value, 0, VerticalScrollSize - VerticalKnobSize); }
        }
        public bool UseVerticalScrollBar { get; set; }
        public int VerticalIncrement { get; set; }

        private Button _downButton;
        private Button _upButton;
        private Button _leftButton;
        private Button _rightButton;
        private Icon _verticalScrollBackground;
        private Icon _verticalScrollKnob;
        private Icon _horizontalScrollBackground;
        private Icon _horizontalScrollKnob;
        private CanvasFrame _scrollCanvasFrame;

        public ScrollBar(ScreenManager Manager)
            : base(Manager)
        {
            this.SkinTargetWidget = SkinReferencingConstants.WidgetTargetScrollBar;
            _upButton = new Button(Manager);
            _upButton.SkinVariant = SkinReferencingConstants.SkinVariantNone;
            _upButton.DoParentClipping = false;
            base.AddChild(_upButton);

            _downButton = new Button(Manager);
            _downButton.SkinVariant = SkinReferencingConstants.SkinVariantNone;
            _downButton.DoParentClipping = false;
            base.AddChild(_downButton);

            _leftButton = new Button(Manager);
            _leftButton.SkinVariant = SkinReferencingConstants.SkinVariantNone;
            _leftButton.DoParentClipping = false;
            base.AddChild(_leftButton);

            _rightButton = new Button(Manager);
            _rightButton.SkinVariant = SkinReferencingConstants.SkinVariantNone;
            _rightButton.DoParentClipping = false;
            base.AddChild(_rightButton);

            _horizontalScrollBackground = new Icon(Manager);
            _horizontalScrollBackground.SkinVariant = SkinReferencingConstants.SkinVariantNone;
            _horizontalScrollBackground.DoParentClipping = false;
            base.AddChild(_horizontalScrollBackground);

            _verticalScrollBackground = new Icon(Manager);
            _verticalScrollBackground.SkinVariant = SkinReferencingConstants.SkinVariantNone;
            _verticalScrollBackground.DoParentClipping = false;
            base.AddChild(_verticalScrollBackground);

            _horizontalScrollKnob = new Icon(Manager);
            _horizontalScrollKnob.SkinVariant = SkinReferencingConstants.SkinVariantNone;
            _horizontalScrollKnob.DoParentClipping = false;
            base.AddChild(_horizontalScrollKnob);

            _verticalScrollKnob = new Icon(Manager);
            _verticalScrollKnob.SkinVariant = SkinReferencingConstants.SkinVariantNone;
            _verticalScrollKnob.DoParentClipping = false;
            base.AddChild(_verticalScrollKnob);

            _scrollCanvasFrame = new CanvasFrame(Manager);
            base.AddChild(_scrollCanvasFrame);

            _leftButton.ButtonClicked += _leftButton_ButtonClicked;
            _rightButton.ButtonClicked += _rightButton_ButtonClicked;
            _upButton.ButtonClicked += _upButton_ButtonClicked;
            _downButton.ButtonClicked += _downButton_ButtonClicked;
            HorizontalPosition = 0;
            HorizontalScrollSize = 1024;
            HorizontalKnobSize = 128;
            HorizontalIncrement = 128;
            VerticalPosition = 0;
            VerticalScrollSize = 1024;
            VerticalKnobSize = 128;
            VerticalIncrement = 128;
        }

        public override void AddChild(ScreenItem child)
        {
            _scrollCanvasFrame.AddChild(child);
        }

        void _downButton_ButtonClicked(object sender, EventArgs e)
        {
            int oldPosition = VerticalPosition;
            VerticalPosition += VerticalIncrement;
            calculateVerticalBar();
            updateChildScrollPosition(new Vector2(0, VerticalPosition - oldPosition));
        }

        void _upButton_ButtonClicked(object sender, EventArgs e)
        {
            int oldPosition = VerticalPosition;
            VerticalPosition -= VerticalIncrement;
            calculateVerticalBar();
            updateChildScrollPosition(new Vector2(0, VerticalPosition - oldPosition));
        }

        void _rightButton_ButtonClicked(object sender, EventArgs e)
        {
            int oldPosition = HorizontalPosition;
            HorizontalPosition += HorizontalIncrement;
            calculateHorizontalBar();
            updateChildScrollPosition(new Vector2(HorizontalPosition - oldPosition, 0));
        }

        void _leftButton_ButtonClicked(object sender, EventArgs e)
        {
            int oldPosition = HorizontalPosition;
            HorizontalPosition -= HorizontalIncrement;
            calculateHorizontalBar();
            updateChildScrollPosition(new Vector2(HorizontalPosition - oldPosition, 0));
        }

        public override void UpdateChildRelativeSize()
        {
            base.UpdateChildRelativeSize();
            calculateHorizontalBar();
            calculateVerticalBar();
            updateChildScrollPosition(new Vector2(HorizontalPosition, VerticalPosition));
            
        }

        private void updateChildScrollPosition(Vector2 delta)
        {
            _scrollCanvasFrame.RelativePosition -= delta;
        }

        private void calculateHorizontalBar()
        {
            AreaDefinition horizontalKnob = HorizontalScrollKnobArea.GetSkinElementByState(VisualState);
            RectangleFloat updatedArea = horizontalKnob.UpdatedSize(SizeIncreaseFromMinimumDimensions, MinimumDimensions.Area.Dimensions);
            updatedArea.X += updatedArea.Width * (HorizontalPosition / (float)HorizontalScrollSize);
            updatedArea.Width *= (HorizontalKnobSize / (float)HorizontalScrollSize);
            
            _horizontalScrollKnob.RelativePosition = updatedArea.Position;
            _horizontalScrollKnob.RelativeDimensions = updatedArea.Dimensions;
        }

        private void calculateVerticalBar()
        {
            AreaDefinition verticalKnob = VerticalScrollKnobArea.GetSkinElementByState(VisualState);
            RectangleFloat updatedArea = verticalKnob.UpdatedSize(SizeIncreaseFromMinimumDimensions, MinimumDimensions.Area.Dimensions);
            updatedArea.Y += updatedArea.Height * (VerticalPosition / (float)VerticalScrollSize);
            updatedArea.Height *= (VerticalKnobSize / (float)VerticalScrollSize);

            _verticalScrollKnob.RelativePosition = updatedArea.Position;
            _verticalScrollKnob.RelativeDimensions = updatedArea.Dimensions;
        }
    }
}
