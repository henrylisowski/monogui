﻿using MonoGUI.Manager;
using MonoGUI.Skinning;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Widgets
{
    public class RadioButton : Checkbox
    {
        private RadioButtonController _radioController;
        public RadioButton(ScreenManager ScreenManager, RadioButtonController RadioController)
            : base(ScreenManager)
        {
            SkinTargetWidget = SkinReferencingConstants.WidgetTargetRadioButton;
            _radioController = RadioController;
            _radioController.AddRadioButton(this);
        }

        public RadioButton(ScreenManager ScreenManager, RadioButtonController RadioController, string LabelText)
            : this(ScreenManager, RadioController)
        {
            _textLabel.SetText(LabelText);
        }

        protected override void buttonClicked(bool silent)
        {
            _radioController.RadioButtonClicked(this, silent);
        }

        public class RadioButtonController
        {
            List<RadioButton> watchedRadioButtons;
            public RadioButtonController()
            {
                watchedRadioButtons = new List<RadioButton>();
            }

            public void AddRadioButton(RadioButton button)
            {
                if (button != null && !watchedRadioButtons.Contains(button))
                {
                    watchedRadioButtons.Add(button);
                }
            }

            public void RemoveRadioButton(RadioButton button)
            {
                if (button != null && watchedRadioButtons.Contains(button))
                {
                    watchedRadioButtons.Remove(button);
                }
            }

            public void RadioButtonClicked(RadioButton button, bool silent)
            {
                foreach (RadioButton b in watchedRadioButtons)
                {
                    if (b != button)
                    {
                        HandleClick(b, false, true);
                    }
                }
                HandleClick(button, true, silent);
            }

            private void HandleClick(RadioButton button, bool set, bool silent)
            {
                if (button.ClickStatus != set)
                {
                    button._toggleButton.ClickButton(silent);
                }
            }
        }
    }
}
