﻿using Microsoft.Xna.Framework;
using MonoGUI.Animations;
using MonoGUI.Manager;
using MonoGUI.Skinning;
using MonoGUI.Skinning.SkinElements;
using MonoGUI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace MonoGUI.Widgets
{
    public class Slider : ProgressBar
    {
        #region Skinned Attributes
        private SkinnedPropertyStateValue<AreaDefinition> _horizontalSliderIconArea;
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> HorizontalSliderIconArea
        {
            get { return _horizontalSliderIconArea; }
            set { _horizontalSliderIconArea = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }

        private SkinnedPropertyStateValue<ISkinElement> _horizontalSliderIconSkin;
        [SkinnedProperty]
        public SkinnedPropertyStateValue<ISkinElement> HorizontalSliderIconSkin
        {
            get { return _horizontalSliderIconSkin; }
            set { _horizontalSliderIconSkin = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }

        private AreaDefinition _horizontalSliderIconMinimumDimensions;
        [SkinnedProperty]
        public AreaDefinition HorizontalSliderIconMinimumDimensions
        {
            get { return _horizontalSliderIconMinimumDimensions; }
            set { _horizontalSliderIconMinimumDimensions = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }

        private SkinnedPropertyStateValue<List<IModifier>> _horizontalSliderIconStateTransitionAnimations;
        [SkinnedProperty]
        public SkinnedPropertyStateValue<List<IModifier>> HorizontalSliderIconStateTransitionAnimations
        {
            get { return _horizontalSliderIconStateTransitionAnimations; }
            set { _horizontalSliderIconStateTransitionAnimations = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }

        private SkinnedPropertyStateValue<AreaDefinition> _verticalSliderIconArea;
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> VerticalSliderIconArea
        {
            get { return _verticalSliderIconArea; }
            set { _verticalSliderIconArea = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }

        private SkinnedPropertyStateValue<ISkinElement> _verticalSliderIconSkin;
        [SkinnedProperty]
        public SkinnedPropertyStateValue<ISkinElement> VerticalSliderIconSkin
        {
            get { return _verticalSliderIconSkin; }
            set { _verticalSliderIconSkin = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }

        private AreaDefinition _verticalSliderIconMinimumDimensions;
        [SkinnedProperty]
        public AreaDefinition VerticalSliderIconMinimumDimensions
        {
            get { return _verticalSliderIconMinimumDimensions; }
            set { _verticalSliderIconMinimumDimensions = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }

        private SkinnedPropertyStateValue<List<IModifier>> _verticalSliderIconStateTransitionAnimations;
        [SkinnedProperty]
        public SkinnedPropertyStateValue<List<IModifier>> VerticalSliderIconStateTransitionAnimations
        {
            get { return _verticalSliderIconStateTransitionAnimations; }
            set { _verticalSliderIconStateTransitionAnimations = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }
        #endregion
        
        private Icon _sliderIcon;
        private float _sizeToProgressRatio;
        private bool _iconPressed;

        public Slider(ScreenManager ScreenManager)
            : base(ScreenManager)
        {
            this.SkinTargetWidget = SkinReferencingConstants.WidgetTargetSlider;
            _sliderIcon = new Icon(ScreenManager);
            _sliderIcon.SkinVariant = SkinReferencingConstants.SkinVariantNone;
            this.ProgressChanged += Slider_ProgressChanged;
            _sliderIcon.MousePressed += _sliderIcon_MousePressed;
            _sliderIcon.MouseMoved += _sliderIcon_MouseMoved;
            _sliderIcon.MouseReleased += _sliderIcon_MouseReleased;
            _sliderIcon.MouseReleasedOutside += _sliderIcon_MouseReleased;
            //TODO add touch here!
            addSubChild(_sliderIcon);
        }

        void _sliderIcon_MouseReleased(object sender, EventHandling.MouseEvents.MouseButtonEvent e)
        {
            _iconPressed = false;
        }

        void _sliderIcon_MouseMoved(object sender, EventHandling.MouseEvents.MouseEvent e)
        {
            if (_iconPressed)
            {
                Vector2 barPoint = Vector2.Transform(e.Position, _sliderIcon.LocalAnimatedTransformation);
                RectangleFloat fillArea = calculateFillArea();
                if (IsHorizontal)
                {
                    if (FillDirection == StandardFillDirection.FillRight)
                    {
                        Progress = (barPoint.X - fillArea.Left) / fillArea.Width;
                    }
                    else
                    {
                        Progress = 1 - ((barPoint.X - fillArea.Left) / fillArea.Width);
                    }
                    
                }
                else
                {
                    if (FillDirection == StandardFillDirection.FillDown)
                    {
                        Progress = (barPoint.Y - fillArea.Top) / fillArea.Height;
                    }
                    else
                    {
                        Progress = 1 - ((barPoint.Y - fillArea.Top) / fillArea.Height);
                    }
                }
            }

        }

        void _sliderIcon_MousePressed(object sender, EventHandling.MouseEvents.MouseButtonEvent e)
        {
            _iconPressed = true;
        }

        void Slider_ProgressChanged(object sender, EventArgs e)
        {
            base.UpdateChildRelativeSize();
            calculateIconPosition();
        }

        protected override void directionChanged()
        {
            base.directionChanged();
            updateIconSkins();
        }

        private void updateIconSkins()
        {
            if (IsHorizontal)
            {
                _sliderIcon.MinimumDimensions = HorizontalSliderIconMinimumDimensions;
                _sliderIcon.IconImage = HorizontalSliderIconSkin;
                _sliderIcon.StateTransitionAnimations = HorizontalSliderIconStateTransitionAnimations;
                SetAreaDefinition(_sliderIcon, HorizontalSliderIconArea);
            }
            else
            {
                _sliderIcon.MinimumDimensions = VerticalSliderIconMinimumDimensions;
                _sliderIcon.IconImage = VerticalSliderIconSkin;
                _sliderIcon.StateTransitionAnimations = VerticalSliderIconStateTransitionAnimations;
                SetAreaDefinition(_sliderIcon, VerticalSliderIconArea);
            }
        }

        public override void UpdateChildRelativeSize()
        {
            base.UpdateChildRelativeSize();
            calculateIconPosition();
        }

        private void calculateIconPosition()
        {
            RectangleFloat updatedFillArea = calculateDirectionSpecificFillArea();
            Vector2 thumbChange = Vector2.Zero;
            if (FillDirection == StandardFillDirection.FillLeft)
            {
                thumbChange = new Vector2(updatedFillArea.Left, 0);
            }
            else if (FillDirection == StandardFillDirection.FillRight)
            {
                thumbChange = new Vector2(updatedFillArea.Right, 0);
            }
            else if (FillDirection == StandardFillDirection.FillDown)
            {
                thumbChange = new Vector2(0, updatedFillArea.Bottom);
            }
            else if (FillDirection == StandardFillDirection.FillUp)
            {
                thumbChange = new Vector2(0, updatedFillArea.Top);
            }
            _sliderIcon.RelativePosition += thumbChange;

            RectangleFloat fullFillArea = calculateFillArea();
            if (IsHorizontal)
            {
                _sizeToProgressRatio = 1f / fullFillArea.Width;
            }
            else
            {
                _sizeToProgressRatio = 1f / fullFillArea.Height;
            }   
        }

        protected override void MidDraw(GameTime gameTime, MSpriteBatch.MatrixSpriteBatch spriteBatch)
        {
            if (_sliderIcon.IconImage == null)
            {
                updateIconSkins();
                UpdateChildRelativeSize();
            }
            base.MidDraw(gameTime, spriteBatch);
        }
    }
}
