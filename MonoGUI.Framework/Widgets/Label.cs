﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGUI.Manager;
using MonoGUI.ScreenItems;
using MonoGUI.ScreenItems.Elements;
using MonoGUI.Skinning;
using MonoGUI.Skinning.Fonts;
using MonoGUI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace MonoGUI.Widgets
{
    public class Label : ElementPanel
    {
        private string _text;
        public string Text
        {
            get { return _text; }
        }

        public void SetText(string text, bool resizeFont)
        {
            _text = text;
            if (resizeFont)
            {
                recalculateFont();
            }
        }

        public void SetText(string text)
        {
            SetText(text, true);
        }

        #region Skinned Values
        private string _fontName;
        [SkinnedProperty]
        public string FontName
        {
            get { return _fontName; }
            set { _fontName = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); recalculateFont(); }
        }

        private int _fontSize;
        [SkinnedProperty]
        public int FontSize 
        {
            get { return _fontSize; }
            set { _fontSize = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); recalculateFont(); }
        }

        private FontRoundMethod _fontRoundingMethod;
        [SkinnedProperty]
        public FontRoundMethod FontRoundingMethod
        {
            get { return _fontRoundingMethod; }
            set { _fontRoundingMethod = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); recalculateFont(); }
        }

        private Color _textColor;
        [SkinnedProperty]
        public Color TextColor
        {
            get { return _textColor; }
            set { _textColor = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }

        private FontFillMethod _fontMethod;
        [SkinnedProperty]
        public FontFillMethod FontMethod
        {
            get { return _fontMethod; }
            set { _fontMethod = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); recalculateFont(); }
        }
        private AlignH _textHorizontalJustification;
        [SkinnedProperty]
        public AlignH TextHorizontalJustification
        {
            get { return _textHorizontalJustification; }
            set { _textHorizontalJustification = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }
        private AlignV _textVerticalJustification;
        [SkinnedProperty]
        public AlignV TextVerticalJustification
        {
            get { return _textVerticalJustification; }
            set { _textVerticalJustification = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }

        #endregion
        private Color _animatedTextColor;
        public Color AnimatedTextColor
        {
            get
            {
                ColorUtils.Multiply(TextColor, AnimationManager.TotalTweenedColors, ref _animatedTextColor);
                return _animatedTextColor;
            }
        }
        private SpriteFont _calculatedFont;
        private bool _fontCalculationNeeded;



        public Label(ScreenManager ScreenManager)
            : base(ScreenManager)
        {
            this.SkinTargetWidget = SkinReferencingConstants.WidgetTargetLabel;
            Width = ScreenItems.ItemSizeOptions.WrapContent;
            Height = ScreenItems.ItemSizeOptions.WrapContent;
            TextColor = Color.Black;
            _fontCalculationNeeded = false;
        }

        public Label(ScreenManager ScreenManager, string Text)
            : this(ScreenManager)
        {
            this._text = Text;
        }

        public override void UpdateRequestedSizeDueToChildResize()
        {
            if (!String.IsNullOrEmpty(FontName))
            {
                SpriteFont font = SkinManager.Instance.GetClosestFont(FontName, FontSize, FontRoundingMethod);
                if (font != null)
                {
                    Vector2 measuredString = font.MeasureString(Text);
                    if (Width == ScreenItems.ItemSizeOptions.WrapContent)
                    {
                        PreferredDimensions = new Vector2(measuredString.X, PreferredDimensions.Y);
                    }
                    if (Height == ScreenItems.ItemSizeOptions.WrapContent)
                    {
                        PreferredDimensions = new Vector2(PreferredDimensions.X, measuredString.Y);
                    }
                }
            }
            base.UpdateRequestedSizeDueToChildResize();
        }

        public override void UpdateChildRelativeSize()
        {
            base.UpdateChildRelativeSize();
            recalculateFont();
        }

        protected override void MidDraw(GameTime gameTime, MSpriteBatch.MatrixSpriteBatch spriteBatch)
        {
            base.MidDraw(gameTime, spriteBatch);
            if (_fontCalculationNeeded)
            {
                _fontCalculationNeeded = false;
                handleRecalculateFont();
            }
            if (!String.IsNullOrEmpty(Text) && _calculatedFont != null)
            {
                Vector2 pos = Vector2.Zero;
                Vector2 dims = _calculatedFont.MeasureString(Text);

                if (TextHorizontalJustification == AlignH.Center)
                {
                    pos += new Vector2((RelativeDimensions.X - dims.X) / 2f, 0f);
                }
                else if (TextHorizontalJustification == AlignH.Right)
                {
                    pos += new Vector2(RelativeDimensions.X - dims.X, 0f);
                }

                if (TextVerticalJustification == AlignV.Center)
                {
                    pos += new Vector2(0, (RelativeDimensions.Y - dims.Y) / 2f);
                }
                else if (TextVerticalJustification == AlignV.Bottom)
                {
                    pos += new Vector2(0, RelativeDimensions.Y - dims.Y);
                }

                spriteBatch.DrawString(_calculatedFont, Text, pos, AnimatedTextColor);
            }
        }

        private void recalculateFont()
        {
            _fontCalculationNeeded = true;
        }

        private void handleRecalculateFont()
        {
            if (!String.IsNullOrEmpty(FontName))
            {
                switch (FontMethod)
                {
                    case FontFillMethod.Absolute:
                        _calculatedFont = SkinManager.Instance.GetClosestFont(FontName, FontSize, FontRoundingMethod);
                        break;
                    case FontFillMethod.Fill:
                        _calculatedFont = SkinManager.Instance.GetLargestFont(FontName, 
                            RelativeDimensions, Text, FontManager.FontClampSide.None);
                        break;
                    case FontFillMethod.ClampHeight:
                        _calculatedFont = SkinManager.Instance.GetLargestFont(FontName,
                            RelativeDimensions, Text, FontManager.FontClampSide.Height);
                        break;
                    case FontFillMethod.ClampWidth:
                        _calculatedFont = SkinManager.Instance.GetLargestFont(FontName,
                            RelativeDimensions, Text, FontManager.FontClampSide.Width);
                        break;
                }
            }
        }
    }
}
