﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using MonoGUI.Manager;
using MonoGUI.MSpriteBatch;
using MonoGUI.ScreenItems;
using MonoGUI.ScreenItems.Elements;
using MonoGUI.Skinning;
using MonoGUI.Skinning.SkinElements;
using MonoGUI.Themes;
using MonoGUI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace MonoGUI.Widgets
{
    public class ProgressBar : HiddenDefinedAreaCanvasFrame
    {
        private const string FillShaderName = "ProgressBarShader";
        private const string ShaderParamMaskPresent = "mask_present";
        private const string ShaderParamFillPercentage = "fill_percentage";
        private const string ShaderParamFillGradient = "fill_gradient";
        private const string ShaderParamFillMask = "fill_mask";
        private const string ShaderParamDestinationRectangle = "destination_rectangle";
        private const string ShaderParamSourceRectangle = "source_rectangle";
        private const string ShaderParamGradientSourceRectangle = "gradient_source_rectangle";
        private const string ShaderParamMaskSourceRectangle = "mask_source_rectangle";

        #region Skinned Properties
        private FillType _progressFillType;
        [SkinnedProperty]
        public FillType ProgressFillType
        {
            get { return _progressFillType; }
            set
            {
                _progressFillType = value;
                if (_progressFillType == FillType.StandardFill)
                {
                    directionChanged();
                }
                SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name);
            }
        }

        private SkinnedPropertyStateValue<ISkinElement> _gradientFillSourceTexture;
        [SkinnedProperty]
        public SkinnedPropertyStateValue<ISkinElement> GradientFillSourceTexture
        {
            get { return _gradientFillSourceTexture; }
            set { _gradientFillSourceTexture = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }

        private SkinnedPropertyStateValue<TextureElement> _gradientFillGradientTexture;
        [SkinnedProperty]
        public SkinnedPropertyStateValue<TextureElement> GradientFillGradientTexture
        {
            get { return _gradientFillGradientTexture; }
            set { _gradientFillGradientTexture = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }

        private SkinnedPropertyStateValue<TextureElement> _gradientFillMaskTexture;
        [SkinnedProperty]
        public SkinnedPropertyStateValue<TextureElement> GradientFillMaskTexture
        {
            get { return _gradientFillMaskTexture; }
            set { _gradientFillMaskTexture = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }

        private SkinnedPropertyStateValue<AreaDefinition> _gradientFillArea;
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> GradientFillArea
        {
            get { return _gradientFillArea; }
            set { _gradientFillArea = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }
        [SkinnedProperty]
        public string HorizontalFillVariant
        {
            get { return _horizontalFillIcon.SkinVariant; }
            set { _horizontalFillIcon.SkinVariant = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name);
                _horizontalFillIcon.ApplySkin(); }
        }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> HorizontalFillArea
        {
            get { return GetAreaDefinition(_horizontalFillIcon); }
            set { SetAreaDefinition(_horizontalFillIcon, value); SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }
        [SkinnedProperty]
        public string HorizontalOutlineVariant
        {
            get { return _horizontalOutlineIcon.SkinVariant; }
            set { _horizontalOutlineIcon.SkinVariant = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); 
                _horizontalOutlineIcon.ApplySkin(); }

        }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> HorizontalOutlineArea
        {
            get { return GetAreaDefinition(_horizontalOutlineIcon); }
            set { SetAreaDefinition(_horizontalOutlineIcon, value); SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }
        [SkinnedProperty]
        public string VerticalFillVariant
        {
            get { return _verticalFillIcon.SkinVariant; }
            set { _verticalFillIcon.SkinVariant = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name);
                _verticalFillIcon.ApplySkin(); }
        }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> VerticalFillArea
        {
            get { return GetAreaDefinition(_verticalFillIcon); }
            set { SetAreaDefinition(_verticalFillIcon, value); SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }
        [SkinnedProperty]
        public string VerticalOutlineVariant
        {
            get { return _verticalOutlineIcon.SkinVariant; }
            set { _verticalOutlineIcon.SkinVariant = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name);
                _verticalOutlineIcon.ApplySkin(); }

        }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> VerticalOutlineArea
        {
            get { return GetAreaDefinition(_verticalOutlineIcon); }
            set { SetAreaDefinition(_verticalOutlineIcon, value); SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }

        #endregion

        private Effect _progressFillShader;
        private Icon _horizontalFillIcon;
        private Icon _horizontalOutlineIcon;
        private Icon _verticalFillIcon;
        private Icon _verticalOutlineIcon;

        private float _progress;
        public float Progress { get { return _progress; } set { _progress = value; clampProgress(); } }

        private StandardFillDirection _fillDirection;
        public StandardFillDirection FillDirection
        {
            get { return _fillDirection; }
            set { _fillDirection = value; directionChanged(); }
        }
        public bool IsHorizontal { get { return FillDirection == StandardFillDirection.FillRight || FillDirection == StandardFillDirection.FillLeft; } }

        public enum FillType { StandardFill, GradientFillNoMask, GradientFillMask }
        public enum StandardFillDirection { FillRight, FillLeft, FillDown, FillUp }

        public ProgressBar(ScreenManager ScreenManager)
            : base(ScreenManager)
        {
            this.SkinTargetWidget = SkinReferencingConstants.WidgetTargetProgressBar;
            _progress = 0;

            _horizontalOutlineIcon = new Icon(Manager);
            _horizontalOutlineIcon.CalculatesOwnHitState = false;
            addSubChild(_horizontalOutlineIcon);
            _horizontalFillIcon = new Icon(Manager);
            _horizontalFillIcon.CalculatesOwnHitState = false;
            addSubChild(_horizontalFillIcon);

            _verticalOutlineIcon = new Icon(Manager);
            _verticalOutlineIcon.CalculatesOwnHitState = false;
            _verticalOutlineIcon.IsEnabled = false;
            addSubChild(_verticalOutlineIcon);
            _verticalFillIcon = new Icon(Manager);
            _verticalFillIcon.CalculatesOwnHitState = false;
            _verticalFillIcon.IsEnabled = false;
            addSubChild(_verticalFillIcon);
        }

        public override void LoadContent(Microsoft.Xna.Framework.Content.ContentManager Content)
        {
            base.LoadContent(Content);

            if (ProgressFillType == FillType.GradientFillNoMask || ProgressFillType == FillType.GradientFillMask)
            {
                //TODO this needs to be removed until its reliable on all platforms
                //_progressFillShader = Content.Load<Effect>(FillShaderName);
            }
        }

        protected override void MidDraw(Microsoft.Xna.Framework.GameTime gameTime, MSpriteBatch.MatrixSpriteBatch spriteBatch)
        {
            base.MidDraw(gameTime, spriteBatch);

            if (ProgressFillType == FillType.GradientFillNoMask || ProgressFillType == FillType.GradientFillMask)
            {
                drawGradientFill(spriteBatch);
            }
        }

        protected virtual void directionChanged()
        {
            RequestResize();
            if (ProgressFillType == FillType.StandardFill)
            {
                if (FillDirection == StandardFillDirection.FillRight || FillDirection == StandardFillDirection.FillLeft)
                {
                    _horizontalFillIcon.IsEnabled = true;
                    _horizontalOutlineIcon.IsEnabled = true;
                    _verticalFillIcon.IsEnabled = false;
                    _verticalOutlineIcon.IsEnabled = false;
                }
                else
                {
                    _horizontalFillIcon.IsEnabled = false;
                    _horizontalOutlineIcon.IsEnabled = false;
                    _verticalFillIcon.IsEnabled = true;
                    _verticalOutlineIcon.IsEnabled = true;
                }
            }
            progressChangeUpdateFill();
        }

        public override void UpdateChildRelativeSize()
        {
            base.UpdateChildRelativeSize();
            progressChangeUpdateFill();
        }

        private void clampProgress()
        {
            _progress = MathHelper.Clamp(_progress, 0, 1);
            progressChangeUpdateFill();
            if (ProgressChanged != null)
            {
                ProgressChanged(this, null);
            }
        }

        public event EventHandler ProgressChanged;

        private void progressChangeUpdateFill()
        {
            if (ProgressFillType == FillType.StandardFill)
            {
                if (FillDirection == StandardFillDirection.FillLeft || FillDirection == StandardFillDirection.FillRight)
                {
                    RectangleFloat dest = calculateDirectionSpecificFillArea();
                    _horizontalFillIcon.RelativePosition = dest.Position;
                    _horizontalFillIcon.RelativeDimensions = dest.Dimensions;
                }
                else
                {
                    RectangleFloat dest = calculateDirectionSpecificFillArea();
                    _verticalFillIcon.RelativePosition = dest.Position;
                    _verticalFillIcon.RelativeDimensions = dest.Dimensions;
                }
            }
        }


        protected RectangleFloat calculateFillArea()
        {
            AreaDefinition fillDestination = null;
            AreaDefinition fillMinimumDimensions = null;

            if (FillDirection == StandardFillDirection.FillLeft || FillDirection == StandardFillDirection.FillRight)
            {
                if (HorizontalFillArea == null || _horizontalFillIcon.MinimumDimensions == null)
                {
                    return new RectangleFloat(0, 0, 0, 0);
                }
                fillDestination = HorizontalFillArea.GetSkinElementByState(VisualState);
                fillMinimumDimensions = _horizontalFillIcon.MinimumDimensions;
            }
            else
            {
                if (VerticalFillArea == null || _verticalFillIcon.MinimumDimensions == null)
                {
                    return new RectangleFloat(0, 0, 0, 0);
                }
                fillDestination = VerticalFillArea.GetSkinElementByState(VisualState);
                fillMinimumDimensions = _verticalFillIcon.MinimumDimensions;
            }

            RectangleFloat updatedFillArea = fillDestination.UpdatedSize(SizeIncreaseFromMinimumDimensions, MinimumDimensions.Area.Dimensions);
            return updatedFillArea;

        }

        protected RectangleFloat calculateDirectionSpecificFillArea()
        {
            RectangleFloat updatedFillArea = calculateFillArea();
            AreaDefinition fillDestination = null;
            AreaDefinition fillMinimumDimensions = null;

            if (FillDirection == StandardFillDirection.FillLeft || FillDirection == StandardFillDirection.FillRight)
            {
                if (HorizontalFillArea == null || _horizontalFillIcon.MinimumDimensions == null)
                {
                    return new RectangleFloat(0, 0, 0, 0);
                }
                fillDestination = HorizontalFillArea.GetSkinElementByState(VisualState);
                fillMinimumDimensions = _horizontalFillIcon.MinimumDimensions;
            }
            else
            {
                if (VerticalFillArea == null || _verticalFillIcon.MinimumDimensions == null)
                {
                    return new RectangleFloat(0, 0, 0, 0);
                }
                fillDestination = VerticalFillArea.GetSkinElementByState(VisualState);
                fillMinimumDimensions = _verticalFillIcon.MinimumDimensions;
            }
            if (FillDirection == StandardFillDirection.FillDown)
            {
                updatedFillArea.Height *= _progress;
                if (updatedFillArea.Height < fillMinimumDimensions.Area.Height)
                {
                    updatedFillArea.Height = fillMinimumDimensions.Area.Height;
                }
            }
            else if (FillDirection == StandardFillDirection.FillUp)
            {
                updatedFillArea.Y += updatedFillArea.Height * (1 - _progress);
                updatedFillArea.Height *= _progress;
                if (updatedFillArea.Height < fillMinimumDimensions.Area.Height)
                {
                    float change = fillMinimumDimensions.Area.Height - updatedFillArea.Height;
                    updatedFillArea.Height = fillMinimumDimensions.Area.Height;
                    updatedFillArea.Y -= change;
                }
            }
            else if (FillDirection == StandardFillDirection.FillRight)
            {
                updatedFillArea.Width *= _progress;
                if (updatedFillArea.Width < fillMinimumDimensions.Area.Width)
                {
                    updatedFillArea.Width = fillMinimumDimensions.Area.Width;
                }
            }
            else if (FillDirection == StandardFillDirection.FillLeft)
            {
                updatedFillArea.X += updatedFillArea.Width * (1 - _progress);
                updatedFillArea.Width *= _progress;
                if (updatedFillArea.Width < fillMinimumDimensions.Area.Width)
                {
                    float change = fillMinimumDimensions.Area.Width - updatedFillArea.Width;
                    updatedFillArea.Width = fillMinimumDimensions.Area.Width;
                    updatedFillArea.X -= change;
                }
            }
            return updatedFillArea;
        }

        private void drawGradientFill(MatrixSpriteBatch spriteBatch)
        {
            spriteBatch.HaltDraw();
            DrawBeginInfo beg = spriteBatch.ModifiableBeginCall();
            beg.Effect = _progressFillShader;
            
            TextureElement gradientElement = GradientFillGradientTexture.GetSkinElementByState(VisualState);
            TextureElement maskElement = null;
            
            
            _progressFillShader.Parameters[ShaderParamFillPercentage].SetValue(_progress);
            _progressFillShader.Parameters[ShaderParamFillGradient].SetValue(gradientElement.SourceTexture);

            if (ProgressFillType == FillType.GradientFillMask)
            {
                _progressFillShader.Parameters[ShaderParamMaskPresent].SetValue(true);
                maskElement = GradientFillMaskTexture.GetSkinElementByState(VisualState);
                _progressFillShader.Parameters[ShaderParamFillMask].SetValue(maskElement.SourceTexture);
            }
            else
            {
                _progressFillShader.Parameters[ShaderParamMaskPresent].SetValue(false);
            }
            
            AreaDefinition fillDestination = GradientFillArea.GetSkinElementByState(VisualState);
            RectangleFloat updatedFillArea = fillDestination.UpdatedSize(SizeIncreaseFromMinimumDimensions, MinimumDimensions.Area.Dimensions);
            drawCommands.Clear();
            GradientFillSourceTexture.GetSkinElementByState(VisualState).GetDrawCommands(updatedFillArea.Dimensions, lifetime,
                ref drawCommands);
            foreach (DrawCommand c in drawCommands)
            {
                c.DestinationRectangle.Position += updatedFillArea.Position;

                Vector4 sourceRectangleParam = new Vector4(c.SourceRectangle.X / (float)c.SourceImage.Width, c.SourceRectangle.Y / (float)c.SourceImage.Height,
                    (c.SourceRectangle.Width / (float)c.SourceImage.Width), (c.SourceRectangle.Height / (float)c.SourceImage.Height));

                Vector4 destinationRectangleParam = new Vector4(c.DestinationRectangle.X / updatedFillArea.Width, c.DestinationRectangle.Y / updatedFillArea.Height,
                    c.DestinationRectangle.Width / updatedFillArea.Width, c.DestinationRectangle.Height / updatedFillArea.Height);

                Rectangle gradientSource = gradientElement.CurrentSourceBox(lifetime);
                Vector4 gradientSourceRectangleParam = new Vector4(
                    gradientSource.X / (float)gradientElement.SourceTexture.Width,
                    gradientSource.Y / (float)gradientElement.SourceTexture.Height,
                    gradientSource.Width / (float)gradientElement.SourceTexture.Width,
                    gradientSource.Height / (float)gradientElement.SourceTexture.Height);

                Vector4 maskSourceRectangleParam = new Vector4();
                if (maskElement != null)
                {
                    Rectangle maskSource = maskElement.CurrentSourceBox(lifetime);
                    maskSourceRectangleParam = new Vector4(
                        maskSource.X / (float)maskElement.SourceTexture.Width,
                        maskSource.Y / (float)maskElement.SourceTexture.Height,
                        maskSource.Width / (float)maskElement.SourceTexture.Width,
                        maskSource.Height / (float)maskElement.SourceTexture.Height
                        );
                    _progressFillShader.Parameters[ShaderParamMaskSourceRectangle].SetValue(maskSourceRectangleParam);
                }

                spriteBatch.Begin(beg);
                _progressFillShader.Parameters[ShaderParamSourceRectangle].SetValue(sourceRectangleParam);
                _progressFillShader.Parameters[ShaderParamDestinationRectangle].SetValue(destinationRectangleParam);
                _progressFillShader.Parameters[ShaderParamGradientSourceRectangle].SetValue(gradientSourceRectangleParam);
                spriteBatch.Draw(c.SourceImage, c.DestinationRectangle.Position, c.SourceRectangle,
                        AnimatedColor, 0f, Vector2.Zero, c.Scale, c.Effects, 0f);
                spriteBatch.End();
            }
            //spriteBatch.End();
            spriteBatch.ResumeDraw();

        }
    }
}
