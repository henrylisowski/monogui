﻿using Microsoft.Xna.Framework;
using MonoGUI.Manager;
using MonoGUI.Skinning;
using MonoGUI.Skinning.SkinElements;
using MonoGUI.Themes;
using MonoGUI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace MonoGUI.Widgets
{
    public class ToggleButton : Button
    {
        #region Skinned Properties        
        private SkinnedPropertyStateValue<ISkinElement> _buttonForegroundClicked;
        [SkinnedProperty]
        public SkinnedPropertyStateValue<ISkinElement> ButtonForegroundClicked
        {
            get { return _buttonForegroundClicked; }
            set { _buttonForegroundClicked = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); 
                assignButtonForegroundBasedOnClickStatus(); }
        }

        private SkinnedPropertyStateValue<ISkinElement> _buttonForegroundNotClicked;
        [SkinnedProperty]
        public SkinnedPropertyStateValue<ISkinElement> ButtonForegroundNotClicked
        {
            get { return _buttonForegroundNotClicked; }
            set { _buttonForegroundNotClicked = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); 
                assignButtonForegroundBasedOnClickStatus(); }
        }
        [SkinnedProperty]
        public string ButtonToggledOnIconVariant
        {
            get { return _toggledIcon.SkinVariant; }
            set { _toggledIcon.SkinVariant = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); 
                _toggledIcon.ApplySkin(); }
        }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> ButtonToggledOnIconArea
        {
            get { return GetAreaDefinition(_toggledIcon); }
            set { SetAreaDefinition(_toggledIcon, value); SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }
        #endregion


        private bool _clickStatus;
        public bool ClickStatus { get { return _clickStatus; } private set { _clickStatus = value; } }

        private Icon _toggledIcon;

        public ToggleButton(ScreenManager ScreenManager)
            : base(ScreenManager)
        {
            this.SkinTargetWidget = SkinReferencingConstants.WidgetTargetToggleButton;
            _toggledIcon = new Icon(ScreenManager);
            _toggledIcon.SkinVariant = SkinReferencingConstants.SkinVariantNone;
            _toggledIcon.CalculatesOwnHitState = false;
            _toggledIcon.DoParentClipping = false;
            AddChild(_toggledIcon);
        }

        public ToggleButton(ScreenManager ScreenManager, string ButtonText)
            : base(ScreenManager, ButtonText)
        {
            this.SkinTargetWidget = SkinReferencingConstants.WidgetTargetToggleButton;
            _toggledIcon = new Icon(ScreenManager);
            _toggledIcon.SkinVariant = SkinReferencingConstants.SkinVariantNone;
            _toggledIcon.CalculatesOwnHitState = false;
            _toggledIcon.DoParentClipping = false;
            AddChild(_toggledIcon);
        }

        public override void Initialize()
        {
            base.Initialize();
            this.ButtonClicked += ToggleButton_ButtonClicked;
        }

        public void SetButton(bool set)
        {
            SetButton(set, false);
        }

        public void SetButton(bool set, bool silent)
        {
            if (ClickStatus != set)
            {
                ClickButton(silent);
            }
        }

        protected override void buttonClicked(bool silent)
        {
            if (_clickStatus)
            {
                _clickStatus = false;
            }
            else if (!_clickStatus)
            {
                _clickStatus = true;
            }
            assignButtonForegroundBasedOnClickStatus();
            base.buttonClicked(silent);
        }

        private void assignButtonForegroundBasedOnClickStatus()
        {
            if (_clickStatus)
            {
                ButtonForeground = ButtonForegroundClicked;
                _toggledIcon.IsEnabled = true;
            }
            else
            {
                ButtonForeground = ButtonForegroundNotClicked;
                _toggledIcon.IsEnabled = false;
            }
        }

        public event EventHandler ButtonClickedOn;
        public event EventHandler ButtonClickedOff;

        void ToggleButton_ButtonClicked(object sender, EventArgs e)
        {
            if (ClickStatus && ButtonClickedOn != null)
            {
                ButtonClickedOn(this, null);
            }
            else if (!ClickStatus && ButtonClickedOff != null)
            {
                ButtonClickedOff(this, null);
            }
        }
    }
}
