﻿using Microsoft.Xna.Framework;
using MonoGUI.Animations;
using MonoGUI.Manager;
using MonoGUI.ScreenItems;
using MonoGUI.ScreenItems.Containers.CanvasContainer;
using MonoGUI.Skinning;
using MonoGUI.Skinning.Fonts;
using MonoGUI.Skinning.SkinElements;
using MonoGUI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace MonoGUI.Widgets
{
    public class Checkbox : DefinedAreasCanvasFrame
    {
        #region Skinned Properties
        [SkinnedProperty]
        public string ToggleButtonSkinVariant
        {
            get { return _toggleButton.SkinVariant; }
            set { _toggleButton.SkinVariant = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); _toggleButton.ApplySkin(); }
        }
        [SkinnedProperty]
        public string LabelSkinVariant
        {
            get { return _textLabel.SkinVariant; }
            set { _textLabel.SkinVariant = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); _textLabel.ApplySkin(); }
        }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> ToggleButtonArea
        {
            get { return GetAreaDefinition(_toggleButton); }
            set { SetAreaDefinition(_toggleButton, value); SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> TextArea
        {
            get { return GetAreaDefinition(_textLabel); }
            set { SetAreaDefinition(_textLabel, value); SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }
        
        private List<Animation> _buttonEventFireAnimations;
        [SkinnedProperty]
        public List<Animation> ButtonEventFireAnimations
        {
            get { return _buttonEventFireAnimations; }
            set
            {
                HandleEventAnimationAssignment(_buttonEventFireAnimations, "ButtonEventFireAnimations", value, _toggleButton);
                _buttonEventFireAnimations = value;
                SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name);
            }
        }
        private List<Animation> _textEventFireAnimations;
        [SkinnedProperty]
        public List<Animation> TextEventFireAnimations
        {
            get { return _textEventFireAnimations; }
            set
            {
                HandleEventAnimationAssignment(_textEventFireAnimations, "TextEventFireAnimations", value, _textLabel);
                _textEventFireAnimations = value;
                SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name);
            }
        }
        #endregion

        public bool ClickStatus { get { return _toggleButton.ClickStatus; } }
        public string Text { get { return _textLabel.Text; } set { _textLabel.SetText(value); } }

        protected ToggleButton _toggleButton;
        protected Label _textLabel;

        public Checkbox(ScreenManager ScreenManager)
            : base(ScreenManager)
        {
            this.SkinTargetWidget = SkinReferencingConstants.WidgetTargetCheckbox;
            _toggleButton = new ToggleButton(ScreenManager);
            _toggleButton.SkinVariant = SkinReferencingConstants.SkinVariantNone;
            _toggleButton.CalculatesOwnHitState = false;
            _textLabel = new Label(ScreenManager);
            _textLabel.SkinVariant = SkinReferencingConstants.SkinVariantNone;
            _textLabel.CalculatesOwnHitState = false;
            AddChild(_toggleButton);
            AddChild(_textLabel);

            this.MouseClicked += Checkbox_MouseClicked;
            this.TouchClicked += Checkbox_TouchClicked;
            this.TapEvent += Checkbox_TapEvent;
            _toggleButton.ButtonClicked += _toggleButton_ButtonClicked;
            _toggleButton.ButtonClickedOn += _toggleButton_ButtonClickedOn;
            _toggleButton.ButtonClickedOff += _toggleButton_ButtonClickedOff;
        }

        public Checkbox(ScreenManager ScreenManager, string Text)
            : this(ScreenManager)
        {
            _textLabel.SetText(Text);
        }

        public event EventHandler ButtonClicked;
        public event EventHandler ButtonClickedOn;
        public event EventHandler ButtonClickedOff;

        void Checkbox_MouseClicked(object sender, EventHandling.MouseEvents.MouseButtonEvent e)
        {
            ClickButton();
        }

        void Checkbox_TapEvent(object sender, EventHandling.GestureEvents.GestureEvent e)
        {
            ClickButton();
        }

        void Checkbox_TouchClicked(object sender, EventHandling.TouchEvents.TouchEvent e)
        {
            ClickButton();
        }

        void _toggleButton_ButtonClicked(object sender, EventArgs e)
        {
            if (ButtonClicked != null)
            {
                ButtonClicked(this, null);
            }
        }

        void _toggleButton_ButtonClickedOff(object sender, EventArgs e)
        {
            if (ButtonClickedOff != null)
            {
                ButtonClickedOff(this, null);
            }
        }

        void _toggleButton_ButtonClickedOn(object sender, EventArgs e)
        {
            if (ButtonClickedOn != null)
            {
                ButtonClickedOn(this, null);
            }
        }

        public void ClickButton()
        {
            ClickButton(false);
        }

        public void ClickButton(bool silent)
        {
            buttonClicked(silent);
        }

        public void SetButton(bool set)
        {
            SetButton(set, false);
        }

        public void SetButton(bool set, bool silent)
        {
            if (set != ClickStatus)
            {
                ClickButton(silent);
            }
        }

        protected virtual void buttonClicked(bool silent)
        {
            _toggleButton.ClickButton(silent);
        }

        public override void UpdateChildRelativeSize()
        {
            base.UpdateChildRelativeSize();
            if (ToggleButtonArea != null)
            {
                AreaDefinition toggleArea = ToggleButtonArea.GetSkinElementByState(VisualState);
                RectangleFloat dims = toggleArea.UpdatedSize(SizeIncreaseFromMinimumDimensions, MinimumDimensions.Area.Dimensions);
                _toggleButton.RelativePosition = dims.Position;
                _toggleButton.RelativeDimensions = dims.Dimensions;
                _toggleButton.UpdateChildRelativeSize();
            }
            if (TextArea != null)
            {
                AreaDefinition textArea = TextArea.GetSkinElementByState(VisualState);
                RectangleFloat dims = textArea.UpdatedSize(SizeIncreaseFromMinimumDimensions, MinimumDimensions.Area.Dimensions);
                _textLabel.RelativePosition = dims.Position;
                _textLabel.RelativeDimensions = dims.Dimensions;
                _textLabel.UpdateChildRelativeSize();
            }
        }


        protected override void VisualStateChanged()
        {
            base.VisualStateChanged();
            _toggleButton.VisualState = VisualState;
            _textLabel.VisualState = VisualState;
        }

        public override void AddChild(ScreenItem child)
        {
            //TODO since this shouldn't be able to add anything, just have it not call base and output an error?
            base.AddChild(child);
            child.CalculatesOwnHitState = false;
        }
    }
}
