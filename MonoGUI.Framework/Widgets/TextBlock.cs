﻿using Microsoft.Xna.Framework;
using MonoGUI.Manager;
using MonoGUI.ScreenItems;
using MonoGUI.ScreenItems.Elements;
using MonoGUI.Skinning;
using MonoGUI.Skinning.Fonts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace MonoGUI.Widgets
{
    public class TextBlock : ElementPanel
    {
        public static string NewLine = Environment.NewLine;

        #region Skinned Values
        private string _fontName;
        [SkinnedProperty]
        public string FontName
        {
            get { return _fontName; }
            set { _fontName = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); _textRecalculate = true; }
        }

        private int _fontSize;
        [SkinnedProperty]
        public int FontSize
        {
            get { return _fontSize; }
            set { _fontSize = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); _textRecalculate = true; }
        }

        private FontRoundMethod _fontRoundingMethod;
        [SkinnedProperty]
        public FontRoundMethod FontRoundingMethod
        {
            get { return _fontRoundingMethod; }
            set { _fontRoundingMethod = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); _textRecalculate = true; }
        }

        private Color _textColor;
        [SkinnedProperty]
        public Color TextColor
        {
            get { return _textColor; }
            set { _textColor = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); _textRecalculate = true; }
        }

        private FontFillMethod _fontMethod;
        [SkinnedProperty]
        public FontFillMethod FontMethod
        {
            get { return _fontMethod; }
            set { _fontMethod = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); _textRecalculate = true; }
        }

        private AlignH _textHorizontalJustification;
        [SkinnedProperty]
        public AlignH TextHorizontalJustification
        {
            get { return _textHorizontalJustification; }
            set { _textHorizontalJustification = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }
        private AlignV _textVerticalJustification;
        [SkinnedProperty]
        public AlignV TextVerticalJustification
        {
            get { return _textVerticalJustification; }
            set { _textVerticalJustification = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }
        #endregion

        private string _text;
        public string Text
        {
            get { return _text; }
            set { _text = value; _textRecalculate = true; }
        }

        public bool TextVerticalFill { get; set; }

        private string[] _lines;
        private Vector2[] _lineDimensions;
        private FontDescriptor _curFont;
        private bool _textRecalculate;

        public TextBlock(ScreenManager Manager)
            : base(Manager)
        {
            this.SkinTargetWidget = SkinReferencingConstants.WidgetTargetTextBlock;
            _lines = new string[] { };
            _lineDimensions = new Vector2[] { };
        }

        public TextBlock(ScreenManager Manager, string Text)
            : base(Manager)
        {
            this.SkinTargetWidget = SkinReferencingConstants.WidgetTargetTextBlock;
            _text = Text;
            _lines = new string[] { };
        }

        protected override void MidDraw(GameTime gameTime, MSpriteBatch.MatrixSpriteBatch spriteBatch)
        {
            base.MidDraw(gameTime, spriteBatch);
            if (_textRecalculate)
            {
                recalculateText();
            }
            Vector2 runningPos = Vector2.Zero;
            float lineIncrease = _curFont.Font.LineSpacing;
            if (TextVerticalFill)
            {
                lineIncrease = RelativeDimensions.Y / _lines.Length;
            }
            else
            {
                float totalHeight = _lines.Length * lineIncrease;
                if (TextVerticalJustification == AlignV.Center)
                {
                    runningPos += new Vector2(0, (RelativeDimensions.Y - totalHeight) / 2f);
                }
                else if (TextVerticalJustification == AlignV.Bottom)
                {
                    runningPos += new Vector2(0, (RelativeDimensions.Y - totalHeight));
                }
            }
            for (int i = 0; i < _lines.Length; i++)
            {
                string l = _lines[i];
                float horizontalOffset = 0f;
                if (TextHorizontalJustification == AlignH.Center)
                {
                    horizontalOffset = (RelativeDimensions.X - _lineDimensions[i].X) / 2f;
                }
                else if (TextHorizontalJustification == AlignH.Right)
                {
                    horizontalOffset = (RelativeDimensions.X - _lineDimensions[i].X);
                }
                spriteBatch.DrawString(_curFont.Font, l, runningPos + new Vector2(horizontalOffset, 0), Color.Black);
                runningPos += new Vector2(0, lineIncrease);
            }
        }

        public override void UpdateChildRelativeSize()
        {
            base.UpdateChildRelativeSize();
            recalculateText();
        }

        private void recalculateText()
        {
            string pattern = "( )|(" + TextBlock.NewLine + ")";
            string[] splitString = Regex.Split(_text, pattern);
            List<FontDescriptor> descFonts = SkinManager.Instance.GetFontsOfDescendingSize(FontName);
            Vector2 dims = RelativeDimensions;
            Vector2 pos = Vector2.Zero;
            Vector2 startPosition = Vector2.Zero;
            List<string> lines = new List<string>();
            bool matchFound = true;
            FontDescriptor curFont = null;
            for (int i = 0; i < descFonts.Count; i++)
            {
                matchFound = true;
                curFont = descFonts[i];
                Vector2 spaceDims = curFont.Font.MeasureString(" ");
                pos = Vector2.Zero;
                startPosition = Vector2.Zero;
                float lineSpacing = curFont.Font.LineSpacing;
                lines.Clear();
                lines.Add("");
                string curString = lines[0];
                
                for (int j = 0; j < splitString.Length; j++)
                {
                    string curWord = splitString[j];
                    if (curWord.Equals(TextBlock.NewLine))
                    {
                        //Start a new line
                        curString = "";
                        lines.Add(curString);
                        pos += new Vector2(-pos.X, lineSpacing);
                    }
                    else
                    {
                        Vector2 curDims = curFont.Font.MeasureString(curWord);
                        if (pos.X + curDims.X > dims.X)
                        {
                            //Drop down to another line
                            if (String.IsNullOrWhiteSpace(curWord))
                            {
                                //Just dont add it so the actual word will carry down
                                //This also fixes having an empty line at the end
                            }
                            else
                            {
                                pos += new Vector2(-pos.X + curDims.X, lineSpacing);
                                curString = curWord;
                                lines.Add(curString);
                            }
                        }
                        else
                        {
                            //Continue this line!
                            lines[lines.Count - 1] = curString + curWord;
                            curString = lines[lines.Count - 1];
                            pos += new Vector2(curDims.X, 0);
                        }

                        if (pos.Y + lineSpacing > dims.Y)
                        {
                            matchFound = false;
                            break;
                        }
                    }
                }

                if (matchFound)
                {
                    break;
                }
            }
            _lines = lines.ToArray();
            _curFont = curFont;
            _lineDimensions = new Vector2[_lines.Length];
            for (int i = 0; i < _lines.Length; i++)
            {
                _lineDimensions[i] = _curFont.Font.MeasureString(_lines[i]);
            }
        }
    }
}
