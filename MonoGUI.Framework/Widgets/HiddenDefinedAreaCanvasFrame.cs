﻿using MonoGUI.Manager;
using MonoGUI.ScreenItems;
using MonoGUI.Skinning;
using MonoGUI.Skinning.SkinElements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Widgets
{
    public class HiddenDefinedAreaCanvasFrame : DefinedAreasCanvasFrame
    {
        public HiddenDefinedAreaCanvasFrame(ScreenManager Manager)
            : base(Manager)
        {

        }

        public override void AddChild(ScreenItem child)
        {
            PlatformSpecificLayer.Instance.LogWarning(this + " attempted to add a child when no child can be added");
        }

        public override void AddChild(ScreenItem child, SkinnedPropertyStateValue<AreaDefinition> area)
        {
            AddChild(child);
        }

        protected void addSubChild(ScreenItem child)
        {
            base.AddChild(child);
        }

        protected void addSubChild(ScreenItem child, SkinnedPropertyStateValue<AreaDefinition> area)
        {
            base.AddChild(child, area);
        }
    }
}
