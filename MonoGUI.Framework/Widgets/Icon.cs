﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGUI.Manager;
using MonoGUI.ScreenItems.Elements;
using MonoGUI.Skinning;
using MonoGUI.Skinning.SkinElements;
using MonoGUI.Themes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace MonoGUI.Widgets
{
    public class Icon : ElementPanel
    {
        private SkinnedPropertyStateValue<ISkinElement> _iconImage;
        [SkinnedProperty]
        public SkinnedPropertyStateValue<ISkinElement> IconImage
        {
            get { return _iconImage; }
            set { _iconImage = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }

        public Texture2D Texture { get; set; }
        public string TextureName { get; private set; }
        
        public Icon(ScreenManager ScreenManager)
            : base(ScreenManager)
        {
            this.SkinTargetWidget = SkinReferencingConstants.WidgetTargetIcon;
        }

        public Icon(ScreenManager Manager, Texture2D Texture)
            : base(Manager)
        {
            this.SkinTargetWidget = SkinReferencingConstants.WidgetTargetIcon;
            this.Texture = Texture;
        }

        public Icon(ScreenManager Manager, string TextureName)
            : base(Manager)
        {
            this.SkinTargetWidget = SkinReferencingConstants.WidgetTargetIcon;
            this.TextureName = TextureName;
        }

        public override void LoadContent(Microsoft.Xna.Framework.Content.ContentManager Content)
        {
            base.LoadContent(Content);
            if (!String.IsNullOrEmpty(TextureName))
            {
                Texture = Content.Load<Texture2D>(TextureName);
            }
        }

        protected override void MidDraw(Microsoft.Xna.Framework.GameTime gameTime, MSpriteBatch.MatrixSpriteBatch spriteBatch)
        {
            base.MidDraw(gameTime, spriteBatch);
            if (IconImage != null)
            {
                ISkinElement iconElement = IconImage.GetSkinElementByState(VisualState);
                drawCommands.Clear();
                iconElement.GetDrawCommands(this.RelativeDimensions, lifetime, ref drawCommands);
                foreach (DrawCommand cmd in drawCommands)
                {
                    spriteBatch.Draw(cmd.SourceImage, cmd.DestinationRectangle.Position, cmd.SourceRectangle,
                        AnimatedColor, 0f, Vector2.Zero, cmd.Scale, cmd.Effects, 0f);
                }
            }
            if (Texture != null)
            {
                spriteBatch.Draw(Texture, new Rectangle(0, 0, (int)RelativeDimensions.X, (int)RelativeDimensions.Y), AnimatedColor);
            }
        }

        public override void UpdateRequestedSizeDueToChildResize()
        {
            base.UpdateRequestedSizeDueToChildResize();
            if (Width == ScreenItems.ItemSizeOptions.WrapContent)
            {
                RequestedDimensions = new Vector2(Texture.Width, RequestedDimensions.Y);
            }
            if (Height == ScreenItems.ItemSizeOptions.WrapContent)
            {
                RequestedDimensions = new Vector2(RequestedDimensions.X, Texture.Height);
            }
        }
    }
}
