﻿using Microsoft.Xna.Framework;
using MonoGUI.Manager;
using MonoGUI.ScreenItems;
using MonoGUI.ScreenItems.Containers.CanvasContainer;
using MonoGUI.Skinning;
using MonoGUI.Skinning.Fonts;
using MonoGUI.Skinning.SkinElements;
using MonoGUI.Themes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace MonoGUI.Widgets
{
    public class Button : DefinedAreasCanvasFrame
    {
        #region Skinned Values
        private SkinnedPropertyStateValue<ISkinElement> _buttonForeground;
        [SkinnedProperty]
        public SkinnedPropertyStateValue<ISkinElement> ButtonForeground
        {
            get { return _buttonForeground; }
            set { _buttonForeground = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }

        [SkinnedProperty]
        public string LabelSkinVariant
        {
            get { return _textLabel.SkinVariant; }
            set { _textLabel.SkinVariant = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); _textLabel.ApplySkin(); }
        }
        #endregion


        public string ButtonText { get; set; }

        protected Label _textLabel;

        private ScreenItemVisualStates _lastDrawCommandState;
        private Vector2 _lastDrawCommandDimensions;
        private List<DrawCommand> _buttonForegroundDrawCommands;

        public Button(ScreenManager ScreenManager)
            : base(ScreenManager)
        {
            this.SkinTargetWidget = SkinReferencingConstants.WidgetTargetButton;
            _textLabel = new Label(ScreenManager);
            this.MouseClicked += Button_MouseClicked;
            this.TouchClicked += Button_TouchClicked;
            this.TapEvent += Button_TapEvent;

            this.ButtonClicked += Button_ButtonClicked;

            _lastDrawCommandState = ScreenItemVisualStates.Disabled;
            _lastDrawCommandDimensions = Vector2.Zero;
            _buttonForegroundDrawCommands = new List<DrawCommand>();
        }

        public Button(ScreenManager ScreenManager, string ButtonText)
            : this(ScreenManager)
        {
            this.ButtonText = ButtonText;
            _textLabel = new Label(ScreenManager, ButtonText);
            _textLabel.SkinVariant = SkinReferencingConstants.SkinVariantNone;
            _textLabel.CalculatesOwnHitState = false;
            _textLabel.Width = ScreenItems.ItemSizeOptions.Percentage;
            _textLabel.Height = ScreenItems.ItemSizeOptions.Percentage;
            _textLabel.PreferredDimensions = new Vector2(0.5f, 0.3f);
            _textLabel.VerticalAlignment = ScreenItems.AlignV.Center;
            _textLabel.HorizontalAlignment = ScreenItems.AlignH.Center;
            this.Width = ScreenItems.ItemSizeOptions.WrapContent;
            this.Height = ScreenItems.ItemSizeOptions.WrapContent;
            this.AddChild(_textLabel);

            this.ButtonClicked += Button_ButtonClicked;
        }

        void Button_ButtonClicked(object sender, EventArgs e)
        {
            Manager.InputManager.MouseTouchHandled = true;
        }

        protected override void MidDraw(Microsoft.Xna.Framework.GameTime gameTime, MSpriteBatch.MatrixSpriteBatch spriteBatch)
        {
            if (ButtonForeground != null)
            {
                if (VisualState != _lastDrawCommandState || !RelativeDimensions.Equals(_lastDrawCommandDimensions))
                {
                    _buttonForegroundDrawCommands.Clear();
                    ButtonForeground.GetSkinElementByState(VisualState).GetDrawCommands(
                        RelativeDimensions, lifetime, ref _buttonForegroundDrawCommands);

                    _lastDrawCommandState = VisualState;
                    _lastDrawCommandDimensions = RelativeDimensions;
                }
                
                foreach (DrawCommand cmd in _buttonForegroundDrawCommands)
                {
                    cmd.UpdateSource(lifetime);
                    spriteBatch.Draw(cmd.SourceImage, cmd.DestinationRectangle.Position, cmd.SourceRectangle, AnimatedColor,
                        0f, Vector2.Zero, cmd.Scale, cmd.Effects, 0f);
                }
            }
            base.MidDraw(gameTime, spriteBatch);
        }

        protected virtual void buttonClicked(bool silent)
        {
            if (ButtonClicked != null && !silent)
            {
                ButtonClicked(this, null);
            }
        }

        public event EventHandler ButtonClicked;

        public void ClickButton(bool silent)
        {
            buttonClicked(silent);
        }

        public void ClickButton()
        {
            buttonClicked(false);
        }

        public override void HandleInput(GameTime gameTime, Manager.Input.InputManager inputManager)
        {
            base.HandleInput(gameTime, inputManager);
        }

        //TODO this only supports mouse, have it select others
        void Button_MouseClicked(object sender, EventHandling.MouseEvents.MouseButtonEvent e)
        {
            ClickButton();
        }

        void Button_TouchClicked(object sender, EventHandling.TouchEvents.TouchEvent e)
        {
            ClickButton();
            PlatformSpecificLayer.Instance.LogInfo("Touch");
        }

        void Button_TapEvent(object sender, EventHandling.GestureEvents.GestureEvent e)
        {
            ClickButton();
            PlatformSpecificLayer.Instance.LogInfo("Tap");
        }

        public override void UpdateRequestedSizeDueToChildResize()
        {
            base.UpdateRequestedSizeDueToChildResize();
            if (UseableChildren.Count() == 0 && ButtonForeground != null)
            {
                if (Width == ScreenItems.ItemSizeOptions.WrapContent)
                {
                    RequestedDimensions = new Vector2(ButtonForeground.GetSkinElementByState(VisualState).DestinationDimensions.X, RequestedDimensions.Y);
                }
                if (Height == ScreenItems.ItemSizeOptions.WrapContent)
                {
                    RequestedDimensions = new Vector2(RequestedDimensions.X, ButtonForeground.GetSkinElementByState(VisualState).DestinationDimensions.Y);
                }
            }
        }

        public override void AddChild(ScreenItems.ScreenItem child)
        {
            base.AddChild(child);
            child.CalculatesOwnHitState = false;
        }
    }
}
