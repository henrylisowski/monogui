﻿using Microsoft.Xna.Framework;
using MonoGUI.Animations;
using MonoGUI.Manager;
using MonoGUI.ScreenItems;
using MonoGUI.ScreenItems.Containers.CanvasContainer;
using MonoGUI.Skinning;
using MonoGUI.Skinning.Fonts;
using MonoGUI.Skinning.SkinElements;
using MonoGUI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace MonoGUI.Widgets
{
    public class GUIWindow : DefinedAreasCanvasFrame
    {
        #region Skinned Properties
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> TitleBarCanvasArea
        {
            get { return GetAreaDefinition(_titleBar); }
            set { SetAreaDefinition(_titleBar, value); SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }           
        }
        [SkinnedProperty]
        public string TitleBarCanvasSkinVariant
        {
            get { return _titleBar.SkinVariant; }
            set { _titleBar.SkinVariant = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); _titleBar.ApplySkin(); }
        }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> TitleLabelArea
        {
            get { return _titleBar.GetAreaDefinition(_titleLabel); }
            set { _titleBar.SetAreaDefinition(_titleLabel, value); SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }
        [SkinnedProperty]
        public string TitleLabelSkinVariant
        {
            get { return _titleLabel.SkinVariant; }
            set { _titleLabel.SkinVariant = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); _titleLabel.ApplySkin(); }
        }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> CloseButtonArea
        {
            get { return _titleBar.GetAreaDefinition(_closeButton); }
            set { _titleBar.SetAreaDefinition(_closeButton, value); SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }
        [SkinnedProperty]
        public string CloseButtonSkinVariant
        {
            get { return _closeButton.SkinVariant; }
            set { _closeButton.SkinVariant = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); _closeButton.ApplySkin(); }
        }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> ChildrenArea
        {
            get { return GetAreaDefinition(_childCanvas); }
            set { SetAreaDefinition(_childCanvas, value); SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }
        private List<Animation> _closeButtonEventFireAnimations;
        [SkinnedProperty]
        public List<Animation> CloseButtonEventFireAnimations
        {
            get { return _closeButtonEventFireAnimations; }
            set
            {
                HandleEventAnimationAssignment(_closeButtonEventFireAnimations, "CloseButtonEventFireAnimations", value, _closeButton);
                _closeButtonEventFireAnimations = value;
                SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name);
            }
        }
        private List<Animation> _titleTextEventFireAnimations;
        [SkinnedProperty]
        public List<Animation> TitleTextEventFireAnimations
        {
            get { return _titleTextEventFireAnimations; }
            set
            {
                HandleEventAnimationAssignment(_titleTextEventFireAnimations, "TitleTextEventFireAnimations", value, _titleLabel);
                _titleTextEventFireAnimations = value;
                SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name);
            }
        }
        private List<Animation> _titleBarEventFireAnimations;
        [SkinnedProperty]
        public List<Animation> TitleBarEventFireAnimations
        {
            get { return _titleBarEventFireAnimations; }
            set
            {
                HandleEventAnimationAssignment(_titleBarEventFireAnimations, "TitleBarEventFireAnimations", value, _titleBar);
                _titleBarEventFireAnimations = value;
                SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name);
            }
        }
        #endregion
        protected DefinedAreasCanvasFrame _titleBar;
        protected Button _closeButton;
        protected Label _titleLabel;
        protected CanvasFrame _childCanvas;

        public string WindowName { get { return _titleLabel.Text; } set { _titleLabel.SetText(value); } }

        public GUIWindow(ScreenManager ScreenManager, string WindowName, bool includeCloseButton)
            : base(ScreenManager)
        {
            this.SkinTargetWidget = SkinReferencingConstants.WidgetTargetGUIWindow;

            _titleBar = new DefinedAreasCanvasFrame(ScreenManager);
            _titleBar.SkinVariant = SkinReferencingConstants.SkinVariantNone;
            _titleBar.CalculatesOwnHitState = false;
            _titleBar.IgnoresParentTransitionAnimations = true;
            actualAddChild(_titleBar);

            _titleLabel = new Label(ScreenManager, WindowName);
            _titleLabel.SkinVariant = SkinReferencingConstants.SkinVariantNone;
            _titleLabel.CalculatesOwnHitState = false;
            _titleBar.AddChild(_titleLabel);

            _closeButton = new Button(ScreenManager);
            _closeButton.SkinVariant = SkinReferencingConstants.SkinVariantNone;
            if (includeCloseButton)
            {
                _titleBar.AddChild(_closeButton);
            }

            _childCanvas = new CanvasFrame(ScreenManager);
            _childCanvas.SkinVariant = SkinReferencingConstants.SkinVariantNone;
            _childCanvas.CalculatesOwnHitState = false;
            _childCanvas.PassesTransitionAnimationsToChildren = true;
            actualAddChild(_childCanvas);

            _closeButton.ButtonClicked += _closeButton_ButtonClicked;
        }

        void _closeButton_ButtonClicked(object sender, EventArgs e)
        {
            this.IsFinished = true;
        }

        private void actualAddChild(ScreenItem child)
        {
            base.AddChild(child);
        }

        public override void AddChild(ScreenItems.ScreenItem child)
        {
            _childCanvas.AddChild(child);
        }

        protected override void MidDraw(GameTime gameTime, MSpriteBatch.MatrixSpriteBatch spriteBatch)
        {
            base.MidDraw(gameTime, spriteBatch);
        }

        protected override void VisualStateChanged()
        {
            base.VisualStateChanged();
            _titleBar.VisualState = VisualState;
            _titleLabel.VisualState = VisualState;
            _childCanvas.VisualState = VisualState;
        }
    }
}
