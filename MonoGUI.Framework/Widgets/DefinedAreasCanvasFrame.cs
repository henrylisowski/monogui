﻿using MonoGUI.Manager;
using MonoGUI.ScreenItems;
using MonoGUI.ScreenItems.Containers.CanvasContainer;
using MonoGUI.Skinning;
using MonoGUI.Skinning.SkinElements;
using MonoGUI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Widgets
{
    public class DefinedAreasCanvasFrame : CanvasFrame
    {
        private Dictionary<ScreenItem, SkinnedPropertyStateValue<AreaDefinition>> screenItemAreaDefinitions;
        public DefinedAreasCanvasFrame(ScreenManager ScreenManager)
            : base(ScreenManager)
        {
            screenItemAreaDefinitions = new Dictionary<ScreenItem, SkinnedPropertyStateValue<AreaDefinition>>();
        }

        public virtual void AddChild(ScreenItem child, SkinnedPropertyStateValue<AreaDefinition> area)
        {
            base.AddChild(child);
            screenItemAreaDefinitions[child] = area;
        }

        public void SetAreaDefinition(ScreenItem child, SkinnedPropertyStateValue<AreaDefinition> area)
        {
            screenItemAreaDefinitions[child] = area;
        }

        public SkinnedPropertyStateValue<AreaDefinition> GetAreaDefinition(ScreenItem child)
        {
            if (screenItemAreaDefinitions.ContainsKey(child))
            {
                return screenItemAreaDefinitions[child];
            }
            return null;
        }

        public SkinnedPropertyStateValue<AreaDefinition> RemoveAreaDefinition(ScreenItem child)
        {
            if (screenItemAreaDefinitions.ContainsKey(child))
            {
                return screenItemAreaDefinitions[child];
            }
            return null;
        }

        public override void UpdateChildRelativeSize()
        {
            base.UpdateChildRelativeSize();
            IEnumerable<ScreenItem> children = UseableChildren;
            foreach (ScreenItem child in children)
            {
                if (screenItemAreaDefinitions.ContainsKey(child))
                {
                    SkinnedPropertyStateValue<AreaDefinition> baseAreas = screenItemAreaDefinitions[child];
                    if (baseAreas != null)
                    {
                        AreaDefinition updatedArea = baseAreas.GetSkinElementByState(VisualState);
                        RectangleFloat dims = updatedArea.UpdatedSize(SizeIncreaseFromMinimumDimensions, MinimumDimensions.Area.Dimensions);
                        child.RelativePosition = dims.Position;
                        child.RelativeDimensions = dims.Dimensions;
                        child.UpdateChildRelativeSize();
                    }
                }
            }
        }
    }
}
