﻿using Microsoft.Xna.Framework;
using MonoGUI.Manager.Input;
using MonoGUI.ScreenItems.CompositionElements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.ScreenItems
{
    public partial class ScreenItem : InputElement
    {
        /// <summary>
        /// Used to determine if the current item is disabled because a parent somewhere in the tree
        /// is disabled
        /// </summary>
        public bool ActualIsEnabled
        {
            get
            {
                if (!IsEnabled)
                {
                    return false;
                }
                else
                {
                    return Parent.ActualIsEnabled;
                }
            }
        }
        /// <summary>
        /// Preference that determines which player is allowed to affect this control with their gamepad
        /// </summary>
        public Manager.Input.GamepadPlayer AllowedPlayer { get; set; }
        /// <summary>
        /// Preference that determines if this item loses focus if clicked somewhere that doesn't gain focus,
        /// but outside of the item
        /// </summary>
        public bool AutoUnfocus { get; set; }
        /// <summary>
        /// Preference for whether or not this item can hold focus
        /// </summary>
        public bool Focusable { get; set; }
        /// <summary>
        /// Preference for whether or not this item gains focus when moused over
        /// </summary>
        public bool FocusWhenMouseOver { get; set; }
        /// <summary>
        /// Setting for whether or not this item is enabled
        /// </summary>
        public bool IsEnabled { get; set; }
        /// <summary>
        /// Setting for whether or not this item is the focus
        /// </summary>
        public bool IsFocused { get; set; }
        /// <summary>
        /// Preference for whether or not this item has children that you can cycle through their focus
        /// </summary>
        public bool IsFocusScope { get; set; }
        /// <summary>
        /// Setting for whether somewhere in this items children something has focus
        /// </summary>
        public bool IsFocusWithin { get; set; }
        /// <summary>
        /// Setting for whether the mouse is over this and only this item
        /// </summary>
        public bool IsMouseDirectlyOver { get; set; }
        /// <summary>
        /// Setting for whether the mouse is over this item
        /// </summary>
        public bool IsMouseOver { get; set; }
        /// <summary>
        /// Setting for whether the mouse was over this item lasts update
        /// </summary>
        public bool WasMouseOver { get; set; }
        /// <summary>
        /// Setting for whether the component calculates its own state. If false, 
        /// hit test will not run and return false for all focus and mouse over variables
        /// </summary>
        public bool CalculatesOwnHitState { get; set; }
        /// <summary>
        /// Method used to request focus for this item
        /// </summary>
        public void Focus()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Called every frame to calculate various settings for input calculation (MouseOver etc)
        /// </summary>
        public virtual void HitTest(InputManager inp)
        {
            WasMouseOver = IsMouseOver;
            if (CalculatesOwnHitState)
            {
                Vector2 mousePosition = inp.MousePosition();
                mousePosition = Vector2.Transform(mousePosition, AbsoluteInverseAnimatedTransformation);
                int[] touchIds = inp.AllTouchIds();
                if (mousePosition.X >= 0 && mousePosition.Y >= 0 &&
                    mousePosition.X <= this.RelativeDimensions.X &&
                    mousePosition.Y <= this.RelativeDimensions.Y && touchIds.Count() == 0)
                {//TODO touchIDS.Count is a temporary fix, maybe find something better?
                    IsMouseOver = true;
                    IsMouseDirectlyOver = true;
                    return;
                }

                
                for (int i = 0; i < touchIds.Length; i++ )
                {
                    Vector2 touchPosition = inp.TouchPosition(touchIds[i]);
                    touchPosition = Vector2.Transform(touchPosition, Matrix.Invert(AbsoluteAnimatedTransformation));
                    if (touchPosition.X >= 0 && touchPosition.Y >= 0 &&
                        touchPosition.X <= this.RelativeDimensions.X &&
                        touchPosition.Y <= this.RelativeDimensions.Y)
                    {
                        IsMouseOver = true;
                        IsMouseDirectlyOver = true;
                        return;
                    }
                }
            }

            IsMouseOver = false;
            IsMouseDirectlyOver = false;
        }
    }
}
