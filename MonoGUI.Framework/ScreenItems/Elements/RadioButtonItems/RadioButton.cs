﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGUI.Manager;
using MonoGUI.Themes;
using MonoGUI.Themes.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.ScreenItems.Elements.RadioButtonItems
{
    public class RadioButton : ElementPanel
    {
        private bool _clickStatus;
        private RadioButtonController _controller;

        public bool ClickStatus { get { return _clickStatus; } }
        public string Text { get; set; }
        
        public RadioButton(RadioButtonController controller, ScreenManager ScreenManager)
            : base(ScreenManager)
        {
            _clickStatus = false;
            _controller = controller;
            _controller.AddRadioButton(this);
            this.SkinTargetWidget = ComponentThemeNames.MonoGUIRadioButtonTheme;
        }

        public override void Initialize()
        {
            base.Initialize();

            this.MouseClicked += RadioButton_MouseClicked;
            
        }

        public override void HandleInput(GameTime gameTime, Manager.Input.InputManager inputManager)
        {
            base.HandleInput(gameTime, inputManager);
        }

        protected override void MidDraw(GameTime gameTime, MSpriteBatch.MatrixSpriteBatch spriteBatch)
        {
            base.MidDraw(gameTime, spriteBatch);
            //IComponentTheme t = ScreenManager.ThemeManager.GetComponentTheme(this.ComponentThemeName, this.ThemeName, VisualState);
            //string drawCommandRequest = _clickStatus ? ComponentThemeDrawCommandNames.MonoGUICheckboxChecked : ComponentThemeDrawCommandNames.MonoGUICheckboxUnchecked;

            //List<DrawCommand> backgroundCommands = t.GetDrawCommands(ComponentThemeDrawCommandNames.MonoGUIBackground, Vector2.Zero, RelativeDimensions, lifetime);
            //foreach (DrawCommand d in backgroundCommands)
            //{
            //    spriteBatch.Draw(d.SourceImage, d.DestinationPosition, d.SourceRectangle, Color.White, 0f, Vector2.Zero, d.Scale, d.Effects, 0f);
            //}

            //List<DrawCommand> drawCommands = t.GetDrawCommands(drawCommandRequest, Vector2.Zero, RelativeDimensions, lifetime);
            //foreach (DrawCommand d in drawCommands)
            //{
            //    spriteBatch.Draw(d.SourceImage, d.DestinationPosition, d.SourceRectangle, Color.White, 0f, Vector2.Zero, d.Scale, d.Effects, 0f);
            //}

            //if (_clickStatus)
            //{
            //    List<DrawCommand> checkDrawCommands = t.GetDrawCommands(ComponentThemeDrawCommandNames.MonoGUICheckboxCheck, Vector2.Zero, RelativeDimensions, lifetime);
            //    foreach (DrawCommand d in checkDrawCommands)
            //    {
            //        spriteBatch.Draw(d.SourceImage, d.DestinationPosition, d.SourceRectangle, Color.White, 0f, Vector2.Zero, d.Scale, d.Effects, 0f);
            //    }
            //}

            //string fontName = t.GetComponentString(ComponentThemeStringNames.FontName);
            //SpriteFont font = ScreenManager.ThemeManager.GetFont(fontName, 12, Themes.Fonts.FontRoundMethod.Closest);
            //Rectangle? textLocation = t.GetThemeLocation(ComponentThemeLocationNames.MonoGUICheckboxText, Vector2.Zero, RelativeDimensions, lifetime);
            //if (textLocation != null)
            //{
            //    Vector2 measured = font.MeasureString(fontName);
            //    float yPos = (textLocation.Value.Height - measured.Y) / 2 + textLocation.Value.Y;
            //    spriteBatch.DrawString(font, Text, new Vector2(textLocation.Value.X, yPos), Color.Black);
            //}
        }

        private void boxClicked()
        {
            _controller.RadioButtonClicked(this);
        }

        void RadioButton_MouseClicked(object sender, EventHandling.MouseEvents.MouseButtonEvent e)
        {
            //IComponentTheme t = ScreenManager.ThemeManager.GetComponentTheme(this.ComponentThemeName, this.ThemeName, VisualState);
            //Rectangle? clickable = t.GetThemeLocation(ComponentThemeLocationNames.MonoGUICheckboxClickable, Vector2.Zero, RelativeDimensions, lifetime);
            //if (clickable != null)
            //{
            //    if (clickable.Value.Contains((int)e.MouseEvent.Position.X, (int)e.MouseEvent.Position.Y))
            //    {
            //        boxClicked();
            //    }
            //}
        }

        public void ClickRadioButton()
        {
            boxClicked();
        }

        public event EventHandler RadioButtonClicked;

        public class RadioButtonController
        {
            List<RadioButton> watchedRadioButtons;
            public RadioButtonController()
            {
                watchedRadioButtons = new List<RadioButton>();
            }

            public void AddRadioButton(RadioButton button)
            {
                if (button != null && !watchedRadioButtons.Contains(button))
                {
                    watchedRadioButtons.Add(button);
                }
            }

            public void RemoveRadioButton(RadioButton button)
            {
                if (button != null && watchedRadioButtons.Contains(button))
                {
                    watchedRadioButtons.Remove(button);
                }
            }

            public void RadioButtonClicked(RadioButton button)
            {
                button._clickStatus = true;
                if (button.RadioButtonClicked != null)
                {
                    button.RadioButtonClicked(button, null);
                }
                foreach (RadioButton b in watchedRadioButtons)
                {
                    if (b != button)
                    {
                        b._clickStatus = false;
                    }
                }
            }
        }
    }
}
