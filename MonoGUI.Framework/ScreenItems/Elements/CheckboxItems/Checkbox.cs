﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGUI.Manager;
using MonoGUI.Themes;
using MonoGUI.Themes.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.ScreenItems.Elements.CheckboxItems
{
    public class Checkbox : ElementPanel
    {
        private bool _clickStatus;
        public bool ClickStatus { get { return _clickStatus; } }
        public string Text { get; set; }
        public Checkbox(ScreenManager ScreenManager)
            : base(ScreenManager)
        {
            _clickStatus = false;
            this.SkinTargetWidget = ComponentThemeNames.MonoGUICheckboxTheme;
        }

        public override void Initialize()
        {
            base.Initialize();

            this.MouseClicked += Checkbox_MouseClicked;
            
        }

        protected override void MidDraw(GameTime gameTime, MSpriteBatch.MatrixSpriteBatch spriteBatch)
        {
            base.MidDraw(gameTime, spriteBatch);
            //IComponentTheme t = ScreenManager.ThemeManager.GetComponentTheme(this.ComponentThemeName, this.ThemeName, VisualState);
            //string drawCommandRequest = _clickStatus ? ComponentThemeDrawCommandNames.MonoGUICheckboxChecked : ComponentThemeDrawCommandNames.MonoGUICheckboxUnchecked;

            //List<DrawCommand> backgroundCommands = t.GetDrawCommands(ComponentThemeDrawCommandNames.MonoGUIBackground, Vector2.Zero, RelativeDimensions, lifetime);
            //foreach (DrawCommand d in backgroundCommands)
            //{
            //    spriteBatch.Draw(d.SourceImage, d.DestinationPosition, d.SourceRectangle, Color.White, 0f, Vector2.Zero, d.Scale, d.Effects, 0f);
            //}

            //List<DrawCommand> drawCommands = t.GetDrawCommands(drawCommandRequest, Vector2.Zero, RelativeDimensions, lifetime);
            //foreach (DrawCommand d in drawCommands)
            //{
            //    spriteBatch.Draw(d.SourceImage, d.DestinationPosition, d.SourceRectangle, Color.White, 0f, Vector2.Zero, d.Scale, d.Effects, 0f);
            //}

            //if (_clickStatus)
            //{
            //    List<DrawCommand> checkDrawCommands = t.GetDrawCommands(ComponentThemeDrawCommandNames.MonoGUICheckboxCheck, Vector2.Zero, RelativeDimensions, lifetime);
            //    foreach (DrawCommand d in checkDrawCommands)
            //    {
            //        spriteBatch.Draw(d.SourceImage, d.DestinationPosition, d.SourceRectangle, Color.White, 0f, Vector2.Zero, d.Scale, d.Effects, 0f);
            //    }
            //}

            //string fontName = t.GetComponentString(ComponentThemeStringNames.FontName);
            //SpriteFont font = ScreenManager.ThemeManager.GetFont(fontName, 12, Themes.Fonts.FontRoundMethod.Closest);
            //Rectangle? textLocation = t.GetThemeLocation(ComponentThemeLocationNames.MonoGUICheckboxText, Vector2.Zero, RelativeDimensions, lifetime);
            //if (textLocation != null)
            //{
            //    Vector2 measured = font.MeasureString(fontName);
            //    float yPos = (textLocation.Value.Height - measured.Y) / 2 + textLocation.Value.Y;
            //    spriteBatch.DrawString(font, Text, new Vector2(textLocation.Value.X, yPos), Color.Black);
            //}
        }

        private void boxClicked()
        {
            if (_clickStatus)
            {
                _clickStatus = false;
            }
            else
            {
                _clickStatus = true;
            }

            if (CheckboxClicked != null)
            {
                CheckboxClicked(this, null);
            }
        }

        void Checkbox_MouseClicked(object sender, EventHandling.MouseEvents.MouseButtonEvent e)
        {
            //IComponentTheme t = ScreenManager.ThemeManager.GetComponentTheme(this.ComponentThemeName, this.ThemeName, VisualState);
            //Rectangle? clickable = t.GetThemeLocation(ComponentThemeLocationNames.MonoGUICheckboxClickable, Vector2.Zero, RelativeDimensions, lifetime);
            //if (clickable != null)
            //{
            //    if (clickable.Value.Contains((int)e.MouseEvent.Position.X, (int)e.MouseEvent.Position.Y))
            //    {
            //        boxClicked();
            //    }
            //}
        }

        public void ClickCheckbox()
        {
            boxClicked();
        }

        public void SetCheckbox(bool val)
        {
            bool oldClickStatus = _clickStatus;
            _clickStatus = val;
            if (val != oldClickStatus)
            {
                if (CheckboxClicked != null)
                {
                    CheckboxClicked(this, null);
                }
            }
        }

        public event EventHandler CheckboxClicked;
    }
}
