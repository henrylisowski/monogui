﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGUI.Manager;
using MonoGUI.Skinning.Fonts;
using MonoGUI.Themes;
using MonoGUI.Themes.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.ScreenItems.Elements.LabelIems
{
    public class Label : ElementPanel
    {
        private string _text;
        public string Text
        {
            get
            {
                return _text;
            }
            set
            {
                _text = value;
                updateDisplay();
            }
        }

        private int _fontSize;
        public int FontSize
        {
            get
            {
                return _fontSize;
            }
            set
            {
                _fontSize = value;
                updateDisplay();
            }
        }

        private FontRoundMethod _fontRoundMethod;
        public FontRoundMethod FontRoundMethod
        {
            get
            {
                return _fontRoundMethod;
            }
            set
            {
                _fontRoundMethod = value;
                updateDisplay();
            }
        }

        public Label(ScreenManager ScreenManager)
            : base(ScreenManager)
        {
            SkinTargetWidget = ComponentThemeNames.MonoGUILabelTheme;
        }

        public Label(string Text, ScreenManager ScreenManager)
            : base(ScreenManager)
        {
            _text = Text;
            SkinTargetWidget = ComponentThemeNames.MonoGUILabelTheme;
        }

        public override void UpdateRequestedSizeDueToChildResize()
        {
            //TODO this is assuming wrapping, work on including others
            //IComponentTheme theme = ScreenManager.ThemeManager.GetComponentTheme(ComponentThemeName, ThemeName, VisualState);
            //string fontName = theme.GetComponentString(ComponentThemeStringNames.FontName);
            //SpriteFont font = ScreenManager.ThemeManager.GetFont(fontName, _fontSize, _fontRoundMethod);
            //Vector2 dimensions = font.MeasureString(Text);
            //this.PreferredDimensions = dimensions;
            base.UpdateRequestedSizeDueToChildResize();
        }

        protected override void MidDraw(GameTime gameTime, MSpriteBatch.MatrixSpriteBatch spriteBatch)
        {
            base.MidDraw(gameTime, spriteBatch);
            //IComponentTheme theme = ScreenManager.ThemeManager.GetComponentTheme(ComponentThemeName, ThemeName, VisualState);
            //string fontName = theme.GetComponentString(ComponentThemeStringNames.FontName);
            //SpriteFont font = ScreenManager.ThemeManager.GetFont(fontName, _fontSize, _fontRoundMethod);

            //spriteBatch.DrawString(font, Text, Vector2.Zero, Color.Black);
        }

        private void updateDisplay()
        {
            
            RequestResize();
        }
    }
}
