﻿using Microsoft.Xna.Framework.Graphics;
using MonoGUI.Manager;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MonoGUI.ScreenItems.Elements.ImageItems
{
    public class ImagePanel : ElementPanel
    {
        private Texture2D image;
        private string imageName;
        ImageSourceType sourceType;

        public ImagePanel(ScreenManager ScreenManager, ParentNode Parent, string imageName, ImageSourceType sourceType)
            : base(ScreenManager)
        {
            this.imageName = imageName;
            this.sourceType = sourceType;
            this.Width = ItemSizeOptions.WrapContent;
            this.Height = ItemSizeOptions.WrapContent;
        }

        public override void HandleInput(Microsoft.Xna.Framework.GameTime gameTime, Manager.Input.InputManager inputManager)
        {
            base.HandleInput(gameTime, inputManager);
        }

        public override void LoadContent(Microsoft.Xna.Framework.Content.ContentManager Content)
        {
            base.LoadContent(Content);

            switch (sourceType)
            {
                case ImageSourceType.Content:
                    image = Content.Load<Texture2D>(imageName);
                    break;
                case ImageSourceType.Stream:
                    //image = ScreenManager.PlatformSpecific.loadTexture2DFromFile(ScreenManager.Graphics.GraphicsDevice, imageName);
                    break;
                    
            }
            if (this.Width == ItemSizeOptions.WrapContent)
            {
                PreferredDimensions = new Microsoft.Xna.Framework.Vector2(image.Width, PreferredDimensions.Y);
            }
            if (this.Height == ItemSizeOptions.WrapContent)
            {
                PreferredDimensions = new Microsoft.Xna.Framework.Vector2(PreferredDimensions.X, image.Height);
            }
        }

        protected override void MidDraw(Microsoft.Xna.Framework.GameTime gameTime, MSpriteBatch.MatrixSpriteBatch spriteBatch)
        {
            base.MidDraw(gameTime, spriteBatch);
            if (image != null)
            {
                spriteBatch.Draw(image, 
                    new Microsoft.Xna.Framework.Rectangle(0, 0, 
                        (int)this.RelativeDimensions.X, (int)this.RelativeDimensions.Y), 
                    Microsoft.Xna.Framework.Color.White);
            }
        }
    }
}
