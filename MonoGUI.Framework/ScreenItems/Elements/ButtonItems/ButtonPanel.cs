﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGUI.Manager;
using MonoGUI.MSpriteBatch;
using MonoGUI.Themes;
using MonoGUI.Themes.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.ScreenItems.Elements.ButtonItems
{
    public class ButtonPanel : ElementPanel
    {
        public ButtonPanel(ScreenManager ScreenManager)
            : base(ScreenManager)
        {
            Width = ItemSizeOptions.Absolute;
            Height = ItemSizeOptions.Absolute;
            SkinTargetWidget = ComponentThemeNames.MonoGUIButtonTheme;
        }

        public override void HandleInput(GameTime gameTime, Manager.Input.InputManager inputManager)
        {
            //base.HandleInput(gameTime, inputManager);
            //if (inputManager.KeyIsDown(Microsoft.Xna.Framework.Input.Keys.Left))
            //{
            //    PreferredDimensions = new Vector2(PreferredDimensions.X - 1, PreferredDimensions.Y);
            //    RequestResize();
            //}
            //if (inputManager.KeyIsDown(Microsoft.Xna.Framework.Input.Keys.Right))
            //{
            //    PreferredDimensions = new Vector2(PreferredDimensions.X + 1, PreferredDimensions.Y);
            //    RequestResize();
            //}
            //if (inputManager.KeyIsDown(Microsoft.Xna.Framework.Input.Keys.Up))
            //{
            //    PreferredDimensions = new Vector2(PreferredDimensions.X, PreferredDimensions.Y - 1);
            //    RequestResize();
            //}
            //if (inputManager.KeyIsDown(Microsoft.Xna.Framework.Input.Keys.Down))
            //{
            //    PreferredDimensions = new Vector2(PreferredDimensions.X, PreferredDimensions.Y + 1);
            //    RequestResize();
            //}
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        protected override void MidDraw(Microsoft.Xna.Framework.GameTime gameTime, MSpriteBatch.MatrixSpriteBatch spriteBatch)
        {
            base.MidDraw(gameTime, spriteBatch);
            //IComponentTheme theme = ScreenManager.ThemeManager.GetComponentTheme(ComponentThemeName, ThemeName, VisualState);
            //List<DrawCommand> commands = theme.GetDrawCommands(ComponentThemeDrawCommandNames.MonoGUIForeground, RelativePosition, RelativeDimensions, lifetime);
            //List<DrawCommand> backgroundCommands = theme.GetDrawCommands(ComponentThemeDrawCommandNames.MonoGUIBackground, RelativePosition, RelativeDimensions, lifetime);

            //spriteBatch.HaltDraw();
            //DrawBeginInfo beg = spriteBatch.ModifiableBeginCall();
            //beg.SamplerState = SamplerState.PointClamp;
            //spriteBatch.Begin(beg);
            //foreach (DrawCommand c in backgroundCommands)
            //{
            //    spriteBatch.Draw(c.SourceImage, c.DestinationPosition, c.SourceRectangle, Color.White, 0f, Vector2.Zero, c.Scale, c.Effects, 0f);
            //}
            //foreach (DrawCommand c in commands)
            //{
            //    spriteBatch.Draw(c.SourceImage, c.DestinationPosition, c.SourceRectangle, Color.White, 0f, Vector2.Zero, c.Scale, c.Effects, 0f);
            //}
            //spriteBatch.End();
            //spriteBatch.ResumeDraw();
        }


        public override void UpdateRequestedSizeDueToChildResize()
        {
            RequestedDimensions = PreferredDimensions;
        }

        public override void UpdateChildRelativeSize()
        {
            
        }
    }
}
