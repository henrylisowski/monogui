﻿using MonoGUI.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.ScreenItems.Elements
{
    public class ElementPanel : ScreenItem
    {
        public ElementPanel(ScreenManager ScreenManager) : base(ScreenManager)
        {

        }

        public override void UpdateRequestedSizeDueToChildResize()
        {
            this.RequestedDimensions = this.PreferredDimensions;
            this.RequestedPosition = this.PreferredPosition;
        }
        public override void UpdateChildRelativeSize()
        {

        }
    }
}
