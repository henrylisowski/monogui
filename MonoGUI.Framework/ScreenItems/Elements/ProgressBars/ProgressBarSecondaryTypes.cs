﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.ScreenItems.Elements.ProgressBars
{
    public enum ProgressBarSecondaryTypes
    {
        None, Separate, AutoIncrement
    }
}
