﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGUI.Manager;
using MonoGUI.MSpriteBatch;
using MonoGUI.Themes;
using MonoGUI.Themes.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.ScreenItems.Elements.ProgressBars
{
    public class ProgressBar : ElementPanel
    {
        //TODO handle how changing of maxvalue and number secondary segments affects currentvalue
        public float MaxValue { get; set; }
        public float CurrentValue { get; protected set; }
        public float SecondaryMaxValue { get; set; }
        public float SecondaryCurrentValue { get; protected set; }
        public ProgressBarSecondaryTypes SecondaryType { get; set; }
        public float SecondaryIncrementAmount { get; set; }
        public float FullPercentage { get; protected set; }
        public float SecondaryFullPercentage { get; protected set; }

        private AlphaTestEffect gradientStencilTest;
        private AlphaTestEffect gradientFillTest;
        private AlphaTestEffect gradientClipTest;
        private DepthStencilState _stencilGradient;
        private DepthStencilState _stencilGradientNoClipping;
        private DepthStencilState _stencilFill;
        private DepthStencilState _stencilClipping;
        public ProgressBar(ScreenManager ScreenManager)
            : base(ScreenManager)
        {
            Width = ItemSizeOptions.Absolute;
            Height = ItemSizeOptions.Absolute;
            PreferredDimensions = new Vector2(100, 200);
            MaxValue = 1;
            CurrentValue = 0;
            SecondaryMaxValue = 1;
            SecondaryCurrentValue = 0;
            SecondaryIncrementAmount = 1;
            SkinTargetWidget = ComponentThemeNames.MonoGUIProgressBarTheme;
            SecondaryType = ProgressBarSecondaryTypes.None;
        }

        public void IncreaseFill(float amount)
        {
            CurrentValue += amount;
            if (CurrentValue > MaxValue)
            {
                if (SecondaryType == ProgressBarSecondaryTypes.AutoIncrement)
                {
                    SecondaryCurrentValue += SecondaryIncrementAmount;
                    if (SecondaryCurrentValue > SecondaryMaxValue)
                    {
                        SecondaryCurrentValue = SecondaryMaxValue;
                        CurrentValue = MaxValue;
                    }
                    else
                    {
                        CurrentValue -= MaxValue;
                    }
                }
                else
                {
                    CurrentValue = MaxValue;
                }
            }
        }

        public void DecreaseFill(float amount)
        {
            CurrentValue -= amount;
            if (CurrentValue < 0)
            {
                if (SecondaryType == ProgressBarSecondaryTypes.AutoIncrement)
                {
                    SecondaryCurrentValue -= SecondaryIncrementAmount;
                    if (SecondaryCurrentValue < 0)
                    {
                        SecondaryCurrentValue = 0;
                        CurrentValue = 0;
                    }
                    else
                    {
                        CurrentValue += MaxValue;
                    }
                }
                else
                {
                    CurrentValue = 0;
                }
            }
        }

        public void SetCurrentValue(float amount)
        {
            CurrentValue = amount;
            if (CurrentValue > MaxValue)
            {
                CurrentValue = MaxValue;
            }
            if (CurrentValue < 0)
            {
                CurrentValue = 0;
            }
        }

        public void SetCurrentSecondaryValue(float amount)
        {
            SecondaryCurrentValue = amount;
            if (SecondaryCurrentValue > SecondaryMaxValue)
            {
                SecondaryCurrentValue = SecondaryMaxValue;
            }
            if (SecondaryCurrentValue < 0)
            {
                SecondaryCurrentValue = 0;
            }
        }

        public override void Initialize()
        {
            base.Initialize();

            createAlphaTestEffects();

            _stencilGradient = new DepthStencilState();
            _stencilGradient.StencilEnable = true;
            _stencilGradient.StencilFunction = CompareFunction.Equal;
            _stencilGradient.StencilPass = StencilOperation.Increment;
            _stencilGradient.ReferenceStencil = 1;

            _stencilGradientNoClipping = new DepthStencilState();
            _stencilGradientNoClipping.StencilEnable = true;
            _stencilGradientNoClipping.StencilFunction = CompareFunction.Always;
            _stencilGradientNoClipping.StencilPass = StencilOperation.Replace;
            _stencilGradientNoClipping.ReferenceStencil = 2;

            _stencilFill = new DepthStencilState();
            _stencilFill.StencilEnable = true;
            _stencilFill.StencilFunction = CompareFunction.Equal;
            _stencilFill.StencilPass = StencilOperation.Keep;
            _stencilFill.ReferenceStencil = 2;

            _stencilClipping = new DepthStencilState();
            _stencilClipping.StencilEnable = true;
            _stencilClipping.StencilFunction = CompareFunction.Always;
            _stencilClipping.StencilPass = StencilOperation.Replace;
            _stencilClipping.ReferenceStencil = 1;
        }

        private void createAlphaTestEffects()
        {
            gradientStencilTest = new AlphaTestEffect(Manager.Graphics.GraphicsDevice);
            gradientStencilTest.AlphaFunction = CompareFunction.Less;
            gradientStencilTest.ReferenceAlpha = 0;

            gradientFillTest = new AlphaTestEffect(Manager.Graphics.GraphicsDevice);
            gradientFillTest.AlphaFunction = CompareFunction.GreaterEqual;
            gradientFillTest.ReferenceAlpha = 1;

            gradientClipTest = new AlphaTestEffect(Manager.Graphics.GraphicsDevice);
            gradientClipTest.AlphaFunction = CompareFunction.GreaterEqual;
            gradientClipTest.ReferenceAlpha = 1;
            gradientClipTest.VertexColorEnabled = true;

            Matrix projection = Matrix.CreateOrthographicOffCenter(0, Manager.Graphics.GraphicsDevice.Viewport.Width,
                Manager.Graphics.GraphicsDevice.Viewport.Height, 0, 0, 1);

            gradientStencilTest.Projection = projection;
            gradientFillTest.Projection = projection;
            gradientClipTest.Projection = projection;
        }

        public override void HandleInput(GameTime gameTime, Manager.Input.InputManager inputManager)
        {
            base.HandleInput(gameTime, inputManager);
            if (inputManager.KeyIsDown(Microsoft.Xna.Framework.Input.Keys.Up))
            {
                IncreaseFill(1f);
            }
            if (inputManager.KeyIsDown(Microsoft.Xna.Framework.Input.Keys.Down))
            {
                DecreaseFill(1f);
            }
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            FullPercentage = ((CurrentValue / MaxValue));
            SecondaryFullPercentage = ((SecondaryCurrentValue / SecondaryMaxValue));
        }

        protected override void MidDraw(Microsoft.Xna.Framework.GameTime gameTime, MSpriteBatch.MatrixSpriteBatch spriteBatch)
        {
            base.MidDraw(gameTime, spriteBatch);
            //IComponentTheme theme = ScreenManager.ThemeManager.GetComponentTheme(ComponentThemeName, ThemeName, VisualState);
            //List<DrawCommand> commands = theme.GetDrawCommands(ComponentThemeDrawCommandNames.MonoGUIForeground, Vector2.Zero, RelativeDimensions, lifetime);
            //List<DrawCommand> backgroundCommands = theme.GetDrawCommands(ComponentThemeDrawCommandNames.MonoGUIBackground, Vector2.Zero, RelativeDimensions, lifetime);

            //DrawCommand stencil = theme.GetComponentThemeTextureAreaPair(ComponentThemeTAPNames.ProgressBarGradientStencil, 
            //    Vector2.Zero, RelativeDimensions, lifetime);
            //DrawCommand fill = theme.GetComponentThemeTextureAreaPair(ComponentThemeTAPNames.ProgressBarFiller,
            //    Vector2.Zero, RelativeDimensions, lifetime);
            //DrawCommand clipping = theme.GetComponentThemeTextureAreaPair(ComponentThemeTAPNames.ProgressBarStencilClipping,
            //    Vector2.Zero, RelativeDimensions, lifetime);

            //DrawCommand stencilSecondary = theme.GetComponentThemeTextureAreaPair(ComponentThemeTAPNames.ProgressBarSecondaryGradientStencil,
            //    Vector2.Zero, RelativeDimensions, lifetime);
            //DrawCommand fillSecondary = theme.GetComponentThemeTextureAreaPair(ComponentThemeTAPNames.ProgressBarSecondaryFiller,
            //    Vector2.Zero, RelativeDimensions, lifetime);
            //DrawCommand clippingSecondary = theme.GetComponentThemeTextureAreaPair(ComponentThemeTAPNames.ProgressBarSecondaryStencilClipping,
            //    Vector2.Zero, RelativeDimensions, lifetime);


            //foreach (DrawCommand c in backgroundCommands)
            //{
            //    //spriteBatch.Draw(c.SourceImage, c.DestinationPosition, c.SourceRectangle, Color.White, 0f, Vector2.Zero, c.Scale, c.Effects, 0f);
            //}

            //spriteBatch.HaltDraw();

            

            //drawStencilObjects(spriteBatch, stencil, fill, clipping, FullPercentage, 1);

            //if (SecondaryType != ProgressBarSecondaryTypes.None)
            //{
            //    drawStencilObjects(spriteBatch, stencilSecondary, fillSecondary, clippingSecondary, SecondaryFullPercentage, 3);
            //}
            

            //spriteBatch.ResumeDraw();

            //foreach (DrawCommand c in commands)
            //{
            //    spriteBatch.Draw(c.SourceImage, c.DestinationPosition, c.SourceRectangle, Color.White, 0f, Vector2.Zero, c.Scale, c.Effects, 0f);
            //}
            
            
        }

        private void drawStencilObjects(MatrixSpriteBatch spriteBatch, DrawCommand stencil, DrawCommand fill, DrawCommand clipping, 
            float alphaReference, int depth)
        {
            _stencilClipping.ReferenceStencil = depth;
            _stencilFill.ReferenceStencil = depth + 1;
            _stencilGradient.ReferenceStencil = depth;
            _stencilGradientNoClipping.ReferenceStencil = depth + 1;
            if (clipping != null && SecondaryType != ProgressBarSecondaryTypes.None)
            {
                DrawBeginInfo stencilBeginClipping = spriteBatch.ModifiableBeginCall();
                stencilBeginClipping.SortMode = SpriteSortMode.Immediate;
                stencilBeginClipping.DepthStencilState = _stencilClipping;
                stencilBeginClipping.Effect = gradientClipTest;

                spriteBatch.Begin(stencilBeginClipping);
                //TODO using a transparency of 1 is a HACK! Need a better way to do this
                //spriteBatch.Draw(clipping.SourceImage, clipping.DestinationPosition, clipping.SourceRectangle,
                //    new Color(0, 0, 0, 1), 0f, Vector2.Zero, clipping.Scale, clipping.Effects, 0f);
                spriteBatch.End();

            }

            DrawBeginInfo stencilBegin = spriteBatch.ModifiableBeginCall();
            stencilBegin.SortMode = SpriteSortMode.Immediate;
            stencilBegin.DepthStencilState = _stencilGradient;
            gradientStencilTest.ReferenceAlpha = (int)(256 * alphaReference);
            stencilBegin.Effect = gradientStencilTest;

            if (SecondaryType == ProgressBarSecondaryTypes.None)
            {
                stencilBegin.DepthStencilState = _stencilGradientNoClipping;
            }

            spriteBatch.Begin(stencilBegin);
            //spriteBatch.Draw(stencil.SourceImage, stencil.DestinationPosition, stencil.SourceRectangle,
            //    Color.White, 0f, Vector2.Zero, stencil.Scale, stencil.Effects, 0f);
            spriteBatch.End();




            ////draw everthing inside stencils
            DrawBeginInfo fillBegin = spriteBatch.ModifiableBeginCall();
            fillBegin.SortMode = SpriteSortMode.Immediate;
            fillBegin.DepthStencilState = _stencilFill;
            fillBegin.Effect = gradientFillTest;
            spriteBatch.Begin(fillBegin);
            //spriteBatch.Draw(fill.SourceImage, fill.DestinationPosition, fill.SourceRectangle,
            //    Color.White, 0f, Vector2.Zero, fill.Scale, fill.Effects, 0f);
            spriteBatch.End();
        }


        public override void UpdateRequestedSizeDueToChildResize()
        {
            RequestedDimensions = PreferredDimensions;
            RequestedPosition = PreferredPosition;
        }

        public override void UpdateChildRelativeSize()
        {
            
        }
    }
}
