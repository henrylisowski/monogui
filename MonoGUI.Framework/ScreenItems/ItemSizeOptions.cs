﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.ScreenItems
{
    public enum ItemSizeOptions
    {
        Absolute, 
        FillParent, 
        Remaining, 
        WrapContent,
        Percentage
    }
}
