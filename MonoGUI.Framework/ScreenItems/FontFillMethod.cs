﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.ScreenItems
{
    public enum FontFillMethod
    {
        Absolute,
        Fill,
        ClampHeight,
        ClampWidth
    }
}
