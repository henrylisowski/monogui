﻿using Microsoft.Xna.Framework;
using MonoGUI.ScreenItems.CompositionElements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.ScreenItems
{
    public interface ParentNode : PositionalElement, InputElement
    {
        
    }
}
