﻿using Microsoft.Xna.Framework;
using MonoGUI.Manager;
using MonoGUI.ScreenItems.Containers.ZoomContainer;
using MonoGUI.Skinning;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.ScreenItems.Containers
{
    public class GridPanel : ContainerPanel
    {
        private Dictionary<ZoomPanel, ScreenItem> zoomToChildMap;
        private Dictionary<ScreenItem, ZoomPanel> childToZoomMap;
        private List<ScreenItem> childList;

        private int _numberOfColumns;
        private float _spacingPercentage;

        public GridPanel(ScreenManager Manager, int NumberOfColumns, float SpacingPercentage)
            : base(Manager)
        {
            this._numberOfColumns = NumberOfColumns;
            this._spacingPercentage = SpacingPercentage;

            zoomToChildMap = new Dictionary<ZoomPanel, ScreenItem>();
            childToZoomMap = new Dictionary<ScreenItem, ZoomPanel>();
            childList = new List<ScreenItem>();

            this.SkinTargetWidget = SkinReferencingConstants.WidgetTargetGridPanel;
        }

        public void AddChild(ScreenItem child)
        {
            childList.Add(child);
            child.Parent = this;
            child.ApplySkin();
            child.Initialize();
            child.LoadContent(Manager.Content);
            RequestResize();
        }

        public ZoomPanel AddChildInZoomPanel(ScreenItem child)
        {
            ZoomPanel z = new ZoomPanel(Manager);
            z.SetChild(child);
            z.Width = ItemSizeOptions.Absolute;
            z.Height = ItemSizeOptions.Absolute;
            AddChild(z);

            zoomToChildMap.Add(z, child);
            childToZoomMap.Add(child, z);

            return z;
        }

        public ZoomPanel GetZoomPanelForChild(ScreenItem child)
        {
            if (childToZoomMap.ContainsKey(child))
            {
                return childToZoomMap[child];
            }
            return null;
        }

        public ScreenItem GetChildForZoomPanel(ZoomPanel z)
        {
            if (zoomToChildMap.ContainsKey(z))
            {
                return zoomToChildMap[z];
            }
            return null;
        }

        public override void CalculateChildren()
        {
            UseableChildren.Clear();
            for (int i = 0; i < childList.Count; i++)
            {
                UseableChildren.Add(childList[i]);
                childList[i].CalculateChildren();
            }
        }

        public override void RemoveChild(ScreenItem toRemove)
        {
            if (childList.Contains(toRemove))
            {
                childList.Remove(toRemove);
            }
            if (childToZoomMap.ContainsKey(toRemove))
            {
                zoomToChildMap.Remove(childToZoomMap[toRemove]);
                childToZoomMap.Remove(toRemove);
            }
        }

        public override void Clear()
        {
            zoomToChildMap.Clear();
            childToZoomMap.Clear();
            childList.Clear();
        }

        public override void UpdateRequestedSizeDueToChildResize()
        {
            foreach (ScreenItem Child in childList)
            {
                Child.UpdateRequestedSizeDueToChildResize();
            }
            //Can't handle wrap, can only handle absolute or sizes based off of parent
            this.RequestedDimensions = PreferredDimensions;
            this.RequestedPosition = PreferredPosition;
        }

        public override void UpdateChildRelativeSize()
        {

            Vector2 useableDimensions = this.RelativeDimensions;
            float numPlusSpacing = (_numberOfColumns * (1 + _spacingPercentage)) + _spacingPercentage;
            float amount = useableDimensions.X / numPlusSpacing;
            float absoluteSpacing = amount * _spacingPercentage;
            float absoluteSize = absoluteSpacing / _spacingPercentage;

            //If Vertical Wrap is on, need to adjust height to correspond to width
            if (Height == ItemSizeOptions.WrapContent)
            {
                int numRows = ((childList.Count - 1) / _numberOfColumns) + 1;
                float newHeight = absoluteSpacing + (numRows * (absoluteSize + absoluteSpacing));
                this.RelativeDimensions = new Vector2(RelativeDimensions.X, newHeight);
            }

            Vector2 ChildSize = new Vector2(absoluteSize, absoluteSize);

            Vector2 startPosition = new Vector2(absoluteSpacing);
            Vector2 runningPosition = startPosition;
            Vector2 incrementPosition = ChildSize + startPosition;

            int columnNumber = 0;
            for (int i = 0; i < childList.Count; i++)
            {
                ScreenItem cur = childList[i];
                if (columnNumber >= _numberOfColumns)
                {
                    columnNumber = 0;
                    //Overflows to the right, start new row
                    runningPosition = new Vector2(startPosition.X, runningPosition.Y + incrementPosition.Y);
                }

                cur.RelativeDimensions = ChildSize;
                cur.RelativePosition = runningPosition;
                runningPosition += new Vector2(incrementPosition.X, 0);
                columnNumber++;
            }

            foreach (ScreenItem Child in childList)
            {
                Child.UpdateChildRelativeSize();
            }
        }
    }
}
