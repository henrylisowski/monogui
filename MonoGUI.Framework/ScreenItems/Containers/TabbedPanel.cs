﻿using Microsoft.Xna.Framework;
using MonoGUI.Manager;
using MonoGUI.ScreenItems.Containers.CanvasContainer;
using MonoGUI.Skinning;
using MonoGUI.Skinning.SkinElements;
using MonoGUI.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace MonoGUI.ScreenItems.Containers
{
    public class TabbedPanel : DefinedAreasCanvasFrame
    {
        #region Skinned Properties
        private string _tabUnselectedButtonVariant;
        [SkinnedProperty]
        public string TabUnselectedButtonVariant
        {
            get { return _tabUnselectedButtonVariant; }
            set { _tabUnselectedButtonVariant = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); handleButtonSkinChange(); }
        }

        private string _tabSelectedButtonVariant;
        [SkinnedProperty]
        public string TabSelectedButtonVariant
        {
            get { return _tabSelectedButtonVariant; }
            set { _tabSelectedButtonVariant = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); handleButtonSkinChange(); }
        }

        private string _childCanvasVariant;
        [SkinnedProperty]
        public string ChildCanvasVariant
        {
            get { return _childCanvasVariant; }
            set { _childCanvasVariant = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); _childFrame.SkinVariant = value; }
        }

        private int _tabSpacingAmount;
        [SkinnedProperty]
        public int TabSpacingAmount
        {
            get { return _tabSpacingAmount; }
            set { _tabSpacingAmount = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); signalButtonSizingRequired(); }
        }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> TabFrameArea
        {
            get { return GetAreaDefinition(_tabFrame); }
            set { SetAreaDefinition(_tabFrame, value); SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> ChildPanelFrameArea
        {
            get { return GetAreaDefinition(_childFrame); }
            set { SetAreaDefinition(_childFrame, value); SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }
        #endregion

        private List<Button> _tabButtons;
        private List<CanvasFrame> _childCanvases;
        private CanvasFrame _tabFrame;
        private CanvasFrame _childFrame;

        private Button _currentButton;

        private bool _buttonSizingRequired;
        private bool _buttonOrderingChanged;

        public int NumberOfFullSizedTabs { get; set; }
        
        public TabbedPanel(ScreenManager Manager, int NumberOfFullSizedTabs)
            :base(Manager)
        {
            this.SkinTargetWidget = SkinReferencingConstants.WidgetTargetTabbedPanel;
            this.NumberOfFullSizedTabs = NumberOfFullSizedTabs;

            _childFrame = new CanvasFrame(Manager);
            AddChild(_childFrame);
            _tabFrame = new CanvasFrame(Manager);
            AddChild(_tabFrame);


            _tabButtons = new List<Button>();
            _childCanvases = new List<CanvasFrame>();
        }

        public CanvasFrame AddTab(string TabName)
        {
            CanvasFrame retVal = new CanvasFrame(Manager);
            retVal.SkinVariant = SkinReferencingConstants.SkinVariantNone;
            retVal.CalculatesOwnHitState = false;
            retVal.PassesTransitionAnimationsToChildren = true;
            retVal.Width = ItemSizeOptions.FillParent;
            retVal.Height = ItemSizeOptions.FillParent;
            _childCanvases.Add(retVal);
            _childFrame.AddChild(retVal);
            retVal.IsEnabled = false;
            
            Button tabButton = new Button(Manager, TabName);
            _currentButton = tabButton;
            _tabButtons.Add(tabButton);
            tabButton.ButtonClicked += tabButton_ButtonClicked;
            signalButtonSizingRequired();
            _buttonOrderingChanged = true;
            handleButtonSkinChange();
            
            return retVal;
        }

        void tabButton_ButtonClicked(object sender, EventArgs e)
        {
            Button s = (Button)sender;
            _currentButton = s;
            _buttonOrderingChanged = true;
            signalButtonSizingRequired();
        }

        public void SetSelectedTab(int index)
        {
            index = MathHelper.Clamp(index, 0, _tabButtons.Count - 1);
            _currentButton = _tabButtons[index];
            _buttonOrderingChanged = true;
            signalButtonSizingRequired();
        }

        private void signalButtonSizingRequired()
        {
            _buttonSizingRequired = true;
            RequestResize();
            if (_buttonOrderingChanged)
            {
                _buttonOrderingChanged = false;
                foreach (Button b in _tabButtons)
                {
                    _tabFrame.RemoveChild(b);
                }

                for (int i = _tabButtons.Count - 1; i >= 0; i--)
                {
                    Button cur = _tabButtons[i];
                    if (!_currentButton.Equals(cur))
                    {
                        _tabFrame.AddChild(cur);
                    }
                }
                _tabFrame.AddChild(_currentButton);
            }
        }

        private void handleButtonSizing()
        {
            int numButtons = _tabButtons.Count;
            int totalSpacing = TabSpacingAmount * (numButtons - 1);
            float remainingSpace = _tabFrame.RelativeDimensions.X - totalSpacing;
            float absoluteSpacePerButton = remainingSpace / numButtons;
            float absoluteHeightPerButton = _tabFrame.RelativeDimensions.Y;
            
            float currentXPosition = 0f;
            foreach (Button b in _tabButtons)
            {
                b.RelativeDimensions = new Vector2(absoluteSpacePerButton, absoluteHeightPerButton);
                b.RelativePosition = new Vector2(currentXPosition, 0f);
                currentXPosition += (absoluteSpacePerButton + TabSpacingAmount);
            }

            for (int i = 0; i < _tabButtons.Count; i++)
            {
                Button but = _tabButtons[i];
                CanvasFrame canv = _childCanvases[i];

                if (but.Equals(_currentButton))
                {
                    canv.IsEnabled = true;
                }
                else
                {
                    canv.IsEnabled = false;
                }
            }
            handleButtonSkinChange();

        }

        private void handleButtonSkinChange()
        {
            foreach (Button b in _tabButtons)
            {
                if (_currentButton.Equals(b))
                {
                    b.SkinVariant = TabSelectedButtonVariant;
                }
                else
                {
                    b.SkinVariant = TabUnselectedButtonVariant;
                }
                b.ApplySkin();
                b.UpdateChildRelativeSize();
            }
        }

        private void handleCanvasSkinChange()
        {

        }

        public override void UpdateChildRelativeSize()
        {
            base.UpdateChildRelativeSize();
            if (_buttonSizingRequired)
            {
                handleButtonSizing();
                _buttonSizingRequired = false;
            }
        }

        public override void Clear()
        {
            base.Clear();
            _tabFrame.Clear();
            _childFrame.Clear();
            AddChild(_tabFrame);
            AddChild(_childFrame);

            _tabButtons.Clear();
            _childCanvases.Clear();
        }
    }
}
