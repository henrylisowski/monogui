﻿using Microsoft.Xna.Framework;
using MonoGUI.Animations;
using MonoGUI.Manager;
using MonoGUI.Skinning;
using MonoGUI.Skinning.SkinElements;
using MonoGUI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace MonoGUI.ScreenItems.Containers
{
    public abstract class ContainerPanel : ScreenItem
    {
        //TODO include padding
        #region Skinned Values
        private SkinnedPropertyStateValue<AreaDefinition> _skinnedChildArea;
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> SkinnedChildArea
        {
            get { return _skinnedChildArea; }
            set { _skinnedChildArea = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }

        private List<IModifier> _childTransitionOnAnimations;
        [SkinnedProperty]
        public List<IModifier> ChildTransitionOnAnimations
        {
            get { return _childTransitionOnAnimations; }
            set { _childTransitionOnAnimations = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }

        private List<IModifier> _childTransitionOffAnimations;
        [SkinnedProperty]
        public List<IModifier> ChildTransitionOffAnimations
        {
            get { return _childTransitionOffAnimations; }
            set { _childTransitionOffAnimations = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }

        private float _childTransitionOnInitialDelay;
        [SkinnedProperty]
        public float ChildTransitionOnInitialDelay
        {
            get { return _childTransitionOnInitialDelay; }
            set { _childTransitionOnInitialDelay = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }

        private float _childTransitionOnStepDelay;
        [SkinnedProperty]
        public float ChildTransitionOnStepDelay
        {
            get { return _childTransitionOnStepDelay; }
            set { _childTransitionOnStepDelay = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }

        private float _childTransitionOffInitialDelay;
        [SkinnedProperty]
        public float ChildTransitionOffInitialDelay
        {
            get { return _childTransitionOffInitialDelay; }
            set { _childTransitionOffInitialDelay = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }

        private float _childTransitionOffStepDelay;
        [SkinnedProperty]
        public float ChildTransitionOffStepDelay
        {
            get { return _childTransitionOffStepDelay; }
            set { _childTransitionOffStepDelay = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }
        #endregion

        public RectangleFloat UseableChildArea
        {
            get
            {
                if (SkinnedChildArea != null && MinimumDimensions != null)
                {
                    Vector2 increase = RelativeDimensions - MinimumDimensions.Area.Dimensions;
                    return SkinnedChildArea.GetSkinElementByState(VisualState).UpdatedSize(increase, MinimumDimensions.Area.Dimensions);
                }
                return new RectangleFloat(0, 0, RelativeDimensions.X, RelativeDimensions.Y);
            }
        }

        public Vector2 DimensionBorderAroundChild
        {
            get
            {
                if (SkinnedChildArea != null && MinimumDimensions != null)
                {
                    return MinimumDimensions.Area.Dimensions - SkinnedChildArea.GetSkinElementByState(VisualState).Area.Dimensions;
                }
                return Vector2.Zero;
            }
        }

        public bool PassesTransitionAnimationsToChildren { get; set; }
        private List<ScreenItem> _childrenToRemove;
        public List<ScreenItem> UseableChildren;
        private Rectangle ClippingRectangle;

        public ContainerPanel(ScreenManager ScreenManager) : base(ScreenManager)
        {
            _childrenToRemove = new List<ScreenItem>();
            UseableChildren = new List<ScreenItem>();
            ClippingRectangle = new Rectangle(0, 0, 0, 0);
        }

        public abstract override void CalculateChildren();

        public override void HandleInput(Microsoft.Xna.Framework.GameTime gameTime, Manager.Input.InputManager inputManager)
        {
            //Only handle input if this is enabled
            if (IsEnabled)
            {
                IEnumerable<ScreenItem> Children = UseableChildren;
                //Input is handled separately based on whether or not there's a focussed item
                if (IsFocusWithin)
                {
                    //Find focussed item first, handle that, then handle rest of children
                    ScreenItem focussed = null;
                    foreach (ScreenItem Child in Children)
                    {
                        if (Child.IsFocused || Child.IsFocusWithin)
                        {
                            focussed = Child;
                            break;
                        }
                    }
                    if (focussed != null)
                    {
                        focussed.HandleInput(gameTime, inputManager);
                    }
                    for (int i = Children.Count() - 1; i >= 0; i-- )
                    {
                        ScreenItem Child = Children.ElementAt(i);
                        if (Child != focussed)
                        {
                            Child.HandleInput(gameTime, inputManager);
                        }
                    }
                }
                else
                {
                    for (int i = Children.Count() - 1; i >= 0; i--)
                    {
                        ScreenItem Child = Children.ElementAt(i);
                        Child.HandleInput(gameTime, inputManager);    
                    }
                }
                //Handle own input last
                base.HandleInput(gameTime, inputManager);
            }
        }

        protected override void VisualStateChanged()
        {//TODO this is meant to resize children if a childbox rectangle changes. Not sure if this is the best way
            //TODO This could possibly be a performance impacter
            base.VisualStateChanged();
            UpdateChildRelativeSize();
        }
        

        public override void UpdateVisualState(GameTime gameTime, Manager.Input.InputManager inputManager)
        {
            //Need to check for "MouseOnlyOver"
            //If no children have the mouse over, this container gets it.
            //If one does, it keeps it
            //If more than one does, remove it from all

            if (this.IsMouseOver)
            {
                int mouseOverCount = 0;
                foreach (ScreenItem Child in UseableChildren)
                {
                    if (Child.IsMouseOver)
                    {
                        mouseOverCount++;
                    }
                }
                if (mouseOverCount == 0)
                {
                    this.IsMouseDirectlyOver = true;
                }
                else
                {
                    this.IsMouseDirectlyOver = false;
                    if (mouseOverCount > 1)
                    {
                        foreach (ScreenItem Child in UseableChildren)
                        {
                            Child.IsMouseDirectlyOver = false;
                        }
                    }
                }
            }

            foreach (ScreenItem Child in UseableChildren)
            {
                Child.UpdateVisualState(gameTime, inputManager);
            }
            base.UpdateVisualState(gameTime, inputManager);
        }

        public override void Update(Microsoft.Xna.Framework.GameTime gameTime)
        {
            base.Update(gameTime);

            

            _childrenToRemove.Clear();
            for (int i = 0; i < UseableChildren.Count; i++)
            {
                UseableChildren[i].Update(gameTime);
                if (UseableChildren[i].IsFinished)
                {
                    if (UseableChildren[i].CurrentTransitionState == TransitionState.TransitionedOff)
                    {
                        _childrenToRemove.Add(UseableChildren[i]);
                    }
                    else if (UseableChildren[i].CurrentTransitionState != TransitionState.TransitioningOff)
                    {
                        UseableChildren[i].RequestTransitionOff();
                    }
                }

            }
            foreach (ScreenItem ToRemove in _childrenToRemove)
            {
                RemoveChild(ToRemove);
            }
            if (_childrenToRemove.Count > 0)
            {
                RequestResize();
            }
            updateTransitionStatesBasedOnChildren();
        }

        private void updateTransitionStatesBasedOnChildren()
        {
            bool transitionedOffValid = true;
            bool transitionedOnValid = true;
            for (int i = 0; i < UseableChildren.Count; i++ )
            {
                switch (UseableChildren[i].CurrentTransitionState)
                {
                    case TransitionState.Transitioned:
                        transitionedOffValid = false;
                        break;
                    case TransitionState.TransitionedOff:
                        transitionedOnValid = false;
                        break;
                    case TransitionState.TransitioningOn:
                        transitionedOffValid = false;
                        transitionedOnValid = false;
                        break;
                    case TransitionState.TransitioningOff:
                        transitionedOffValid = false;
                        transitionedOnValid = false;
                        break;
                }
            }
            if (!transitionedOnValid && CurrentTransitionState == TransitionState.Transitioned)
            {
                CurrentTransitionState = TransitionState.TransitioningOn;
            }
            if (!transitionedOffValid && CurrentTransitionState == TransitionState.TransitionedOff)
            {
                CurrentTransitionState = TransitionState.TransitioningOff;
            }
        }

        protected override void MidDraw(Microsoft.Xna.Framework.GameTime gameTime, MSpriteBatch.MatrixSpriteBatch spriteBatch)
        {
            base.MidDraw(gameTime, spriteBatch);
        }

        protected override void HandleChildrenDraw(GameTime gameTime, MSpriteBatch.MatrixSpriteBatch spriteBatch)
        {
            base.HandleChildrenDraw(gameTime, spriteBatch);
            for (int i = 0; i < UseableChildren.Count; i++ )
            {
                if (UseableChildren[i].IsEnabled)
                {
                    PreDrawChildren(gameTime, spriteBatch, UseableChildren[i]);
                    MidDrawChildren(gameTime, spriteBatch, UseableChildren[i]);
                    PostDrawChildren(gameTime, spriteBatch, UseableChildren[i]);
                }
            }
        }

        protected void MidDrawChildren(GameTime gameTime, MSpriteBatch.MatrixSpriteBatch spriteBatch, ScreenItem Child)
        {
            spriteBatch.PushMatrix();
            spriteBatch.LoadMatrix(Child.AbsoluteAnimatedTransformation);
            if (Child.Has3DTransformations)
            {
                spriteBatch.Push3DMatrix(Child.Transformation3DMatrix);
            }
            spriteBatch.PushColor(Child.AnimatedColor);
            Child.Draw(gameTime, spriteBatch);
            spriteBatch.PopColor();
            if (Child.Has3DTransformations)
            {
                spriteBatch.Pop3DMatrix();
            }
            spriteBatch.PopMatrix();
        }

        protected void PreDrawChildren(GameTime gameTime, MSpriteBatch.MatrixSpriteBatch spriteBatch, ScreenItem Child)
        {
            if (Child.DoParentClipping)
            {
                RectangleFloat useable = UseableChildArea;
                ClippingRectangle.X = (int)useable.X;
                ClippingRectangle.Y = (int)useable.Y;
                ClippingRectangle.Width = (int)useable.Width;
                ClippingRectangle.Height = (int)useable.Height;
                spriteBatch.PushClippingRectangle(ClippingRectangle, true);
            }
        }

        protected void PostDrawChildren(GameTime gameTime, MSpriteBatch.MatrixSpriteBatch spriteBatch, ScreenItem Child)
        {
            if (Child.DoParentClipping)
            {
                spriteBatch.PopClippingRectangle();
            }
        }

        public override void HandleTransitions()
        {
            base.HandleTransitions();
            List<ScreenItem> Children = UseableChildren;
            for (int i = 0; i < Children.Count; i++)
            {
                Children[i].HandleTransitions();
            }
        }

        public override void HandleTransitionOn(bool playAnimations)
        {
            base.HandleTransitionOn(playAnimations);
            IEnumerable<ScreenItem> Children = UseableChildren;
            float delay = ChildTransitionOnInitialDelay;
            float delayIncrease = ChildTransitionOnStepDelay;
            foreach (ScreenItem Child in Children)
            {
                Child.HandleTransitionOn(playAnimations);
                if (!Child.IgnoresParentTransitionAnimations)
                {
                    Child.ApplyChildTransitionAnimations(ChildTransitionOnAnimations, ref delay,
                        AnimationTypeKeys.ScreenTransitionOnAnimations, delayIncrease);
                    delay += ChildTransitionOnStepDelay;
                }
            }
        }

        public override void HandleTransitionOff(bool playAnimations)
        {
            base.HandleTransitionOff(playAnimations);
            IEnumerable<ScreenItem> Children = UseableChildren;
            float delay = ChildTransitionOffInitialDelay;
            float delayIncrease = ChildTransitionOffStepDelay;
            foreach (ScreenItem Child in Children)
            {
                Child.HandleTransitionOff(playAnimations);
                if (!Child.IgnoresParentTransitionAnimations)
                {
                    Child.ApplyChildTransitionAnimations(ChildTransitionOffAnimations, ref delay,
                        AnimationTypeKeys.ScreenTransitionOffAnimations, delayIncrease);
                    delay += ChildTransitionOffStepDelay;
                }
            }
        }

        public override void HandleTransitionBack(bool playAnimations)
        {
            base.HandleTransitionBack(playAnimations);
            IEnumerable<ScreenItem> Children = UseableChildren;
            float delay = ChildTransitionOffInitialDelay;
            float delayIncrease = ChildTransitionOffStepDelay;
            foreach (ScreenItem Child in Children)
            {
                Child.HandleTransitionBack(playAnimations);
                if (!Child.IgnoresParentTransitionAnimations)
                {
                    Child.ApplyChildTransitionAnimations(ChildTransitionOffAnimations, ref delay,
                        AnimationTypeKeys.ScreenTransitionOffAnimations, delayIncrease);
                    delay += ChildTransitionOffStepDelay;
                }
            }
        }

        public override void HandleApplySkin(bool forceApply)
        {//TODO I dislike how this is done, probably calculate _applySkinRequested based off children as well
            if (_applySkinRequested || forceApply)
            {
                base.HandleApplySkin(true);
            }
            foreach (ScreenItem Child in UseableChildren)
            {
                Child.HandleApplySkin(forceApply);
            }
        }

        public override void ApplyChildTransitionAnimations(List<IModifier> animations, ref float delay, string animationKey, float delayIncrease)
        {
            if (PassesTransitionAnimationsToChildren)
            {
                IEnumerable<ScreenItem> Children = UseableChildren;
                foreach (ScreenItem Child in Children)
                {
                    if (!Child.IgnoresParentTransitionAnimations)
                    {
                        Child.ApplyChildTransitionAnimations(animations, ref delay, animationKey, delayIncrease);
                    }
                }
            }
            else
            {
                base.ApplyChildTransitionAnimations(animations, ref delay, animationKey, delayIncrease);
            }
        }

        public override void UpdateTransformationMatrices()
        {
            base.UpdateTransformationMatrices();
            IEnumerable<ScreenItem> Children = UseableChildren;
            foreach (ScreenItem Child in Children)
            {
                Child.UpdateTransformationMatrices();
            }
        }

        public abstract void RemoveChild(ScreenItem toRemove);
        public abstract void Clear();
    }
}
