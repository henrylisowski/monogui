﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGUI.Manager;
using MonoGUI.MSpriteBatch;
using MonoGUI.Skinning;
using MonoGUI.Skinning.SkinElements;
using MonoGUI.Themes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.ScreenItems.Containers.CanvasContainer
{
    public class CanvasFrame : ContainerPanel
    {
        protected List<ScreenItem> Children;

        public CanvasFrame(ScreenManager ScreenManager)
            : base(ScreenManager)
        {
            Children = new List<ScreenItem>();
            this.SkinTargetWidget = SkinReferencingConstants.WidgetTargetCanvas;
        }

        public virtual void AddChild(ScreenItem child)
        {
            Children.Add(child);
            child.Parent = this;
            child.ApplySkin();
            child.Initialize();
            child.LoadContent(Manager.Content);
            RequestResize();
        }

        public override void UpdateRequestedSizeDueToChildResize()
        {
            foreach (ScreenItem Child in Children)
            {
                Child.UpdateRequestedSizeDueToChildResize();
            }
            UpdateRequestedWidth();
            UpdateRequestedHeight();
            RequestedPosition = PreferredPosition;
        }

        private void UpdateRequestedWidth()
        {
            switch (Width)
            {
                case ItemSizeOptions.Absolute:
                case ItemSizeOptions.FillParent:
                case ItemSizeOptions.Percentage:
                case ItemSizeOptions.Remaining:
                    RequestedDimensions = new Vector2(PreferredDimensions.X, RequestedDimensions.Y);
                    break;
                case ItemSizeOptions.WrapContent:
                    float w = 0f;
                    foreach (ScreenItem s in Children)
                    {
                        w = Math.Max(w, s.RequestedDimensions.X);
                    }
                    RequestedDimensions = new Vector2(w + DimensionBorderAroundChild.X, RequestedDimensions.Y);
                    break;
            }
        }

        private void UpdateRequestedHeight()
        {
            switch (Width)
            {
                case ItemSizeOptions.Absolute:
                case ItemSizeOptions.FillParent:
                case ItemSizeOptions.Percentage:
                case ItemSizeOptions.Remaining:
                    RequestedDimensions = new Vector2(RequestedDimensions.X, PreferredDimensions.Y);
                    break;
                case ItemSizeOptions.WrapContent:
                    float h = 0f;
                    foreach (ScreenItem s in Children)
                    {
                        h = Math.Max(h, s.RequestedDimensions.Y);
                    }
                    RequestedDimensions = new Vector2(RequestedDimensions.X, h + DimensionBorderAroundChild.Y);
                    break;
            }
        }

        public override void UpdateChildRelativeSize()
        {
            foreach (ScreenItem child in Children)
            {
                float currentX = 0f;
                float currentY = 0f;
                switch (child.HorizontalPositionType)
                {
                    case ItemPositionOptions.Absolute:
                        currentX = child.RequestedPosition.X;
                        break;
                    case ItemPositionOptions.Percentage:
                        currentX = child.RequestedPosition.X * UseableChildArea.Width;
                        break;
                }
                switch (child.VerticalPositionType)
                {
                    case ItemPositionOptions.Absolute:
                        currentY = child.RequestedPosition.Y;
                        break;
                    case ItemPositionOptions.Percentage:
                        currentY = child.RequestedPosition.Y * UseableChildArea.Height;
                        break;
                }
                Vector2 actualChildRequestedPosition = new Vector2(currentX, currentY);
                switch (child.Width)
                {
                    case ItemSizeOptions.Remaining:
                    case ItemSizeOptions.WrapContent:
                    case ItemSizeOptions.Absolute:
                        child.RelativeDimensions = new Vector2(child.RequestedDimensions.X, child.RelativeDimensions.Y);
                        switch (child.HorizontalAlignment)
                        {
                            case AlignH.Center:
                                child.RelativePosition = new Vector2(UseableChildArea.Width / 2 + 
                                    actualChildRequestedPosition.X - (child.RelativeDimensions.X / 2), 
                                    child.RelativePosition.Y) + new Vector2(UseableChildArea.Left, 0);
                                break;
                            case AlignH.Left:
                                child.RelativePosition = new Vector2(actualChildRequestedPosition.X, child.RelativePosition.Y) + 
                                    new Vector2(UseableChildArea.Left, 0);
                                break;
                            case AlignH.Right:
                                child.RelativePosition = new Vector2(UseableChildArea.Width - (actualChildRequestedPosition.X + child.RelativeDimensions.X),
                                    child.RelativePosition.Y) + new Vector2(UseableChildArea.Left, 0);
                                break;
                        }
                        break;
                    case ItemSizeOptions.FillParent:
                        child.RelativeDimensions = new Vector2(UseableChildArea.Width, child.RelativeDimensions.Y);
                        child.RelativePosition = new Vector2(UseableChildArea.X, child.RelativePosition.Y);
                        break;
                    case ItemSizeOptions.Percentage:
                        child.RelativeDimensions = new Vector2(child.RequestedDimensions.X * UseableChildArea.Width, child.RelativeDimensions.Y);
                        switch (child.HorizontalAlignment)
                        {
                            case AlignH.Center:
                                child.RelativePosition = new Vector2(UseableChildArea.Width / 2 + 
                                    actualChildRequestedPosition.X - (child.RelativeDimensions.X / 2),
                                    child.RelativePosition.Y) + new Vector2(UseableChildArea.Left, 0);
                                break;
                            case AlignH.Left:
                                child.RelativePosition = new Vector2(actualChildRequestedPosition.X, child.RelativePosition.Y) + 
                                    new Vector2(UseableChildArea.Left, 0);
                                break;
                            case AlignH.Right:
                                child.RelativePosition = new Vector2(UseableChildArea.Width - (actualChildRequestedPosition.X + child.RelativeDimensions.X),
                                    child.RelativePosition.Y) + new Vector2(UseableChildArea.Left, 0);
                                break;
                        }
                        break;
                }
                switch (child.Height)
                {
                    case ItemSizeOptions.Remaining:
                    case ItemSizeOptions.WrapContent:
                    case ItemSizeOptions.Absolute:
                        child.RelativeDimensions = new Vector2(child.RelativeDimensions.X, child.RequestedDimensions.Y);
                        switch (child.VerticalAlignment)
                        {
                            case AlignV.Center:
                                child.RelativePosition = new Vector2(child.RelativePosition.X,
                                    UseableChildArea.Height / 2 + (actualChildRequestedPosition.Y - (child.RelativeDimensions.Y / 2))) + 
                                    new Vector2(0, UseableChildArea.Top);
                                break;
                            case AlignV.Top:
                                child.RelativePosition = new Vector2(child.RelativePosition.X, actualChildRequestedPosition.Y) + 
                                    new Vector2(0, UseableChildArea.Top);
                                break;
                            case AlignV.Bottom:
                                child.RelativePosition = new Vector2(child.RelativePosition.X,
                                    UseableChildArea.Height - (actualChildRequestedPosition.Y + child.RelativeDimensions.Y)) +
                                    new Vector2(0, UseableChildArea.Top);
                                break;
                        }
                        break;
                    case ItemSizeOptions.FillParent:
                        child.RelativeDimensions = new Vector2(child.RelativeDimensions.X, UseableChildArea.Height);
                        child.RelativePosition = new Vector2(child.RelativePosition.X, UseableChildArea.Top);
                        break;
                    case ItemSizeOptions.Percentage:
                        child.RelativeDimensions = new Vector2(child.RelativeDimensions.X, child.RequestedDimensions.Y * UseableChildArea.Height);
                        switch (child.VerticalAlignment)
                        {
                            case AlignV.Center:
                                child.RelativePosition = new Vector2(child.RelativePosition.X,
                                    UseableChildArea.Height / 2 + (actualChildRequestedPosition.Y - (child.RelativeDimensions.Y / 2))) + 
                                    new Vector2(0, UseableChildArea.Top);
                                break;
                            case AlignV.Top:
                                child.RelativePosition = new Vector2(child.RelativePosition.X, actualChildRequestedPosition.Y) +
                                    new Vector2(0, UseableChildArea.Top);
                                break;
                            case AlignV.Bottom:
                                child.RelativePosition = new Vector2(child.RelativePosition.X,
                                    UseableChildArea.Height - (actualChildRequestedPosition.Y + child.RelativeDimensions.Y)) +
                                    new Vector2(0, UseableChildArea.Top);
                                break;
                        }
                        break;
                }
            }
            foreach (ScreenItem Child in Children)
            {
                Child.UpdateChildRelativeSize();
            }
        }

        public override void CalculateChildren()
        {
            UseableChildren.Clear();
            for (int i = 0; i < Children.Count; i++)
            {
                UseableChildren.Add(Children[i]);
                Children[i].CalculateChildren();
            }
        }

        public override void Clear()
        {
            Children.Clear();
        }

        public override void RemoveChild(ScreenItem toRemove)
        {
            Children.Remove(toRemove);
        }
    }
}
