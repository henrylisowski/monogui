﻿using Microsoft.Xna.Framework;
using MonoGUI.Manager;
using MonoGUI.Skinning;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.ScreenItems.Containers.StackContainer
{
    public class StackPanel : ContainerPanel
    {
        List<ScreenItem> Children;
        public bool Clip { get; set; }
        public StackDirection Direction;

        public StackPanel(ScreenManager ScreenManager)
            : base(ScreenManager)
        {
            Clip = false;
            Children = new List<ScreenItem>();
            Direction = StackDirection.Vertical;
            Width = ItemSizeOptions.WrapContent;
            Height = ItemSizeOptions.WrapContent;
            this.SkinTargetWidget = SkinReferencingConstants.WidgetTargetStack;
        }

        public void AddChild(ScreenItem child)
        {
            Children.Add(child);
            child.Parent = this;
            child.ApplySkin();
            child.Initialize();
            child.LoadContent(Manager.Content);
            RequestResize();
        }

        public override void UpdateRequestedSizeDueToChildResize()
        {
            foreach (ScreenItem Child in Children)
            {
                Child.UpdateRequestedSizeDueToChildResize();
            }

            UpdateWrapPreferredSizeHorizontal();
            UpdateWrapPreferredSizeVertical();
            if (Width == ItemSizeOptions.Absolute || Width == ItemSizeOptions.Percentage)
            {
                RequestedDimensions = new Vector2(PreferredDimensions.X, RequestedDimensions.Y);
            }
            if (Height == ItemSizeOptions.Absolute || Height == ItemSizeOptions.Percentage)
            {
                RequestedDimensions = new Vector2(RequestedDimensions.X, PreferredDimensions.Y);
            }
            if (Width == ItemSizeOptions.WrapContent)
            {
                RequestedDimensions += new Vector2(DimensionBorderAroundChild.X, 0);
            }
            if (Height == ItemSizeOptions.WrapContent)
            {
                RequestedDimensions += new Vector2(0, DimensionBorderAroundChild.Y);
            }
            RequestedPosition = PreferredPosition;
        }

        private void UpdateWrapPreferredSizeVertical()
        {
            if (Direction == StackDirection.Vertical && Height == ItemSizeOptions.WrapContent)
            {
                float totalHeight = 0;
                foreach (ScreenItem Child in Children)
                {
                    if (Child.Height != ItemSizeOptions.Percentage)
                    {
                        totalHeight += Child.RequestedDimensions.Y;
                    }

                }
                float totalPercentageHeight = 0f;
                foreach (ScreenItem Child in Children)
                {
                    if (Child.Height == ItemSizeOptions.Percentage)
                    {
                        totalPercentageHeight += Child.RequestedDimensions.Y;
                    }
                }
                float currentPercentHeight = 1 - totalPercentageHeight;
                float ratioHeight = totalPercentageHeight / currentPercentHeight;
                float additionalHeight = ratioHeight * totalHeight;
                totalHeight += additionalHeight;
                RequestedDimensions = new Vector2(RequestedDimensions.X, totalHeight);
            }
            if (Direction == StackDirection.Vertical && Width == ItemSizeOptions.WrapContent)
            {
                float totalWidth = 0;
                foreach (ScreenItem Child in Children)
                {
                    totalWidth = Math.Max(totalWidth, Child.RequestedDimensions.X);
                }
                RequestedDimensions = new Vector2(totalWidth, RequestedDimensions.Y);
            }
        }

        private void UpdateWrapPreferredSizeHorizontal()
        {
            if (Direction == StackDirection.Horizontal && Width == ItemSizeOptions.WrapContent)
            {
                float totalWidth = 0;
                foreach (ScreenItem Child in Children)
                {
                    if (Child.Width != ItemSizeOptions.Percentage)
                    {
                        totalWidth += Child.RequestedDimensions.X;
                    }

                }
                float totalPercentageWidth = 0f;
                foreach (ScreenItem Child in Children)
                {
                    if (Child.Width == ItemSizeOptions.Percentage)
                    {
                        totalPercentageWidth += Child.RequestedDimensions.X;
                    }
                }
                float currentPercentWidth = 1 - totalPercentageWidth;
                float ratioWidth = totalPercentageWidth / currentPercentWidth;
                float additionalWidth = ratioWidth * totalWidth;
                totalWidth += additionalWidth;
                RequestedDimensions = new Vector2(totalWidth, RequestedDimensions.Y);
            }
            if (Direction == StackDirection.Horizontal && Height == ItemSizeOptions.WrapContent)
            {
                float totalHeight = 0;
                foreach (ScreenItem Child in Children)
                {
                    totalHeight = Math.Max(totalHeight, Child.RequestedDimensions.Y);
                }
                RequestedDimensions = new Vector2(RequestedDimensions.X, totalHeight);
            }
            
        }

        public override void UpdateChildRelativeSize()
        {
            //To assign values to Children:
            //First, set their proper "Not extended value" (If horizontal aligned, the height value)
            //Apply this accordingly (if fill, set to height of parent, if not, set it to preferred, or height of value if preferred is larger
            //While doing this, set their position according to orientation
            //Then go along the length attribute, give proper size first to absolute, then to wrap. If you run out of space, start giving zeros!
            //From this point, divide remaining space to Fill/Remaining. If there are both types, give all to fill and 0 to remaining
            //Then go through list and give positions
            switch (Direction)
            {
                case StackDirection.Vertical:
                    UpdateChildrenVertical();
                    break;

                case StackDirection.Horizontal:
                    UpdateChildrenHorizontal();
                    break;
            }

            foreach (ScreenItem Child in Children)
            {
                Child.UpdateChildRelativeSize();
            }
        }

        private void UpdateChildrenVertical()
        {
            Dictionary<ItemSizeOptions, List<ScreenItem>> childrenByWidthType = new Dictionary<ItemSizeOptions, List<ScreenItem>>();
            Dictionary<ItemSizeOptions, List<ScreenItem>> childrenByHeightType = new Dictionary<ItemSizeOptions, List<ScreenItem>>();
            foreach (ScreenItem Child in Children)
            {
                if (!childrenByWidthType.ContainsKey(Child.Width))
                {
                    childrenByWidthType.Add(Child.Width, new List<ScreenItem>());
                }
                childrenByWidthType[Child.Width].Add(Child);

                if (!childrenByHeightType.ContainsKey(Child.Height))
                {
                    childrenByHeightType.Add(Child.Height, new List<ScreenItem>());
                }
                childrenByHeightType[Child.Height].Add(Child);
            }

            float remainingHeight = this.UseableChildArea.Height;
            if (childrenByHeightType.ContainsKey(ItemSizeOptions.Absolute))
            {
                foreach (ScreenItem child in childrenByHeightType[ItemSizeOptions.Absolute])
                {
                    child.RelativeDimensions = new Vector2(Math.Min(
                        child.RequestedDimensions.X, this.UseableChildArea.Width),
                        Math.Min(remainingHeight, child.RequestedDimensions.Y));
                    remainingHeight -= child.RelativeDimensions.Y;
                }
            }
            if (childrenByHeightType.ContainsKey(ItemSizeOptions.WrapContent))
            {
                foreach (ScreenItem child in childrenByHeightType[ItemSizeOptions.WrapContent])
                {
                    child.RelativeDimensions = new Vector2(Math.Min(
                        child.RequestedDimensions.X, this.UseableChildArea.Width),
                        Math.Min(remainingHeight, child.RequestedDimensions.Y));
                    remainingHeight -= child.RelativeDimensions.Y;
                }
            }
            if (childrenByHeightType.ContainsKey(ItemSizeOptions.Percentage))
            {
                foreach (ScreenItem child in childrenByHeightType[ItemSizeOptions.Percentage])
                {
                    child.RelativeDimensions = new Vector2(Math.Min(
                        child.RequestedDimensions.X, this.UseableChildArea.Width),
                        Math.Min(remainingHeight, child.RequestedDimensions.Y * this.UseableChildArea.Height));
                    remainingHeight -= child.RelativeDimensions.Y;
                }
            }
            if (childrenByHeightType.ContainsKey(ItemSizeOptions.FillParent) &&
                childrenByHeightType[ItemSizeOptions.FillParent] != null &&
                childrenByHeightType[ItemSizeOptions.FillParent].Count > 0)
            {
                float heightPer = remainingHeight / childrenByHeightType[ItemSizeOptions.FillParent].Count;
                foreach (ScreenItem child in childrenByHeightType[ItemSizeOptions.FillParent])
                {
                    child.RelativeDimensions = new Vector2(Math.Min(
                        child.RequestedDimensions.X, this.UseableChildArea.Width),
                        heightPer);
                    remainingHeight -= child.RelativeDimensions.Y;
                }
            }
            if (childrenByHeightType.ContainsKey(ItemSizeOptions.Remaining) &&
                childrenByHeightType[ItemSizeOptions.Remaining] != null &&
                childrenByHeightType[ItemSizeOptions.Remaining].Count > 0)
            {
                float heightPer = remainingHeight / childrenByHeightType[ItemSizeOptions.Remaining].Count;
                foreach (ScreenItem child in childrenByHeightType[ItemSizeOptions.Remaining])
                {
                    child.RelativeDimensions = new Vector2(Math.Min(
                        child.RequestedDimensions.X, this.UseableChildArea.Width),
                        heightPer);
                    remainingHeight -= child.RelativeDimensions.Y;
                }
            }
            float runningVerticalPosition = UseableChildArea.Top;
            foreach (ScreenItem child in Children)
            {
                child.RelativePosition = new Vector2(child.RelativePosition.X, runningVerticalPosition);
                runningVerticalPosition += child.RelativeDimensions.Y;
            }
            foreach (ScreenItem child in Children)
            {
                float width = 0f;
                switch (child.Width)
                {
                    case ItemSizeOptions.Absolute:
                    case ItemSizeOptions.WrapContent:
                        width = Math.Min(UseableChildArea.Width, child.RequestedDimensions.X);
                        break;
                    case ItemSizeOptions.FillParent:
                    case ItemSizeOptions.Remaining:
                        width = UseableChildArea.Width;
                        break;
                    case ItemSizeOptions.Percentage:
                        width = UseableChildArea.Width * child.RequestedDimensions.X;
                        break;
                }
                child.RelativeDimensions = new Vector2(width, child.RelativeDimensions.Y);
                float actualRequestedXPosition = child.RequestedPosition.X;
                if (HorizontalPositionType == ItemPositionOptions.Percentage)
                {
                    actualRequestedXPosition *= this.RelativeDimensions.X;
                }
                switch (child.HorizontalAlignment)
                {
                    case AlignH.Center:
                        float positionC = (UseableChildArea.Width - child.RelativeDimensions.X) / 2f + actualRequestedXPosition;
                        if (positionC + child.RelativeDimensions.X > UseableChildArea.Width)
                        {
                            positionC = UseableChildArea.Width - child.RelativeDimensions.X;
                        }
                        if (positionC < 0)
                        {
                            positionC = 0;
                        }
                        child.RelativePosition = new Vector2(positionC + UseableChildArea.Left, child.RelativePosition.Y);
                        break;
                    case AlignH.Left:
                        float positionL = actualRequestedXPosition;
                        if (positionL + child.RelativeDimensions.X > UseableChildArea.Width)
                        {
                            positionL = UseableChildArea.Width - child.RelativeDimensions.X;
                        }
                        if (positionL < 0)
                        {
                            positionL = 0;
                        }
                        child.RelativePosition = new Vector2(positionL + UseableChildArea.Left, child.RelativePosition.Y);
                        break;
                    case AlignH.Right:
                        float positionR = UseableChildArea.Width - (child.RelativeDimensions.X + actualRequestedXPosition);
                        if (positionR + child.RelativeDimensions.X > UseableChildArea.Width)
                        {
                            positionR = UseableChildArea.Width;
                        }
                        if (positionR < 0)
                        {
                            positionR = 0;
                        }
                        child.RelativePosition = new Vector2(positionR + UseableChildArea.Left, child.RelativePosition.Y);
                        break;
                }
            }

        }

        private void UpdateChildrenHorizontal()
        {
            Dictionary<ItemSizeOptions, List<ScreenItem>> childrenByWidthType = new Dictionary<ItemSizeOptions, List<ScreenItem>>();
            Dictionary<ItemSizeOptions, List<ScreenItem>> childrenByHeightType = new Dictionary<ItemSizeOptions, List<ScreenItem>>();
            foreach (ScreenItem Child in Children)
            {
                if (!childrenByWidthType.ContainsKey(Child.Width))
                {
                    childrenByWidthType.Add(Child.Width, new List<ScreenItem>());
                }
                childrenByWidthType[Child.Width].Add(Child);

                if (!childrenByHeightType.ContainsKey(Child.Height))
                {
                    childrenByHeightType.Add(Child.Height, new List<ScreenItem>());
                }
                childrenByHeightType[Child.Height].Add(Child);
            }

            float remainingWidth = this.UseableChildArea.Width;
            if (childrenByWidthType.ContainsKey(ItemSizeOptions.Absolute))
            {
                foreach (ScreenItem child in childrenByWidthType[ItemSizeOptions.Absolute])
                {
                    child.RelativeDimensions = new Vector2(Math.Min(remainingWidth, child.RequestedDimensions.X),
                        Math.Min(child.RequestedDimensions.Y, UseableChildArea.Height));
                    remainingWidth -= child.RelativeDimensions.X;
                }
            }
            if (childrenByWidthType.ContainsKey(ItemSizeOptions.WrapContent))
            {
                foreach (ScreenItem child in childrenByWidthType[ItemSizeOptions.WrapContent])
                {
                    child.RelativeDimensions = new Vector2(Math.Min(remainingWidth, child.RequestedDimensions.X),
                        Math.Min(child.RequestedDimensions.Y, UseableChildArea.Height));
                    remainingWidth -= child.RelativeDimensions.X;
                }
            }
            if (childrenByWidthType.ContainsKey(ItemSizeOptions.Percentage))
            {
                foreach (ScreenItem child in childrenByWidthType[ItemSizeOptions.Percentage])
                {
                    child.RelativeDimensions = new Vector2(Math.Min(remainingWidth, child.RequestedDimensions.X * UseableChildArea.Width),
                        Math.Min(child.RequestedDimensions.Y, UseableChildArea.Height));
                    remainingWidth -= child.RelativeDimensions.X;
                }
            }
            if (childrenByWidthType.ContainsKey(ItemSizeOptions.FillParent) &&
                childrenByWidthType[ItemSizeOptions.FillParent] != null &&
                childrenByWidthType[ItemSizeOptions.FillParent].Count > 0)
            {
                float widthPer = remainingWidth / childrenByWidthType[ItemSizeOptions.FillParent].Count;
                foreach (ScreenItem child in childrenByWidthType[ItemSizeOptions.FillParent])
                {
                    child.RelativeDimensions = new Vector2(widthPer,
                        Math.Min(child.RequestedDimensions.Y, UseableChildArea.Height));
                    remainingWidth -= child.RelativeDimensions.X;
                }
            }
            if (childrenByWidthType.ContainsKey(ItemSizeOptions.Remaining) &&
                childrenByWidthType[ItemSizeOptions.Remaining] != null &&
                childrenByWidthType[ItemSizeOptions.Remaining].Count > 0)
            {
                float widthPer = remainingWidth / childrenByWidthType[ItemSizeOptions.Remaining].Count;
                foreach (ScreenItem child in childrenByWidthType[ItemSizeOptions.Remaining])
                {
                    child.RelativeDimensions = new Vector2(widthPer,
                        Math.Min(child.RequestedDimensions.Y, UseableChildArea.Height));
                    remainingWidth -= child.RelativeDimensions.X;
                }
            }
            float runningHorizontalPosition = UseableChildArea.Left;
            foreach (ScreenItem child in Children)
            {
                child.RelativePosition = new Vector2(runningHorizontalPosition, child.RelativePosition.Y);
                runningHorizontalPosition += child.RelativeDimensions.X;
            }
            foreach (ScreenItem child in Children)
            {
                float height = 0f;
                switch (child.Height)
                {
                    case ItemSizeOptions.Absolute:
                    case ItemSizeOptions.WrapContent:
                        height = Math.Min(UseableChildArea.Height, child.RequestedDimensions.Y);
                        break;
                    case ItemSizeOptions.FillParent:
                    case ItemSizeOptions.Remaining:
                        height = UseableChildArea.Height;
                        break;
                    case ItemSizeOptions.Percentage:
                        height = UseableChildArea.Height * child.RequestedDimensions.Y;
                        break;
                }
                child.RelativeDimensions = new Vector2(child.RelativeDimensions.X, height);
                float actualRequestedYPosition = child.RequestedPosition.Y;
                if (VerticalPositionType == ItemPositionOptions.Percentage)
                {
                    actualRequestedYPosition *= this.RelativeDimensions.Y;
                }
                switch (child.VerticalAlignment)
                {
                    case AlignV.Center:
                        float positionC = (UseableChildArea.Height - child.RelativeDimensions.Y) / 2f + actualRequestedYPosition;
                        if (positionC + child.RelativeDimensions.Y > UseableChildArea.Height)
                        {
                            positionC = UseableChildArea.Height - child.RelativeDimensions.Y;
                        }
                        if (positionC < 0)
                        {
                            positionC = 0;
                        }
                        child.RelativePosition = new Vector2(child.RelativePosition.X, positionC + UseableChildArea.Top);
                        break;
                    case AlignV.Top:
                        float positionL = actualRequestedYPosition;
                        if (positionL + child.RelativeDimensions.Y > UseableChildArea.Height)
                        {
                            positionL = UseableChildArea.Height - child.RelativeDimensions.Y;
                        }
                        if (positionL < 0)
                        {
                            positionL = 0;
                        }
                        child.RelativePosition = new Vector2(child.RelativePosition.X, positionL + UseableChildArea.Top);
                        break;
                    case AlignV.Bottom:
                        float positionR = UseableChildArea.Height - (child.RelativeDimensions.Y + actualRequestedYPosition);
                        if (positionR + child.RelativeDimensions.Y > UseableChildArea.Height)
                        {
                            positionR = UseableChildArea.Height;
                        }
                        if (positionR < 0)
                        {
                            positionR = 0;
                        }
                        child.RelativePosition = new Vector2(child.RelativePosition.X, positionR + UseableChildArea.Top);
                        break;
                }
            }
        }

        public override void CalculateChildren()
        {
            UseableChildren.Clear();
            for (int i = 0; i < Children.Count; i++)
            {
                UseableChildren.Add(Children[i]);
                Children[i].CalculateChildren();
            }
        }

        public override void Clear()
        {
            Children.Clear();
        }

        public override void RemoveChild(ScreenItem toRemove)
        {
            Children.Remove(toRemove);
        }
    }
}
