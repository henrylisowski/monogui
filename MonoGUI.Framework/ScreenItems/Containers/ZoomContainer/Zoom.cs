﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.ScreenItems.Containers.ZoomContainer
{
    public class Zoom
    {
        public ZoomType Type { get; set; }

        public Zoom()
        {
            Type = Zoom.ZoomType.FillContain;
        }

        public enum ZoomType{FillStretch, FillContain, FillOverflow}
    }
}
