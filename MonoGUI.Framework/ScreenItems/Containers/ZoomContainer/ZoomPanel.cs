﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGUI.Manager;
using MonoGUI.Skinning;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.ScreenItems.Containers.ZoomContainer
{
    public class ZoomPanel : ContainerPanel
    {
        ScreenItem Child;
        public bool Clip { get; set; }

        public ZoomPanel(ScreenManager ScreenManager)
            : base(ScreenManager)
        {
            Clip = false;
            this.SkinTargetWidget = SkinReferencingConstants.WidgetTargetZoom;
        }

        public void SetChild(ScreenItem child)
        {
            Child = child;
            child.Parent = this;
            child.Initialize();
            child.LoadContent(Manager.Content);
            
            RequestResize();
        }

        public override void UpdateRequestedSizeDueToChildResize()
        {
            //This is determined by either a pre-set Preferred, or by the container settings
            if (Child != null)
            {
                Child.UpdateRequestedSizeDueToChildResize();
            }
            this.RequestedDimensions = this.PreferredDimensions + DimensionBorderAroundChild;
            this.RequestedPosition = this.PreferredPosition;
        }

        public override void UpdateChildRelativeSize()
        {
            if (Child != null)
            {
                float zoomX = UseableChildArea.Width / Child.RequestedDimensions.X;
                float zoomY = UseableChildArea.Height / Child.RequestedDimensions.Y;
                Child.RelativeDimensions = Child.RequestedDimensions;
                Vector2 center = (UseableChildArea.Dimensions / 2f) + UseableChildArea.Position;
                switch (Child.Zoom.Type)
                {
                    case ZoomContainer.Zoom.ZoomType.FillContain:
                        float zoomContain = Math.Min(zoomX, zoomY);
                        Child.RelativePosition = center - (Child.RelativeDimensions * zoomContain / 2);
                        Child.RelativeScale =  new Vector3(zoomContain, zoomContain, 1f);
                    //Matrix.CreateScale(zoomContain, zoomContain, 0f) * Matrix.CreateTranslation(new Vector3(Child.RelativePosition, 0f));
                        break;
                    case ZoomContainer.Zoom.ZoomType.FillOverflow:
                        float zoomOverflow = Math.Max(zoomX, zoomY);
                        Child.RelativePosition = center - (Child.RelativeDimensions * zoomOverflow / 2);
                        Child.RelativeScale =  new Vector3(zoomOverflow, zoomOverflow, 1f);
                    //Matrix.CreateScale(zoomOverflow, zoomOverflow, 0f) * Matrix.CreateTranslation(new Vector3(Child.RelativePosition, 0f));
                        break;
                    case ZoomContainer.Zoom.ZoomType.FillStretch:
                        Child.RelativeScale = new Vector3(zoomX, zoomY, 1f);
                            //Matrix.CreateScale(zoomX, zoomY, 0f);
                        Child.RelativePosition = new Vector2(0, 0);
                        break;
                }
                Child.UpdateChildRelativeSize();
            }

        }

        public override void CalculateChildren()
        {
            UseableChildren.Clear();
            if(Child != null)
            {
                UseableChildren.Add(Child);
                Child.CalculateChildren();
            }
        }

        public override void Clear()
        {
            Child = null;
        }

        public override void RemoveChild(ScreenItem toRemove)
        {
            if (Child == toRemove)
            {
                Child = null;
            }
        }
    }
}
