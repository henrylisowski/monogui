﻿using Microsoft.Xna.Framework;
using MonoGUI.Manager;
using MonoGUI.ScreenItems.CompositionElements.Implementations;
using MonoGUI.ScreenItems.Containers.CanvasContainer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.ScreenItems.Containers
{
    public class MonoGUIGameWindow : CanvasFrame
    {
        private bool _needsResize;
        public MonoGUIGameWindow(ScreenManager ScreenManager)
            : base(ScreenManager)
        {
            this.Parent = new RootParentNode(ScreenManager, this);
            _needsResize = false;
        }

        public override void UpdateChildRelativeSize()
        {
            this.RelativeDimensions = Manager.ScreenDimensions;
            base.UpdateChildRelativeSize();
        }

        public override void RequestResize()
        {
            _needsResize = true;
        }

        public void HandleResizeRequested()
        {
            if (_needsResize)
            {
                UpdateRequestedSizeDueToChildResize();
                UpdateChildRelativeSize();
                _needsResize = false;
            }
        }
        
    }
}
