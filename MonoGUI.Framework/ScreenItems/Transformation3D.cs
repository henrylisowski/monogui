﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.ScreenItems
{
    public class Transformation3D
    {
        public Vector2 Origin { get; private set; }
        public float RotateX { get; private set; }
        public float RotateY { get; private set; }

        public Transformation3D(Vector2 Origin, float RotateX, float RotateY)
        {
            this.Origin = Origin;
            this.RotateX = RotateX;
            this.RotateY = RotateY;
        }
    }
}
