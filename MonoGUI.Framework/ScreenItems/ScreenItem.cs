﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using MonoGUI.Animations;
using MonoGUI.EventHandling;
using MonoGUI.EventHandling.MouseEvents;
using MonoGUI.Manager;
using MonoGUI.Manager.Input;
using MonoGUI.MSpriteBatch;
using MonoGUI.ScreenItems.Containers.CanvasContainer;
using MonoGUI.ScreenItems.Containers.ZoomContainer;
using MonoGUI.Skinning;
using MonoGUI.Skinning.SkinElements;
using MonoGUI.Themes;
using MonoGUI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace MonoGUI.ScreenItems
{
    public abstract partial class ScreenItem : ParentNode
    {
        protected ScreenManager Manager;
        protected float lifetime;
        
        
        /// <summary>
        /// Whether the ScreenComponent is finished and signalling to be removed
        /// </summary>
        public bool IsFinished { get; protected set; }

        public Zoom Zoom { get; private set; }

        public ItemSizeOptions Width { get; set; }
        public ItemSizeOptions Height { get; set; }
        public AlignH HorizontalAlignment { get; set; }
        public AlignV VerticalAlignment { get; set; }
        public ItemPositionOptions HorizontalPositionType { get; set; }
        public ItemPositionOptions VerticalPositionType { get; set; }

        public bool DoSelfClipping { get; set; }

        public string SkinVariant { get; set; }
        public string SkinTargetWidget { get; set; }

        public bool DoParentClipping { get; set; }
        public bool IgnoresParentTransitionAnimations { get; set; }

        public AnimationManager AnimationManager { get; private set; }
        private ScreenItemVisualStates _visualState;
        public ScreenItemVisualStates VisualState
        {
            get { return _visualState; }
            set
            {
                bool doUpdate = false;
                if (_visualState != value) doUpdate = true;
                _visualState = value;
                if (doUpdate) VisualStateChanged();
            }
        }

        private bool _transitionOnRequested, _transitionOffRequested, _transitionBackRequested;
        private bool _playTransitionOnAnimations, _playTransitionOffAnimations, _playTransitionBackAnimations;
        protected bool _applySkinRequested;

        public enum TransitionState { Transitioned, TransitioningOn, TransitioningOff, TransitionedOff }
        public TransitionState CurrentTransitionState;

        #region Skinned Values
        private SkinnedPropertyStateValue<ISkinElement> _backgroundSkin;
        [SkinnedProperty]
        public SkinnedPropertyStateValue<ISkinElement> BackgroundSkin
        {
            get { return _backgroundSkin; }
            set { _backgroundSkin = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }

        private AreaDefinition _minimumDimensions;
        [SkinnedProperty]
        public AreaDefinition MinimumDimensions
        {
            get { return _minimumDimensions; }
            set { _minimumDimensions = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }

        private Color _drawColor;
        [SkinnedProperty]
        public Color DrawColor
        {
            get { return _drawColor; }
            set { _drawColor = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); CalculateAnimatedColor(); }
        }

        private SkinnedPropertyStateValue<List<IModifier>> _stateTransitionAnimations;
        [SkinnedProperty]
        public SkinnedPropertyStateValue<List<IModifier>> StateTransitionAnimations
        {
            get { return _stateTransitionAnimations; }
            set { _stateTransitionAnimations = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }

        private List<IModifier> _transitionOnAnimations;
        [SkinnedProperty]
        public List<IModifier> TransitionOnAnimations
        {
            get { return _transitionOnAnimations; }
            set { _transitionOnAnimations = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }

        private List<IModifier> _transitionOffAnimations;
        [SkinnedProperty]
        public List<IModifier> TransitionOffAnimations
        {
            get { return _transitionOffAnimations; }
            set { _transitionOffAnimations = value; SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name); }
        }

        private List<Animation> _eventFireAnimations;
        [SkinnedProperty]
        public List<Animation> EventFireAnimations
        {
            get { return _eventFireAnimations; }
            set
            {
                HandleEventAnimationAssignment(_eventFireAnimations, "EventFireAnimations", value, this);
                _eventFireAnimations = value;
                SkinnedPropertyOverrides.Add(MethodBase.GetCurrentMethod().Name);
            }
        }

        public HashSet<string> SkinnedPropertyOverrides;
        #endregion

        private Color _animatedColor;
        public Color AnimatedColor
        {
            get
            {
                return _animatedColor;
            }
        }

        public void CalculateAnimatedColor()
        {
            ColorUtils.Multiply(DrawColor, AnimationManager.TotalTweenedColors, ref _animatedColor);
        }

        private Dictionary<string, List<Delegate>> _eventAssignedAnimations;
        protected List<DrawCommand> drawCommands;
        

        public Vector2 SizeIncreaseFromMinimumDimensions
        {
            get { if (MinimumDimensions == null) return Vector2.Zero; else return RelativeDimensions - MinimumDimensions.Area.Dimensions; }
        }

        public ScreenItem(ScreenManager ScreenManager)
        {
            this.Manager = ScreenManager;
            this.Parent = new MonoGUI.ScreenItems.CompositionElements.Implementations.NullParentNode(ScreenManager);
            lifetime = 0f;
            SkinVariant = SkinReferencingConstants.SkinVariantDefault;
            RelativeScale = new Vector3(1, 1, 1);
            _additionalTransformations = new List<Matrix>();
            _3dTransformations = new List<Transformation3D>();

            SkinnedPropertyOverrides = new HashSet<string>();

            Width = ItemSizeOptions.Absolute;
            Height = ItemSizeOptions.Absolute;
            HorizontalAlignment = AlignH.Left;
            VerticalAlignment = AlignV.Top;
            IsFinished = false;
            IsEnabled = true;

            CalculatesOwnHitState = true;
            DoParentClipping = false;

            Zoom = new Zoom();

            _eventAssignedAnimations = new Dictionary<string, List<Delegate>>();
            drawCommands = new List<DrawCommand>();
            AnimationManager = new Animations.AnimationManager();
            DrawColor = Color.White;
            SkinnedPropertyOverrides.Remove("set_DrawColor");

            
            VisualState = ScreenItemVisualStates.Default;
            InitializeInputHandler();
        }

        public virtual void Initialize()
        {

        }

        public virtual void LoadContent(ContentManager Content)
        {

        }

        public virtual void CalculateChildren()
        {

        }

        public virtual void Update(GameTime gameTime)
        {
            lifetime += (float)gameTime.ElapsedGameTime.TotalSeconds;
            AnimationManager.Update(gameTime, this.RelativeDimensions);
            if (AnimationManager.AnimationChanged)
            {
                UpdateTransformationMatrices();
                CalculateAnimatedColor();
            }

            updateTransitionStates();
        }

        protected virtual void updateTransitionStates()
        {
            switch (CurrentTransitionState)
            {
                case TransitionState.TransitioningOn:
                    List<IModifier> onAnimations = AnimationManager.GetModifiersByKey(AnimationTypeKeys.ScreenTransitionOnAnimations);
                    if (onAnimations.Count == 0)
                    {
                        CurrentTransitionState = TransitionState.Transitioned;
                    }
                    else
                    {
                        bool isFinished = true;
                        foreach (IModifier mod in onAnimations)
                        {
                            isFinished &= mod.IsFinished;
                        }
                        if (isFinished)
                        {
                            foreach (IModifier mod in onAnimations)
                            {
                                AnimationManager.RemoveModifier(mod);
                            }
                            CurrentTransitionState = TransitionState.Transitioned;
                        }
                    }
                    break;
                case TransitionState.TransitioningOff:
                    List<IModifier> offAnimations = AnimationManager.GetModifiersByKey(AnimationTypeKeys.ScreenTransitionOffAnimations);
                    if (offAnimations.Count == 0)
                    {
                        CurrentTransitionState = TransitionState.TransitionedOff;
                    }
                    else
                    {
                        bool isOffFinished = true;
                        foreach (IModifier mod in offAnimations)
                        {
                            isOffFinished &= mod.IsFinished;
                        }
                        if (isOffFinished)
                        {
                            foreach (IModifier mod in offAnimations)
                            {
                                AnimationManager.RemoveModifier(mod);
                            }
                            CurrentTransitionState = TransitionState.TransitionedOff;
                        }
                    }
                    break;
            }
        }

        public void Draw(GameTime gameTime, MatrixSpriteBatch spriteBatch)
        {
            PreDraw(gameTime, spriteBatch);
            MidDraw(gameTime, spriteBatch);
            HandleChildrenDraw(gameTime, spriteBatch);
            PostDraw(gameTime, spriteBatch);
        }

        protected virtual void PreDraw(GameTime gameTime, MatrixSpriteBatch spriteBatch)
        {
            if (DoSelfClipping)
            {
                RectangleFloat useable = new RectangleFloat(0, 0, RelativeDimensions.X, RelativeDimensions.Y);
                Rectangle ClippingRectangle = new Rectangle();
                ClippingRectangle.X = (int)useable.X;
                ClippingRectangle.Y = (int)useable.Y;
                ClippingRectangle.Width = (int)useable.Width;
                ClippingRectangle.Height = (int)useable.Height;
                spriteBatch.PushClippingRectangle(ClippingRectangle, true);
            }
        }

        protected virtual void MidDraw(GameTime gameTime, MatrixSpriteBatch spriteBatch)
        {
            if (BackgroundSkin != null)
            {
                ISkinElement backgroundElement = BackgroundSkin.GetSkinElementByState(VisualState);
                drawCommands.Clear();
                backgroundElement.GetDrawCommands(this.RelativeDimensions, lifetime, ref drawCommands);
                foreach (DrawCommand cmd in drawCommands)
                {
                    spriteBatch.Draw(cmd.SourceImage, cmd.DestinationRectangle.Position, cmd.SourceRectangle, 
                        AnimatedColor, 0f, Vector2.Zero, cmd.Scale, cmd.Effects, 0f);
                }
            }
            
        }

        protected virtual void PostDraw(GameTime gameTime, MatrixSpriteBatch spriteBatch)
        {
            if (DoSelfClipping)
            {
                spriteBatch.PopClippingRectangle();
            }
        }

        protected virtual void HandleChildrenDraw(GameTime gameTime, MatrixSpriteBatch spriteBatch)
        {

        }

        public virtual void HandleTransitions()
        {
            if (_transitionOnRequested)
            {
                HandleTransitionOn(_playTransitionOnAnimations);
            }
            if (_transitionOffRequested)
            {
                HandleTransitionOff(_playTransitionOffAnimations);
            }
            if (_transitionBackRequested)
            {
                HandleTransitionBack(_playTransitionBackAnimations);
            }
        }

        public void RequestTransitionOn(bool playAnimations)
        {
            _transitionOnRequested = true;
            _transitionOffRequested = false;
            _transitionBackRequested = false;
            _playTransitionOnAnimations = playAnimations;
        }

        public void RequestTransitionOn()
        {
            RequestTransitionOn(true);
        }

        public void RequestTransitionOff(bool playAnimations)
        {
            _transitionOffRequested = true;
            _transitionBackRequested = false;
            _transitionOnRequested = false;
            _playTransitionOffAnimations = playAnimations;
        }

        public void RequestTransitionOff()
        {
            RequestTransitionOff(true);
        }

        public void RequestTransitionBack(bool playAnimations)
        {
            _transitionBackRequested = true;
            _transitionOnRequested = false;
            _transitionOffRequested = false;
            _playTransitionBackAnimations = playAnimations;
        }

        public void RequestTransitionBack()
        {
            RequestTransitionBack(true);
        }

        public virtual void HandleTransitionBack(bool playAnimations)
        {
            CurrentTransitionState = TransitionState.TransitioningOff;
            if (playAnimations)
            {
                RemoveAnimations(AnimationTypeKeys.ScreenTransitionOffAnimations);
                RemoveAnimations(AnimationTypeKeys.ScreenTransitionOnAnimations);
                ApplyAnimations(TransitionOffAnimations, 0f, AnimationTypeKeys.ScreenTransitionOffAnimations);
            }
            _transitionBackRequested = false;
            _playTransitionBackAnimations = false;
        }

        public virtual void HandleTransitionOn(bool playAnimations)
        {
            CurrentTransitionState = TransitionState.TransitioningOn;
            if (playAnimations)
            {
                RemoveAnimations(AnimationTypeKeys.ScreenTransitionOffAnimations);
                RemoveAnimations(AnimationTypeKeys.ScreenTransitionOnAnimations);
                ApplyAnimations(TransitionOnAnimations, 0f, AnimationTypeKeys.ScreenTransitionOnAnimations);
            }
            _transitionOnRequested = false;
            _playTransitionOnAnimations = false;
        }

        public virtual void HandleTransitionOff(bool playAnimations)
        {
            CurrentTransitionState = TransitionState.TransitioningOff;
            if (playAnimations)
            {
                RemoveAnimations(AnimationTypeKeys.ScreenTransitionOffAnimations);
                RemoveAnimations(AnimationTypeKeys.ScreenTransitionOnAnimations);
                ApplyAnimations(TransitionOffAnimations, 0f, AnimationTypeKeys.ScreenTransitionOffAnimations);
            }
            _transitionOffRequested = false;
            _playTransitionOffAnimations = false;
        }

        public virtual void ApplyChildTransitionAnimations(List<IModifier> animations, ref float delay, string animationKey, float delayIncrease)
        {
            ApplyAnimations(animations, delay, animationKey);
            delay += delayIncrease;
        }

        public void ApplyAnimations(List<IModifier> animations, float delay, string animationKey)
        {
            if (animations != null)
            {
                foreach (IModifier mod in animations)
                {
                    IModifier curMod = mod.Copy();
                    curMod.StartDelay = delay;
                    AnimationManager.AddModifier(animationKey, curMod);
                }
            }
        }

        public void RemoveAnimations(string animationKey)
        {
            AnimationManager.RemoveModifiers(animationKey);
        }

        public virtual void ApplySkin()
        {
            _applySkinRequested = true;
        }

        public virtual void HandleApplySkin(bool forceApply)
        {
            if (_applySkinRequested || forceApply)
            {
                _applySkinRequested = false;
                SkinManager.Instance.ApplySkin(this);
            }
        }

        #region Methods for handling reflexive assignment of event animations
        public void HandleEventAnimationAssignment(List<Animation> existingValue, string propertyName,
            List<Animation> newValue, ScreenItem targetScreen)
        {
            if (existingValue != null)
            {
                foreach (Animation a in existingValue)
                {
                    //Remove
                    EventInfo ev = GetType().GetEvent(a.EventName);
                    RemoveEventAnimations(ev, propertyName);
                }
            }

            if (newValue != null)
            {
                foreach (Animation a in newValue)
                {
                    EventInfo ev = GetType().GetEvent(a.EventName);
                    Type[] genericArguments = ev.EventHandlerType.GetGenericArguments();
                    MethodInfo handleMethod;
                    if (genericArguments.Length == 0)
                    {
                        handleMethod = GetType().GetMethod("AddBasicEventAnimations");
                    }
                    else
                    {
                        handleMethod = GetType().GetMethod("AddEventAnimations");
                        handleMethod = handleMethod.MakeGenericMethod(ev.EventHandlerType.GetGenericArguments());
                    }
                    handleMethod.Invoke(this, new object[] { a, ev, targetScreen, propertyName });
                }
            }
        }

        public void AddEventAnimations<T>(Animation animationToAdd, EventInfo eventThatTriggers,
            ScreenItem targetScreenItem, string propertyAssigned) where T : EventArgs
        {
            EventHandler<T> del = delegate(object src, T arg)
            {
                foreach (IModifier mod in animationToAdd.ClonedModifiers())
                {
                    targetScreenItem.AnimationManager.AddModifier("", mod);
                }
            };
            eventThatTriggers.AddEventHandler(this, del);
            if (!_eventAssignedAnimations.ContainsKey(propertyAssigned))
            {
                _eventAssignedAnimations.Add(propertyAssigned, new List<Delegate>());
            }
            _eventAssignedAnimations[propertyAssigned].Add(del);
        }

        public void AddBasicEventAnimations(Animation animationToAdd, EventInfo eventThatTriggers,
            ScreenItem targetScreenItem, string propertyAssigned)
        {
            EventHandler del = delegate(object src, EventArgs arg)
            {
                foreach (IModifier mod in animationToAdd.ClonedModifiers())
                {
                    targetScreenItem.AnimationManager.AddModifier("", mod);
                }
            };
            eventThatTriggers.AddEventHandler(this, del);
            if (!_eventAssignedAnimations.ContainsKey(propertyAssigned))
            {
                _eventAssignedAnimations.Add(propertyAssigned, new List<Delegate>());
            }
            _eventAssignedAnimations[propertyAssigned].Add(del);
        }

        public void RemoveEventAnimations(EventInfo eventThatTriggers, string propertyAssigned)
        {
            if (_eventAssignedAnimations.ContainsKey(propertyAssigned))
            {
                foreach (Delegate d in _eventAssignedAnimations[propertyAssigned])
                {
                    eventThatTriggers.RemoveEventHandler(this, d);
                }
            }
        }
        #endregion
        public abstract void UpdateRequestedSizeDueToChildResize();
        public abstract void UpdateChildRelativeSize();
    }
}
