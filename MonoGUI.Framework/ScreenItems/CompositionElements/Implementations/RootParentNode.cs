﻿using Microsoft.Xna.Framework;
using MonoGUI.Manager;
using MonoGUI.ScreenItems.Containers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.ScreenItems.CompositionElements.Implementations
{
    public class RootParentNode : ParentNode
    {
        private ScreenManager ScreenManager;
        private MonoGUIGameWindow window;
        public ParentNode Parent { get { return null; } set { } }
        public RootParentNode(ScreenManager ScreenManager, MonoGUIGameWindow window)
        {
            this.ScreenManager = ScreenManager;
            this.window = window;
        }

        public Microsoft.Xna.Framework.Vector2 RelativePosition
        {
            get
            {
                return Vector2.Zero;
            }
            set
            {
                
            }
        }

        public Microsoft.Xna.Framework.Vector2 AbsolutePosition
        {
            get { return Vector2.Zero; }
        }

        public Microsoft.Xna.Framework.Vector2 RelativeDimensions
        {
            get
            {
                return ScreenManager.ScreenDimensions;
            }
            set
            {
                
            }
        }

        public Microsoft.Xna.Framework.Vector2 PreferredDimensions
        {
            get
            {
                return ScreenManager.ScreenDimensions;
            }
            set
            {
                
            }
        }


        public Vector3 RelativeScale
        {
            get
            {
                return AbsoluteScale;
            }
            set
            {
                
            }
        }

        public Vector3 AbsoluteScale
        {
            get { return new Vector3(1, 1, 1); }
        }

        public float RelativeRotation
        {
            get
            {
                return AbsoluteRotation;
            }
            set
            {
                
            }
        }

        public float AbsoluteRotation
        {
            get { return 0f; }
        }

        public Matrix LocalTransformation
        {
            get { return Matrix.Identity; }
        }

        public Matrix AbsoluteTransformation
        {
            get { return Matrix.Identity; }
        }

        public Matrix LocalAnimatedTransformation { get { return Matrix.Identity; } }
        public Matrix AbsoluteAnimatedTransformation { get { return Matrix.Identity; } }


        public void RequestResize()
        {
            window.UpdateRequestedSizeDueToChildResize();
            window.UpdateChildRelativeSize();
        }

        public Vector2 PreferredPosition
        {
            get;
            set;
        }


        public Vector2 AbsoluteDimensions
        {
            get { return ScreenManager.ScreenDimensions; }
        }


        public Matrix LocalInverseTransformation
        {
            get { return Matrix.Identity; }
        }

        public Matrix AbsoluteInverseTransformation
        {
            get { return Matrix.Identity; }
        }

        public Matrix LocalInverseAnimatedTransformation { get { return Matrix.Identity; } }
        public Matrix AbsoluteInverseAnimatedTransformation { get { return Matrix.Identity; } }

        public Vector2 RequestedPosition
        {
            get
            {
                return Vector2.Zero;
            }
            set
            {

            }
        }

        public Vector2 RequestedDimensions
        {
            get
            {
                return ScreenManager.ScreenDimensions;
            }
            set
            {

            }
        }

        public bool ActualIsEnabled
        {
            get { return true; }
        }

        public Manager.Input.GamepadPlayer AllowedPlayer
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool AutoUnfocus
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool Focusable
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool FocusWhenMouseOver
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool IsEnabled
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool IsFocused
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool IsFocusScope
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool IsFocusWithin
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool IsMouseDirectlyOver
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool IsMouseOver
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public void Focus()
        {
            throw new NotImplementedException();
        }

        public void HitTest(Manager.Input.InputManager inp)
        {
            throw new NotImplementedException();
        }


        public bool WasMouseOver
        {
            get
            {
                return true;
            }
            set
            {
                
            }
        }


        public Matrix AbsoluteInverseAnimatedScaleRotation
        {
            get { return Matrix.Identity; }
        }
    }
}
