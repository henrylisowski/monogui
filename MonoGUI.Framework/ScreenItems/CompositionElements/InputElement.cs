﻿using MonoGUI.Manager.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.ScreenItems.CompositionElements
{
    public interface InputElement
    {
        bool ActualIsEnabled { get; }
        GamepadPlayer AllowedPlayer { get; set; }
        bool AutoUnfocus { get; set; }
        bool Focusable { get; set; }
        bool FocusWhenMouseOver { get; set; }
        bool IsEnabled { get; set; }
        bool IsFocused { get; set; }
        bool IsFocusScope { get; set; }
        bool IsFocusWithin { get; set; }
        bool IsMouseDirectlyOver { get; set; }
        bool IsMouseOver { get; set; }
        bool WasMouseOver { get; set; }

        void Focus();
        void HitTest(InputManager  inp);
    }
}
