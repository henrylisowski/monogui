﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.ScreenItems.CompositionElements
{
    public interface PositionalElement
    {
        ParentNode Parent { get; set; }

        Vector2 PreferredPosition { get; set; }
        Vector2 RequestedPosition { get; set; }
        Vector2 RelativePosition { get; set; }
        Vector2 AbsolutePosition { get; }

        Vector3 RelativeScale { get; set; }
        Vector3 AbsoluteScale { get; }

        float RelativeRotation { get; set; }
        float AbsoluteRotation { get; }

        Vector2 RelativeDimensions { get; set; }
        Vector2 PreferredDimensions { get; set; }
        Vector2 RequestedDimensions { get; set; }

        Vector2 AbsoluteDimensions { get; }

        Matrix LocalTransformation { get; }
        Matrix AbsoluteTransformation { get; }
        Matrix LocalAnimatedTransformation { get; }
        Matrix AbsoluteAnimatedTransformation { get; }

        Matrix LocalInverseTransformation { get; }
        Matrix AbsoluteInverseTransformation { get; }
        Matrix LocalInverseAnimatedTransformation { get; }
        Matrix AbsoluteInverseAnimatedTransformation { get; }

        Matrix AbsoluteInverseAnimatedScaleRotation { get; }

        void RequestResize();
    }
}
