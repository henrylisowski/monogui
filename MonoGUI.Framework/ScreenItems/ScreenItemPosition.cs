﻿using Microsoft.Xna.Framework;
using MonoGUI.ScreenItems.CompositionElements;
using MonoGUI.Themes.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.ScreenItems
{
    public partial class ScreenItem : PositionalElement
    {
        private List<Matrix> _additionalTransformations;
        private List<Transformation3D> _3dTransformations;
        public bool Has3DTransformations { get; set; }
        public ParentNode Parent { get; set; }
        private bool _updateMatrixes = false;

        public void ClearAdditionalTransformations()
        {
            _additionalTransformations.Clear();
            UpdateTransformationMatrices();
        }

        public void AddAdditionalTransformation(Matrix transform)
        {
            _additionalTransformations.Add(transform);
            UpdateTransformationMatrices();
        }

        private Vector2 preferredPosition;
        public Vector2 PreferredPosition
        {
            get { return preferredPosition; }
            set { preferredPosition = value; }
        }
        private Vector2 requestedPosition;
        public Vector2 RequestedPosition
        {
            get { return requestedPosition; }
            set { requestedPosition = value; }
        }

        private Vector2 _relativePosition;
        public Vector2 RelativePosition
        {
            get { return _relativePosition; }
            set { _relativePosition = value; _updateMatrixes = true; }
        }

        public Vector2 AbsolutePosition
        {
            get { return this.RelativePosition + Parent.AbsolutePosition; }
        }

        private Vector2 _relativeDimensions;
        public Vector2 RelativeDimensions
        {
            get { return _relativeDimensions; }
            set { _relativeDimensions = value; _updateMatrixes = true; }
        }

        private Vector2 preferredDimensions;
        public Vector2 PreferredDimensions
        {
            get { return preferredDimensions; }
            set { preferredDimensions = value; }
        }
        private Vector2 requestedDimensions;
        public Vector2 RequestedDimensions
        {
            get { return requestedDimensions; }
            set { requestedDimensions = value; }
        }

        private Vector3 _relativeScale;
        public Vector3 RelativeScale
        {
            get { return _relativeScale; }
            set { _relativeScale = value; _updateMatrixes = true; }
        }

        public Vector3 AbsoluteScale
        {
            get { return this.RelativeScale * Parent.AbsoluteScale; }
        }

        public float RelativeRotation
        {
            get;
            set;
        }

        public float AbsoluteRotation
        {
            get { return this.RelativeRotation + Parent.AbsoluteRotation; }
        }

        private Matrix _localTransformation;
        public Matrix LocalTransformation
        {
            get
            {
                if (_updateMatrixes)
                {
                    _updateMatrixes = false;
                    UpdateTransformationMatrices();
                }
                return _localTransformation;
            }
        }

        private Matrix _absoluteTransformation;
        public Matrix AbsoluteTransformation
        {
            get 
            {
                if (_updateMatrixes)
                {
                    _updateMatrixes = false;
                    UpdateTransformationMatrices();
                }
                return _absoluteTransformation;
            }
        }

        private Matrix _localAnimatedTransformation;
        public Matrix LocalAnimatedTransformation
        {
            get
            {
                if (_updateMatrixes)
                {
                    _updateMatrixes = false;
                    UpdateTransformationMatrices();
                }
                return _localAnimatedTransformation;
            }
        }

        private Matrix _absoluteAnimatedTransformation;
        public Matrix AbsoluteAnimatedTransformation
        {
            get
            {
                if (_updateMatrixes)
                {
                    _updateMatrixes = false;
                    UpdateTransformationMatrices();
                }
                return _absoluteAnimatedTransformation;
            }
        }

        private Matrix _localInverseTransformation;
        public Matrix LocalInverseTransformation
        {
            get
            {
                if (_updateMatrixes)
                {
                    _updateMatrixes = false;
                    UpdateTransformationMatrices();
                }
                return _localInverseTransformation;
            }
        }

        private Matrix _absoluteInverseTransformation;
        public Matrix AbsoluteInverseTransformation
        {
            get
            {
                if (_updateMatrixes)
                {
                    _updateMatrixes = false;
                    UpdateTransformationMatrices();
                }
                return _absoluteInverseTransformation;
            }
        }

        private Matrix _localInverseAnimatedTransformation;
        public Matrix LocalInverseAnimatedTransformation
        {
            get
            {
                if (_updateMatrixes)
                {
                    _updateMatrixes = false;
                    UpdateTransformationMatrices();
                }
                return _localInverseAnimatedTransformation;
            }
        }

        private Matrix _absoluteInverseAnimatedTransformation;
        public Matrix AbsoluteInverseAnimatedTransformation
        {
            get
            {
                if (_updateMatrixes)
                {
                    _updateMatrixes = false;
                    UpdateTransformationMatrices();
                }
                return _absoluteInverseAnimatedTransformation;
            }
        }

        private Matrix _absoluteInverseAnimatedScaleRotation;
        public Matrix AbsoluteInverseAnimatedScaleRotation
        {
            get
            {
                if (_updateMatrixes)
                {
                    _updateMatrixes = false;
                    UpdateTransformationMatrices();
                }
                return _absoluteInverseAnimatedScaleRotation;
            }
        }

        public virtual void UpdateTransformationMatrices()
        {
            Update3DMatrices();
            _localTransformation = Matrix.CreateScale(RelativeScale) *
                Matrix.CreateRotationZ(RelativeRotation) *
                Matrix.CreateTranslation(new Vector3(RelativePosition, 0f));
            foreach (Matrix m in _additionalTransformations)
            {
                _localTransformation = _localTransformation * m;
            }

            _localAnimatedTransformation = Matrix.CreateScale(RelativeScale * new Vector3(AnimationManager.TotalTweenedScaling, 1)) *
                Matrix.CreateRotationZ(RelativeRotation) *
                Matrix.CreateTranslation(new Vector3(RelativePosition + AnimationManager.TotalTweenedPositions, 0f));
            foreach (Matrix m in _additionalTransformations)
            {
                _localAnimatedTransformation = _localAnimatedTransformation * m;
            }

            _localInverseTransformation = Matrix.CreateTranslation(new Vector3(-RelativePosition, 0f)) *
                Matrix.CreateRotationZ(-RelativeRotation) *
                Matrix.CreateScale(new Vector3(1f / RelativeScale.X, 1f / RelativeScale.Y, 1f / RelativeScale.Z));

            _localInverseAnimatedTransformation = Matrix.CreateTranslation(new Vector3(-(RelativePosition + AnimationManager.TotalTweenedPositions), 0f)) *
                Matrix.CreateRotationZ(-RelativeRotation) *
                Matrix.CreateScale(new Vector3(
                    1f / (RelativeScale.X * AnimationManager.TotalTweenedScaling.X),
                    1f / (RelativeScale.Y * AnimationManager.TotalTweenedScaling.Y),
                    1f / (RelativeScale.Z)));

            _absoluteTransformation = LocalTransformation * Parent.AbsoluteTransformation;
            _absoluteAnimatedTransformation = LocalAnimatedTransformation * Parent.AbsoluteAnimatedTransformation;
            _absoluteInverseTransformation = Parent.AbsoluteInverseTransformation * LocalInverseTransformation;
            _absoluteInverseAnimatedTransformation = Parent.AbsoluteInverseAnimatedTransformation * LocalInverseAnimatedTransformation;

            Vector3 position3, scale3;
            Quaternion rotationQ;

            _absoluteInverseAnimatedTransformation.Decompose(out scale3, out rotationQ, out position3);
            Vector2 direction = Vector2.Transform(Vector2.UnitX, rotationQ);
            float Rotation = (float)Math.Atan2((double)(direction.Y), (double)(direction.X));

            _absoluteInverseAnimatedScaleRotation = Matrix.CreateScale(scale3) * Matrix.CreateRotationZ(Rotation);

           
        }

        public void Update3DMatrices()
        {
            Transformation3DMatrix = Matrix.Identity;
            foreach (Transformation3D t in _3dTransformations)
            {
                //TODO include scale?
                Transformation3DMatrix *= Matrix.CreateTranslation(-(AbsolutePosition.X + (RelativeDimensions.X * t.Origin.Y)),
                    -(AbsolutePosition.Y + (RelativeDimensions.Y * t.Origin.X)), 0) *
                    Matrix.CreateRotationX(t.RotateX) * Matrix.CreateRotationY(t.RotateY) *
                    Matrix.CreateTranslation((AbsolutePosition.X + (RelativeDimensions.X * t.Origin.Y)),
                    (AbsolutePosition.Y + (RelativeDimensions.Y * t.Origin.X)), 0);
            }
        }

        public void ResetXYRotation()
        {
            _3dTransformations.Clear();
            Has3DTransformations = false;
            Update3DMatrices();
        }

        public void SetXRotation(float degrees, Vector2 origin)
        {
            _3dTransformations.Clear();
            _3dTransformations.Add(new Transformation3D(origin, MathHelper.ToRadians(degrees), 0f));
            Has3DTransformations = true;
            Update3DMatrices();
        }

        public void SetYRotation(float degrees, Vector2 origin)
        {
            _3dTransformations.Clear();
            _3dTransformations.Add(new Transformation3D(origin, 0f, MathHelper.ToRadians(degrees)));
            Has3DTransformations = true;
            Update3DMatrices();
        }

        public void SetXYRotation(float xDegrees, float yDegrees, Vector2 origin)
        {
            _3dTransformations.Clear();
            _3dTransformations.Add(new Transformation3D(origin, MathHelper.ToRadians(xDegrees), MathHelper.ToRadians(yDegrees)));
            Has3DTransformations = true;
            Update3DMatrices();
        }

        public Matrix Transformation3DMatrix { get; set; }

        public virtual void RequestResize()
        {
            Parent.RequestResize();
        }

        public Vector2 AbsoluteDimensions
        {
            get
            {
                return RelativeDimensions * new Vector2(AbsoluteScale.X, AbsoluteScale.Y);
            }
        }
    }
}
