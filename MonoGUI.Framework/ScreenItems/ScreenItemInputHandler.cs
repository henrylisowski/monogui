﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input.Touch;
using MonoGUI.Animations;
using MonoGUI.EventHandling.GestureEvents;
using MonoGUI.EventHandling.MouseEvents;
using MonoGUI.EventHandling.TouchEvents;
using MonoGUI.Manager.Input;
using MonoGUI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.ScreenItems
{
    public partial class ScreenItem
    {
        public List<IModifier> currentTransitionModifiers;
        public float PressHoldTime = 1f;
        private Dictionary<MouseButton, bool> MouseButtonStatus;
        private Dictionary<int, bool> TouchIdStatus;
        private Dictionary<MouseButton, float> MouseHoldElapsedTime;
        private Dictionary<int, float> TouchHoldElapsedTime;
        
        private GestureEvent RunningDragEvent;
        private GestureEvent RunningPinchEvent;

        private MouseButtonEvent _reuseableMouseButtonEvent;
        private MouseEvent _reuseableMouseEvent;
        private TouchEvent _reuseableTouchEvent;
        private List<MouseButton> _reuseableMouseHoldRemove;
        private List<MouseButton> _reuseableMouseToHoldKeys;
        private List<int> _reuseableTouchHoldRemove;
        private List<int> _reuseableTouchToHoldKeys;
        private Dictionary<GestureType, ISet<GestureSample>> _reuseableSortedGestures;

        private void InitializeInputHandler()
        {
            MouseButtonStatus = new Dictionary<MouseButton, bool>();
            foreach (MouseButton b in EnumUtils.MouseButton)
            {
                MouseButtonStatus.Add(b, false);
            }
            TouchIdStatus = new Dictionary<int, bool>();

            MouseHoldElapsedTime = new Dictionary<MouseButton, float>();
            TouchHoldElapsedTime = new Dictionary<int, float>();

            _reuseableMouseButtonEvent = new MouseButtonEvent(null, MouseButton.LeftButton);
            _reuseableMouseEvent = new MouseEvent(Vector2.Zero, Vector2.Zero);
            _reuseableTouchEvent = new TouchEvent(-1, Vector2.Zero, Vector2.Zero, 0, TouchLocationState.Invalid);
            _reuseableSortedGestures = new Dictionary<GestureType, ISet<GestureSample>>();
            _reuseableMouseToHoldKeys = new List<MouseButton>();
            _reuseableMouseHoldRemove = new List<MouseButton>();
            _reuseableTouchHoldRemove = new List<int>();
            _reuseableTouchToHoldKeys = new List<int>();
        }

        public virtual void HandleInput(GameTime gameTime, InputManager inputManager)
        {
            HitTest(inputManager);
            HandleTouch(gameTime, inputManager);
            HandleGestures(gameTime, inputManager);
            HandleMouse(gameTime, inputManager);
        }

        public virtual void UpdateVisualState(GameTime gameTime, InputManager inputManager)
        {
            if (CalculatesOwnHitState)
            {
                ScreenItemVisualStates oldState = VisualState;
                if (!this.IsEnabled)
                {
                    VisualState = ScreenItemVisualStates.Disabled;
                }
                else if (this.IsFocused)
                {
                    VisualState = ScreenItemVisualStates.Focussed;
                }
                //TODO temporary fix
                else if (this.IsMouseDirectlyOver && (inputManager.MouseButtonIsDown(MouseButton.LeftButton)
                    || inputManager.AllTouchIds().Length > 0))
                {
                    VisualState = ScreenItemVisualStates.Pressed;
                }
                else if (this.IsMouseDirectlyOver)
                {
                    VisualState = ScreenItemVisualStates.Hover;
                }
                else
                {
                    VisualState = ScreenItemVisualStates.Default;
                }
            }
        }

        protected virtual void VisualStateChanged()
        {
            if (currentTransitionModifiers != null)
            {
                foreach (IModifier mod in currentTransitionModifiers)
                {
                    //mod.Reverse();

                    mod.SignalFinish();
                    //TODO right now the reversed animations stay in the animation manager, so this'll have to be fixed
                }
            }
            currentTransitionModifiers = new List<IModifier>();
            if (StateTransitionAnimations != null)
            {
                List<IModifier> modsToApply = StateTransitionAnimations.GetSkinElementByState(VisualState);

                if (modsToApply != null)
                {
                    foreach (IModifier mod in modsToApply)
                    {
                        IModifier newMod = mod.Copy();
                        AnimationManager.AddModifier(AnimationTypeKeys.StateTransitionAnimations, newMod);
                        currentTransitionModifiers.Add(newMod);
                    }
                }
            }
        }

        protected void HandleTouch(GameTime gameTime, InputManager inputManager)
        {
            if (!IsMouseOver || inputManager.MouseTouchHandled)
            {
                TouchHoldElapsedTime.Clear();
            }

            if (!inputManager.MouseTouchHandled)
            {
                foreach (TouchPointState unmodifiedState in inputManager.TouchStates())
                {
                    GetTouchEvent(inputManager, unmodifiedState, this);

                    if (_reuseableTouchEvent.State == Microsoft.Xna.Framework.Input.Touch.TouchLocationState.Moved)
                    {
                        if (TouchMoved != null)
                        {
                            TouchMoved(this, _reuseableTouchEvent);
                        }
                    }
                    if (IsMouseOver)
                    {
                        if (!WasMouseOver)
                        {
                            if (TouchEntered != null)
                            {
                                TouchEntered(this, _reuseableTouchEvent);
                            }
                        }
                    }
                    else
                    {
                        if (WasMouseOver)
                        {
                            if (TouchExited != null)
                            {
                                TouchExited(this, _reuseableTouchEvent);
                            }
                        }
                    }

                    if (_reuseableTouchEvent.State == Microsoft.Xna.Framework.Input.Touch.TouchLocationState.Pressed)
                    {
                        if (IsMouseOver)
                        {
                            TouchIdStatus[_reuseableTouchEvent.Id] = true;
                            if (TouchPressed != null)
                            {
                                TouchPressed(this, _reuseableTouchEvent);
                            }

                            if (TouchHoldElapsedTime.ContainsKey(_reuseableTouchEvent.Id))
                            {
                                TouchHoldElapsedTime[_reuseableTouchEvent.Id] = 0f;
                            }
                            else
                            {
                                TouchHoldElapsedTime.Add(_reuseableTouchEvent.Id, 0f);
                            }
                        }
                        else
                        {
                            TouchIdStatus[_reuseableTouchEvent.Id] = false;
                            if (TouchPressedOutside != null)
                            {
                                TouchPressedOutside(this, _reuseableTouchEvent);
                            }
                        }
                    }
                    if (_reuseableTouchEvent.State == Microsoft.Xna.Framework.Input.Touch.TouchLocationState.Released)
                    {
                        if (IsMouseOver)
                        {
                            if (TouchReleased != null)
                            {
                                TouchReleased(this, _reuseableTouchEvent);
                            }
                            if (TouchIdStatus.ContainsKey(_reuseableTouchEvent.Id))
                            {
                                if (TouchIdStatus[_reuseableTouchEvent.Id] == true)
                                {
                                    if (TouchClicked != null)
                                    {
                                        TouchClicked(this, _reuseableTouchEvent);
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (TouchReleasedOutside != null)
                            {
                                TouchReleasedOutside(this, _reuseableTouchEvent);
                            }
                        }
                        TouchIdStatus[_reuseableTouchEvent.Id] = false;
                        if (TouchHoldElapsedTime.ContainsKey(_reuseableTouchEvent.Id))
                        {
                            TouchHoldElapsedTime.Remove(_reuseableTouchEvent.Id);
                        }
                    }

                    if (!IsMouseOver)
                    {
                        if (TouchHoldElapsedTime.ContainsKey(_reuseableTouchEvent.Id))
                        {
                            TouchHoldElapsedTime.Remove(_reuseableTouchEvent.Id);
                        }
                    }

                    if (TouchHoldElapsedTime.Count > 0)
                    {
                        _reuseableTouchToHoldKeys = TouchHoldElapsedTime.Keys.ToList();
                        for (int i = 0; i < _reuseableTouchToHoldKeys.Count; i++)
                        {
                            TouchHoldElapsedTime[_reuseableTouchToHoldKeys[i]] += (float)gameTime.ElapsedGameTime.TotalSeconds;
                            if (TouchHoldElapsedTime[_reuseableTouchToHoldKeys[i]] >= PressHoldTime)
                            {
                                _reuseableTouchHoldRemove.Add(_reuseableTouchToHoldKeys[i]);
                                TouchIdStatus[_reuseableTouchToHoldKeys[i]] = false;
                                //Fire mouse hold event
                                if (TouchHold != null)
                                {
                                    TouchHold(this,
                                        new TouchHoldEvent(_reuseableTouchEvent, TouchHoldElapsedTime[_reuseableTouchToHoldKeys[i]]));
                                }
                            }
                        }
                        for (int i = 0; i < _reuseableTouchHoldRemove.Count; i++)
                        {
                            TouchHoldElapsedTime.Remove(_reuseableTouchHoldRemove[i]);
                        }
                        _reuseableTouchHoldRemove.Clear();
                    }
                }
            }
        }

        protected void HandleGestures(GameTime gameTime, InputManager inputManager)
        {
            if (!inputManager.MouseTouchHandled)
            {
                List<GestureSample> curGestures = inputManager.CurrentGestureSamples;
                _reuseableSortedGestures.Clear();

                foreach (GestureSample sample in curGestures)
                {
                    if (!_reuseableSortedGestures.ContainsKey(sample.GestureType))
                    {
                        _reuseableSortedGestures.Add(sample.GestureType, new HashSet<GestureSample>());
                    }
                    _reuseableSortedGestures[sample.GestureType].Add(sample);
                }
                if (TapEvent != null)
                {
                    if (_reuseableSortedGestures.ContainsKey(GestureType.Tap))
                    {
                        foreach (GestureSample sample in _reuseableSortedGestures[GestureType.Tap])
                        {
                            GestureEvent ev = GetScaledGesture(sample, this);
                            if (ev.StartPosition1.X > 0 && ev.StartPosition1.X < this.RelativeDimensions.X &&
                                ev.StartPosition1.Y > 0 && ev.StartPosition1.Y < this.RelativeDimensions.Y)
                            {
                                TapEvent(this, ev);
                            }

                        }
                    }
                }
                if (DoubleTapEvent != null)
                {
                    if (_reuseableSortedGestures.ContainsKey(GestureType.DoubleTap))
                    {
                        foreach (GestureSample sample in _reuseableSortedGestures[GestureType.DoubleTap])
                        {
                            GestureEvent ev = GetScaledGesture(sample, this);
                            if (ev.StartPosition1.X > 0 && ev.StartPosition1.X < this.RelativeDimensions.X &&
                                ev.StartPosition1.Y > 0 && ev.StartPosition1.Y < this.RelativeDimensions.Y)
                            {
                                DoubleTapEvent(this, ev);
                            }
                        }
                    }
                }
                if (HoldEvent != null)
                {
                    if (_reuseableSortedGestures.ContainsKey(GestureType.Hold))
                    {
                        foreach (GestureSample sample in _reuseableSortedGestures[GestureType.Hold])
                        {
                            GestureEvent ev = GetScaledGesture(sample, this);
                            if (ev.StartPosition1.X > 0 && ev.StartPosition1.X < this.RelativeDimensions.X &&
                                ev.StartPosition1.Y > 0 && ev.StartPosition1.Y < this.RelativeDimensions.Y)
                            {
                                HoldEvent(this, ev);
                            }
                        }
                    }
                }
                if (FlickEvent != null)
                {
                    if (_reuseableSortedGestures.ContainsKey(GestureType.Flick))
                    {
                        foreach (GestureSample sample in _reuseableSortedGestures[GestureType.Flick])
                        {
                            GestureEvent ev = GetScaledGesture(sample, this);
                            FlickEvent(this, ev);
                        }
                    }
                }
                if (HorizontalDragEvent != null)
                {
                    if (_reuseableSortedGestures.ContainsKey(GestureType.HorizontalDrag))
                    {
                        foreach (GestureSample sample in _reuseableSortedGestures[GestureType.HorizontalDrag])
                        {
                            GestureEvent ev = GetScaledGesture(sample, this);
                            if ((RunningDragEvent == null && ev.StartPosition1.X > 0 && ev.StartPosition1.X < this.RelativeDimensions.X &&
                                ev.StartPosition1.Y > 0 && ev.StartPosition1.Y < this.RelativeDimensions.Y) || RunningDragEvent != null)
                            {
                                HorizontalDragEvent(this, ev);
                                if (RunningDragEvent == null)
                                {
                                    RunningDragEvent = ev;
                                }
                                else
                                {
                                    RunningDragEvent.AddGestureIteration(ev);
                                }
                            }

                        }
                    }
                }
                if (VerticalDragEvent != null)
                {
                    if (_reuseableSortedGestures.ContainsKey(GestureType.VerticalDrag))
                    {
                        foreach (GestureSample sample in _reuseableSortedGestures[GestureType.VerticalDrag])
                        {
                            GestureEvent ev = GetScaledGesture(sample, this);
                            if ((RunningDragEvent == null && ev.StartPosition1.X > 0 && ev.StartPosition1.X < this.RelativeDimensions.X &&
                                ev.StartPosition1.Y > 0 && ev.StartPosition1.Y < this.RelativeDimensions.Y) || RunningDragEvent != null)
                            {
                                VerticalDragEvent(this, ev);
                                if (RunningDragEvent == null)
                                {
                                    RunningDragEvent = ev;
                                }
                                else
                                {
                                    RunningDragEvent.AddGestureIteration(ev);
                                }
                            }
                        }
                    }
                }
                if (FreeDragEvent != null)
                {
                    if (_reuseableSortedGestures.ContainsKey(GestureType.FreeDrag))
                    {
                        foreach (GestureSample sample in _reuseableSortedGestures[GestureType.FreeDrag])
                        {
                            GestureEvent ev = GetScaledGesture(sample, this);
                            if ((RunningDragEvent == null && ev.StartPosition1.X > 0 && ev.StartPosition1.X < this.RelativeDimensions.X &&
                                ev.StartPosition1.Y > 0 && ev.StartPosition1.Y < this.RelativeDimensions.Y) || RunningDragEvent != null)
                            {
                                FreeDragEvent(this, ev);
                                if (RunningDragEvent == null)
                                {
                                    RunningDragEvent = ev;
                                }
                                else
                                {
                                    RunningDragEvent.AddGestureIteration(ev);
                                }
                            }
                        }
                    }
                }
                if (PinchEvent != null)
                {
                    if (_reuseableSortedGestures.ContainsKey(GestureType.Pinch))
                    {
                        foreach (GestureSample sample in _reuseableSortedGestures[GestureType.Pinch])
                        {
                            GestureEvent ev = GetScaledGesture(sample, this);
                            if ((RunningPinchEvent == null && ev.StartPosition1.X > 0 && ev.StartPosition1.X < this.RelativeDimensions.X &&
                                ev.StartPosition1.Y > 0 && ev.StartPosition1.Y < this.RelativeDimensions.Y) || RunningPinchEvent != null)
                            {
                                PinchEvent(this, ev);
                                if (RunningPinchEvent == null)
                                {
                                    RunningPinchEvent = ev;
                                }
                                else
                                {
                                    RunningPinchEvent.AddGestureIteration(ev);
                                }
                            }
                        }
                    }
                }
                if (DragCompletedEvent != null)
                {
                    if (_reuseableSortedGestures.ContainsKey(GestureType.DragComplete))
                    {
                        DragCompletedEvent(this, RunningDragEvent);
                        RunningDragEvent = null;
                    }
                }
                if (PinchCompletedEvent != null)
                {
                    if (_reuseableSortedGestures.ContainsKey(GestureType.PinchComplete))
                    {
                        PinchCompletedEvent(this, RunningPinchEvent);
                        RunningPinchEvent = null;
                    }
                }
            }
        }

        protected void HandleMouse(GameTime gameTime, InputManager inputManager)
        {
            if (!inputManager.MouseTouchHandled)
            {
                GetMouseEvent(inputManager, this);
                if (inputManager.MouseScrollDown())
                {
                    if (MouseScrolled != null)
                    {
                        MouseScrolled(this, new MouseScrollEvent(-1f));
                    }
                }
                if (inputManager.MouseScrollUp())
                {
                    if (MouseScrolled != null)
                    {
                        MouseScrolled(this, new MouseScrollEvent(1f));
                    }
                }
                if (MouseMoved != null)
                {
                    if (_reuseableMouseEvent.Delta.X != 0 || _reuseableMouseEvent.Delta.Y != 0)
                    {
                        MouseMoved(this, _reuseableMouseEvent);
                    }
                }
                if (IsMouseOver)
                {
                    if (!WasMouseOver)
                    {
                        if (MouseEntered != null)
                        {
                            MouseEntered(this, _reuseableMouseEvent);
                        }
                    }
                }
                else
                {
                    if (WasMouseOver)
                    {
                        if (MouseExited != null)
                        {
                            MouseExited(this, _reuseableMouseEvent);
                        }
                    }
                }
                foreach (MouseButton b in EnumUtils.MouseButton)
                {
                    bool prevStatus = MouseButtonStatus[b];
                    if (IsMouseOver)
                    {
                        if (inputManager.MousePress(b))
                        {
                            MouseButtonStatus[b] = true;
                            if (MousePressed != null)
                            {
                                _reuseableMouseButtonEvent.NewValues(_reuseableMouseEvent, b);
                                MousePressed(this, _reuseableMouseButtonEvent);
                            }

                            //Start counting for press and hold
                            if (MouseHoldElapsedTime.ContainsKey(b))
                            {
                                MouseHoldElapsedTime[b] = 0f;
                            }
                            else
                            {
                                MouseHoldElapsedTime.Add(b, 0f);
                            }
                        }
                        else if (inputManager.MouseRelease(b))
                        {
                            if (MouseReleased != null)
                            {
                                _reuseableMouseButtonEvent.NewValues(_reuseableMouseEvent, b);
                                MouseReleased(this, _reuseableMouseButtonEvent);
                            }
                            if (prevStatus)
                            {
                                if (MouseClicked != null)
                                {
                                    _reuseableMouseButtonEvent.NewValues(_reuseableMouseEvent, b);
                                    MouseClicked(this, _reuseableMouseButtonEvent);
                                }
                            }

                            //Remove checking of press time
                            if (MouseHoldElapsedTime.ContainsKey(b))
                            {
                                MouseHoldElapsedTime.Remove(b);
                            }
                        }
                    }
                    else
                    {
                        if (inputManager.MouseRelease(b))
                        {
                            if (MouseReleasedOutside != null)
                            {
                                _reuseableMouseButtonEvent.NewValues(_reuseableMouseEvent, b);
                                MouseReleasedOutside(this, _reuseableMouseButtonEvent);
                            }
                        }
                        if (inputManager.MousePress(b))
                        {
                            if (MousePressedOutside != null)
                            {
                                _reuseableMouseButtonEvent.NewValues(_reuseableMouseEvent, b);
                                MousePressedOutside(this, _reuseableMouseButtonEvent);
                            }
                        }


                        //Remove checking of press time
                        if (MouseHoldElapsedTime.ContainsKey(b))
                        {
                            MouseHoldElapsedTime.Remove(b);
                        }
                    }

                    if (inputManager.MouseRelease(b))
                    {
                        MouseButtonStatus[b] = false;
                    }

                    if (inputManager.MouseButtonIsDown(b))
                    {

                    }
                }

                if (MouseHoldElapsedTime.Count > 0)
                {
                    _reuseableMouseToHoldKeys = MouseHoldElapsedTime.Keys.ToList();

                    for (int i = 0; i < _reuseableMouseToHoldKeys.Count; i++)
                    {
                        MouseHoldElapsedTime[_reuseableMouseToHoldKeys[i]] += (float)gameTime.ElapsedGameTime.TotalSeconds;
                        if (MouseHoldElapsedTime[_reuseableMouseToHoldKeys[i]] >= PressHoldTime)
                        {
                            _reuseableMouseHoldRemove.Add(_reuseableMouseToHoldKeys[i]);
                            MouseButtonStatus[_reuseableMouseToHoldKeys[i]] = false;
                            //Fire mouse hold event
                            if (MouseHold != null)
                            {
                                MouseHold(this, new MouseHoldEvent(_reuseableMouseEvent, _reuseableMouseToHoldKeys[i],
                                    MouseHoldElapsedTime[_reuseableMouseToHoldKeys[i]]));
                            }
                        }
                    }
                    for (int i = 0; i < _reuseableMouseHoldRemove.Count; i++)
                    {
                        MouseHoldElapsedTime.Remove(_reuseableMouseHoldRemove[i]);
                    }
                    _reuseableMouseHoldRemove.Clear();
                }
            }
        }

        public event EventHandler<MouseEvent> MouseEntered;
        public event EventHandler<MouseEvent> MouseExited;
        public event EventHandler<MouseEvent> MouseMoved;
        public event EventHandler<MouseButtonEvent> MousePressed;
        public event EventHandler<MouseButtonEvent> MouseReleased;
        public event EventHandler<MouseButtonEvent> MouseClicked;
        public event EventHandler<MouseButtonEvent> MouseReleasedOutside;
        public event EventHandler<MouseButtonEvent> MousePressedOutside;
        public event EventHandler<MouseScrollEvent> MouseScrolled;
        public event EventHandler<MouseHoldEvent> MouseHold;

        public event EventHandler<TouchEvent> TouchEntered;
        public event EventHandler<TouchEvent> TouchExited;
        public event EventHandler<TouchEvent> TouchMoved;
        public event EventHandler<TouchEvent> TouchPressed;
        public event EventHandler<TouchEvent> TouchReleased;
        public event EventHandler<TouchEvent> TouchClicked;
        public event EventHandler<TouchEvent> TouchReleasedOutside;
        public event EventHandler<TouchEvent> TouchPressedOutside;
        public event EventHandler<TouchHoldEvent> TouchHold;

        public event EventHandler<GestureEvent> TapEvent;
        public event EventHandler<GestureEvent> DoubleTapEvent;
        public event EventHandler<GestureEvent> HoldEvent;
        public event EventHandler<GestureEvent> HorizontalDragEvent;
        public event EventHandler<GestureEvent> VerticalDragEvent;
        public event EventHandler<GestureEvent> FreeDragEvent;
        public event EventHandler<GestureEvent> PinchEvent;
        public event EventHandler<GestureEvent> FlickEvent;
        public event EventHandler<GestureEvent> DragCompletedEvent;
        public event EventHandler<GestureEvent> PinchCompletedEvent;



        private void GetTouchEvent(InputManager input, TouchPointState state, ScreenItems.ParentNode curScreen)
        {
            Matrix inverse = curScreen.AbsoluteInverseAnimatedTransformation;

            Vector2 transformedPosition = Vector2.Transform(state.Position, inverse);

            Vector2 delt = Vector2.Zero;
            if (input.PreviousTouchIds().Contains(state.Id))
            {
                delt = input.TouchPositionDelta(state.Id);
            }
            //TODO support delta!
            //Vector2 transformedDelta = Vector2.Transform(delt, deltaTransform);
            _reuseableTouchEvent.NewValues(state.Id, transformedPosition, delt/*transformedDelta*/, state.Pressure, state.State);
        }

        private void GetMouseEvent(InputManager input, ScreenItems.ParentNode curScreen)
        {
            Matrix inverse = curScreen.AbsoluteInverseAnimatedTransformation;
            _reuseableMouseEvent.NewValues(Vector2.Transform(input.MousePosition(), inverse),
                Vector2.Transform(input.MouseDelta(), curScreen.AbsoluteInverseAnimatedScaleRotation));
        }

        private GestureEvent GetScaledGesture(GestureSample sample, ScreenItems.ParentNode parent)
        {
            Matrix inverse = parent.AbsoluteInverseAnimatedTransformation;

            Vector3 position3, scale3;
            Quaternion rotationQ;

            inverse.Decompose(out scale3, out rotationQ, out position3);
            Vector2 direction = Vector2.Transform(Vector2.UnitX, rotationQ);
            float Rotation = (float)Math.Atan2((double)(direction.Y), (double)(direction.X));

            Matrix deltaTransform = Matrix.CreateScale(scale3) * Matrix.CreateRotationZ(Rotation);

            GestureEvent retVal = new GestureEvent(sample);
            retVal.StartPosition1 = Vector2.Transform(sample.Position, inverse);
            retVal.StartPosition2 = Vector2.Transform(sample.Position2, inverse);
            retVal.Delta1 = Vector2.Transform(sample.Delta, deltaTransform);
            retVal.Delta2 = Vector2.Transform(sample.Delta2, deltaTransform);
            return retVal;
        }
    }
}
