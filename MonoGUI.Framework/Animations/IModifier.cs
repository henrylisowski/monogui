﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Animations
{
    public interface IModifier
    {
        Vector2? Position { get; }
        Vector2? Scale { get; }
        Color? Color { get; }
        void Update(GameTime gameTime, Vector2 Dimensions);
        bool IsFinished { get; }
        float StartDelay { get; set; }
        

        void Reverse();
        void SignalFinish();
        IModifier Copy();
    }
}
