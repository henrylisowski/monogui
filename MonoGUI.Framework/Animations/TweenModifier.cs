﻿using Microsoft.Xna.Framework;
using MonoGUI.Animations.Tweens;
using MonoGUI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Animations
{
    public class TweenModifier : IModifier
    {
        public VectorTween PositionTween { get; set; }
        public VectorTween ScaleTween { get; set; }
        public ColorTween ColorTween { get; set; }
        public VectorAnchor ScaleAnchor { get; set; }
        public PositionType PositionMovementType { get; set; }

        private float elapsedTime;

        public TweenModifier()
        {
            PositionTween = null;
            ScaleTween = null;
            ColorTween = null;
            ScaleAnchor = VectorAnchor.TopLeft;
            StartDelay = 0f;
            elapsedTime = 0f;
        }

        public TweenModifier(VectorTween positionTween, VectorTween scaleTween, ColorTween colorTween, VectorAnchor scaleAnchor)
            : this(positionTween, scaleTween, colorTween, scaleAnchor, 0f)
        {

        }

        public TweenModifier(VectorTween positionTween, VectorTween scaleTween, ColorTween colorTween, VectorAnchor scaleAnchor, float startDelay) :
            this(positionTween, scaleTween, colorTween, scaleAnchor, PositionType.Absolute, startDelay)
        {
            
        }

        public TweenModifier(VectorTween positionTween, VectorTween scaleTween, ColorTween colorTween, VectorAnchor scaleAnchor,
            PositionType positionType)
            : this(positionTween, scaleTween, colorTween, scaleAnchor, positionType, 0f)
        {

        }

        public TweenModifier(VectorTween positionTween, VectorTween scaleTween, ColorTween colorTween, VectorAnchor scaleAnchor,
            PositionType positionType, float startDelay)
        {
            this.PositionTween = positionTween;
            this.ScaleTween = scaleTween;
            this.ColorTween = colorTween;
            this.ScaleAnchor = scaleAnchor;
            this.StartDelay = startDelay;
            this.PositionMovementType = positionType;
            elapsedTime = -startDelay;
        }
            

        public Microsoft.Xna.Framework.Vector2? Position
        {
            get;
            private set;
        }

        public Microsoft.Xna.Framework.Vector2? Scale
        {
            get;
            private set;
        }

        public Microsoft.Xna.Framework.Color? Color
        {
            get;
            private set;
        }

        private float _startDelay;
        public float StartDelay
        {
            get { return _startDelay; }
            set 
            { 
                _startDelay = value;
                elapsedTime = -_startDelay;
            }
        }

        public void Update(Microsoft.Xna.Framework.GameTime gameTime, Vector2 Dimensions)
        {
            float deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
            elapsedTime += deltaTime;
            if (elapsedTime < 0f)
            {
                deltaTime = 0f;
            }
            if (PositionTween != null && !PositionTween.IsFinished)
            {
                PositionTween.Update(deltaTime);
                Position = PositionTween.TweenedVector;
                if (PositionMovementType == PositionType.Percentage)
                {
                    Position *= Dimensions;
                }
            }
            else
            {
                Position = Vector2.Zero;
            }
            bool doScale = true;
            if (ScaleTween != null && !ScaleTween.IsFinished)
            {
                ScaleTween.Update(deltaTime);
                Scale = ScaleTween.TweenedVector;
            }
            else
            {
                Scale = new Vector2(1, 1);
                doScale = false;
            }

            if (doScale)
            {
                Vector2 increasedDimensions = Dimensions * Scale.Value;
                Vector2 dimensionsDif = increasedDimensions - Dimensions;
                Vector2 positionChange = Vector2.Zero;
                if (ScaleAnchor == VectorAnchor.TopMiddle || ScaleAnchor == VectorAnchor.Center || ScaleAnchor == VectorAnchor.BottomMiddle)
                {
                    //Left is centered
                    positionChange -= new Vector2(dimensionsDif.X * 0.5f, 0f);
                }
                if (ScaleAnchor == VectorAnchor.TopRight || ScaleAnchor == VectorAnchor.MiddleRight || ScaleAnchor == VectorAnchor.BottomRight)
                {
                    positionChange -= new Vector2(dimensionsDif.X, 0f);
                }
                if (ScaleAnchor == VectorAnchor.MiddleLeft || ScaleAnchor == VectorAnchor.Center || ScaleAnchor == VectorAnchor.MiddleRight)
                {
                    positionChange -= new Vector2(0, dimensionsDif.Y * 0.5f);
                }
                if (ScaleAnchor == VectorAnchor.BottomLeft || ScaleAnchor == VectorAnchor.BottomMiddle || ScaleAnchor == VectorAnchor.BottomRight)
                {
                    //Top is pushed off of bottom
                    positionChange -= new Vector2(0, dimensionsDif.Y);
                }
                if (Position != null)
                {
                    Position += positionChange;
                }
            }
            

            if (ColorTween != null && !ColorTween.IsFinished)
            {
                ColorTween.Update(deltaTime);
                Color = ColorTween.TweenedColor;
            }
            else
            {
                Color = Microsoft.Xna.Framework.Color.White;
            }
        }

        public bool IsFinished
        {
            get 
            {
                return ((PositionTween == null ? true : PositionTween.IsFinished) &&
                    (ScaleTween == null ? true : ScaleTween.IsFinished) &&
                    (ColorTween == null ? true : ColorTween.IsFinished));
            }
        }


        public void Reverse()
        {
            if (PositionTween != null)
            {
                PositionTween.Reverse();
            }
            if (ScaleTween != null)
            {
                ScaleTween.Reverse();
            }
            if (ColorTween != null)
            {
                ColorTween.Reverse();
            }
        }

        public void SignalFinish()
        {
            if (PositionTween != null)
            {
                PositionTween.SignalFinish();
            }
            if (ScaleTween != null)
            {
                ScaleTween.SignalFinish();
            }
            if (ColorTween != null)
            {
                ColorTween.SignalFinish();
            }
        }

        public IModifier Copy()
        {
            return new TweenModifier(PositionTween == null ? null : PositionTween.Copy(), 
                ScaleTween == null ? null : ScaleTween.Copy(), 
                ColorTween == null ? null : ColorTween.Copy(), 
                ScaleAnchor, PositionMovementType, StartDelay);
        }
    }
}
