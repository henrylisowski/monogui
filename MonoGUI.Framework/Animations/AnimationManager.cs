﻿using Microsoft.Xna.Framework;
using MonoGUI.Animations.Tweens;
using MonoGUI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Animations
{
    public class AnimationManager
    {
        private Dictionary<string, List<IModifier>> activeModifiers;
        private Dictionary<string, Vector2> tweenedPositions;
        private Dictionary<string, Vector2> tweenedScales;
        private Dictionary<string, Color> tweenedColors;

        public Vector2 TotalTweenedPositions { get; private set; }
        public Vector2 TotalTweenedScaling { get; private set; }
        public Color TotalTweenedColors
        {
            get { return _totalTweenedColors; }
            private set { _totalTweenedColors = value; }
        }
        private Color _totalTweenedColors;
        public bool AnimationChanged { get; private set; }

        public AnimationManager()
        {
            activeModifiers = new Dictionary<string, List<IModifier>>();

            tweenedPositions = new Dictionary<string, Vector2>();
            tweenedScales = new Dictionary<string, Vector2>();
            tweenedColors = new Dictionary<string, Color>();

            TotalTweenedColors = Color.White;
            TotalTweenedPositions = Vector2.Zero;
            TotalTweenedScaling = new Vector2(1, 1);

            AnimationChanged = false;
        }

        public void Update(GameTime gameTime, Vector2 Dimensions)
        {
            tweenedPositions.Clear();
            tweenedScales.Clear();
            tweenedColors.Clear();

            Vector2 _oldPosition = TotalTweenedPositions;
            Vector2 _oldScaling = TotalTweenedScaling;

            TotalTweenedColors = Color.White;
            TotalTweenedPositions = Vector2.Zero;
            TotalTweenedScaling = new Vector2(1, 1);
            AnimationChanged = false;
            foreach (string key in activeModifiers.Keys)
            {
                Vector2 accumulatedPosition = Vector2.Zero;
                Vector2 accumulatedScale = new Vector2(1f, 1f);
                Color accumulatedColor = Color.White;
                foreach (IModifier curModifier in activeModifiers[key])
                {
                    curModifier.Update(gameTime, Dimensions);
                    if (curModifier.Scale != null)
                    {
                        accumulatedScale *= curModifier.Scale.Value;
                    }
                    if (curModifier.Position != null)
                    {
                        accumulatedPosition += curModifier.Position.Value;
                    }
                    if (curModifier.Color != null)
                    {
                        ColorUtils.Multiply(accumulatedColor, curModifier.Color.Value, ref accumulatedColor);
                    }
                }
                activeModifiers[key].RemoveAll(i => i.IsFinished == true);
                tweenedPositions.Add(key, accumulatedPosition);
                tweenedScales.Add(key, accumulatedScale);
                tweenedColors.Add(key, accumulatedColor);

                TotalTweenedPositions += accumulatedPosition;
                TotalTweenedScaling *= accumulatedScale;
                ColorUtils.Multiply(TotalTweenedColors, accumulatedColor, ref _totalTweenedColors);
                AnimationChanged = true;
                //TODO why does AnimationChanged need to be here to work properly?
            }
            
            if (!TotalTweenedPositions.Equals(_oldPosition) || !(TotalTweenedScaling.Equals(_oldScaling)))
            {
                AnimationChanged = true;
            }
            else
            {
                //AnimationChanged = false;
            }
        }

        public Vector2 ModifiedPosition(string key)
        {
            if (tweenedPositions.ContainsKey(key))
            {
                return tweenedPositions[key];
            }
            return Vector2.Zero;
        }

        public Vector2 ModifiedScale(string key)
        {
            if (tweenedScales.ContainsKey(key))
            {
                return tweenedScales[key];
            }
            return new Vector2(1f, 1f);
        }

        public Color ModifiedColor(string key)
        {
            if (tweenedColors.ContainsKey(key))
            {
                return tweenedColors[key];
            }
            return Color.White;
        }

        public void AddModifier(string key, IModifier mod)
        {
            if (!activeModifiers.ContainsKey(key))
            {
                activeModifiers.Add(key, new List<IModifier>());
            }
            activeModifiers[key].Add(mod);
            AnimationChanged = true;
        }

        public void RemoveModifier(IModifier mod)
        {
            foreach (List<IModifier> l in activeModifiers.Values)
            {
                if (l.Contains(mod))
                {
                    l.Remove(mod);
                }
            }
            AnimationChanged = true;
        }

        public void RemoveModifiers(string key)
        {
            if (activeModifiers.ContainsKey(key))
            {
                activeModifiers[key].Clear();
            }
        }

        public List<IModifier> GetModifiersByKey(string key)
        {
            if (activeModifiers.ContainsKey(key))
            {
                return activeModifiers[key];
            }

            return new List<IModifier>();
        }
    }
}
