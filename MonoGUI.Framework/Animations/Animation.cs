﻿using MonoGUI.Skinning;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Animations
{
    public class Animation
    {
        [SkinnedProperty]
        public string EventName { get; set; }
        [SkinnedProperty]
        public List<IModifier> AnimationModifiers { get; set; }

        public Animation()
        {
            AnimationModifiers = new List<IModifier>();
        }

        public Animation(List<IModifier> modifiers)
        {
            AnimationModifiers = new List<IModifier>(modifiers);
        }

        public Animation(params IModifier[] modifiers)
        {
            AnimationModifiers = new List<IModifier>();
            foreach (IModifier mod in modifiers)
            {
                AnimationModifiers.Add(mod);
            }
        }

        public List<IModifier> ClonedModifiers()
        {
            List<IModifier> retVal = new List<IModifier>();
            foreach (IModifier mod in AnimationModifiers)
            {
                retVal.Add(mod.Copy());
            }
            return retVal;
        }
    }
}
