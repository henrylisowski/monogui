﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Animations
{
    public class AnimationTypeKeys
    {
        public const string StateTransitionAnimations = "state_transition_animations";
        public const string ScreenTransitionOnAnimations = "screen_transition_on_animations";
        public const string ScreenTransitionOffAnimations = "screen_transition_off_animations";
    }
}
