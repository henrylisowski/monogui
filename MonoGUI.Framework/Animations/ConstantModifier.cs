﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Animations
{
    public class ConstantModifier : IModifier
    {
        private bool isFinished;
        public ConstantModifier()
        {
            Position = null;
            Scale = null;
            Color = null;
            isFinished = false;
        }
        public ConstantModifier(Vector2? positionModifier, Vector2? scaleModifier, Color? colorModifier)
        {
            this.Position = positionModifier;
            this.Scale = scaleModifier;
            this.Color = colorModifier;
            isFinished = false;
        }
        public Microsoft.Xna.Framework.Vector2? Position
        {
            get;
            set;
        }

        public Microsoft.Xna.Framework.Vector2? Scale
        {
            get;
            set;
        }

        public Microsoft.Xna.Framework.Color? Color
        {
            get;
            set;
        }

        public void Update(Microsoft.Xna.Framework.GameTime gameTime, Vector2 Dimensions)
        {
            
        }

        public bool IsFinished
        {
            get { return isFinished; }
        }

        public float StartDelay
        {
            get { return 0; }
            set { }
        }

        public void Reverse()
        {
            isFinished = true;
        }

        public void SignalFinish()
        {
            isFinished = true;
        }

        public IModifier Copy()
        {
            return new ConstantModifier(Position, Scale, Color);
        }
    }
}
