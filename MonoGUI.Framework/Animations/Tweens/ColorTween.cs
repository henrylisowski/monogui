﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Animations.Tweens
{
    public class ColorTween
    {
        public EasingFunction REasing { get; set; }
        public EasingFunction GEasing { get; set; }
        public EasingFunction BEasing { get; set; }
        public EasingFunction AEasing { get; set; }

        public float ElapsedTime { get; set; }
        public float Duration { get; set; }
        public bool HoldDestination { get; set; }
        public bool RoundTrip { get; set; }
        public bool Looping { get; set; }
        
        [ContentSerializerIgnore]
        public Color TweenedColor { get; private set; }

        private bool isFinished;
        private bool isReversed;
        private float initialDelay;
        private bool signalFinished;

        public ColorTween()
            : this(Color.White, Color.White, 0f, EasingFunction.Equations.Linear, false)
        {

        }

        public ColorTween(Color startColor, Color endColor, float duration, EasingFunction.Equations easingFunction, bool holdDestination)
            : this(startColor, endColor, duration, easingFunction, holdDestination, 0f, false, false)
        {

        }

        public ColorTween(Color startColor, Color endColor, float duration, EasingFunction.Equations easingFunction, bool holdDestination, float startDelay, 
            bool roundTrip, bool looping)
        {
            REasing = new EasingFunction(easingFunction, startColor.R, endColor.R, duration);
            GEasing = new EasingFunction(easingFunction, startColor.G, endColor.G, duration);
            BEasing = new EasingFunction(easingFunction, startColor.B, endColor.B, duration);
            AEasing = new EasingFunction(easingFunction, startColor.A, endColor.A, duration);

            this.Duration = duration;
            isFinished = false;
            isReversed = false;
            ElapsedTime = -startDelay;
            initialDelay = startDelay;
            HoldDestination = holdDestination;
            RoundTrip = roundTrip;
            Looping = looping;
        }

        public void Update(float deltaTime)
        {
            if (!isFinished)
            {
                if (isReversed)
                {
                    this.ElapsedTime -= deltaTime;
                }
                else
                {
                    this.ElapsedTime += deltaTime;
                }

                if (ElapsedTime >= Duration)
                {
                    ElapsedTime = Duration;
                    TweenedColor = new Color((int)REasing.To, (int)GEasing.To, (int)BEasing.To, (int)AEasing.To);
                    if (RoundTrip)
                    {
                        isReversed = true;
                    }
                    else if (Looping)
                    {
                        ElapsedTime = 0f;
                    }
                    else if (!isReversed)
                    {
                        isFinished = true;
                    }
                }
                else if (ElapsedTime <= 0)
                {
                    TweenedColor = new Color((int)REasing.From, (int)GEasing.From, (int)BEasing.From, (int)AEasing.From);
                    if (signalFinished)
                    {
                        isFinished = true;
                    }
                    else if (Looping)
                    {
                        isReversed = false;
                    }
                    else if (isReversed)
                    {
                        isFinished = true;
                        ElapsedTime = 0f;
                    }
                }
                else
                {
                    TweenedColor = new Color((int)REasing.Calculate(ElapsedTime), (int)GEasing.Calculate(ElapsedTime),
                        (int)BEasing.Calculate(ElapsedTime), (int)AEasing.Calculate(ElapsedTime));
                }

            }
        }

        public bool IsFinished
        {
            get { return HoldDestination == true ? false : isFinished; }
        }

        public void Reverse()
        {
            isReversed = !isReversed;
            isFinished = false;
        }

        public void SignalFinish()
        {
            signalFinished = true;
            isReversed = true;
            isFinished = false;
        }

        public ColorTween Copy()
        {
            return new ColorTween(new Color((int)REasing.From, (int)GEasing.From, (int)BEasing.From, (int)AEasing.From),
                new Color((int)REasing.To, (int)GEasing.To, (int)BEasing.To, (int)AEasing.To), Duration, REasing.Equation, 
                HoldDestination, initialDelay, RoundTrip, Looping);
        }
    }
}
