﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Animations.Tweens
{
    public class VectorTween
    {
        public EasingFunction XFunction { get; set; }
        public EasingFunction YFunction { get; set; }
        public float ElapsedTime { get; set; }
        public float Duration { get; set; }
        public bool HoldDestination { get; set; }
        public bool RoundTrip { get; set; }
        public bool Looping { get; set; }

        [ContentSerializerIgnore]
        public Vector2 TweenedVector { get; private set; }
        private bool isFinished;
        private bool isReversed;
        private float initialDelay;
        private bool signalFinished;

        public VectorTween() : 
            this(Vector2.Zero, Vector2.Zero, 0f, EasingFunction.Equations.Linear, EasingFunction.Equations.Linear, false)
        {
            
        }

        public VectorTween(Vector2 startPosition, Vector2 endPosition, float duration, 
            EasingFunction.Equations xPositionEquation, EasingFunction.Equations yPositionEquation, bool holdDestination) : 
            this(startPosition, endPosition, duration, xPositionEquation, yPositionEquation, holdDestination, 0f, false, false)
        {
            
        }

        public VectorTween(Vector2 startPosition, Vector2 endPosition, float duration, 
            EasingFunction.Equations xPositionEquation, EasingFunction.Equations yPositionEquation, bool holdDestination,
            float startDelay, bool roundTrip, bool looping)
        {
            XFunction = new EasingFunction(xPositionEquation, startPosition.X, endPosition.X, duration);
            YFunction = new EasingFunction(yPositionEquation, startPosition.Y, endPosition.Y, duration);
            this.Duration = duration;
            ElapsedTime = -startDelay;
            initialDelay = startDelay;
            isFinished = false;
            isReversed = false;
            TweenedVector = startPosition;
            HoldDestination = holdDestination;
            RoundTrip = roundTrip;
            Looping = looping;
        }

        public void Update(float deltaTime)
        {
            if (!isFinished)
            {
                if (isReversed)
                {
                    this.ElapsedTime -= deltaTime;
                }
                else
                {
                    this.ElapsedTime += deltaTime;
                }
                
                if (ElapsedTime >= Duration)
                {
                    ElapsedTime = Duration;
                    TweenedVector = new Vector2((float)XFunction.To, (float)YFunction.To);

                    if (RoundTrip)
                    {
                        isReversed = true;
                    }
                    else if (Looping)
                    {
                        ElapsedTime = 0f;
                    }
                    else if (!isReversed)
                    {
                        isFinished = true;
                    }
                }
                else if (ElapsedTime <= 0)
                {
                    TweenedVector = new Vector2((float)XFunction.From, (float)YFunction.From);
                    if (signalFinished)
                    {
                        isFinished = true;
                    }
                    else if (Looping)
                    {
                        isReversed = false;
                    }
                    else if (isReversed)
                    {
                        isFinished = true;
                        ElapsedTime = 0f;
                    }
                }
                else
                {
                    TweenedVector = new Vector2((float)XFunction.Calculate(ElapsedTime), (float)YFunction.Calculate(ElapsedTime));
                }
                
            }
        }


        public bool IsFinished
        {
            get { return HoldDestination==true?false:isFinished; }
        }

        public void Reverse()
        {
            isReversed = !isReversed;
            isFinished = false;
        }

        public void SignalFinish()
        {
            signalFinished = true;
            isReversed = true;
            isFinished = false;
        }

        public VectorTween Copy()
        {
            return new VectorTween(new Vector2((float)XFunction.From, (float)YFunction.From), new Vector2((float)XFunction.To, (float)YFunction.To), 
                Duration, XFunction.Equation, YFunction.Equation, HoldDestination, initialDelay, RoundTrip, Looping);
        }
    }
}
