﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Skinning
{
    public class SkinReferencingConstants
    {
        public const string SkinVariantDefault = "default";
        public const string SkinVariant1 = "skin_variant_1";
        public const string SkinVariant2 = "skin_variant_2";
        public const string SkinVariant3 = "skin_variant_3";
        public const string SkinVariant4 = "skin_variant_4";
        public const string SkinVariant5 = "skin_variant_5";
        public const string SkinVariant6 = "skin_variant_6";
        public const string SkinVariant7 = "skin_variant_7";
        public const string SkinVariant8 = "skin_variant_8";
        public const string SkinVariant9 = "skin_variant_9";
        public const string SkinVariant10 = "skin_variant_10";
        public const string SkinVariant11 = "skin_variant_11";
        public const string SkinVariant12 = "skin_variant_12";
        public const string SkinVariant13 = "skin_variant_13";
        public const string SkinVariant14 = "skin_variant_14";
        public const string SkinVariant15 = "skin_variant_15";
        public const string SkinVariant16 = "skin_variant_16";
        public const string SkinVariant17 = "skin_variant_17";
        public const string SkinVariant18 = "skin_variant_18";
        public const string SkinVariant19 = "skin_variant_19";
        public const string SkinVariant20 = "skin_variant_20";
        public const string SkinVariantNone = "skin_variant_none";

        //Single Widgets
        public const string WidgetTargetIcon = "icon_widget_target";
        public const string WidgetTargetLabel = "label_widget_target";
        public const string WidgetTargetTextBlock = "text_block_target";
        public const string WidgetTargetButton = "button_widget_target";
        public const string WidgetTargetToggleButton = "togglebutton_widget_target";
        public const string WidgetTargetCheckbox = "checkbox_widget_target";
        public const string WidgetTargetRadioButton = "radiobutton_widget_target";
        public const string WidgetTargetGUIWindow = "guiwindow_widget_target";
        public const string WidgetTargetProgressBar = "progressbar_widget_target";
        public const string WidgetTargetSlider = "slider_widget_target";
        public const string WidgetTargetScrollBar = "scrollbar_widget_target";

        //Container Widgets
        public const string WidgetTargetCanvas = "canvas_widget_target";
        public const string WidgetTargetStack = "stack_widget_target";
        public const string WidgetTargetZoom = "zoom_widget_target";
        public const string WidgetTargetGridPanel = "grid_panel_widget_target";
        public const string WidgetTargetTabbedPanel = "tabbed_panel_widget_target";

        public const string PropertySkinnedBackground = "BackgroundSkin";
    }
}
