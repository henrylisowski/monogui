﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using MonoGUI.Manager;
using MonoGUI.ScreenItems;
using MonoGUI.Skinning.Fonts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Skinning
{
    public class SkinManager
    {
        private static SkinManager _instance;
        public static SkinManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new SkinManager();
                }
                return _instance;
            }
        }

        public Dictionary<string, MonoGUISkin> SkinMap { get; private set; }
        public MonoGUISkin DefaultSkin { get; private set; }
        public MonoGUISkin ActiveSkin { get; private set; }
        public string ActiveSkinName { get; private set; }
        private SkinManager() { SkinMap = new Dictionary<string, MonoGUISkin>(); }

        public void InitializeSkin(MonoGUISkin container, ContentManager Content, string SkinName)
        {
            container.Initialize();
            container.LoadContent(Content);
            container.ResolveGlobalReference();

            if (DefaultSkin == null)
            {
                DefaultSkin = container;
                ActiveSkin = container;
                ActiveSkinName = SkinName;
            }
            SkinMap[SkinName] = container;
        }

        public void SetActiveSkin(string SkinName)
        {
            if (SkinMap.ContainsKey(SkinName))
            {
                ActiveSkin = SkinMap[SkinName];
                ActiveSkinName = SkinName;
            }
            else
            {
                PlatformSpecificLayer.Instance.LogError("Attempted to apply skin " + SkinName + " which doesn't exist!");
            }
        }

        public void ApplySkin(ScreenItem screenItem)
        {
            if (ActiveSkin != null)
            {
                ActiveSkin.ApplySkin(screenItem);
            }

        }

        public SpriteFont GetClosestFont(string fontCodename, int desiredSize, FontRoundMethod roundMethod)
        {
            return ActiveSkin.FontManager.GetClosestFont(fontCodename, desiredSize, roundMethod);
        }

        public SpriteFont GetLargestFont(string fontCodename, Vector2 Dimensions, string text, 
            FontManager.FontClampSide ClampSide)
        {
            return ActiveSkin.FontManager.GetLargestFont(fontCodename, Dimensions, text, ClampSide);
        }

        public List<FontDescriptor> GetFontsOfDescendingSize(string fontCodename)
        {
            return ActiveSkin.FontManager.GetFontsOfDescendingSize(fontCodename);
        }
    }
}
