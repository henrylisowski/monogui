﻿using Microsoft.Xna.Framework;
using MonoGUI.Themes;
using MonoGUI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Skinning.SkinElements
{
    public abstract class SingleFillElement : ISkinElement
    {
        public enum FillTypes { NoFillCenter, Stretch };
        public FillTypes HorizontalFillType { get; set; }
        public FillTypes VerticalFillType { get; set; }
        private List<DrawCommand> sourceCommands;

        public SingleFillElement()
        {
            HorizontalFillType = FillTypes.NoFillCenter;
            VerticalFillType = FillTypes.NoFillCenter;
            sourceCommands = new List<DrawCommand>();
        }

        public SingleFillElement(FillTypes FillType)
        {
            HorizontalFillType = FillType;
            VerticalFillType = FillType;
            sourceCommands = new List<DrawCommand>();
        }

        public SingleFillElement(FillTypes HorizontalFillType, FillTypes VerticalFillType)
        {
            this.HorizontalFillType = HorizontalFillType;
            this.VerticalFillType = VerticalFillType;
            sourceCommands = new List<DrawCommand>();
        }

        public override void GetDrawCommands(Vector2 Dimensions, float componentLifetime, ref List<DrawCommand> retVal)
        {
            Vector2 dimensionsIncrease = Dimensions - new Vector2(this.DestinationBox.Width, this.DestinationBox.Height);

            sourceCommands.Clear();
            GetAtSizeDrawCommands(componentLifetime, ref sourceCommands);

            switch (HorizontalFillType)
            {
                case FillTypes.NoFillCenter:
                    foreach (DrawCommand c in sourceCommands)
                    {
                        c.DestinationRectangle.Position += new Vector2(dimensionsIncrease.X, 0) / 2;
                        c.CalculateScale();
                    }
                    break;
                case FillTypes.Stretch:
                    foreach (DrawCommand c in sourceCommands)
                    {
                        c.DestinationRectangle.Dimensions += new Vector2(dimensionsIncrease.X, 0);
                        c.CalculateScale();
                    }
                    break;
            }
            switch (VerticalFillType)
            {
                case FillTypes.NoFillCenter:
                    foreach (DrawCommand c in sourceCommands)
                    {
                        c.DestinationRectangle.Position += new Vector2(0, dimensionsIncrease.Y) / 2;
                        c.CalculateScale();
                    }
                    break;
                case FillTypes.Stretch:
                    foreach (DrawCommand c in sourceCommands)
                    {
                        c.DestinationRectangle.Dimensions += new Vector2(0, dimensionsIncrease.Y);
                        c.CalculateScale();
                    }
                    break;
            }
            retVal.AddRange(sourceCommands);
        }

        public abstract void GetAtSizeDrawCommands(float componentLifetime, ref List<DrawCommand> retVal);
    }
}
