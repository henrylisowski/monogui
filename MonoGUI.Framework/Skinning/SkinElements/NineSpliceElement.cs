﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGUI.Themes;
using MonoGUI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Skinning.SkinElements
{
    public class NineSpliceElement : ContainerFillElement
    {
        public ISkinElement TopLeftCorner { get; set; }
        public ISkinElement TopRightCorner { get; set; }
        public ISkinElement BottomLeftCorner { get; set; }
        public ISkinElement BottomRightCorner { get; set; }
        public ISkinElement TopBorder { get; set; }
        public ISkinElement RightBorder { get; set; }
        public ISkinElement BottomBorder { get; set; }
        public ISkinElement LeftBorder { get; set; }
        public ISkinElement CenterPiece { get; set; }

        public NineSpliceElement()
        {

        }

        public NineSpliceElement(Rectangle DestinationBox)
        {
            this.DestinationBox = DestinationBox;
        }

        public NineSpliceElement(ISkinElement TopLeftCorner, ISkinElement TopRightCorner, ISkinElement BottomLeftCorner, ISkinElement BottomRightCorner, 
            ISkinElement TopBorder, ISkinElement RightBorder, ISkinElement BottomBorder, ISkinElement LeftBorder, ISkinElement CenterPiece,
            Rectangle DestinationBox)
        {
            this.TopLeftCorner = TopLeftCorner;
            this.TopRightCorner = TopRightCorner;
            this.BottomLeftCorner = BottomLeftCorner;
            this.BottomRightCorner = BottomRightCorner;
            this.TopBorder = TopBorder;
            this.RightBorder = RightBorder;
            this.BottomBorder = BottomBorder;
            this.LeftBorder = LeftBorder;
            this.CenterPiece = CenterPiece;
            this.DestinationBox = DestinationBox;

            this.AddSkinElement(CenterPiece, new ScalingValues(new Vector2(), new Vector2(1, 1)));
            this.AddSkinElement(TopBorder, new ScalingValues(new Vector2(0, 0), new Vector2(1, 0)));
            this.AddSkinElement(RightBorder, new ScalingValues(new Vector2(1, 0), new Vector2(0, 1)));
            this.AddSkinElement(BottomBorder, new ScalingValues(new Vector2(0, 1), new Vector2(1, 0)));
            this.AddSkinElement(LeftBorder, new ScalingValues(new Vector2(0, 0), new Vector2(0, 1)));
            this.AddSkinElement(TopLeftCorner, new ScalingValues(new Vector2(0, 0), new Vector2(0, 0)));
            this.AddSkinElement(TopRightCorner, new ScalingValues(new Vector2(1, 0), new Vector2(0, 0)));
            this.AddSkinElement(BottomLeftCorner, new ScalingValues(new Vector2(0, 1), new Vector2(0, 0)));
            this.AddSkinElement(BottomRightCorner, new ScalingValues(new Vector2(1, 1), new Vector2(0, 0)));
        }

        public static NineSpliceElement GetSimpleNineSplice(Point TL, Point BR, Point ITL, Point IBR, 
            string sourceTexture)
        {
            Point TR = new Point(BR.X, TL.Y);
            Point BL = new Point(TL.X, BR.Y);
            Point ITR = new Point(IBR.X, ITL.Y);
            Point IBL = new Point(ITL.X, IBR.Y);

            Rectangle tlCorner = new Rectangle(TL.X, TL.Y, ITL.X - TL.X, ITL.Y - TL.Y);
            Rectangle tlDest = new Rectangle(TL.X - TL.X, TL.Y - TL.Y, tlCorner.Width, tlCorner.Height);
            Rectangle trCorner = new Rectangle(ITR.X, TR.Y, TR.X - ITR.X, ITR.Y - TR.Y);
            Rectangle trDest = new Rectangle(ITR.X - TL.X, TR.Y - TL.Y, trCorner.Width, trCorner.Height);
            Rectangle blCorner = new Rectangle(BL.X, IBL.Y, IBL.X - BL.X, BL.Y - IBL.Y);
            Rectangle blDest = new Rectangle(BL.X - TL.X, IBL.Y - TL.Y, blCorner.Width, blCorner.Height);
            Rectangle brCorner = new Rectangle(IBR.X, IBR.Y, BR.X - IBR.X, BR.Y - IBR.Y);
            Rectangle brDest = new Rectangle(IBR.X - TL.X, IBR.Y - TL.Y, brCorner.Width, brCorner.Height);

            Rectangle tBar = new Rectangle(ITL.X, TL.Y, ITR.X - ITL.X, ITL.Y - TL.Y);
            Rectangle tDest = new Rectangle(ITL.X - TL.X, TL.Y - TL.Y, tBar.Width, tBar.Height);
            Rectangle rBar = new Rectangle(ITR.X, ITR.Y, TR.X - ITR.X, IBR.Y - ITR.Y);
            Rectangle rDest = new Rectangle(ITR.X - TL.X, ITR.Y - TL.Y, rBar.Width, rBar.Height);
            Rectangle lBar = new Rectangle(TL.X, ITL.Y, ITL.X - TL.X, IBL.Y - ITL.Y);
            Rectangle lDest = new Rectangle(TL.X - TL.X, ITL.Y - TL.Y, lBar.Width, lBar.Height);
            Rectangle bBar = new Rectangle(IBL.X, IBL.Y, IBR.X - IBL.X, BL.Y - IBL.Y);
            Rectangle bDest = new Rectangle(IBL.X - TL.X, IBL.Y - TL.Y, bBar.Width, bBar.Height);

            Rectangle center = new Rectangle(ITL.X, ITL.Y, IBR.X - ITL.X, IBR.Y - ITL.Y);
            Rectangle centerDest = new Rectangle(ITL.X - TL.X, ITL.Y - TL.Y, center.Width, center.Height);

            ISkinElement TopLeftCorner = new TextureElement(sourceTexture, tlCorner, tlDest, SpriteEffects.None, 
                SingleFillElement.FillTypes.NoFillCenter, SingleFillElement.FillTypes.NoFillCenter);
            ISkinElement TopRightCorner = new TextureElement(sourceTexture, trCorner, trDest, SpriteEffects.None,
                SingleFillElement.FillTypes.NoFillCenter, SingleFillElement.FillTypes.NoFillCenter);
            ISkinElement BottomLeftCorner = new TextureElement(sourceTexture, blCorner, blDest, SpriteEffects.None,
                SingleFillElement.FillTypes.NoFillCenter, SingleFillElement.FillTypes.NoFillCenter);
            ISkinElement BottomRightCorner = new TextureElement(sourceTexture, brCorner, brDest, SpriteEffects.None,
                SingleFillElement.FillTypes.NoFillCenter, SingleFillElement.FillTypes.NoFillCenter);

            ISkinElement TopBar = new TextureElement(sourceTexture, tBar, tDest, SpriteEffects.None,
                SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.NoFillCenter);
            ISkinElement RightBar = new TextureElement(sourceTexture, rBar, rDest, SpriteEffects.None,
                SingleFillElement.FillTypes.NoFillCenter, SingleFillElement.FillTypes.Stretch);
            ISkinElement LeftBar = new TextureElement(sourceTexture, lBar, lDest, SpriteEffects.None,
                SingleFillElement.FillTypes.NoFillCenter, SingleFillElement.FillTypes.Stretch);
            ISkinElement BottomBar = new TextureElement(sourceTexture, bBar, bDest, SpriteEffects.None,
                SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.NoFillCenter);

            ISkinElement CenterPiece = new TextureElement(sourceTexture, center, centerDest, SpriteEffects.None,
                SingleFillElement.FillTypes.Stretch, SingleFillElement.FillTypes.Stretch);

            return new NineSpliceElement(TopLeftCorner, TopRightCorner, BottomLeftCorner, BottomRightCorner, TopBar, RightBar, BottomBar, LeftBar,
                CenterPiece, new Rectangle(0, 0, BR.X - TL.X, BR.Y - TL.Y));
        }
    }
}
