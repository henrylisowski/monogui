﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using MonoGUI.Themes;
using MonoGUI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace MonoGUI.Skinning.SkinElements
{
    public abstract class ISkinElement
    {
        [ContentSerializer(Optional=true)]
        public string ElementName { get; set; }
        
        public virtual Rectangle DestinationBox { get; set; }
        public Vector2 DestinationDimensions { get { return new Vector2(DestinationBox.Width, DestinationBox.Height); } }

        public abstract void GetDrawCommands(Vector2 Dimensions, float componentLifetime, ref List<DrawCommand> retVal);
        public abstract void Initialize();
        public abstract void LoadContent(ContentManager Content);
        public virtual void ResolveGlobalReferences(MonoGUISkin RootSkin) { }

        public virtual void Scale(float scaleAmount)
        {
            DestinationBox = new Rectangle((int)(scaleAmount * DestinationBox.X), (int)(scaleAmount * DestinationBox.Y),
                (int)(scaleAmount * DestinationBox.Width), (int)(scaleAmount * DestinationBox.Height));
        }
    }
}
