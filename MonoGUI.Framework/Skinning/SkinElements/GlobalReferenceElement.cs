﻿using Microsoft.Xna.Framework.Content;
using MonoGUI.Themes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Skinning.SkinElements
{
    public class GlobalReferenceElement : ISkinElement
    {
        public string ReferenceName { get; set; }
        private ISkinElement _globalReference;

        public GlobalReferenceElement(string ReferenceName)
        {
            this.ReferenceName = ReferenceName;
        }

        public override void GetDrawCommands(Microsoft.Xna.Framework.Vector2 Dimensions, float componentLifetime,
            ref List<DrawCommand> retVal)
        {
            _globalReference.GetDrawCommands(Dimensions, componentLifetime, ref retVal);
        }

        public override void Initialize()
        {
            
        }

        public override void LoadContent(Microsoft.Xna.Framework.Content.ContentManager Content)
        {
            
        }

        public override void ResolveGlobalReferences(MonoGUISkin RootSkin)
        {
            base.ResolveGlobalReferences(RootSkin);
            _globalReference = RootSkin.FindGlobalElement(ReferenceName);
            this.DestinationBox = _globalReference.DestinationBox;
        }
    }
}
