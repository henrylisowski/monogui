﻿using Microsoft.Xna.Framework;
using MonoGUI.Themes;
using MonoGUI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace MonoGUI.Skinning.SkinElements
{
    public class ContainerFillElement : ISkinElement
    {
        private List<KeyValuePair<ScalingValues, ISkinElement>> _childSkinElements;
        private List<DrawCommand> _childCommands;
        public List<ISkinElement> SkinElements
        {
            get
            {
                List<ISkinElement> retVal = new List<ISkinElement>();
                foreach (KeyValuePair<ScalingValues, ISkinElement> v in _childSkinElements)
                {
                    retVal.Add(v.Value);
                }
                return retVal;
            }           
        }

        public ContainerFillElement()
        {
            _childSkinElements = new List<KeyValuePair<ScalingValues, ISkinElement>>();
            _childCommands = new List<DrawCommand>();
        }

        public void AddSkinElement(ISkinElement skinElement, ScalingValues scaling)
        {
            _childSkinElements.Add(new KeyValuePair<ScalingValues, ISkinElement>(scaling, skinElement));
        }

        public override void GetDrawCommands(Vector2 Dimensions, float componentLifetime, ref List<DrawCommand> retVal)
        {
            Vector2 increase = Dimensions - new Vector2(DestinationBox.Width, DestinationBox.Height);
            foreach (KeyValuePair<ScalingValues, ISkinElement> p in _childSkinElements)
            {
                _childCommands.Clear();
                p.Value.GetDrawCommands(p.Value.DestinationDimensions + p.Key.DimensionScaling * increase, 
                    componentLifetime, ref _childCommands);
                for (int i = 0; i < _childCommands.Count; i++)
                {
                    _childCommands[i].DestinationRectangle.Position += p.Key.PositionScaling * increase;
                    retVal.Add(_childCommands[i]);
                }
            }
            foreach (DrawCommand c in retVal)
            {
                c.DestinationRectangle.Position += new Vector2(DestinationBox.X, DestinationBox.Y);
                c.CalculateScale();
            }
        }

        public override void Initialize()
        {
            foreach (KeyValuePair<ScalingValues, ISkinElement> p in _childSkinElements)
            {
                p.Value.Initialize();
            }
        }

        public override void LoadContent(Microsoft.Xna.Framework.Content.ContentManager Content)
        {
            foreach (KeyValuePair<ScalingValues, ISkinElement> p in _childSkinElements)
            {
                p.Value.LoadContent(Content);
            }
        }

        public override void Scale(float scaleAmount)
        {
            base.Scale(scaleAmount);
            foreach (ISkinElement e in SkinElements)
            {
                e.Scale(scaleAmount);
            }
        }
    }
}
