﻿using MonoGUI.Themes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Skinning.SkinElements
{
    public class EmptyElement : ISkinElement
    {
        public EmptyElement() { }
        public override void GetDrawCommands(Microsoft.Xna.Framework.Vector2 Dimensions, float componentLifetime,
            ref List<DrawCommand> retVal)
        {
        }

        public override void Initialize()
        {
            
        }

        public override void LoadContent(Microsoft.Xna.Framework.Content.ContentManager Content)
        {
            
        }
    }
}
