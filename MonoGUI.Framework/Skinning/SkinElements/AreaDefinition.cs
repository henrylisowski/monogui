﻿using Microsoft.Xna.Framework;
using MonoGUI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Skinning.SkinElements
{
    /// <summary>
    /// Represents an area defined within a skinned element. Used to determine minimum dimensions of a skinned widget, 
    /// as well as how sub-widgets should scale and move based on parent size
    /// </summary>
    public class AreaDefinition
    {
        public ScalingValues Scaling { get; set; }
        public RectangleFloat Area { get; set; }
        public VectorAnchor ScaleAnchor { get; set; }
        public bool ScaleWithAspectRatio { get; set; }
        
        
        public AreaDefinition()
        {
            Area = new RectangleFloat();
            Scaling = new ScalingValues(0, 0, 0, 0);
            ScaleWithAspectRatio = false;
            ScaleAnchor = VectorAnchor.TopLeft;
        }

        /// <summary>
        /// Constructs a standard area definition grounded at zero that increases dimensions only
        /// This can be used for minimum dimensions of widgets
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public AreaDefinition(int width, int height)
        {
            Area = new RectangleFloat(0, 0, width, height);
            Scaling = new ScalingValues(0, 0, 1, 1);
            ScaleWithAspectRatio = false;
            ScaleAnchor = VectorAnchor.TopLeft;
        }

        public AreaDefinition(RectangleFloat Area, ScalingValues Scaling)
        {
            this.Area = Area;
            this.Scaling = Scaling;
            ScaleWithAspectRatio = false;
            ScaleAnchor = VectorAnchor.TopLeft;
        }

        public AreaDefinition(RectangleFloat Area, ScalingValues Scaling, bool ScaleWithAspectRatio)
            : this(Area, Scaling)
        {
            this.ScaleWithAspectRatio = ScaleWithAspectRatio;
            this.ScaleAnchor = VectorAnchor.TopLeft;
        }

        public RectangleFloat UpdatedSize(Vector2 increasedDimensions, Vector2 minimumDimensions)
        {
            //float xPos = Area.X + (Scaling.PositionScaling.X * increasedDimensions.X);
            //float yPos = Area.Y + (Scaling.PositionScaling.Y * increasedDimensions.Y);

            //if (AspectConstraint == AspectRatioConstraint.ScaleWithSmallest)
            //{
            //    if (increasedDimensions.X < increasedDimensions.Y)
            //    {
            //        increasedDimensions.Y = increasedDimensions.X;
            //    }
            //    else
            //    {
            //        increasedDimensions.X = increasedDimensions.Y;
            //    }
            //}
            //else if (AspectConstraint == AspectRatioConstraint.ScaleWithLargest)
            //{
            //    if (increasedDimensions.X > increasedDimensions.Y)
            //    {
            //        increasedDimensions.Y = increasedDimensions.X;
            //    }
            //    else
            //    {
            //        increasedDimensions.X = increasedDimensions.Y;
            //    }
            //}

            //xPos = Area.X + (Scaling.PositionScaling.X * increasedDimensions.X);
            //yPos = Area.Y + (Scaling.PositionScaling.Y * increasedDimensions.Y);

            //float widthIncrease = (Scaling.DimensionScaling.X * increasedDimensions.X);
            //float heightIncrease = (Scaling.DimensionScaling.Y * increasedDimensions.Y);

            //Vector2 scaleMod = ScaleAnchor.DirectionModifier();
            //xPos += scaleMod.X * widthIncrease;
            //yPos += scaleMod.Y * heightIncrease;

            //float width = Area.Width + widthIncrease;
            //float height = Area.Height + heightIncrease;
            //RectangleFloat retVal = new RectangleFloat(
            //    xPos,
            //    yPos,
            //    width,
            //    height);

            //return retVal;

            float xPos = Area.X;
            float yPos = Area.Y;
            float width = Area.Width;
            float height = Area.Height;

            if (ScaleWithAspectRatio && minimumDimensions != null && minimumDimensions != Vector2.Zero)
            {
                Vector2 totalDimensions = minimumDimensions + increasedDimensions;
                Vector2 percentIncrease = totalDimensions / minimumDimensions;
                float aspect = Math.Min(percentIncrease.X, percentIncrease.Y);
                xPos *= aspect;
                yPos *= aspect;
                width *= aspect;
                height *= aspect;
                increasedDimensions = totalDimensions - (minimumDimensions * aspect);
            }



            xPos += (increasedDimensions.X * Scaling.PositionScaling.X);
            yPos += (increasedDimensions.Y * Scaling.PositionScaling.Y);
            width += (increasedDimensions.X * Scaling.DimensionScaling.X);
            height += (increasedDimensions.Y * Scaling.DimensionScaling.Y);

            return new RectangleFloat(xPos, yPos, width, height);
        }

        public void Scale(float scaleAmount)
        {
            Area = new RectangleFloat(scaleAmount * Area.X, scaleAmount * Area.Y, scaleAmount * Area.Width, scaleAmount * Area.Height);
        }
    }
}
