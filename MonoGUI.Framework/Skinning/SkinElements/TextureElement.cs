﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using MonoGUI.Themes;
using MonoGUI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Skinning.SkinElements
{
    public class TextureElement : SingleFillElement
    {
        [ContentSerializerIgnore]
        public Texture2D SourceTexture { get; set; }

        public List<Rectangle> SourceBoxes { get; set; }
        public string SourceTextureName { get; set; }
        public SpriteEffects Effect { get; set; }
        public float FrameTime { get; set; }
        public bool SingleFrameAnimation { get; set; }

        public TextureElement Clone()
        {
            List<Rectangle> newList = new List<Rectangle>();
            foreach (Rectangle r in SourceBoxes)
            {
                newList.Add(new Rectangle(r.X, r.Y, r.Width, r.Height));
            }

            Rectangle newDest = new Rectangle(DestinationBox.X, DestinationBox.Y, 
                DestinationBox.Width, DestinationBox.Height);

            TextureElement retVal = new TextureElement(SourceTextureName, newList, newDest, Effect, 
                FrameTime, HorizontalFillType, VerticalFillType);
            retVal.SingleFrameAnimation = SingleFrameAnimation;
            return retVal;
        }

        public TextureElement()
        {

        }


        public TextureElement(string SourceTextureName, Rectangle SourceBox, Rectangle DestinationBox, SpriteEffects Effect)
            : this(SourceTextureName, SourceBox, DestinationBox, Effect, FillTypes.NoFillCenter)
        {

        }

        public TextureElement(string SourceTextureName, Rectangle SourceBox, Rectangle DestinationBox, SpriteEffects Effect, FillTypes FillType) : 
            this(SourceTextureName, SourceBox, DestinationBox, Effect, FillType, FillType)
        {

        }

        public TextureElement(string SourceTextureName, Rectangle SourceBox, Rectangle DestinationBox, SpriteEffects Effect, 
            FillTypes HorizontalFillType, FillTypes VerticalFillType) :
            base(HorizontalFillType, VerticalFillType)
        {
            this.SourceTextureName = SourceTextureName;
            this.SourceBoxes = new List<Rectangle>();
            this.SourceBoxes.Add(SourceBox);
            this.DestinationBox = DestinationBox;
            this.Effect = Effect;
            this.FrameTime = 1f;
            SingleFrameAnimation = true;
        }

        public TextureElement(string SourceTextureName, List<Rectangle> SourceBox, Rectangle DestinationBox, SpriteEffects Effect, float FrameTime)
            : this(SourceTextureName, SourceBox, DestinationBox, Effect, FrameTime, FillTypes.NoFillCenter)
        {

        }

        public TextureElement(string SourceTextureName, List<Rectangle> SourceBoxes, 
            Rectangle DestinationBox, SpriteEffects Effect, float FrameTime, FillTypes FillType) : 
            this(SourceTextureName, SourceBoxes, DestinationBox, Effect, FrameTime, FillType, FillType)
        {

        }

        public TextureElement(string SourceTextureName, List<Rectangle> SourceBoxes,
            Rectangle DestinationBox, SpriteEffects Effect, float FrameTime, FillTypes HorizontalFillType, FillTypes VerticalFillType)
            : base(HorizontalFillType, VerticalFillType)
        {
            this.SourceTextureName = SourceTextureName;
            this.SourceBoxes = SourceBoxes;
            this.DestinationBox = DestinationBox;
            this.Effect = Effect;
            this.FrameTime = FrameTime;
            SingleFrameAnimation = false;
        }

        public Rectangle CurrentSourceBox(float componentLifetime)
        {
            if (SingleFrameAnimation)
            {
                return SourceBoxes[0];
            }
            int numFrames = (int)(componentLifetime / FrameTime);
            if (numFrames < SourceBoxes.Count)
            {
                return (SourceBoxes[numFrames]);
            }
            else
            {
                return SourceBoxes[numFrames % SourceBoxes.Count];
            }
        }

        public override string ToString()
        {
            return SourceTextureName + " " + SourceBoxes + " " + DestinationBox;
        }

        public override void GetAtSizeDrawCommands(float componentLifetime, ref List<DrawCommand> retVal)
        {
            retVal.Add(new DrawCommand(SourceTexture, SourceTextureName, CurrentSourceBox(componentLifetime), Effect,
                new RectangleFloat(DestinationBox.X, DestinationBox.Y, DestinationBox.Width, DestinationBox.Height), this));
        }

        public override void Initialize()
        {
            
        }

        public override void LoadContent(ContentManager Content)
        {
            SourceTexture = Content.Load<Texture2D>(SourceTextureName);
        }

        
    }
}
