﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Skinning.SkinElements
{
    public class ScalingValues
    {
        public Vector2 PositionScaling { get; set; }
        public Vector2 DimensionScaling { get; set; }

        public ScalingValues()
        {
            PositionScaling = new Vector2();
            DimensionScaling = new Vector2();
        }

        public ScalingValues(float xScale, float yScale, float widthScale, float heightScale)
        {
            PositionScaling = new Vector2(xScale, yScale);
            DimensionScaling = new Vector2(widthScale, heightScale);
        }

        public ScalingValues(Vector2 PositionScaling, Vector2 DimensionScaling)
        {
            this.PositionScaling = PositionScaling;
            this.DimensionScaling = DimensionScaling;
        }
    }
}
