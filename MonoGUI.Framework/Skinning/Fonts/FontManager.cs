﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Skinning.Fonts
{
    public class FontManager
    {
        public FontDescriptor DefaultFont { get; set; }
        public List<FontDescriptor> ThemeFontDescriptors { get; set; }

        private Dictionary<string, List<FontDescriptor>> FontsSortedByCodename;

        public FontManager()
        {
            ThemeFontDescriptors = new List<FontDescriptor>();
        }

        public void LoadContent(ContentManager Content)
        {
            DefaultFont.Font = Content.Load<SpriteFont>(DefaultFont.FontFilePath);
            DefaultFont.Font.Texture.Name = DefaultFont.FontFilePath;

            FontsSortedByCodename = new Dictionary<string, List<FontDescriptor>>();
            foreach (FontDescriptor f in ThemeFontDescriptors)
            {
                f.Font = Content.Load<SpriteFont>(f.FontFilePath);
                f.Font.Texture.Name = f.FontFilePath;
                if (!FontsSortedByCodename.ContainsKey(f.FontCodeName))
                {
                    FontsSortedByCodename.Add(f.FontCodeName, new List<FontDescriptor>());
                }
                FontsSortedByCodename[f.FontCodeName].Add(f);
            }

            foreach (List<FontDescriptor> fontsByCodename in FontsSortedByCodename.Values)
            {
                fontsByCodename.Sort();
            }
        }

        public SpriteFont GetClosestFont(string fontCodename, int desiredSize, FontRoundMethod roundMethod)
        {
            List<FontDescriptor> matchedCodename = FontsSortedByCodename.ContainsKey(fontCodename) ? FontsSortedByCodename[fontCodename] : null;
            if (matchedCodename != null)
            {
                FontDescriptor low = null;
                FontDescriptor high = null;
                FontDescriptor match = null;
                foreach (FontDescriptor f in matchedCodename)
                {
                    if (f.FontSize == desiredSize)
                    {
                        match = f;
                    }
                    else if (f.FontSize < desiredSize)
                    {
                        low = f;
                    }
                    else if (f.FontSize > desiredSize && high == null)
                    {
                        high = f;
                    }
                }
                if (match != null)
                {
                    return match.Font;
                }
                switch (roundMethod)
                {
                    case FontRoundMethod.NoMatchDefault:
                        return DefaultFont.Font;
                    case FontRoundMethod.RoundDown:
                        if (low == null)
                        {
                            return high.Font;
                        }
                        else
                        {
                            return low.Font;
                        }
                    case FontRoundMethod.RoundUp:
                        if (high == null)
                        {
                            return low.Font;
                        }
                        else
                        {
                            return high.Font;
                        }
                    case FontRoundMethod.Closest:
                        if (high == null)
                        {
                            return low.Font;
                        }
                        else if (low == null)
                        {
                            return high.Font;
                        }
                        else
                        {
                            int lowVar = desiredSize - low.FontSize;
                            int highVar = high.FontSize - desiredSize;
                            if (lowVar < highVar)
                            {
                                return low.Font;
                            }
                            else if (highVar < lowVar)
                            {
                                return high.Font;
                            }
                            else
                            {
                                return low.Font;
                            }
                        }
                }
            }

            return DefaultFont.Font;
        }

        public enum FontClampSide { None, Height, Width }

        public SpriteFont GetLargestFont(string fontCodename, Vector2 Dimensions, string text, FontClampSide clamp)
        {
            List<FontDescriptor> matchedCodename = FontsSortedByCodename.ContainsKey(fontCodename) ? FontsSortedByCodename[fontCodename] : null;
            if (matchedCodename != null && matchedCodename.Count > 0)
            {
                //Descending sort
                //TODO THIS ISN"T ACTUALLY DESCENDING I DONT KNOW WHY IT WORKS
                matchedCodename.Sort((x, y) => x.FontSize.CompareTo(y.FontSize));
                int currentSize = 0;
                FontDescriptor curF = null;
                foreach (FontDescriptor f in matchedCodename)
                {
                    if (f.FontSize > currentSize)
                    {
                        Vector2 measured = f.Font.MeasureString(text);
                        if((clamp == FontClampSide.None && measured.X < Dimensions.X && measured.Y < Dimensions.Y)
                            || (clamp == FontClampSide.Height && measured.Y < Dimensions.Y)
                            || (clamp == FontClampSide.Width && measured.X < Dimensions.X))
                        {
                            curF = f;
                            currentSize = f.FontSize;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                if (curF != null)
                {
                    return curF.Font;
                }
                else
                {
                    //If nothing fit, return the smallest
                    FontDescriptor f = matchedCodename[0];
                    return f.Font;
                }
            }

            return DefaultFont.Font;
        }

        public List<FontDescriptor> GetFontsOfDescendingSize(string fontCodename)
        {
            List<FontDescriptor> matchedCodename = FontsSortedByCodename.ContainsKey(fontCodename) ? 
                FontsSortedByCodename[fontCodename] : null;
            if (matchedCodename != null && matchedCodename.Count > 0)
            {
                //Descending sort
                matchedCodename.Sort((x, y) => -1 * x.FontSize.CompareTo(y.FontSize));
                return matchedCodename;
            }

            List<FontDescriptor> retVal = new List<FontDescriptor>();
            retVal.Add(DefaultFont);
            return retVal;
        }
    }
}
