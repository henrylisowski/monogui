﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Skinning.Fonts
{
    public class FontDescriptor : IComparable<FontDescriptor>
    {
        public string FontCodeName { get; set; }
        public string FontFilePath { get; set; }
        public int FontSize { get; set; }

        public SpriteFont Font { get; set; }

        public FontDescriptor()
        {

        }

        public FontDescriptor(string FontCodeName, string FontFilePath, int FontSize)
        {
            this.FontCodeName = FontCodeName;
            this.FontFilePath = FontFilePath;
            this.FontSize = FontSize;
        }

        public int CompareTo(FontDescriptor other)
        {
            return FontSize.CompareTo(other.FontSize);
        }
    }
}
