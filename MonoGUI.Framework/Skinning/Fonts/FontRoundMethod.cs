﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Skinning.Fonts
{
    public enum FontRoundMethod
    {
        RoundDown, RoundUp, Closest, NoMatchDefault
    }
}
