﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using MonoGUI.ScreenItems;
using MonoGUI.Skinning.Fonts;
using MonoGUI.Skinning.SkinElements;
using MonoGUI.Skinning.WidgetSkins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Skinning
{
    public class MonoGUISkin
    {
        public List<ISkinElement> GlobalSkinElements { get; set; }
        public List<GenericWidgetSkin> SkinsForAllWidgets { get; set; }
        public FontManager FontManager { get; set; }
        public MonoGUISkin()
        {
            FontManager = new Fonts.FontManager();
            SkinsForAllWidgets = new List<GenericWidgetSkin>();
            GlobalSkinElements = new List<ISkinElement>();
        }

        public MonoGUISkin(List<GenericWidgetSkin> SkinsForUIElements) : this()
        {
            this.SkinsForAllWidgets = SkinsForUIElements;
        }

        public MonoGUISkin(params GenericWidgetSkin[] uiElementContainers) : this()
        {
            SkinsForAllWidgets = new List<GenericWidgetSkin>();
            foreach (GenericWidgetSkin c in uiElementContainers)
            {
                SkinsForAllWidgets.Add(c);
            }
        }

        public void Initialize()
        {
            foreach (ISkinElement globalElement in GlobalSkinElements)
            {
                globalElement.Initialize();
            }
            foreach (GenericWidgetSkin cont in SkinsForAllWidgets)
            {
                cont.Initialize();
            }
        }

        public void LoadContent(ContentManager Content)
        {
            foreach (ISkinElement globalElement in GlobalSkinElements)
            {
                globalElement.LoadContent(Content);
            }
            foreach (GenericWidgetSkin cont in SkinsForAllWidgets)
            {
                cont.LoadContent(Content);
            }
            if (FontManager != null)
            {
                FontManager.LoadContent(Content);
            }
        }

        public void ResolveGlobalReference()
        {
            foreach (GenericWidgetSkin cont in SkinsForAllWidgets)
            {
                cont.ResolveGlobalReferences(this);
            }
        }

        public ISkinElement FindGlobalElement(string ElementName)
        {
            foreach (ISkinElement element in GlobalSkinElements)
            {
                if (element.ElementName.Equals(ElementName))
                {
                    return element;
                }
            }
            return new EmptyElement();
        }

        public void ApplySkin(ScreenItem screenItem)
        {
            foreach (GenericWidgetSkin c in SkinsForAllWidgets)
            {
                if (!String.IsNullOrEmpty(screenItem.SkinTargetWidget) && !String.IsNullOrEmpty(screenItem.SkinVariant))
                {
                    if (screenItem.SkinTargetWidget.Equals(c.TargetWidget) && screenItem.SkinVariant.Equals(c.SkinVariant))
                    {
                        c.ApplySkin(screenItem);
                    }
                }
            }
        }

        public SpriteFont GetClosestFont(string fontCodename, int desiredSize, FontRoundMethod roundMethod)
        {
            return FontManager.GetClosestFont(fontCodename, desiredSize, roundMethod);
        }

        public void Scale(float ScaleAmount)
        {
            foreach (ISkinElement e in GlobalSkinElements)
            {
                e.Scale(ScaleAmount);
            }

            foreach (GenericWidgetSkin s in SkinsForAllWidgets)
            {
                s.Scale(ScaleAmount);
            }
        }
    }
}
