﻿using Microsoft.Xna.Framework.Content;
using MonoGUI.ScreenItems;
using MonoGUI.Skinning.SkinElements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Skinning
{
    public class SkinnedPropertyStateValue<T>
    {
        public Dictionary<int, T> SkinElementsByState { get; set; }

        public SkinnedPropertyStateValue()
        {
            this.SkinElementsByState = new Dictionary<int, T>();
        }

        public SkinnedPropertyStateValue(Dictionary<int, T> SkinElementsByState)
        {
            this.SkinElementsByState = SkinElementsByState;
        }

        public SkinnedPropertyStateValue(int[] elementStates, T[] elementSkins)
        {
            if (elementStates.Length != elementSkins.Length)
            {
                throw new Exception("Unmatched number of states and elements");
            }
            SkinElementsByState = new Dictionary<int, T>();
            for (int i = 0; i < elementStates.Length; i++)
            {
                SkinElementsByState.Add(elementStates[i], elementSkins[i]);
            }
        }

        public SkinnedPropertyStateValue(T defaultValue)
        {
            SkinElementsByState = new Dictionary<int, T>();
            SkinElementsByState.Add((int)ScreenItemVisualStates.Default, defaultValue);
        }

        public void Initialize()
        {
            foreach (T s in SkinElementsByState.Values)
            {
                if (s.GetType().IsSubclassOf(typeof(ISkinElement)))
                {
                    ((ISkinElement)(object)s).Initialize();
                }
                
            }
        }

        public void LoadContent(ContentManager Content)
        {
            foreach (T s in SkinElementsByState.Values)
            {
                if (s.GetType().IsSubclassOf(typeof(ISkinElement)))
                {
                    ((ISkinElement)(object)s).LoadContent(Content);
                }
            }
        }

        public void ResolveGlobalReferences(MonoGUISkin RootSkin)
        {
            foreach (T s in SkinElementsByState.Values)
            {
                if (s.GetType().IsSubclassOf(typeof(ISkinElement)))
                {
                    ((ISkinElement)(object)s).ResolveGlobalReferences(RootSkin);
                }
            }
        }

        public T GetSkinElementByState(int state)
        {
            if (SkinElementsByState.ContainsKey(state))
            {
                return SkinElementsByState[state];
            }
            if (SkinElementsByState.ContainsKey(0))
            {
                return SkinElementsByState[0];
            }
            return default(T);
        }

        public T GetSkinElementByState(ScreenItemVisualStates state)
        {
            return GetSkinElementByState((int)state);
        }
    }
}
