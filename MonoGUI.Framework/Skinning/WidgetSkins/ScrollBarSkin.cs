﻿using MonoGUI.Skinning.SkinElements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Skinning.WidgetSkins
{
    public class ScrollBarSkin : GenericContainerSkin
    {
        [SkinnedProperty]
        public string DownButtonSkinVariant { get; set; }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> DownButtonArea { get; set; }
        [SkinnedProperty]
        public string UpButtonSkinVariant { get; set; }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> UpButtonArea { get; set; }
        [SkinnedProperty]
        public string LeftButtonSkinVariant { get; set; }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> LeftButtonArea { get; set; }
        [SkinnedProperty]
        public string RightButtonSkinVariant { get; set; }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> RightButtonArea { get; set; }
        [SkinnedProperty]
        public string HorizontalScrollBackgroundVariant { get; set; }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> HorizontalScrollBackgroundArea { get; set; }
        [SkinnedProperty]
        public string HorizontalScrollKnobVariant { get; set; }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> HorizontalScrollKnobArea { get; set; }
        [SkinnedProperty]
        public string VerticalScrollBackgroundVariant { get; set; }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> VerticalScrollBackgroundArea { get; set; }
        [SkinnedProperty]
        public string VerticalScrollKnobVariant { get; set; }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> VerticalScrollKnobArea { get; set; }
        
        public ScrollBarSkin(string TargetWidget, string SkinVariant)
            : base(TargetWidget, SkinVariant)
        {

        }
    }
}
