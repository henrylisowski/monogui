﻿using MonoGUI.Skinning.SkinElements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Skinning.WidgetSkins
{
    public class IconSkin : GenericWidgetSkin
    {
        [SkinnedProperty]
        public SkinnedPropertyStateValue<ISkinElement> IconImage { get; set; }

        public IconSkin()
        {

        }

        public IconSkin(string TargetWidget, string SkinVariant)
            : base(TargetWidget, SkinVariant)
        {

        }
    }
}
