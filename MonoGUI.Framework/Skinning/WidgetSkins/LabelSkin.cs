﻿using Microsoft.Xna.Framework;
using MonoGUI.ScreenItems;
using MonoGUI.Skinning.Fonts;
using MonoGUI.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Skinning.WidgetSkins
{
    public class LabelSkin : GenericWidgetSkin
    {
        [SkinnedProperty]
        public string FontName { get; set; }
        [SkinnedProperty]
        public int FontSize { get; set; }
        [SkinnedProperty]
        public FontRoundMethod FontRoundingMethod { get; set; }
        [SkinnedProperty]
        public Color TextColor { get; set; }
        [SkinnedProperty]
        public FontFillMethod FontMethod { get; set; }
        [SkinnedProperty]
        public AlignH TextHorizontalJustification { get; set; }
        [SkinnedProperty]
        public AlignV TextVerticalJustification { get; set; }

        public LabelSkin()
        {

        }

        public LabelSkin(string TargetWidget, string SkinVariant)
            : base(TargetWidget, SkinVariant)
        {
            TextColor = Color.Black;
        }
    }
}
