﻿using MonoGUI.Skinning.Fonts;
using MonoGUI.Skinning.SkinElements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Skinning.WidgetSkins
{
    public class ToggleButtonSkin : GenericContainerSkin
    {
        [SkinnedProperty]
        public SkinnedPropertyStateValue<ISkinElement> ButtonForegroundClicked { get; set; }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<ISkinElement> ButtonForegroundNotClicked { get; set; }
        [SkinnedProperty]
        public string ButtonToggledOnIconVariant { get; set; }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> ButtonToggledOnIconArea { get; set; }
        [SkinnedProperty]
        public string LabelSkinVariant { get; set; }

        public ToggleButtonSkin()
        {

        }

        public ToggleButtonSkin(string TargetWidget, string SkinVariant)
            : base(TargetWidget, SkinVariant)
        {

        }
    }
}
