﻿using MonoGUI.Animations;
using MonoGUI.Skinning.SkinElements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Skinning.WidgetSkins
{
    public class SliderSkin : ProgressBarSkin
    {
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> HorizontalSliderIconArea { get; set; }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<ISkinElement> HorizontalSliderIconSkin { get; set; }
        [SkinnedProperty]
        public AreaDefinition HorizontalSliderIconMinimumDimensions { get; set; }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<List<IModifier>> HorizontalSliderIconStateTransitionAnimations { get; set; }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> VerticalSliderIconArea { get; set; }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<ISkinElement> VerticalSliderIconSkin { get; set; }
        [SkinnedProperty]
        public AreaDefinition VerticalSliderIconMinimumDimensions { get; set; }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<List<IModifier>> VerticalSliderIconStateTransitionAnimations { get; set; }
        public SliderSkin(string TargetWidget, string SkinVariant)
            : base(TargetWidget, SkinVariant)
        {
            
        }
    }
}
