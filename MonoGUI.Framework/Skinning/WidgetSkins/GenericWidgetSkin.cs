﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using MonoGUI.Animations;
using MonoGUI.ScreenItems;
using MonoGUI.Skinning.SkinElements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace MonoGUI.Skinning.WidgetSkins
{
    public class GenericWidgetSkin
    {
        public string TargetWidget { get; set; }
        public string SkinVariant { get; set; }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<ISkinElement> BackgroundSkin { get; set; }
        [SkinnedProperty]
        public AreaDefinition MinimumDimensions { get; set; }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<List<IModifier>> StateTransitionAnimations { get; set; }
        [SkinnedProperty]
        public List<Animation> EventFireAnimations { get; set; }
        [SkinnedProperty]
        public List<IModifier> TransitionOnAnimations { get; set; }
        [SkinnedProperty]
        public List<IModifier> TransitionOffAnimations { get; set; }
        [SkinnedProperty]
        public Color DrawColor { get; set; }


        public GenericWidgetSkin() { }

        public GenericWidgetSkin(string TargetWidget, string SkinVariant)
        {
            this.TargetWidget = TargetWidget;
            this.SkinVariant = SkinVariant;
            this.DrawColor = Color.White;
        }

        public void Initialize()
        {
            IEnumerable<PropertyInfo> properties = this.GetType().GetProperties();
            foreach (PropertyInfo prop in properties)
            {
                object[] customAttributes = prop.GetCustomAttributes(typeof(SkinnedProperty), false);
                if (customAttributes.Length > 0)
                {
                    object value = prop.GetValue(this, null);
                    if (value != null)
                    {
                        MethodInfo inf = value.GetType().GetMethod("Initialize");
                        if (inf != null)
                        {
                            inf.Invoke(value, null);
                        }
                    }
                }
            }
        }

        public void LoadContent(ContentManager Content)
        {
            IEnumerable<PropertyInfo> properties = this.GetType().GetProperties();
            foreach (PropertyInfo prop in properties)
            {
                object[] customAttributes = prop.GetCustomAttributes(typeof(SkinnedProperty), false);
                if (customAttributes.Length > 0)
                {
                    object value = prop.GetValue(this, null);
                    if (value != null)
                    {
                        MethodInfo inf = value.GetType().GetMethod("LoadContent", new Type[] { typeof(ContentManager) });
                        if (inf != null)
                        {
                            inf.Invoke(value, new object[] { Content });
                        }
                    }
                }
            }
            //IEnumerable<PropertyInfo> properties = this.GetType().GetProperties().Where(
            //    prop => prop.GetValue(.PropertyType.Equals(typeof(SkinnedPropertyStateValue<ISkinElement>)));
            //foreach (PropertyInfo prop in properties)
            //{
            //    SkinnedPropertyStateValue<ISkinElement> skinnedProperty = (SkinnedPropertyStateValue<ISkinElement>)prop.GetValue(this, null);
            //    if (skinnedProperty != null)
            //    {
            //        skinnedProperty.LoadContent(Content);
            //    }
            //}
        }

        public void ResolveGlobalReferences(MonoGUISkin RootSkin)
        {
            IEnumerable<PropertyInfo> properties = this.GetType().GetProperties().Where(
                prop => prop.PropertyType.Equals(typeof(SkinnedPropertyStateValue<ISkinElement>)));
            foreach (PropertyInfo prop in properties)
            {
                SkinnedPropertyStateValue<ISkinElement> skinnedProperty = (SkinnedPropertyStateValue<ISkinElement>)prop.GetValue(this, null);
                if (skinnedProperty != null)
                {
                    skinnedProperty.ResolveGlobalReferences(RootSkin);
                }
            }
        }

        public void ApplySkin(ScreenItem item)
        {
            List<PropertyInfo> skinProperties = this.GetType().GetProperties().Where(prop => Attribute.IsDefined(prop, typeof(SkinnedProperty))).ToList();
            List<PropertyInfo> widgetProperties = item.GetType().GetProperties().Where(prop => Attribute.IsDefined(prop, typeof(SkinnedProperty))).ToList();

            for (int i = 0; i < skinProperties.Count; i++)
            {
                PropertyInfo skinProp = skinProperties[i];
                for (int j = 0; j < widgetProperties.Count; j++ )
                {
                    PropertyInfo widgetProp = widgetProperties[j];
                    if (skinProp.Name.Equals(widgetProp.Name) && skinProp.GetType().Equals(widgetProp.GetType()) &&
                        !item.SkinnedPropertyOverrides.Contains("set_" + skinProp.Name))
                    {
                        widgetProp.SetValue(item, skinProp.GetValue(this, null), null);
                        item.SkinnedPropertyOverrides.Remove("set_" + skinProp.Name);
                    }
                }
            }
        }

        public void Scale(float scaleAmount)
        {
            PropertyInfo[] props = GetType().GetProperties();
            foreach (PropertyInfo p in props)
            {
                if (p.PropertyType.Equals(typeof(AreaDefinition)))
                {
                    AreaDefinition a = (AreaDefinition)p.GetValue(this, null);
                    if (a != null)
                    {
                        a.Scale(scaleAmount);
                    }
                }
                if (p.PropertyType.Equals(typeof(SkinnedPropertyStateValue<AreaDefinition>)))
                {
                    SkinnedPropertyStateValue<AreaDefinition> a = (SkinnedPropertyStateValue<AreaDefinition>)p.GetValue(this, null);
                    if (a != null)
                    {
                        foreach (AreaDefinition def in a.SkinElementsByState.Values)
                        {
                            if (def != null)
                            {
                                def.Scale(scaleAmount);
                            }
                        }
                    }
                }
            }
        }
    }
}
