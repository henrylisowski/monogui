﻿using MonoGUI.Animations;
using MonoGUI.Skinning.SkinElements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Skinning.WidgetSkins
{
    public class GenericContainerSkin : GenericWidgetSkin
    {
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> SkinnedChildArea { get; set; }
        [SkinnedProperty]
        public List<IModifier> ChildTransitionOnAnimations { get; set; }
        [SkinnedProperty]
        public List<IModifier> ChildTransitionOffAnimations { get; set; }
        [SkinnedProperty]
        public float ChildTransitionOnInitialDelay { get; set; }
        [SkinnedProperty]
        public float ChildTransitionOnStepDelay { get; set; }
        [SkinnedProperty]
        public float ChildTransitionOffInitialDelay { get; set; }
        [SkinnedProperty]
        public float ChildTransitionOffStepDelay { get; set; }

        public GenericContainerSkin()
        {

        }

        public GenericContainerSkin(string TargetWidget, string SkinVariant)
            : base(TargetWidget, SkinVariant)
        {

        }
    }
}
