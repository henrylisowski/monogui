﻿using MonoGUI.Animations;
using MonoGUI.Skinning.Fonts;
using MonoGUI.Skinning.SkinElements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Skinning.WidgetSkins
{
    public class CheckboxSkin : GenericContainerSkin
    {
        [SkinnedProperty]
        public string ToggleButtonSkinVariant { get; set; }
        [SkinnedProperty]
        public string LabelSkinVariant { get; set; }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> ToggleButtonArea { get; set; }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> TextArea { get; set; }
        [SkinnedProperty]
        public List<Animation> ButtonEventFireAnimations { get; set; }
        [SkinnedProperty]
        public List<Animation> TextEventFireAnimations { get; set; }

        public CheckboxSkin()
        {

        }

        public CheckboxSkin(string TargetWidget, string SkinVariant)
            : base(TargetWidget, SkinVariant)
        {

        }
    }
}
