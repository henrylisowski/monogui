﻿using Microsoft.Xna.Framework;
using MonoGUI.Animations;
using MonoGUI.Skinning.Fonts;
using MonoGUI.Skinning.SkinElements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Skinning.WidgetSkins
{
    public class GUIWindowSkin : GenericContainerSkin
    {
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> TitleBarCanvasArea { get; set; }
        [SkinnedProperty]
        public string TitleBarCanvasSkinVariant { get; set; }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> TitleLabelArea { get; set; }
        [SkinnedProperty]
        public string TitleLabelSkinVariant { get; set; }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> CloseButtonArea { get; set; }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> ChildrenArea { get; set; }
        [SkinnedProperty]
        public string CloseButtonSkinVariant { get; set; }
        [SkinnedProperty]
        public List<Animation> CloseButtonEventFireAnimations { get; set; }
        [SkinnedProperty]
        public List<Animation> TitleTextEventFireAnimations { get; set; }
        [SkinnedProperty]
        public List<Animation> TitleBarEventFireAnimations { get; set; }
        
        public GUIWindowSkin(string TargetWidget, string SkinVariant)
            : base(TargetWidget, SkinVariant)
        {
            
        }
    }
}
