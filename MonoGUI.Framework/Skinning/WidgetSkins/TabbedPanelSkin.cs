﻿using MonoGUI.Skinning.SkinElements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Skinning.WidgetSkins
{
    public class TabbedPanelSkin : GenericContainerSkin
    {
        [SkinnedProperty]
        public string TabUnselectedButtonVariant { get; set; }
        [SkinnedProperty]
        public string TabSelectedButtonVariant { get; set; }
        [SkinnedProperty]
        public string ChildCanvasVariant { get; set; }
        [SkinnedProperty]
        public int TabSpacingAmount { get; set; }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> TabFrameArea { get; set; }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> ChildPanelFrameArea { get; set; }

        public TabbedPanelSkin()
        {

        }

        public TabbedPanelSkin(string TargetWidget, string SkinVariant)
            : base(TargetWidget, SkinVariant)
        {

        }
    }
}
