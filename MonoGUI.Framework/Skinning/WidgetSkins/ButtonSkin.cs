﻿using MonoGUI.Skinning.Fonts;
using MonoGUI.Skinning.SkinElements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Skinning.WidgetSkins
{
    public class ButtonSkin : GenericContainerSkin
    {
        [SkinnedProperty]
        public SkinnedPropertyStateValue<ISkinElement> ButtonForeground { get; set; }
        [SkinnedProperty]
        public string LabelSkinVariant { get; set; }

        public ButtonSkin()
        {

        }

        public ButtonSkin(string TargetWidget, string SkinVariant)
            : base(TargetWidget, SkinVariant)
        {

        }
    }
}
