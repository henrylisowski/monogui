﻿using MonoGUI.Skinning.SkinElements;
using MonoGUI.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Skinning.WidgetSkins
{
    public class ProgressBarSkin : GenericWidgetSkin
    {
        [SkinnedProperty]
        public MonoGUI.Widgets.ProgressBar.FillType ProgressFillType { get; set; }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<ISkinElement> GradientFillSourceTexture { get; set; }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<TextureElement> GradientFillGradientTexture { get; set; }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<TextureElement> GradientFillMaskTexture { get; set; }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> GradientFillArea { get; set; }
        [SkinnedProperty]
        public string HorizontalFillVariant { get; set; }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> HorizontalFillArea { get; set; }
        [SkinnedProperty]
        public string HorizontalOutlineVariant { get; set; }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> HorizontalOutlineArea { get; set; }
        [SkinnedProperty]
        public string VerticalFillVariant { get; set; }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> VerticalFillArea { get; set; }
        [SkinnedProperty]
        public string VerticalOutlineVariant { get; set; }
        [SkinnedProperty]
        public SkinnedPropertyStateValue<AreaDefinition> VerticalOutlineArea { get; set; }

        public ProgressBarSkin(string TargetWidget, string SkinVariant)
            : base(TargetWidget, SkinVariant)
        {
            
        }
    }
}
