﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Utilities
{
    public static class ColorUtils
    {
        public static void Multiply(Color a, Color b, ref Color outColor)
        {
            float rC = (a.R / 255f) * (b.R / 255f);
            float gC = (a.G / 255f) * (b.G / 255f);
            float bC = (a.B / 255f) * (b.B / 255f);
            float aC = (a.A / 255f) * (b.A / 255f);

            outColor.R = (byte)(rC * 255);
            outColor.G = (byte)(gC * 255);
            outColor.B = (byte)(bC * 255);
            outColor.A = (byte)(aC * 255);
        }
    }
    
}
