﻿using Microsoft.Xna.Framework.Input;
using MonoGUI.Manager.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Utilities
{
    public static class EnumUtils
    {
        public static MouseButton[] MouseButton = (MouseButton[])Enum.GetValues(typeof(MouseButton));
        public static Keys[] Keys = (Keys[])Enum.GetValues(typeof(Keys));
    }
}
