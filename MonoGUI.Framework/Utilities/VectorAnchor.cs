﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Utilities
{
    public static class Extensions
    {
        public static Vector2 DirectionModifier(this VectorAnchor Anchor)
        {
            float xMod = 0f;
            float yMod = 0f;
            if (Anchor == VectorAnchor.TopLeft || Anchor == VectorAnchor.MiddleLeft || Anchor == VectorAnchor.BottomLeft)
            {
                xMod = 0f;
            }
            else if (Anchor == VectorAnchor.TopMiddle || Anchor == VectorAnchor.Center || Anchor == VectorAnchor.BottomMiddle)
            {
                xMod = -0.5f;
            }
            else
            {
                xMod = -1f;
            }

            if (Anchor == VectorAnchor.TopLeft || Anchor == VectorAnchor.TopMiddle || Anchor == VectorAnchor.TopRight)
            {
                yMod = 0f;
            }
            else if (Anchor == VectorAnchor.MiddleLeft || Anchor == VectorAnchor.Center || Anchor == VectorAnchor.MiddleRight)
            {
                yMod = -0.5f;
            }
            else
            {
                yMod = -1f;
            }
            return new Vector2(xMod, yMod);
        }
    }
    public enum VectorAnchor
    {
        TopLeft, TopMiddle, TopRight, MiddleLeft, Center, MiddleRight, BottomLeft, BottomMiddle, BottomRight
    }
}
