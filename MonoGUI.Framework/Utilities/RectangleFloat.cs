﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGUI.Utilities
{
    public class RectangleFloat
    {
        private float _x;
        public float X { get { return _x; } set { _x = value; assignPosition(); } }
        private float _y;
        public float Y { get { return _y; } set { _y = value; assignPosition(); } }
        private float _width;
        public float Width { get { return _width; } set { _width = value; assignDimensions(); } }
        private float _height;
        public float Height { get { return _height; } set { _height = value; assignDimensions(); } }

        public float Left { get { return X; } }
        public float Right { get { return X + Width; } }
        public float Top { get { return Y; } }
        public float Bottom { get { return Y + Height; } }

        public Rectangle ToRectangle
        {
            get { return new Rectangle((int)X, (int)Y, (int)Width, (int)Height); }
        }

        private Vector2 _position;
        [ContentSerializerIgnore]
        public Vector2 Position { get { return _position; } set { _x = value.X; _y = value.Y; assignPosition(); } }

        private Vector2 _dimensions;
        [ContentSerializerIgnore]
        public Vector2 Dimensions { get { return _dimensions; } set { _width = value.X; _height = value.Y; assignDimensions(); } }
        public Vector2 Center { get { return new Vector2(_x + _width / 2f, _y + _width / 2f); } }

        public RectangleFloat()
        {
            X = 0;
            Y = 0;
            Width = 0;
            Height = 0;
        }

        public RectangleFloat(float X, float Y, float Width, float Height)
        {
            this._x = X;
            this._y = Y;
            this._width = Width;
            this._height = Height;

            assignPosition();
            assignDimensions();
        }

        public void AssignNew(float x, float y, float width, float height)
        {
            this._x = x;
            this._y = y;
            this._width = width;
            this._height = height;

            assignPosition();
            assignDimensions();
        }

        public bool Intersects(RectangleFloat other, out Vector2 delta)
        {
            float cw = (this.Width + other.Width) / 2f;
            float ch = (this.Height + other.Height) / 2f;

            Vector2 distance = new Vector2(Math.Abs(Center.X - other.Center.X), Math.Abs(Center.Y - other.Center.Y));

            bool intersects = false;
            delta = Vector2.Zero;

            float xAxis = Math.Abs(distance.X);
            float yAxis = Math.Abs(distance.Y);

            float ox = Math.Abs(xAxis - cw); //overlap on x
            float oy = Math.Abs(yAxis - ch); //overlap on y

            //direction
            Vector2 dir = (Center - other.Center);
            dir.Normalize();

            if (xAxis > cw) return intersects;
            if (yAxis > ch) return intersects;

            if (ox == 0 && oy == 0)
            {

            }
            else if (ox > oy)
            {
                intersects = true;
                delta = new Vector2(0, dir.Y * oy);//top-bottom collision
            }
            else if (ox < oy)
            {
                intersects = true;
                delta = new Vector2(dir.X * ox, 0);//left-right collision
            }
            else //if(ox == oy)
            {
                intersects = true;
                delta = new Vector2(dir.X * ox, dir.Y * oy); //corner-corner collision, 
                //unlikely with float's.
            }

            return intersects;
        }

        private void assignPosition()
        {
            _position = new Vector2(X, Y);
        }

        private void assignDimensions()
        {
            _dimensions = new Vector2(Width, Height);
        }
    }
}
